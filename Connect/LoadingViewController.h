//
//  LoadingViewController.h
//  EZToken
//
//  Created by Harsha on 9/30/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharedData.h"

@interface LoadingViewController : UIViewController
  @property (retain, nonatomic) IBOutlet UIImageView *loading;
   @property (nonatomic, strong) NSMutableArray *groupNames;

@end
