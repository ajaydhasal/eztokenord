//
//  VVDemoEnrollerController.h
//  IOSVVDemo
//
//  Created by Karl Ridgeway on 8/28/14.
//  Copyright (c) 2014 Sensory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <smma/smma/smma.h>
#import "SharedData.h"

@interface VVDemoRecognizerController : UIViewController<
    AVCaptureVideoDataOutputSampleBufferDelegate,
    AVCaptureAudioDataOutputSampleBufferDelegate > {
    AVCaptureConnection* _audioConnection;
    AVCaptureConnection* _videoConnection;
    AVAudioSession* _audioSession;
    MultiEnroller* _enroller;
    MultiAuthenticator* _authenticator;
    BOOL _enrolling;
    BOOL _authenticating;
    uint32_t _running;
    long _currTimeMS;
    BOOL _finishedInitializing;
    int _authTimeout;
        
    UIImage* _iconNoiseOK;
    UIImage* _iconNoiseMED;
    UIImage* _iconNoiseHIGH;
        
    UIImage* _iconLightOK;
    UIImage* _iconLightLOW;
    UIImage* _iconLightNONE;
    
    UIImage* _iconFaceOK;
    UIImage* _iconFacePOOR;
    UIImage* _iconFaceNONE;
        
    UIAlertView *alertEnrollment,*alertAuthenticate,*alertTimeOut,*cameraAlert;
    SharedData *shareData;
}

@property (weak, nonatomic) IBOutlet UIView *cameraPreviewView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *startButton;

@property (weak, nonatomic) IBOutlet UIProgressView *vuMeter;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *gradientOverlayView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *authenticateButton;
@property (weak, nonatomic) IBOutlet UIImageView *noiseIndicatorIcon;
@property (weak, nonatomic) IBOutlet UIImageView *lightingIndicatorIcon;
@property (weak, nonatomic) IBOutlet UIImageView *faceStatusIndicatorIcon;

@property (weak, nonatomic) IBOutlet UIToolbar *enrollAuthToolbar;

@property (assign, atomic) id callbackMe;
- (IBAction)enroll:(id)sender;
- (IBAction)authenticate:(id)sender;

@end
