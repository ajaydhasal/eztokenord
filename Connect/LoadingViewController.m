//
//  LoadingViewController.m
//  EZToken
//
//  Created by Harsha on 9/30/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import "LoadingViewController.h"
#import "Constant.h"
#import "SharedData.h"
#import "ResultViewController.h"

@interface LoadingViewController ()
{
    BOOL isPendingProcessResult;
    BOOL isWaitingDelayTimeOver;
    
    NSTimer *loadingDelayTimer;
    BOOL isAllLoadingProcessDone;
    float fLoadingDelayTime;
    
    UIAlertView *activateAlertView;
    UIAlertView *errorAlertView;
    UIAlertView *errordeleteAlertView;
    UIAlertView *transalertView;
}
@end

@implementation LoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //NSLog(@"LOADINGVC ****ViewDidLoad");
    // Do any additional setup after loading the view.
    UIColor * color = [UIColor colorWithRed:92.0 /255.0f green:171.0/255.0f blue:222.00/255.0f alpha:1.0f];
    self.navigationController.navigationBar.barTintColor = color;

    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(doneLoadingProcess:) name:NOTIFICATION_NAME_ACTIVATE_TOKEN_DONE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(doneLoadingProcess:) name:NOTIFICATION_NAME_TRANSACTION_SEND_DONE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(doneLoadingProcess:) name:NOTIFICATION_NAME_ONLINE_LOGIN_DONE object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_NAME_TRANSACTION_SEND_DONE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_NAME_ACTIVATE_TOKEN_DONE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_NAME_ONLINE_LOGIN_DONE object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{

    [self.navigationController.navigationBar setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor],UITextAttributeFont: [UIFont systemFontOfSize:17.0f]}];
    self.navigationItem.hidesBackButton = YES;
    //loading image
    NSArray * imageArray  = [[NSArray alloc] initWithObjects:
                             [UIImage imageNamed:@"image001.png"],
                             [UIImage imageNamed:@"image002.png"],
                             [UIImage imageNamed:@"image003.png"],
                             [UIImage imageNamed:@"image004.png"],
                             [UIImage imageNamed:@"image005.png"],
                             [UIImage imageNamed:@"image006.png"],
                             nil];
    self.loading = [[UIImageView alloc] initWithFrame:
                    CGRectMake((([[UIScreen mainScreen] bounds].size.width)-200)/2, (([[UIScreen mainScreen] bounds].size.height)-200)/2, 150, 130)];
    self.loading.animationImages = imageArray;
    self.loading.animationDuration = 1.1;
    self.loading.contentMode = UIViewContentModeBottomLeft;
    [self.view addSubview:self.loading];
    [self.loading startAnimating];
    [self startLoadingDelayTimer];
}


//MODUFY BY HARSHA on 27th March 2014////////////////

- (void) startLoadingDelayTimer {
    isWaitingDelayTimeOver = YES;
    loadingDelayTimer = [NSTimer scheduledTimerWithTimeInterval:fLoadingDelayTime
                                                         target:self
                                                       selector:@selector(overLoadingDelayTime)
                                                       userInfo:nil
                                                        repeats:NO];
}

- (void) cancelLoadingDelayTimer {
    isWaitingDelayTimeOver = NO;
    //NSLog(@"cancelLoadingDelayTimer");
    
    if (loadingDelayTimer != nil)
    {
        [loadingDelayTimer invalidate];
        loadingDelayTimer = nil;
    }
}

- (void) overLoadingDelayTime {
    isWaitingDelayTimeOver = NO;
    //NSLog(@"overLoadingDelayTime");
    if (!isPendingProcessResult) {
        if (isAllLoadingProcessDone) {
            //NSLog(@"overLoadingDelayTime");
            [self goNextViewController];
            return;
        } else {
            //[self startLoadingProcess];
        }
    }
}


- (void) doneLoadingProcess:(NSNotification *) notification
{
    SharedData *sharedData = [SharedData sharedData];
    //NSLog(@"LVC doneLoadingProcess processTarget %d",sharedData.processTarget);
    
    if (sharedData.processTarget == PROCESS_TARGET_ACTIVATE_TOKEN)
    {
        //NSLog(@"LVC doneLoadingProcess processResult %d",sharedData.processResult);
        if (![[notification name] isEqualToString:NOTIFICATION_NAME_ACTIVATE_TOKEN_DONE])
            return;
        
    }
    else if (sharedData.processTarget == PROCESS_TARGET_TRANSACTION_SEND)
    {
        //NSLog(@"LVC doneLoadingProcess processResult %d",sharedData.processResult);
        if (![[notification name] isEqualToString:NOTIFICATION_NAME_TRANSACTION_SEND_DONE])
        {
            //remove the observer
            //[[NSNotificationCenter defaultCenter]removeObserver:self name:NOTIFICATION_NAME_TRANSACTION_SEND_DONE object:nil];
            return;
        }
    }else if(sharedData.processTarget == PROCESS_TARGET_ONLINE_LOGIN){
        if (![[notification name] isEqualToString:NOTIFICATION_NAME_ONLINE_LOGIN_DONE])
        {
            return;
        }
    }
    [self allLoadingProcessesDone];
}

//- (void) loadingProcessDone {
//    //NSLog(@"loadingProcessDone");
//    isAllLoadingProcessDone = NO;
//    isPendingProcessResult = NO;
//    
//    if (!isWaitingDelayTimeOver)
//    {
//        //[self startLoadingProcess];
//        NSLog(@"loadingProcessNOTDone");
//    }
//}

- (void) allLoadingProcessesDone {
    //NSLog(@"allLoadingProcessesDone isWaitingDelayTimeOver %d", isWaitingDelayTimeOver);
//    isAllLoadingProcessDone = YES;
//    isPendingProcessResult = NO;
//    
//    if (!isWaitingDelayTimeOver)
//    {
        [self goNextViewController];
//    }
}

- (void) goNextViewController
{
    [self.loading stopAnimating];
    //[self dismissAlert:activateAlertView];
    //NSLog(@"goNextViewController");
    SharedData *sharedData = [SharedData sharedData];
    
    //NSLog(@"sharedData.processResult -> %d", sharedData.processResult);
    if (sharedData.processTarget == PROCESS_TARGET_ACTIVATE_TOKEN) {
        //remove the observer
        [[NSNotificationCenter defaultCenter]removeObserver:self name:NOTIFICATION_NAME_ACTIVATE_TOKEN_DONE object:nil];
        if (sharedData.processResult == PROCESS_RESULT_ACTIVATE_TOKEN_SUCCESS) {
            //save devicewithout pin called to be persisted/
            //this created deviceObject
            [sharedData saveDeviceWithoutPin];
            //here i need to check if the tilt value is enable.
            //if yes skip the set secure alignment screen n go back to home
            //if not go to set secure alignment.
//            NSLog(@"LVC **** sharedData isTiltEnable : %hhd", sharedData.isTiltEnable);
//            NSLog(@"LVC **** sharedData isSecuritySet : %hhd", sharedData.isSecuritySet);
//            NSLog(@"LVC **** sharedData dTiltValue : %@", sharedData.dTiltValue);
            
            //check the profile name, null it first to save the value then resave the profile name
//            NSString *tempProfile = sharedData.profileName;
//            sharedData.profileName = nil;
            [sharedData newloadAppStatus];
            
             self.groupNames = [sharedData load];
            NSLog(@"Total Number of accounts=%lu",(unsigned long)self.groupNames.count);
            
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            if((self.groupNames.count<=1 && [defaults objectForKey:@"ActivationDoneFirstTime"]==nil)&&[defaults objectForKey:@"SECURE_TILT_VALUE"]==nil){
            
                [self performSegueWithIdentifier:@"activationsuccess" sender:nil];
            }
            else{
                [defaults setObject:@"ActivationDoneFirstTime" forKey:@"ActivationDoneFirstTime"];
                [defaults synchronize];
                [self performSegueWithIdentifier:@"firstView" sender:nil];
            }
//            UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"activation"];
//            [self.navigationController pushViewController:vc animated:NO];
//            sharedData.profileName = tempProfile;
            
//            if (sharedData.dTiltValue == nil) {
//                if (sharedData.isSecuritySet == NO) {
//                    
//                    
//                  //  [self performSegueWithIdentifier:@"verifyView" sender:self];
//                }
//                else
//                {
//                    
//                    //[self performSegueWithIdentifier:@"firstView" sender:self];
//                }
//            }
//            else
//            {
//                //go back home
//                //NSLog(@"------------activated tilt already set------ gobackfirstview----");
//                [self performSegueWithIdentifier:@"firstView" sender:self];
//                //[self.navigationController popViewControllerAnimated:YES];
//            }
        }
        
        else{
            //failed activation
            
            /*if(sharedData.returnCode <=-998)
            {
                 NSMutableArray *array =[sharedData checkGnmUid];
                NSLog(@"LV got internet but unable to reach server %@ %@",array,[array objectAtIndex:0]);
                if ([array count]==1) {
                    
                    sharedData.profileName = [array objectAtIndex:0];
                    NSLog(@"LV got server remove name: %@",sharedData.profileName);
                    [sharedData removeAppStatus];
                    [sharedData removeNotSaved];
                }
                NSString *title = [NSString stringWithFormat:@"Activation Failed [%d]",sharedData.returnCode];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                                    message:ALERT_INTERNET_CONNECTION_ERROR_MSG
                                                                   delegate:self
                                                          cancelButtonTitle:nil
                                                          otherButtonTitles:@"OK", nil];
                [alertView show];
                
            }
            else {*/
                int ptvReturnCode = -1 * sharedData.returnCode;
                //NSLog(@"ptvReturnCode -> %d", ptvReturnCode);
                NSString *title ;
                if (ptvReturnCode < 10)
                {
                    title = [NSString stringWithFormat:@"Activation Failed [-%d]",ptvReturnCode];
                    NSLog(@"********AVCMODE**********");
                    
                }
                else
                {
                    title = [NSString stringWithFormat:@"Activation Failed [-%d]",ptvReturnCode];
                    NSLog(@"$$$$$$$ACMODE 0XX $$$$$$$$$$$");
                }
                
            
            NSLog(@"sharedData.processResult=%d",sharedData.processResult);
            
                if (sharedData.processResult == PROCESS_RESULT_ACTIVATE_TOKEN_FAILED)
                {
                    if (ptvReturnCode == 14) {
                        errordeleteAlertView = [[UIAlertView alloc] initWithTitle:title
                                                                          message:@"A network connectivity error has occurred. Please try again later. If the problem persists, please contact your local system administrator."
                                                                         delegate:self
                                                                cancelButtonTitle:nil
                                                                otherButtonTitles:@"OK", nil];
                        [errordeleteAlertView show];
                    }
                    else{
                        NSLog(@"!!!!!!!!!!!!!!!!");
                        errorAlertView = [[UIAlertView alloc] initWithTitle:title
                                                                    message:@"A network connectivity error has occurred. Please try again later. If the problem persists, please contact your local system administrator."
                                                                   delegate:self
                                                          cancelButtonTitle:nil
                                                          otherButtonTitles:@"OK", nil];
                        [errorAlertView show];
                    }
                }
                
            }
        }
    //}
    if (sharedData.processTarget == PROCESS_TARGET_TRANSACTION_SEND) {
        
        NSLog(@"%@",sharedData.jsonDictionary);
        
        if (sharedData.processResult == PROCESS_RESULT_TRANSACTION_SEND_SUCCESS &&
            [[[sharedData.jsonDictionary valueForKey:@"rc"] stringValue] isEqualToString:@"0"]) {
            //success transaction
             //NSLog(@" LVC~~~~gonextVC~~~~success transaction~~~~~~~~~");
            ResultViewController *resulteVC = [self.storyboard instantiateViewControllerWithIdentifier:@"resultView"];
            resulteVC.dict = sharedData.jsonDictionary;
           [self.navigationController pushViewController:resulteVC animated:NO];
            //[self performSegueWithIdentifier:@"result" sender:self];
        }
        else{
            //failed
            NSLog(@"LV goNEXTVC got internet but unable to reach server");
            NSString *title = [NSString stringWithFormat:@"Transaction Failed [%@]",[[sharedData.jsonDictionary valueForKey:@"rc"] stringValue]];
            transalertView = [[UIAlertView alloc] initWithTitle:title
                                                                message:@"Failed Transaction"
                                                               delegate:self
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"OK", nil];
            [transalertView show];
        }
    }
    
    
    if(sharedData.processTarget == PROCESS_TARGET_ONLINE_LOGIN){
        if(sharedData.processResult == PROCESS_RESULT_ONLINE_LOGIN_SUCCESS){
            //sharedData.jsonDictionary
        }else{
            NSString *title = [NSString stringWithFormat:@"Invalid Login [%d]",sharedData.returnCode];
            transalertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:@"Login failed!"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
            [transalertView show];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //[baseDelegate updateUserEventTime];
    // SharedData *sharedData = [SharedData sharedData];
    if (alertView == errordeleteAlertView) {
        if (buttonIndex == 0) {
            SharedData *sharedData = [SharedData sharedData];
            NSLog(@"LVc errorAlertView profilename -------> %@", sharedData.profileName);
            [sharedData removeFromList];
            [self performSegueWithIdentifier:@"firstView" sender:nil];
            
            [alertView release];
        }
    }
    else if (alertView == errorAlertView)
    {
        if (buttonIndex == 0) {
            // YES
            //other delete device
            //go back 1st screen
            SharedData *sharedData = [SharedData sharedData];
            NSLog(@"LVc errorAlertView profilename -------> %@", sharedData.profileName);
            [sharedData removeFromList];
            [self performSegueWithIdentifier:@"firstView" sender:nil];
            
            [alertView release];
            
        }
        
    }
    else if (alertView == transalertView) {
        SharedData *sharedData = [SharedData sharedData];
        NSLog(@"LVC trans errorAlertView profilename ----------> %@", sharedData.profileName);
        [self performSegueWithIdentifier:@"firstView" sender:nil];
        [alertView release];
        
    }
    else {

        //if its failed with the error
        //remove save file.
        
        SharedData *sharedData = [SharedData sharedData];
        NSLog(@"LVC XerrorAlertView profilename ----------> %@", sharedData.profileName);
        //[sharedData removeFromList];
        [self performSegueWithIdentifier:@"firstView" sender:nil];
        
        [alertView release];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [_loading release];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}
@end
