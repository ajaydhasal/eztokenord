//
//  ResponseXMLParser.h
//  MSign
//
//  Created by Venus Kim on 6/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TXDescriptionHTMLParser.h"

@protocol ResponseXMLParserDelegate

- (void) setReturnCodeVal:(int)returnCodeVal;
- (void) setDeviceACString:(NSString *)deviceACString;
//- (void) setServerPubKeyString:(NSString *)serverPubKeyString;
//- (void) setCertStatusVal:(int)certStatusVal;
//
//- (void) addServiceProviderInfo:(NSMutableDictionary *)spInfo;
//
//- (void) addTransactionInfo:(NSMutableDictionary *)txInfo;
//- (void) addSignedTransactionID:(NSString *)txID withStatus:(int)statusVal;
//
//- (void) parseErrorOccurred:(NSError *)error;

@end


@interface ResponseXMLParser : NSObject
<NSXMLParserDelegate, TXDescriptionHTMLParserDelegate>
{
	id <ResponseXMLParserDelegate> delegate;
	
	int responseXMLType;
	NSMutableString *elementString;
	NSMutableDictionary *tempDict;
	NSMutableDictionary *attachDict;
	
	NSString *gatewayHost;
}

@property (nonatomic, assign) id <ResponseXMLParserDelegate> delegate;

@property (nonatomic) int responseXMLType;
@property (nonatomic, retain) NSMutableString *elementString;
@property (nonatomic, retain) NSMutableDictionary *tempDict;
@property (nonatomic, retain) NSMutableDictionary *attachDict;

@property (nonatomic, retain) NSString *gatewayHost;


+ (ResponseXMLParser *) sharedXMLParser;

- (void) parseXML:(NSData *)xmlData;


@end
