//
//  DownloaderFactory.h
//  MSign
//
//  Created by Venus Kim on 6/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Downloader.h"


@interface DownloaderFactory : NSObject {
	NSMutableArray *downloaderPool;
}

+ (DownloaderFactory *) sharedFactory;

- (void) initFactory;

- (Downloader *) getDownloader;
- (Downloader *) getFreeDownloader;
- (Downloader *) getNewDownloader;
	

@end
