//
//  TXDescriptionHTMLParser.m
//  MSIGN
//
//  Created by Venus Kim on 11/19/12.
//
//

#import "TXDescriptionHTMLParser.h"

@implementation TXDescriptionHTMLParser

@synthesize delegate;


static TXDescriptionHTMLParser *_sharedTXDescHTMLParserInstance;

+ (TXDescriptionHTMLParser *) sharedHTMLParser {
	if (_sharedTXDescHTMLParserInstance == nil) {
		_sharedTXDescHTMLParserInstance = [[TXDescriptionHTMLParser alloc] init];
	}
	return _sharedTXDescHTMLParserInstance;
}


- (id) init {
	if (self = [super init]) {
		// Custom initializing code
	}
	return self;
}

- (void) dealloc {
	delegate = nil;
	
	[super dealloc];
}

//////////////////////////////////////////////////////////////

#define HTML_TAG_NAME_META                  @"meta"
#define HTML_ATTRIBUTE_NAME_NAME			@"name"
#define HTML_ATTRIBUTE_NAME_CONTENT			@"content"
#define HTML_ATTRIBUTE_VALUE_DESCRIPTION	@"description"


- (void) parseHTML:(NSData *)htmlData
{
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:htmlData];
    parser.delegate = self;
    [parser parse];
    [parser release];
}


- (void)parserDidStartDocument:(NSXMLParser *)parser
{

}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *) qualifiedName attributes:(NSDictionary *)attributeDict
{
	NSString *lowercaseName = [elementName lowercaseString];
	if ([lowercaseName isEqualToString:HTML_TAG_NAME_META])
	{
        if ([attributeDict objectForKey:HTML_ATTRIBUTE_NAME_NAME])
        {
            NSString *lowercaseNameValue = [attributeDict objectForKey:HTML_ATTRIBUTE_NAME_NAME];
            if ([lowercaseNameValue isEqualToString:HTML_ATTRIBUTE_VALUE_DESCRIPTION])
            {
                if ([attributeDict objectForKey:HTML_ATTRIBUTE_NAME_CONTENT])
                {
                    [delegate setMessageString:[attributeDict objectForKey:HTML_ATTRIBUTE_NAME_CONTENT]];
                }
            }
        }
	}
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{

}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{

}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
	printf("Exception occurs during - Description HTML - parsing\n");
}


@end
