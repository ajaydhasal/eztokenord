//
//  ResponseXMLParser.m
//  MSign
//
//  Created by Venus Kim on 6/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ResponseXMLParser.h"
#import "Constant.h"
#import "SharedData.h"


@implementation ResponseXMLParser

@synthesize delegate;

@synthesize responseXMLType;
@synthesize elementString;
@synthesize tempDict;
@synthesize attachDict;
@synthesize gatewayHost;


static ResponseXMLParser *_sharedResponseXMLParserInstance;

+ (ResponseXMLParser *) sharedXMLParser {
	if (_sharedResponseXMLParserInstance == nil) {
		_sharedResponseXMLParserInstance = [[ResponseXMLParser alloc] init];
	}
	return _sharedResponseXMLParserInstance;
}


- (id) init {
	if (self = [super init]) {
		// Custom initializing code
		self.elementString = nil;
		self.tempDict = nil;
		self.attachDict = nil;
		self.gatewayHost = nil;
	}
	return self;
}

- (void) dealloc {
	delegate = nil;
	
	[elementString release];
	[tempDict release];
	[attachDict release];
	[gatewayHost release];
	
	[super dealloc];
}

//////////////////////////////////////////////////////////////

- (void) parseXML:(NSData *)xmlData 
{
	NSURL *gatewayURL = [NSURL URLWithString:[[SharedData sharedData] gatewayURL]];
	self.gatewayHost = [gatewayURL host];
	
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:xmlData];
    parser.delegate = self;
    [parser parse];
    [parser release];
}


#define XML_ELEMENT_NAME_DEVICE_ACT_CODE		@"actcode"

#define XML_ELEMENT_NAME_TRANSACTION			@"tx"
#define XML_ATTRIBUTE_NAME_ID					@"id"
#define XML_ATTRIBUTE_NAME_STATUS				@"status"

#define XML_ELEMENT_NAME_TRANSACTION_NO			@"no"
#define XML_ELEMENT_NAME_TRANSACTION_DATE		@"date"
#define XML_ELEMENT_NAME_TRANSACTION_EXPIRY		@"expiry"
#define XML_ELEMENT_NAME_TRANSACTION_SERVER		@"server"
#define XML_ELEMENT_NAME_TRANSACTION_DESC		@"desc"


#define XML_ELEMENT_NAME_RESPONSE				@"resp"
#define XML_ATTRIBUTE_NAME_RETURN_CODE			@"retcode"

#define XML_ELEMENT_NAME_SERVER_PUBLIC_KEY		@"serverpubkey"
#define XML_ELEMENT_NAME_CERTIFICATION_STATUS	@"certstatus"

#define XML_ELEMENT_NAME_SERVICE				@"sp"
#define XML_ATTRIBUTE_NAME_NAME					@"name"
#define XML_ATTRIBUTE_NAME_TYPE					@"type"

#define XML_ELEMENT_NAME_SERVICE_APP_NAME				@"appname"
#define XML_ELEMENT_NAME_SERVICE_APP_DESCRIPTION		@"appdesc"
#define XML_ELEMENT_NAME_SERVICE_APP_IMAGE				@"appimg"
#define XML_ELEMENT_NAME_SERVICE_ORG_IMAGE				@"orgimg"
#define XML_ATTRIBUTE_NAME_CHECKSUM                     @"chksum"
#define XML_ELEMENT_NAME_SERVICE_TRANSACTION_COUNT		@"txcount"

#define XML_ELEMENT_NAME_TRANSACTION_ATTACHMENT			@"attachment"
#define XML_ATTRIBUTE_NAME_SIGN                         @"sign"

#define XML_ELEMENT_NAME_TRANSACTION_ATTACHMENT_SIZE		@"size"
#define XML_ELEMENT_NAME_TRANSACTION_ATTACHMENT_CHECKSUM	@"chksum"
#define XML_ELEMENT_NAME_TRANSACTION_ATTACHMENT_KEY			@"key"
#define XML_ELEMENT_NAME_TRANSACTION_SUMMARY		@"summary"
#define XML_ELEMENT_NAME_TRANSACTION_ATTACHMENT_URL			@"url"

#define XML_ELEMENT_NAME_PARAM					@"param"


- (void)parserDidStartDocument:(NSXMLParser *)parser 
{
	switch (responseXMLType) 
	{
		case PROCESS_TARGET_ACTIVATE_TOKEN:
		{
			self.tempDict = nil;
			break;
		}
	}
}

- (void)parserDidEndDocument:(NSXMLParser *)parser 
{

}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *) qualifiedName attributes:(NSDictionary *)attributeDict 
{
	NSString *lowercaseName = [elementName lowercaseString];
	if ([lowercaseName isEqualToString:XML_ELEMENT_NAME_RESPONSE])
	{
        if ([attributeDict objectForKey:XML_ATTRIBUTE_NAME_RETURN_CODE])
        {
            NSString *returnCodeString = [attributeDict objectForKey:XML_ATTRIBUTE_NAME_RETURN_CODE];
            [delegate setReturnCodeVal:[returnCodeString intValue]];
        }
	}
	
	switch (responseXMLType) 
	{
		
		case PROCESS_TARGET_ACTIVATE_TOKEN:
		{
			break;
		}
			
		default: 
		{
			break;
		}
	}
	
	self.elementString = [NSMutableString stringWithString:@""];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName 
{
	NSString *lowercaseName = [elementName lowercaseString];

	switch (responseXMLType) 
	{
		case PROCESS_TARGET_ACTIVATE_TOKEN: 
		{
			if ([lowercaseName isEqualToString:XML_ELEMENT_NAME_DEVICE_ACT_CODE]) 
			{
				[delegate setDeviceACString:elementString];
			}
			break;
		}
			
		default: 
		{
			
			break;
		}
	}
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	[elementString appendString:string];
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
	printf("Exception occurs during - Response XML - parsing\n");
	[delegate parseErrorOccurred:parseError];
}

@end
