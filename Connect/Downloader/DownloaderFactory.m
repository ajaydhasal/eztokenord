//
//  DownloaderFactory.m
//  MSign
//
//  Created by Venus Kim on 6/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DownloaderFactory.h"


#define DOWNLOADER_POOL_SIZE	8


@implementation DownloaderFactory


static DownloaderFactory *_sharedDownloaderFactoryInstance;

+ (DownloaderFactory *) sharedFactory {
	if (_sharedDownloaderFactoryInstance == nil) {
		_sharedDownloaderFactoryInstance = [[DownloaderFactory alloc] init];
	}
	return _sharedDownloaderFactoryInstance;
}

- (id) init {
	if (self = [super init]) {
		[self initFactory];
	}
	return self;
}
	
- (void) initFactory {
	downloaderPool = [[NSMutableArray alloc] init];
}


- (Downloader *) getDownloader {
	Downloader *aDownloader = [self getFreeDownloader];
	if (aDownloader == nil) {
		aDownloader = [self getNewDownloader];
	}	
	return aDownloader;
}


- (Downloader *) getFreeDownloader {
    //NSLog(@"Getting Free Downloader");
	if ([downloaderPool count] > 0) {
		for (int i=0; i<[downloaderPool count]; i++) {
			Downloader *aDownloader = [downloaderPool objectAtIndex:i];
			if (aDownloader.isFree) {
				return aDownloader;
			}
		}
	}
	
	return nil;
}

- (Downloader *) getNewDownloader {
    //NSLog(@"Getting getNewDownloader");
	if ([downloaderPool count] < DOWNLOADER_POOL_SIZE) {
		Downloader *aDownloader = [[Downloader alloc] init];
		[downloaderPool addObject:aDownloader];
		//[aDownloader release];
		
		return [downloaderPool lastObject];
	}
	
	return nil;
}


@end
