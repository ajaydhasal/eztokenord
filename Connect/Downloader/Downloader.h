//
//  Downloader.h
//  LoadImage
//
//  Created by Venus Kim on 12/30/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (URLEncoding)

-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;

@end


@protocol SmallFileDownloaderDelegate

- (void) didFinishLoadData:(NSData *)data loadingFlag:(NSInteger)flag statusParam:(NSInteger)param;
- (void) didFailLoadData:(NSError *)error loadingFlag:(NSInteger)flag statusParam:(NSInteger)param;
@optional
- (void) setEncryptMode:(NSInteger)bEncrypt;

@end

@protocol LargeFileDownloaderDelegate

- (void) didFinishDownloadFile:(NSString *)filePath;
- (void) didFailDownloadFile;

- (void) didReceiveDataBySize:(long long int)size;
- (void) didReceiveDataByPercent:(double)percent;

- (void) notEnoughFreeSpaceAvailable;

- (void) setEncryptMode:(NSInteger)bEncrypt;

@end


@interface Downloader : NSObject {
	id <SmallFileDownloaderDelegate> smallDownloadDelegate;
	id <LargeFileDownloaderDelegate> largeDownloadDelegate;
	
    NSURLConnection *conn;
	
	NSInteger loadingFlag;
	NSInteger statusParam;
    NSMutableData *receivedData;
	
	BOOL isFree;

	BOOL isLargeFileDownloading;
	long long int expectedFileSize;
	
	long long int receivedDataSize;
	//NSString *downloadFileName;
	NSString *downloadFilePath;
	NSOutputStream *writeFileStream;
}

@property (nonatomic, assign) id <SmallFileDownloaderDelegate> smallDownloadDelegate;
@property (nonatomic, assign) id <LargeFileDownloaderDelegate> largeDownloadDelegate;

@property (nonatomic, retain) NSURLConnection *conn;

@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic) NSInteger loadingFlag;
@property (nonatomic) NSInteger statusParam;

@property (nonatomic) BOOL isFree;

@property (nonatomic) BOOL isLargeFileDownloading;
@property (nonatomic) long long int expectedFileSize;

@property (nonatomic) long long int receivedDataSize;
//@property (nonatomic, retain) NSString *downloadFileName;
@property (nonatomic, retain) NSString *downloadFilePath;
@property (nonatomic, retain) NSOutputStream *writeFileStream;


- (void)downloadSmallFileFromURL:(NSString *)strURL;
//- (void)downloadLargeFileFromURL:(NSString *)strURL;
- (void)downloadLargeFileFromURL:(NSString *)strURL toPath:(NSString *)filePath;
- (void)downloadXMLContentsFromURL:(NSString *)strURL withXML:(NSString *)requestXML;

- (void)cancelLargeFileDownloading;

@end
