//
//  Downloader.m
//  LoadImage
//
//  Created by Venus Kim on 12/30/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Downloader.h"
#import "Constant.h"

@implementation NSString (URLEncoding)

- (NSString *) urlEncodeUsingEncoding:(NSStringEncoding)encoding
{
	return (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
															   (CFStringRef)self,
															   NULL,
															   (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
															   CFStringConvertNSStringEncodingToEncoding(encoding)));
}

@end

@implementation Downloader

@synthesize smallDownloadDelegate;
@synthesize largeDownloadDelegate;

@synthesize conn;
@synthesize receivedData;
@synthesize loadingFlag, statusParam;
@synthesize isFree;
@synthesize isLargeFileDownloading;
@synthesize expectedFileSize;

@synthesize receivedDataSize;
//@synthesize downloadFileName;
@synthesize downloadFilePath;
@synthesize writeFileStream;

- (id) init {
	if (self = [super init]) {
		self.isFree = YES;
		self.isLargeFileDownloading = NO;
		self.expectedFileSize = 0;
		self.receivedDataSize = 0;
		//self.downloadFileName = nil;
		self.downloadFilePath = nil;
		self.writeFileStream = nil;
	}
	return self;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	NSLog(@"\n# Response Status Code: [%ld]\n", (long)[httpResponse statusCode]);
	
	// Check All Fields.
	NSDictionary *headerFields = [httpResponse allHeaderFields];
	for (NSString *headerKey in [headerFields allKeys]) {
		//printf("* [%s] = [%s]\n", [headerKey UTF8String], [[headerFields objectForKey:headerKey] UTF8String]);
		if ([headerKey caseInsensitiveCompare:RESPONSE_HEADER_NAME_ENCRYPT_MODE] == NSOrderedSame) {
			if (isLargeFileDownloading) {
				[largeDownloadDelegate setEncryptMode:[[headerFields objectForKey:headerKey] intValue]];
			} else {
				[smallDownloadDelegate setEncryptMode:[[headerFields objectForKey:headerKey] intValue]];
			}
		}
	}

	// Downloading Contents Size.
	if (isLargeFileDownloading) {
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);  
		NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths objectAtIndex:0] error:nil];
		
		if (dictionary) {
			//float freeSpaceSize = diskFreeSpace();
			NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
			long long int freeSpaceSize = [freeFileSystemSizeInBytes longLongValue];

			self.expectedFileSize = [response expectedContentLength];
			if (expectedFileSize >= freeSpaceSize) {
				// Not enough disk space
				[largeDownloadDelegate notEnoughFreeSpaceAvailable];
				[largeDownloadDelegate didFailDownloadFile];
				
				[conn cancel];
				[conn release];
			} else {			
				writeFileStream = [[NSOutputStream alloc] initToFileAtPath:downloadFilePath append:YES];
				[writeFileStream open];
			}
		}
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{
	if (isLargeFileDownloading) {
		long long int receivedPackageSize = [data length];
		if (CHECK_DOWNLOADING_PROGRESS_BY_PERCENT == YES) {
			NSUInteger left = [data length];
			NSUInteger nwr = 0;
			do {
				nwr = [writeFileStream write:(const uint8_t *)[data bytes] maxLength:left];
				if (-1 == nwr) break;
				left -= nwr;
			} while (left > 0);
			if (left) {
				NSLog(@"stream error: %@", [writeFileStream streamError]);
			}
			
			self.receivedDataSize += receivedPackageSize;
			[largeDownloadDelegate didReceiveDataByPercent:100.0f * receivedDataSize/ expectedFileSize];
		} else {
			[largeDownloadDelegate didReceiveDataBySize:receivedDataSize];
		}
	} else {
		[receivedData appendData:data];
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
    //NSLog(@"Error is: %ld",(long)error.code);
    // Release the connection
    [connection release];
	
	// Set this downloader is free, ready to download another
	self.isFree = YES;
	
	if (isLargeFileDownloading) 
	{
		self.isLargeFileDownloading = NO;
		
		[largeDownloadDelegate didFailDownloadFile];
		
		if (writeFileStream) {
			[writeFileStream close];
			[writeFileStream release];
		}
	}
	else 
	{
		[receivedData release];
		[smallDownloadDelegate didFailLoadData:error 
								   loadingFlag:loadingFlag 
								   statusParam:statusParam];
	}
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection 
{
    //NSLog(@"connectionDidFinishLoading");
	if (isLargeFileDownloading) 
	{
		self.isLargeFileDownloading = NO;
		
		[largeDownloadDelegate didFinishDownloadFile:downloadFilePath];

		if (writeFileStream) {
			[writeFileStream close];
			[writeFileStream release];
		}
	}
	else 
	{
		[smallDownloadDelegate didFinishLoadData:receivedData 
									 loadingFlag:loadingFlag 
									 statusParam:statusParam];
		
		// Release the received data object
		if (receivedData) [receivedData release];
	}
	
    // Release the connection
	if (connection) [connection release];
	
	// Set this downloader is free, ready to download another
	self.isFree = YES;
}


- (void)downloadSmallFileFromURL:(NSString *)strURL 
{	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strURL] 
															  cachePolicy:NSURLRequestUseProtocolCachePolicy
														  timeoutInterval:HTTP_REQUEST_TIME_OUT];
	[request setHTTPMethod:@"POST"];
	[request setHTTPBody:nil];
	
	self.isLargeFileDownloading = NO;
	
	//Create the connection with the request and start loading the data
    conn =  [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    if (conn) {
		self.isFree = NO;
		
        //Create the NSMutableData that will hold the recieve data
        receivedData = [[NSMutableData data] retain];
    } else {
		[conn release];
    }
}

- (void)downloadLargeFileFromURL:(NSString *)strURL toPath:(NSString *)filePath 
{
	printf("\n# Bulk File URL: [%s]\n", [strURL UTF8String]);
	
	self.expectedFileSize = 0;
	self.receivedDataSize = 0;

	/*
	// Temporary Directory
	NSString *tempPath = NSTemporaryDirectory();
	NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:tempPath error:nil];
	if (dirContents) {
		NSArray *onlyPDFs = [dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.pdf'"]];
		NSFileManager *fileManager = [NSFileManager defaultManager];	
		if (onlyPDFs) {
			for (int i = 0; i < [onlyPDFs count]; i++) {
				NSLog(@"Directory Count: %i", [onlyPDFs count]);
				NSString *contentsOnly = [NSString stringWithFormat:@"%@%@", tempPath, [onlyPDFs objectAtIndex:i]];
				[fileManager removeItemAtPath:contentsOnly error:nil];
			}
		}
	}
	*/	
	self.downloadFilePath = filePath;
	
	// Create Request
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strURL] 
														   cachePolicy:NSURLRequestUseProtocolCachePolicy
													   timeoutInterval:HTTP_REQUEST_TIME_OUT];
	[request setHTTPMethod:@"POST"];
	[request setHTTPBody:nil];
	
	self.isLargeFileDownloading = YES;
	
	//Create the connection with the request and start loading the data
    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    if (conn) {
		self.isFree = NO;
    } else {
		[conn release];
    }
}


- (void)downloadXMLContentsFromURL:(NSString *)strURL withXML:(NSString *)requestXML 
{
	NSLog(@"\n# URL: [%s]\n", [strURL UTF8String]);
	NSLog(@"\n# JSON =\n%s\n", [requestXML UTF8String]);
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strURL]
														   cachePolicy:NSURLRequestUseProtocolCachePolicy
													   timeoutInterval:HTTP_REQUEST_TIME_OUT];
	[request setHTTPMethod:@"POST"];
	
	NSString *encodedXML = [requestXML urlEncodeUsingEncoding:NSUTF8StringEncoding];
	NSString *params = [NSString stringWithFormat:@"%@=%@", REQUEST_PARAMETER_NAME, encodedXML];
	[request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
	
	self.isLargeFileDownloading = NO;
	
	//Create the connection with the request and start loading the data
    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self ];//startImmediately:YES];
    [conn scheduleInRunLoop:[NSRunLoop mainRunLoop]
                          forMode:NSDefaultRunLoopMode];
    [conn start];
    if (conn) {
        
		self.isFree = NO;
		//NSLog(@"Connection VALID");
        //Create the NSMutableData that will hold the receive data
        receivedData = [[NSMutableData data] retain];
        //NSLog(@"RECEIVED DATA: %@", receivedData);
    } else {
        //NSLog(@"Connection NOT VALID");
		[conn release];
    }
}


- (void)cancelLargeFileDownloading {	
	if (isLargeFileDownloading) {
		// Release the connection
		if (conn) {
			[conn cancel];
			[conn release];
		}
		
		// Release file stream
		if (writeFileStream) {
			[writeFileStream close];
			[writeFileStream release];
		}
		
		// Set this downloader is free, ready to download another
		self.isLargeFileDownloading = NO;
		self.isFree = YES;
	}
}


- (void)dealloc {
	smallDownloadDelegate = nil;
	largeDownloadDelegate = nil;
	
    [conn release];
    [receivedData release];
	
	//[downloadFileName release];
	[downloadFilePath release];
	[writeFileStream release];
	
    [super dealloc];
}


//- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace 
//{
//	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
//}

#define ALLOW_CONNECTION_DESPITE_BAD_CERTIFICATE	YES

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge 
{
	if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) 
	{
		if (ALLOW_CONNECTION_DESPITE_BAD_CERTIFICATE) 
		{
			[challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]
				 forAuthenticationChallenge:challenge];
		}
	}

	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}


- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)space {
    if([[space authenticationMethod] isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        //if(shouldAllowSelfSignedCert) {
            return YES; // Self-signed cert will be accepted
       // } else {
       //     return NO;  // Self-signed cert will be rejected
       // }
        // Note: it doesn't seem to matter what you return for a proper SSL cert
        //       only self-signed certs
    }
    // If no other authentication is required, return NO for everything else
    // Otherwise maybe YES for NSURLAuthenticationMethodDefault and etc.
    return NO;
}



@end
