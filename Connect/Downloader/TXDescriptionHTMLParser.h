//
//  TXDescriptionHTMLParser.h
//  MSIGN
//
//  Created by Venus Kim on 11/19/12.
//
//

#import <Foundation/Foundation.h>

@protocol TXDescriptionHTMLParserDelegate <NSObject>
@optional
- (void) setMessageString:(NSString *)messageString;

@end

@interface TXDescriptionHTMLParser : NSObject <NSXMLParserDelegate>
{
    id <TXDescriptionHTMLParserDelegate> delegate;
}

@property (nonatomic, assign) id <TXDescriptionHTMLParserDelegate> delegate;

+ (TXDescriptionHTMLParser *) sharedHTMLParser;

- (void) parseHTML:(NSData *)htmlData;

@end
