//
//  NSMutableData-AES.m
//  NETSECURE
//
//  Created by Harsha on 4/4/13.
//
//

#import "NSMutableData-AES.h"
#import <CommonCrypto/CommonCryptor.h>

@implementation NSMutableData(AES)

- (NSMutableData*) Scramble: (NSString *) key
{
    char keyPtr[kCCKeySizeAES256+1];
    bzero( keyPtr, sizeof(keyPtr) );
    
    bool bResult = [key getCString: keyPtr maxLength: sizeof(keyPtr) encoding: NSUTF16StringEncoding];

    size_t numBytesScramble = 0;
    
    NSUInteger dataLength = [self length];
    
    size_t bufferSize = 0;
    bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    
    
    //NSMutableData *output = [[NSMutableData alloc] init];
    
    CCCryptorStatus result = CCCrypt( kCCEncrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
                                     keyPtr, kCCKeySizeAES256,
                                     NULL,
                                     [self mutableBytes], [self length],
                                     buffer, bufferSize,
                                     &numBytesScramble );
    
    
    NSMutableData *output = [NSMutableData dataWithBytesNoCopy:buffer length:numBytesScramble];
    if( result == kCCSuccess )
    {
        return output;
    }
    return NULL;
}

- (NSMutableData*) UnScramble: (NSString *) key andForData:(NSMutableData*)objScrambleData
{
    
    char  keyPtr[kCCKeySizeAES256+1];
    bzero( keyPtr, sizeof(keyPtr) );
    
    bool bResult = [key getCString: keyPtr maxLength: sizeof(keyPtr) encoding: NSUTF16StringEncoding];
    
    size_t numBytesScramble = 0;
    
    NSUInteger dataLength = [self length];
    
    size_t bufferSize = 0;
    bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer_decrypt = malloc(bufferSize);
    //NSMutableData *output_unScramble = [[[NSMutableData alloc] init]autorelease];
    CCCryptorStatus result = CCCrypt( kCCDecrypt , kCCAlgorithmAES128, kCCOptionPKCS7Padding,
                                     keyPtr, kCCKeySizeAES256,
                                     NULL,
                                     [self mutableBytes], [self length],
                                     buffer_decrypt, bufferSize,
                                     &numBytesScramble );
    
    NSMutableData *output_unScramble = [NSMutableData dataWithBytesNoCopy:buffer_decrypt length:numBytesScramble];
    if( result == kCCSuccess )
    {
        return output_unScramble;
    }
    return NULL;
}
@end
