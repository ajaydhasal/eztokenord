//
//  NSMutableData-AES.h
//  NETSECURE
//
//  Created by Harsha on 4/4/13.
//
//

#import <Foundation/Foundation.h>

@interface NSMutableData(AES)
- (NSMutableData*) Scramble: (NSString *) key;
- (NSMutableData*) UnScramble: (NSString *) key andForData:(NSMutableData*)objScrambleData;

@end
