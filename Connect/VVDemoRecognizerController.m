//
//  VVDemoEnrollerController.m
//  IOSVVDemo
//
//  Created by Karl Ridgeway on 8/28/14.
//  Copyright (c) 2014 Sensory. All rights reserved.
//

/*if(sharedData.flagForMatchAngle || sharedData.flagForDisableSecureAligment)
 [defaults setObject:@"IS_TILT_CHANGED" forKey:@"IS_TILT_CHANGED"];
 [defaults setObject:@"SECURITY_TYPE_SECUREALIGNMENT" forKey:@"SECURITY_TYPE"];
 [defaults setObject:@"FLAG_FOR_MATCH_ANGLE" forKey:@"FLAG_FOR_MATCH_ANGLE"];
 [defaults setObject:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT" forKey:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT"];
 [defaults synchronize];*/
//
//            if([defaults objectForKey:@"IS_SECURITY_SET"]){
//                [defaults setObject:@"IS_SECURITY_SET" forKey:@"IS_SECURITY_SET"];
//                [defaults setObject:@"SECURITY_TYPE_TOUCHID" forKey:@"SECURITY_TYPE"];
//            }else{
//                [defaults removeObjectForKey:@"IS_SECURITY_SET"];
//                [defaults removeObjectForKey:@"SECURITY_TYPE"];
//
//            }

//[defaults removeObjectForKey:@"IS_SECURITY_SET"];
//@"SECURE_TILT_VALUE"

#import "VVDemoRecognizerController.h"
#import "VVDemoAlertView.h"
#import <AVFoundation/AVFoundation.h>
#import <smma/smma/smma.h>
#import <libkern/OSAtomic.h>
#import "settings.h"
#include <limits.h>

#define TEST_USER @"test"

@implementation VVDemoRecognizerController

- (AVCaptureDevice *)getFrontCamera {
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == AVCaptureDevicePositionFront) {
            return device;
        }
    }
    return nil;
}

- (AVCaptureDevice*)getMicrophone {
    AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    return device;
}

- (NSString *) applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

- (NSString *) applicationCachesDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory  , NSUserDomainMask, YES);
    NSString *path = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    BOOL isDir = NO;
    NSError *error;
    if (! [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDir] && isDir == NO) {
        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:&error];
    }
    return path;
}

- (void) reset
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.noiseIndicatorIcon setImage:nil];
        [self.lightingIndicatorIcon setImage:nil];
        [self.faceStatusIndicatorIcon setImage:nil];
        [self.vuMeter setProgress:0.0f];
        [self.statusLabel setText:@"Idle"];
    });
    OSAtomicAnd32Barrier(0, &_running);
    _enrolling = _authenticating = false;
    _currTimeMS = 0;
}

-(NSString*) getGradientOverlayImageFilePath
{
    NSString* docsDir = [self applicationDocumentsDirectory];
    NSString* filePath = [docsDir stringByAppendingString:@"/gradient_640x480.png"];
    if( ! [[NSFileManager defaultManager] fileExistsAtPath:filePath] ) {
        [ImageUtils makeCenterGradientImage:640 cols:480 gradientRGB:0xFF000000 centerARGB:0x00000000 path:filePath];
    }
    return filePath;
}

-(void) loadIcons
{
    NSString* bundlepath = [[NSBundle mainBundle] bundlePath];
    _iconNoiseOK = [UIImage imageWithContentsOfFile:[bundlepath stringByAppendingString:@"/noise_ok.png"]];
    _iconNoiseMED = [UIImage imageWithContentsOfFile:[bundlepath stringByAppendingString:@"/noise_med.png"]];
    _iconNoiseHIGH = [UIImage imageWithContentsOfFile:[bundlepath stringByAppendingString:@"/noise_high.png"]];
    _iconLightOK = [UIImage imageWithContentsOfFile:[bundlepath stringByAppendingString:@"/light_ok.png"]];
    _iconLightLOW = [UIImage imageWithContentsOfFile:[bundlepath stringByAppendingString:@"/light_low.png"]];
    _iconLightNONE = [UIImage imageWithContentsOfFile:[bundlepath stringByAppendingString:@"/light_none.png"]];
    _iconFaceOK = [UIImage imageWithContentsOfFile:[bundlepath stringByAppendingString:@"/face_good.png"]];
    _iconFacePOOR = [UIImage imageWithContentsOfFile:[bundlepath stringByAppendingString:@"/face_poor.png"]];
    _iconFaceNONE = [UIImage imageWithContentsOfFile:[bundlepath stringByAppendingString:@"/face_none.png"]];
}

-(void) updateIcons:(MultiRecognizer*) recog
{
    enum FaceRecognizerState_FaceStatus faceStatus = [[recog getFaceRecognizerState] getFaceStatus];
    enum FaceRecognizerState_LightStatus lightStatus = [[recog getFaceRecognizerState] getLightStatus];
    enum VoiceRecognizerState_NoiseStatus noiseStatus = [[recog getVoiceRecognizerState] getNoiseStatus];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImage* faceImage = nil;
        switch( faceStatus ) {
            default:
            case FaceRecognizerState_FACE_OK: faceImage = _iconFaceOK; break;
            case FaceRecognizerState_FACE_POOR: faceImage = _iconFacePOOR; break;
            case FaceRecognizerState_FACE_NONE: faceImage = _iconFaceNONE; break;
        }
        [self.faceStatusIndicatorIcon setImage:faceImage];
        
        UIImage* lightImage = nil;
        switch( lightStatus ) {
            default:
            case FaceRecognizerState_LIGHT_OK: lightImage = _iconLightOK; break;
            case FaceRecognizerState_LIGHT_LOW: lightImage = _iconLightLOW; break;
            case FaceRecognizerState_LIGHT_NONE: lightImage = _iconLightNONE; break;
        }
        [self.lightingIndicatorIcon setImage:lightImage];
        
        UIImage* noiseImage = nil;
        switch( noiseStatus ) {
            default:
            case VoiceRecognizerState_NOISE_OK: noiseImage = _iconNoiseOK; break;
            case VoiceRecognizerState_NOISE_MED: noiseImage = _iconNoiseMED; break;
            case VoiceRecognizerState_NOISE_HIGH: noiseImage = _iconNoiseHIGH; break;
        }
        [self.noiseIndicatorIcon setImage:noiseImage];
    });
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    [_startButton setTitle:@""];
    [_authenticateButton setTitle:@""];
    [_startButton setEnabled:NO];
    [_authenticateButton setEnabled:NO];
     shareData=[SharedData sharedData];
    
    UIBarButtonItem *backbutton =  [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];
    [backbutton setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor],UITextAttributeFont: [UIFont systemFontOfSize:17.0f]} forState:UIControlStateNormal];
    
    self.navigationItem.leftBarButtonItem=backbutton;
    [backbutton setTarget:self];
    [backbutton setAction:@selector(ClickCancel:)];
    
    [self generalSetting];
    _finishedInitializing = false;
    [self loadIcons];
    [self reset];
    
    AVCaptureSession* captureSession =
        [[AVCaptureSession alloc]init];
    captureSession.sessionPreset = AVCaptureSessionPreset640x480;
    
    {
        AVCaptureDevice *device = [self getFrontCamera];
        if( device == nil ) {
            NSLog(@"Couldn't find front-facing camera device");
            return;
        }
        NSError* error = nil;
        AVCaptureDeviceInput* input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
        if(!input) {
            NSLog(@"Couldn't create video capture device");
            return;
        }
        [captureSession addInput:input];
    }
    
    {
        AVCaptureDevice* mic = [self getMicrophone];
        if( mic == nil ) {
            NSLog(@"Couldn't find microphone");
            return;
        }
        NSError* error = nil;
        AVCaptureDeviceInput* input = [AVCaptureDeviceInput deviceInputWithDevice:mic error:&error];
        if(!input) {
            NSLog(@"Couldn't create audio capture device");
            return;
        }
        [captureSession addInput:input];
    }
    dispatch_queue_t audioQueue = dispatch_queue_create("smmaProcessingQueueAudio", NULL);
    dispatch_queue_t videoQueue = dispatch_queue_create("smmaProcessingQueueVideo", NULL);
    
    AVCaptureVideoDataOutput* videoOutput = [[AVCaptureVideoDataOutput alloc] init];
    [videoOutput setSampleBufferDelegate:self queue:videoQueue];
    videoOutput.videoSettings = [NSDictionary
        dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]
        forKey:(id)kCVPixelBufferPixelFormatTypeKey
    ];
    [captureSession addOutput:videoOutput];
    
    AVCaptureAudioDataOutput* audioOutput = [[AVCaptureAudioDataOutput alloc] init];
    [audioOutput setSampleBufferDelegate:self queue:audioQueue];
    [captureSession addOutput:audioOutput];
    
    // TODO How to set the video frame rate?
    // Setting it here is invalid and throws.
    //device.activeVideoMaxFrameDuration = CMTimeMake(1,10);
    //device.activeVideoMinFrameDuration = CMTimeMake(1,10);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        AVCaptureVideoPreviewLayer* previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:captureSession];
        UIView *view = self.cameraPreviewView;
        CALayer* viewLayer = [view layer];
        
        previewLayer.frame = view.bounds;
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        
        [viewLayer addSublayer:previewLayer];
        
        [captureSession startRunning];
        _videoConnection = [videoOutput connectionWithMediaType:AVMediaTypeVideo];
        _audioConnection = [audioOutput connectionWithMediaType:AVMediaTypeAudio];
        
        _finishedInitializing = true;
        
        [self.gradientOverlayView setImage:[UIImage imageWithContentsOfFile:[self getGradientOverlayImageFilePath]]];
        
        // Gets rid of the border on the toolbar
        self.enrollAuthToolbar.clipsToBounds = YES;
        
        [view addSubview:self.statusLabel];
        [view addSubview:self.gradientOverlayView];
        [view addSubview:self.noiseIndicatorIcon];
        [view addSubview:self.lightingIndicatorIcon];
        [view addSubview:self.faceStatusIndicatorIcon];
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        NSString *str=[defaults objectForKey:@"FACE_REC"];
        if([str isEqualToString:@"DoEnrollment"])
        {
            [self DoEnrollment];
        }
        else{
            [self DoAuthentication];
        }

    });
}
-(void)viewWillAppear:(BOOL)animated{
    [self generalSetting];
    _finishedInitializing = false;
    [self loadIcons];
    [self reset];
    
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        
        
        AVCaptureSession* captureSession =
        [[AVCaptureSession alloc]init];
        captureSession.sessionPreset = AVCaptureSessionPreset640x480;
        
        {
            AVCaptureDevice *device = [self getFrontCamera];
            if( device == nil ) {
                NSLog(@"Couldn't find front-facing camera device");
                return;
            }
            NSError* error = nil;
            AVCaptureDeviceInput* input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
            if(!input) {
                NSLog(@"Couldn't create video capture device");
                return;
            }
            [captureSession addInput:input];
        }
        
        {
            AVCaptureDevice* mic = [self getMicrophone];
            if( mic == nil ) {
                NSLog(@"Couldn't find microphone");
                return;
            }
            NSError* error = nil;
            AVCaptureDeviceInput* input = [AVCaptureDeviceInput deviceInputWithDevice:mic error:&error];
            if(!input) {
                NSLog(@"Couldn't create audio capture device");
                return;
            }
            [captureSession addInput:input];
        }
        dispatch_queue_t audioQueue = dispatch_queue_create("smmaProcessingQueueAudio", NULL);
        dispatch_queue_t videoQueue = dispatch_queue_create("smmaProcessingQueueVideo", NULL);
        
        AVCaptureVideoDataOutput* videoOutput = [[AVCaptureVideoDataOutput alloc] init];
        [videoOutput setSampleBufferDelegate:self queue:videoQueue];
        
        videoOutput.videoSettings = [NSDictionary
                                     dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]
                                     forKey:(id)kCVPixelBufferPixelFormatTypeKey
                                     ];
        [captureSession addOutput:videoOutput];
        
        AVCaptureAudioDataOutput* audioOutput = [[AVCaptureAudioDataOutput alloc] init];
        [audioOutput setSampleBufferDelegate:self queue:audioQueue];
        [captureSession addOutput:audioOutput];
        
        // TODO How to set the video frame rate?
        // Setting it here is invalid and throws.
        //device.activeVideoMaxFrameDuration = CMTimeMake(1,10);
        //device.activeVideoMinFrameDuration = CMTimeMake(1,10);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            AVCaptureVideoPreviewLayer* previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:captureSession];
            UIView *view = self.cameraPreviewView;
            CALayer* viewLayer = [view layer];
            
            previewLayer.frame = view.bounds;
            previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            
            [viewLayer addSublayer:previewLayer];
            
            [captureSession startRunning];
            _videoConnection = [videoOutput connectionWithMediaType:AVMediaTypeVideo];
            _audioConnection = [audioOutput connectionWithMediaType:AVMediaTypeAudio];
            
            _finishedInitializing = true;
            
            [self.gradientOverlayView setImage:[UIImage imageWithContentsOfFile:[self getGradientOverlayImageFilePath]]];
            
            // Gets rid of the border on the toolbar
            self.enrollAuthToolbar.clipsToBounds = YES;
            
            [view addSubview:self.statusLabel];
            [view addSubview:self.gradientOverlayView];
            [view addSubview:self.noiseIndicatorIcon];
            [view addSubview:self.lightingIndicatorIcon];
            [view addSubview:self.faceStatusIndicatorIcon];
        });
        
        // do your logic
    } else if(authStatus == AVAuthorizationStatusDenied){
        
        cameraAlert=[[UIAlertView alloc]initWithTitle:@"Camera Setting" message:@"Please Check Camera Setting" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [cameraAlert show];
        
        
        // denied
    } else if(authStatus == AVAuthorizationStatusRestricted){
        // restricted, normally won't happen
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        // not determined?!
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){
                NSLog(@"Granted access to %@", AVMediaTypeVideo);
            } else {
                NSLog(@"Not granted access to %@", AVMediaTypeVideo);
            }
        }];
    } else {
        // impossible, unknown authorization status
    }
    
    
    
    
    
    
    
    
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *str=[defaults objectForKey:@"FACE_REC"];
    if([str isEqualToString:@"DoEnrollment"])
    {
        [self DoEnrollment];
    }
    else{
        [self DoAuthentication];
    }
}
-(void)ClickCancel:(id)sender{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"AUTHTOBETURNEDON"];
    
     [self.navigationController popViewControllerAnimated:YES];
    
    
}

//- (IBAction)ClickCancel:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES];
//}
// video capture
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    if( !OSAtomicOr32Barrier(0, &_running) ) {
        return;
    }
    bool isAudio = connection == _audioConnection;
    bool isVideo = connection == _videoConnection;
    MultiRecognizer* recognizer = _enrolling ? _enroller : _authenticator;
    if( isAudio ) {
        AudioBufferList audioBufferList;
        CMBlockBufferRef blockBuffer;
        CMSampleBufferGetAudioBufferListWithRetainedBlockBuffer(sampleBuffer, NULL, &audioBufferList, sizeof(audioBufferList), NULL, NULL, 0, &blockBuffer);
        
        float lastEnergy = 0.0f;
        for( int y =0; y < audioBufferList.mNumberBuffers; y++ ) {
            AudioBuffer audioBuffer = audioBufferList.mBuffers[y];
            short* frame = (short*) audioBuffer.mData;
            size_t nSamples =audioBuffer.mDataByteSize / sizeof(short);
            [recognizer processData:VOICE timestampMS:_currTimeMS data:(unsigned char*)frame len:nSamples*sizeof(short)];
            lastEnergy= [[recognizer getVoiceRecognizerState] getEnergy];
            _currTimeMS += (((float)nSamples) / 44100.0f ) * 1000;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.vuMeter setProgress:lastEnergy];
        });
        CFRelease( blockBuffer );
    } else if( isVideo ) {
        CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
        CVPixelBufferLockBaseAddress(imageBuffer, 0);
        
        size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
        //size_t width = CVPixelBufferGetWidth(imageBuffer);
        size_t height = CVPixelBufferGetHeight(imageBuffer);
        unsigned char* buffer = (unsigned char *)CVPixelBufferGetBaseAddress( imageBuffer );
        // Assuming row-major storage
        size_t len = bytesPerRow * height;
        [recognizer processData:FACE timestampMS:_currTimeMS data:buffer len:len];
    }
    
    if( isVideo ) {
        // The video thread is in charge of managing the enrollment/auth state.
        // Otherwise, the threads might try to save an enrollment simultaneously
        if( _enrolling ) {
            if( [_enroller isEnrollmentDone] ) {
                [self reset];
                [_enroller stop];
                [_enroller save];
                NSLog(@"Enrollment done!");
                [self showAlertWithTitle:@"Enrollment" message:@"Enrollment complete"];
            }
        }

        if( _authenticating ) {
            if( ![[[[_authenticator getAuthenticationResult] getUser] getId] isEqualToString:@""]) {
                [self reset];
                [_authenticator stop];
                [self showAlertWithTitle:@"Authentication" message:@"Authenticated"];
            } else if ( _currTimeMS >= _authTimeout ) {
                [self reset];
                [_authenticator stop];
                if( [self doAdaptiveEnrollmentInteraction] == NO )
                    [self showAlertWithTitle:@"Timed Out" message:@"Authentication has timed out."];
            }
        }
    }
    
    if( _authenticating || _enrolling ) {
        [self updateIcons:_authenticating ? _authenticator : _enroller];
    }
}

- (BOOL)doAdaptiveEnrollmentInteraction {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if( [defaults boolForKey:KEY_USE_ADAPTIVE_ENROLLMENT] ) {
        if( [defaults boolForKey:KEY_REQUIRE_CONFIRMATION] ) {
            UIAlertView* alert = [[VVDemoAlertView alloc] initWithTitle:@"Adaptive Enrollment"
                                                                message:@"Use the last attempt for adaptive enrollment?"
                                                      cancelButtonTitle:@"No"
                                                      otherButtonTitles:@[@"Yes"]
                                                          cancelHandler:^{ }
                                                    confirmationHandler:^(NSInteger idx){
                                                        [_authenticator enhanceEnrollment:TEST_USER];
                                                    }];
            dispatch_async(dispatch_get_main_queue(), ^{
                [alert show];
            });
        } else {
            [_authenticator enhanceEnrollment:TEST_USER];
            [self showAlertWithTitle:@"Adaptive Enrollment" message:@"The last attempt was ued to adapt your enrollment"];
        }
        return YES;
    }
    return NO;
}


- (void)showAlertWithTitle:(NSString*)title message:(NSString*)message
{
//    dispatch_async(dispatch_get_main_queue(), ^{
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//                                                        message:message
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
//    });
    if([message isEqualToString:@"Authenticated"])
    {
        NSLog(@"delegate Of Authenticated");
        dispatch_async(dispatch_get_main_queue(), ^{
            alertAuthenticate = [[UIAlertView alloc] initWithTitle:title
                                                           message:message
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
            [alertAuthenticate show];
            
            
        });
    }
    else if ([message isEqualToString:@"Authentication has timed out."]){
        NSLog(@"delegate Of Enrollment complete");
        dispatch_async(dispatch_get_main_queue(), ^{
            alertTimeOut = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            [alertTimeOut show];
        });

    }
    else
    {
        
        NSLog(@"delegate Of Enrollment complete");
        dispatch_async(dispatch_get_main_queue(), ^{
            alertEnrollment = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            [alertEnrollment show];
        });
    }

}


- (void)captureOutput:(AVCaptureOutput *)captureOutput didDropSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    // This is a bummer but we're going to ignore it for now
}

- (void) setupRecognizer:(MultiRecognizer *) recognizer
{
    NSString* bundlepath = [[NSBundle mainBundle] bundlePath];
    NSMutableString* modelPath = [NSMutableString stringWithString:bundlepath];
    [modelPath appendString:@"/model"];
    NSString* docsDir = [self applicationDocumentsDirectory];
    NSMutableString* enrollPath = [NSMutableString stringWithString:docsDir];
    [enrollPath appendString:@"/enrollments"];
    NSString* cachesDir= [self applicationCachesDirectory];
    NSMutableString* logPath= [NSMutableString stringWithString:cachesDir];
    [logPath appendString:@"/log"];

    // get list of model paths
    NSArray* modelNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:modelPath error:nil];
    NSMutableArray* modelPaths= [NSMutableArray new];
    [modelNames enumerateObjectsUsingBlock:^(id object, NSUInteger idx, BOOL *stop) {
        [modelPaths addObject:[modelPath stringByAppendingPathComponent:object]];
    }];

    NSLog(@"loading %@", modelPaths);
    
    
    [recognizer load:modelPaths enrollPath:enrollPath logPath:logPath];
    
//    [recognizer load:modelPath enrollPath:enrollPath logPath:logPath];
    
    AudioDescriptor* voiceDescriptor = [[AudioDescriptor alloc] init];
    [voiceDescriptor setSampleRate:44100];
    [voiceDescriptor setSampleSize:2];
    [voiceDescriptor setNumChannels:1];
    [recognizer setModeDescriptor:VOICE descriptor:voiceDescriptor];
    
    ImageDescriptor* imageDescriptor = [[ImageDescriptor alloc] init];
    [imageDescriptor setRows:480];
    [imageDescriptor setCols:640];
    // kCVPixelFormatType_32BGRA == ImageDescriptor_IMAGETYPE_BGRA_8
    [imageDescriptor setImageType:ImageDescriptor_IMAGETYPE_BGRA_8];
    [imageDescriptor setOrientation:90];
    [recognizer setModeDescriptor:FACE descriptor:imageDescriptor];
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    _authTimeout = (int)[defaults integerForKey:KEY_AUTHENTICATION_TIMEOUT];
}

-(void) startEnrollment {
    [self.statusLabel setText:@"Enrolling"];
    _enroller = [[MultiEnroller alloc] init];
    [_enroller setCurrUser:[_enroller getUser:TEST_USER]];
    [self setupRecognizer:_enroller];
    [_enroller start];
    _enrolling = true;
    OSAtomicOr32Barrier(1, &_running);
}

- (IBAction)enroll:(id)sender {
//    if( !_finishedInitializing ) return;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        VVDemoAlertView *alert = [[VVDemoAlertView alloc] initWithTitle:@"Enrollment"
//                                                                message:@"Say \"Hello, Blue Genie\" 3 times."
//                                                      cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil
//                                                    cancelHandler:^{
//                                                        [self startEnrollment];
//                                                    }
//                                                    confirmationHandler:^(NSInteger idx){ }
//                                  ];
//        [alert show];
//    });
}

- (IBAction)authenticate:(id)sender {
    
//    if( !_finishedInitializing ) return;
//    [self.statusLabel setText:@"Authenticating"];
//    _authenticator = [[MultiAuthenticator alloc] init];
//    [self setupRecognizer:_authenticator];
//    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
//    
//    
//    NSLog(@"CombinationMethod=%i",[defaults boolForKey:KEY_REQUIRE_BOTH] ? COMBINE_SMART : COMBINE_ANY);
//    
//    [_authenticator setCombinationMethod:([defaults boolForKey:KEY_REQUIRE_BOTH] ? COMBINE_SMART : COMBINE_ANY)];
//    [_authenticator setSecurityLevel:(int)[defaults integerForKey:KEY_SECURITY_LEVEL]];
//    [[_authenticator getFaceRecognizerState] setUseLiveness:[defaults boolForKey:KEY_FACE_MOTION_ANALYSIS]];
//    
//    NSLog(@"require all %d security level %ld use liveness %d", [defaults boolForKey:KEY_REQUIRE_BOTH], 
//          (long)[defaults integerForKey:KEY_SECURITY_LEVEL], [defaults boolForKey:KEY_FACE_MOTION_ANALYSIS]);
//    [_authenticator start];
//    _authenticating = true;
//    OSAtomicOr32Barrier(1, &_running);
}


-(void)generalSetting{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:KEY_DEFAULTS_INITIALIZED];
    NSLog(@"KEY_REQUIRE_BOTH=%@",[defaults objectForKey:KEY_REQUIRE_BOTH]);
    // [defaults setBool:NO forKey:KEY_REQUIRE_BOTH];
    [defaults setBool:NO forKey:KEY_FACE_MOTION_ANALYSIS];
    [defaults setBool:NO forKey:KEY_USE_ADAPTIVE_ENROLLMENT];
    [defaults setBool:NO forKey:KEY_REQUIRE_CONFIRMATION];
    [defaults setInteger:10000 forKey:KEY_AUTHENTICATION_TIMEOUT];
    [defaults setInteger:2 forKey:KEY_SECURITY_LEVEL];
}

-(void)DoEnrollment{
    // _viewToDisplay.hidden=YES;
    
//    if( !_finishedInitializing )
//    {
//        NSLog(@"!not initialized");
//        return;
//    }
//    dispatch_async(dispatch_get_main_queue(), ^{
//        UIAlertView *alert = [[VVDemoAlertView alloc] initWithTitle:@"Enrollment"
//                                                            message:@"Say \"Hello, Blue Genie\" 3 times."
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil
//                                                      cancelHandler:^{
//                                                          [self startEnrollment];
//                                                      }
//                                                confirmationHandler:^(NSInteger idx){ }
//                              ];
//        [alert show];
//    });
    
    if( !_finishedInitializing ) return;
    dispatch_async(dispatch_get_main_queue(), ^{
        VVDemoAlertView *alert = [[VVDemoAlertView alloc] initWithTitle:@"Enrollment"
                                                                message:@"Say \"Hello, Blue Genie\" 3 times."
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil
                                                          cancelHandler:^{
                                                              [self startEnrollment];
                                                          }
                                                    confirmationHandler:^(NSInteger idx){ }
                                  ];
        [alert show];
    });
}

-(void)DoAuthentication{
    //_viewToDisplay.hidden=YES;
    
    if( !_finishedInitializing ) return;
    [self.statusLabel setText:@"Authenticating"];
    _authenticator = [[MultiAuthenticator alloc] init];
    [self setupRecognizer:_authenticator];
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
    
    NSLog(@"CombinationMethod=%i",[defaults boolForKey:KEY_REQUIRE_BOTH] ? COMBINE_SMART : COMBINE_ANY);
    
    [_authenticator setCombinationMethod:([defaults boolForKey:KEY_REQUIRE_BOTH] ? COMBINE_SMART : COMBINE_ANY)];
    [_authenticator setSecurityLevel:(int)[defaults integerForKey:KEY_SECURITY_LEVEL]];
    [[_authenticator getFaceRecognizerState] setUseLiveness:[defaults boolForKey:KEY_FACE_MOTION_ANALYSIS]];
    
    NSLog(@"require all %d security level %ld use liveness %d", [defaults boolForKey:KEY_REQUIRE_BOTH],
          (long)[defaults integerForKey:KEY_SECURITY_LEVEL], [defaults boolForKey:KEY_FACE_MOTION_ANALYSIS]);
    [_authenticator start];
    _authenticating = true;
    OSAtomicOr32Barrier(1, &_running);
}







/**************************Handle alert delegate****************************/

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    if(alertView==cameraAlert){
        
        [self.navigationController popViewControllerAnimated:YES];
    }

    if(alertView==alertTimeOut){
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    if(alertView==alertEnrollment){
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:@"Enrolled" forKey:@"IS_ENROLLED"];
        [defaults setObject:@"SECURITY_TYPE_FACEVOICE" forKey:@"SECURITY_TYPE"];
        [defaults synchronize];
        shareData.isFaceRecognised=YES;
        shareData.isTiltEnable=NO;
        shareData.isTouchIDEnable=NO;
        [shareData newsaveAppStatus];
        [shareData newloadAppStatus];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
    
    if(alertView==alertAuthenticate){
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        if([defaults objectForKey:@"TURNOFFFACE"] != nil){
            
            if([[defaults objectForKey:@"TURNOFFFACE"] isEqualToString:@"YES"]){
                shareData.isFaceRecognised = NO;
                
                [defaults removeObjectForKey:@"TURNOFFFACE"];
                [defaults removeObjectForKey:@"SECURITY_TYPE"];
                [defaults synchronize];
                [shareData newsaveAppStatus];
                [shareData newloadAppStatus];
            }else{
                [defaults setObject:@"SECURITY_TYPE_FACEVOICE" forKey:@"SECURITY_TYPE"];
                [defaults synchronize];
                shareData.isFaceRecognised = YES;
                [shareData newsaveAppStatus];
                [shareData newloadAppStatus];
            }
            [self.navigationController popViewControllerAnimated:YES];
            if(_callbackMe && [_callbackMe respondsToSelector:@selector(authenticateSuccess:)]){
                [_callbackMe performSelector:@selector(authenticateSuccess:) withObject:nil afterDelay:0.1];
            }
        }
        else{
            [defaults removeObjectForKey:@"TURNOFFFACE"];
            [defaults setObject:@"SECURITY_TYPE_FACEVOICE" forKey:@"SECURITY_TYPE"];
            [defaults synchronize];
            shareData.isFaceRecognised = YES;
            [shareData newsaveAppStatus];
            [shareData newloadAppStatus];
        }
        
        if([defaults objectForKey:@"GENERATE_OTP"]!=nil){
            
            if([[defaults objectForKey:@"GENERATE_OTP"] isEqualToString:@"1"])
                [self.navigationController popViewControllerAnimated:NO];
            [defaults removeObjectForKey:@"GENERATE_OTP"];
            //[defaults setObject:@"0" forKey:@"GENERATE_OTP"];
            if(_callbackMe && [_callbackMe respondsToSelector:@selector(generateOTP:)]){
                [_callbackMe performSelector:@selector(generateOTP:) withObject:nil afterDelay:0.1];
            }
        }
        
        
        if([defaults objectForKey:@"APPROVE_OR_DENY"]!=nil){
            
            if([[defaults objectForKey:@"APPROVE_OR_DENY"] isEqualToString:@"1"]) {
                [defaults removeObjectForKey:@"APPROVE_OR_DENY"];
                [defaults synchronize];
                
                [self.navigationController popViewControllerAnimated:NO];
                if(_callbackMe && [_callbackMe respondsToSelector:@selector(approved:)]){
                    [_callbackMe performSelector:@selector(approved:) withObject:nil afterDelay:0.1];
                }
            }
            if([[defaults objectForKey:@"APPROVE_OR_DENY"] isEqualToString:@"0"]) {
                [defaults removeObjectForKey:@"APPROVE_OR_DENY"];
                [defaults synchronize];
                [self.navigationController popViewControllerAnimated:NO];
                if(_callbackMe && [_callbackMe respondsToSelector:@selector(denied:)]){
                    [_callbackMe performSelector:@selector(denied:) withObject:nil afterDelay:0.1];
                }
            }
            
            if([[defaults objectForKey:@"APPROVE_OR_DENY"] isEqualToString:@"2"]) {
                [defaults removeObjectForKey:@"APPROVE_OR_DENY"];
                [defaults synchronize];
                // [self.navigationController popViewControllerAnimated:YES];
                if(_callbackMe && [_callbackMe respondsToSelector:@selector(generateOTP1:)]){
                    [_callbackMe performSelector:@selector(generateOTP1:) withObject:nil afterDelay:0.1];
                }
            }
        }
    }
}



@end
