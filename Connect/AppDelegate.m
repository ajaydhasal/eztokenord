//
//  AppDelegate.m
//  Connect
//
//  Created by Harsha on 8/27/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "SharedData.h"
#import "AuthorizeViewController.h"
#import "RegisterViewController.h"

@implementation AppDelegate
@synthesize dictSequence;
#define PARSEID @"fnGz8KzPEkjaS3jhayHCZ1fNCp4RWgttL3S77JK7"
#define PARSECLIENT @"RKXpXqfE1QPAUHKgt6dYWJsyugquSwOxcOz4HaOL"
double clatitude;
double clongitude;
double distance;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];

    // Override point for customization after application launch.
    [Parse setApplicationId:PARSEID clientKey:PARSECLIENT];
    [application setStatusBarStyle:UIStatusBarStyleLightContent];
 
    
#ifdef __IPHONE_8_0
    //Right, that is the point
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:
                                             UIUserNotificationTypeSound | UIUserNotificationTypeAlert categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
#else
    //register to receive notifications
    [UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
#endif
    
    NSDictionary *userInfo = [launchOptions valueForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"];
    NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(CurrentLocationIdentifier) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    
    [self CurrentLocationIdentifier];
    
    if(apsInfo) {
        
        NSString *str = [NSString stringWithFormat:@"### didFinishLaunchingWithOptions ###"];
        [SharedData writeToLogFile:str];
        //there is some pending push notification, so do something
        //in your case, show the desired viewController in this if block
        //NSLog(@"this is in didFinishLaunchingWithOptions once push receive");
        //need to handle the push..
        //when receive it need to lauch the transaction n show the details to accept or reject..
        //if require tilt then will force user to set it.
        
        //need to open authorize view.
        //remove this line of code to remove the alertview //30/12/2014
        [PFPush handlePush:userInfo];
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userInfo
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:&error];
        
        [self jsonParser:jsonData];
        
        SharedData *sharedData = [SharedData sharedData];
        [sharedData newloadAppStatus];
        
        
        NSLog(@"*****************DISTANCE**** %f",distance);
        NSLog(@"****sharedData.isAutomationEnable -> %d", sharedData.isAutomationEnable);
        NSLog(@"****self.isWithinRange -> %d", self.isWithinRange);

        if (sharedData.isAutomationEnable && self.isWithinRange) {
            [self.locationManager stopUpdatingLocation];
            //this is automated. remove the push received.
            NSString *strPin = [NSString stringWithFormat:@"### transaction automated ###"];
            [SharedData writeToLogFile:strPin];
            //NSLog(@"*****************APPROVE TRANSACTION*******************");
            NSString *txstat = @"1";
            sharedData.signatureParamString = self.combinedString;
            [sharedData transactionApproval:[sharedData generateSIGOTP] txref:[userInfo valueForKey:@"ref"] txstat:txstat];
            sharedData.dUse = nil;
            //this works by dismiss the notification center
            if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  )
            {
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
                [UIApplication sharedApplication].applicationIconBadgeNumber=0;
                
            }
            
        }
        else{
            //this is lauch if the distance is not within25m or automated is not selected.
            NSLog(@"*****************NOT AUTOMATED didFinishLaunchingWithOptions *********");
            NSString *strPin = [NSString stringWithFormat:@"!!! transaction not automated ###"];
            [SharedData writeToLogFile:strPin];
            [self.locationManager stopUpdatingLocation];
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            AuthorizeViewController *authorizeVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"authorizeView"];
            authorizeVC.dict = self.dict;
            authorizeVC.combStr = self.combinedString;
            authorizeVC.arrmsg = self.arrMsg;
            authorizeVC.txref = [userInfo valueForKey:@"ref"];
            authorizeVC.distance = distance;
            authorizeVC.clongitude = clongitude;
            authorizeVC.clatitude = clatitude;
            NSLog(@"authorizeVC.dict =%@ ",authorizeVC.dict);
            
            //  [(UINavigationController*)self.window.rootViewController pushViewController:authorizeVC animated:NO];
            if(authorizeVC.dict){
                
                [(UINavigationController*)self.window.rootViewController pushViewController:authorizeVC animated:NO];
            }else{
                UIStoryboard *stroyboard=  [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                UIViewController *vc = [stroyboard instantiateViewControllerWithIdentifier:@"register"];//[[RegisterViewController alloc] init];
                
                //UINavigationController *navController = (UINavigationController  *)self.window.rootViewController;
                
                
                UINavigationController *navController = (UINavigationController  *)self.window.rootViewController;
                
                
                [navController.visibleViewController.navigationController pushViewController:vc animated:YES];
                
                
                self.window.rootViewController = navController;
            }

        }
        //}
        NSString *strPin = [NSString stringWithFormat:@"### end didFinishLaunchingWithOptions ###"];
        [SharedData writeToLogFile:strPin];
    }
    return YES;
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    //NSLog(@"**************didRegisterUserNotificationSettings************");
    [application registerForRemoteNotifications];
    
}

#endif

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
    
    SharedData *sharedData = [SharedData sharedData];
    if(sharedData.devicePID.length == 0){
        NSString *idStr =[NSString stringWithFormat:@"id-%@",[sharedData generateSerialNumber]];
        //NSLog(@"PID: %@",idStr);
        currentInstallation.channels =@[idStr];
        sharedData.devicePID = idStr;
        [currentInstallation saveInBackground];
    }
}

BOOL bLoginAPICall = TRUE;

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
//    NSString *strPin = [NSString stringWithFormat:@"&&& enter UIBackgroundFetchResult ###"];
//    [SharedData writeToLogFile:strPin];
    bLoginAPICall = FALSE;
    UIApplicationState state = [application applicationState];
    // user tapped notification while app was in background
    if (state == UIApplicationStateInactive || state == UIApplicationStateBackground) {
        // go to screen relevant to Notification content
//        NSString *strPin = [NSString stringWithFormat:@"&&& enter UIApplicationStateInactive ||  UIApplicationStateBackground ###"];
//        [SharedData writeToLogFile:strPin];
    }
        else {
            // App is in UIApplicationStateActive (running in foreground)
            // perhaps show an UIAlertView
//            NSString *strPin = [NSString stringWithFormat:@"&&& enter UIApplicationStateActive  ###"];
//            [SharedData writeToLogFile:strPin];
        }
        
    NSLog(@"~~~~~~~~~~~~~~~~performFetchWithCompletionHandler~~~~~~~~~~~~~~~~~~ completionHandler : %@", completionHandler);
    
    completionHandler(UIBackgroundFetchResultNewData);
    
    
    ////handle push in the background///without user tapping on the notification///
    //need to handle the push..
    //when receive it need to lauch the transaction n show the details to accept or reject..
    //if require tilt then will force user to set it.
    
    
    //remove this line of code to remove the alertview //30/12/2014
    //[PFPush handlePush:userInfo];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userInfo
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    [self jsonParser:jsonData];
    
    if(bLoginAPICall){
        // This will be handled automatically
        return;
    }
    
    SharedData *sharedData = [SharedData sharedData];
    [sharedData newloadAppStatus];
    
    NSLog(@"*****************DISTANCE**** %f",distance);
    NSLog(@"****sharedData.isAutomationEnable -> %d", sharedData.isAutomationEnable);
    NSLog(@"****self.isWithinRange -> %d", self.isWithinRange);
    
    if (sharedData.isAutomationEnable && self.isWithinRange) {
        [self.locationManager stopUpdatingLocation];
        //NSLog(@"signature: %@ - profile: %@", self.combinedString, sharedData.profileName);
        sharedData.signatureParamString = self.combinedString;
        NSString *strPin = [NSString stringWithFormat:@"### transaction automated ###"];
        [SharedData writeToLogFile:strPin];
        //NSLog(@"*****************AUTOMATED TRANSACTION*******************");
        NSString *txstat = @"1";
        [sharedData transactionApproval:[sharedData generateSIGOTP] txref:[userInfo valueForKey:@"ref"] txstat:txstat];
        sharedData.dUse = nil;
        //this works by dismiss the notification center
        if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  )
        {
            //NSLog(@"this is in  once push receive application.applicationState %ld", application.applicationState);
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
            [UIApplication sharedApplication].applicationIconBadgeNumber=0;
            
        }
    }
    else{
        //NSLog(@"*****************MANUAL  TRANSACTION*******************");
        NSString *strPin = [NSString stringWithFormat:@"!!! transaction not automated ###"];
        [SharedData writeToLogFile:strPin];
        [self.locationManager stopUpdatingLocation];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AuthorizeViewController *authorizeVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"authorizeView"];
        
        
        
        authorizeVC.dict = self.dict;
        authorizeVC.combStr = self.combinedString;
        authorizeVC.arrmsg = self.arrMsg;
        authorizeVC.txref = [userInfo valueForKey:@"ref"];
        authorizeVC.distance = distance;
        authorizeVC.clongitude = clongitude;
        authorizeVC.clatitude = clatitude;
        NSLog(@"authorizeVC.dict =%@ ",authorizeVC.dict);
        
        //  [(UINavigationController*)self.window.rootViewController pushViewController:authorizeVC animated:NO];
        if(authorizeVC.dict){
            
            [(UINavigationController*)self.window.rootViewController pushViewController:authorizeVC animated:NO];
        }else{
            UIStoryboard *stroyboard=  [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            UIViewController *vc = [stroyboard instantiateViewControllerWithIdentifier:@"register"];//[[RegisterViewController alloc] init];
            
            //UINavigationController *navController = (UINavigationController  *)self.window.rootViewController;
            
            
            UINavigationController *navController = (UINavigationController  *)self.window.rootViewController;
            
            
            [navController.visibleViewController.navigationController pushViewController:vc animated:YES];
            
            
            self.window.rootViewController = navController;
        }

    }
    
//    NSString *Pin = [NSString stringWithFormat:@"&&& end UIBackgroundFetchResult ###"];
//    [SharedData writeToLogFile:Pin];

}

-(void)jsonParser:(NSData *)data
{
    NSError *e = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
    //NSLog(@"Details: %@",jsonArray);
    if (!jsonArray) {
        NSLog(@"Error parsing JSON: %@", e);
        return;
    }
    else{
        if (([jsonArray valueForKey:@"aps"] != nil)) {

        }
        if ([jsonArray valueForKey:@"msg"] != nil) {
            //NSLog(@"Value: %@", [jsonArray valueForKey:@"msg"]);
            NSArray *array = [jsonArray valueForKey:@"msg"];
            self.arrMsg = jsonArray;
            self.dict = [[NSMutableDictionary alloc] init];
            //NSLog(@"array!!: %@",self.arrMsg);
            [self.dict setValue:[array valueForKey:@"a"] forKey:@"Application Name"];
            [self.dict setValue:[array valueForKey:@"b"] forKey:@"Time"];
            [self.dict setValue:[array valueForKey:@"c"] forKey:@"Location"];
            [self.dict setValue:[array valueForKey:@"d"] forKey:@"Geo Coordinates"];
            [self.dict setValue:[array valueForKey:@"e"] forKey:@"Device Name"];
            [self.dict setValue:[array valueForKey:@"f"] forKey:@"Client IP"];
            [self.dict setValue:[array valueForKey:@"g"] forKey:@"Server IP"];
            [self.dict setValue:[array valueForKey:@"h"] forKey:@"Transaction Message"];
            [self.dict setValue:[array valueForKey:@"i"] forKey:@"  "];
            
            for (id key in array){
                //NSLog(@"~~~~~~~~~array key: %@", key);
                if (![key isEqualToString:@"a"] && ![key isEqualToString:@"b"] && ![key isEqualToString:@"c"] && ![key isEqualToString:@"d"] && ![key isEqualToString:@"e"] && ![key isEqualToString:@"f"] && ![key isEqualToString:@"g"] && ![key isEqualToString:@"h"] ) {
                    
                    [self.dict setValue:[array valueForKey:key] forKey:key];
                }
            }
            
            NSMutableDictionary *msgDict = [[NSMutableDictionary alloc] init];
            for (id key in array){
                [msgDict setValue:[array valueForKey:key] forKey:key];
            }
            
            NSArray *msgkeys = [msgDict allKeys];
            NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
            NSArray *sorters = [[NSArray alloc] initWithObjects:sorter, nil];
            NSArray *sortedArray = [msgkeys sortedArrayUsingDescriptors:sorters];
            NSMutableArray *sortArray = [[NSMutableArray alloc] init];
            [sortArray addObjectsFromArray:sortedArray];
            NSString *str = nil;
            self.combinedString = [[NSMutableString alloc]init];
            for(int i=0; i<[sortArray count]; i++)
            {
                NSString *test = [msgDict valueForKey:[sortArray objectAtIndex:i]];
                str = [NSString stringWithFormat:@"%@",test];
                [self.combinedString appendFormat:@"%@", str];
            }
        }
        
        
        // New code for handling the Login via Push Notification
        if([jsonArray valueForKey:@"message"] != nil){
            // New message format
            NSString *strEnc = [jsonArray valueForKey:@"message"];
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:strEnc options:0];
            NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
            
            NSArray *jsonArray1 = [NSJSONSerialization JSONObjectWithData:decodedData options: NSJSONReadingMutableContainers error: &e];
            
            SharedData *sharedData = [SharedData sharedData];
            NSString *profile = [NSString stringWithFormat:@"%@%@",[sharedData getgnm:[[jsonArray1 valueForKey:@"gid"] stringValue]],[[jsonArray1 valueForKey:@"uid"] lowercaseString]];
            sharedData.profileName = profile;
            [sharedData newloadAppStatus];
            NSString *strOTP = [sharedData generateOTP];
            if(strOTP == nil || strOTP.length == 0){
                // Fake push we recieved
                UIAlertView *v = [[UIAlertView alloc] initWithTitle:@"Error processing transaction" message:@"There was an error while trying to process the transaction. Please contact your service provider for assistance." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [v show];
                RegisterViewController *reg = [[UIStoryboard storyboardWithName:@"Main" bundle:nil ] instantiateViewControllerWithIdentifier:@"register"];//[[RegisterViewController alloc] init];
                
                
                [(UINavigationController*)self.window.rootViewController pushViewController:reg animated:NO];
                return;
            }
            NSString *jsonResp = [sharedData onlineLogin:[[jsonArray1 valueForKey:@"uid"] lowercaseString] otp:strOTP ref:[jsonArray1 valueForKey:@"ref"]];
            bLoginAPICall = TRUE;
            return;
        }
        
        
        SharedData *sharedData = [SharedData sharedData];
        NSString *profile = [NSString stringWithFormat:@"%@%@",[sharedData getgnm:[[jsonArray valueForKey:@"gid"] stringValue]],[[jsonArray valueForKey:@"uid"] lowercaseString]];
        sharedData.profileName = profile;
        [sharedData newloadAppStatus];
        
        //NSLog(@"************sharedData.automation %d",sharedData.isAutomationEnable);
        //NSLog(@"*************Details of distance: clat:%f clon:%f sLon:%f sLat:%f ************",clatitude, clongitude, sharedData.longitude, sharedData.latitude);
        
        
        NSLog(@"LOCATION SAVED %@",sharedData.arrayOfLocations);
        NSLog(@"sharedData.isAutomationEnable %d",sharedData.isAutomationEnable);
        if (sharedData.isAutomationEnable == YES) {
            NSLog(@"THis IS Automated from jsonParser");
            NSMutableArray *locArr = [sharedData loadLocation:sharedData.profileName];
            CLLocation* current = [[CLLocation alloc] initWithLatitude:clatitude longitude:clongitude];
            
            for(int i=0;i<[locArr count];i++){
            NSDictionary* dicts = [locArr objectAtIndex:i];
            double lat = [[dicts valueForKey:@"latitude"]  doubleValue];
            double lon = [[dicts valueForKey:@"longitude"] doubleValue];
            
            CLLocation* stored = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
            
            //CLLocation* stored = [[CLLocation alloc] initWithLatitude:sharedData.latitude longitude:sharedData.longitude];
            distance = [stored distanceFromLocation:current];
            NSLog(@"distance measured %f", distance);
                if(distance <= 200)
                {
                    self.isWithinRange = YES;
                    return;
                }
               
            }
            //NSString *strPin = [NSString stringWithFormat:@"!!! transaction automated with location \n cLocation = %@ \nStoredLocation = %@ \nDistance=%f ###",current, stored, distance];
            //[SharedData writeToLogFile:strPin];
            
//            distance=[self measureDistance:clatitude long1:clongitude latit2:sharedData.latitude long2:sharedData.longitude];
        }
        

    }
}



/*- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    //get current location
    [self CurrentLocationIdentifier];
    NSLog(@"*****************didReceiveRemoteNotification*******************");
    NSString *strPin = [NSString stringWithFormat:@"### enter didReceiveRemoteNotification ###"];
    [SharedData writeToLogFile:strPin];
    [PFPush handlePush:userInfo];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userInfo
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    [self jsonParser:jsonData];
    SharedData *sharedData = [SharedData sharedData];
    [sharedData newloadAppStatus];
    if (sharedData.isAutomationEnable && distance <25.0) {
        [self.locationManager stopUpdatingLocation];
        //NSLog(@"signature: %@ - profile: %@", self.combinedString, sharedData.profileName);
        sharedData.signatureParamString = self.combinedString;
        NSString *strPin = [NSString stringWithFormat:@"### transaction automated ###"];
        [SharedData writeToLogFile:strPin];
        //NSLog(@"*****************AUTOMATED TRANSACTION*******************");
        NSString *txstat = @"1";
        [sharedData transactionApproval:[sharedData generateSIGOTP] txref:[userInfo valueForKey:@"ref"] txstat:txstat];
        sharedData.dUse = nil;
        //this works by dismiss the notification center
        if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  )
        {
            //NSLog(@"this is in  once push receive application.applicationState %ld", application.applicationState);
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
            [UIApplication sharedApplication].applicationIconBadgeNumber=0;
            
        }
    }
    else{
        //NSLog(@"*****************MANUAL  TRANSACTION*******************");
        NSString *strPin = [NSString stringWithFormat:@"### transaction not automated ###"];
        [SharedData writeToLogFile:strPin];
        [self.locationManager stopUpdatingLocation];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AuthorizeViewController *authorizeVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"authorizeView"];
        authorizeVC.dict = self.dict;
        authorizeVC.combStr = self.combinedString;
        authorizeVC.arrmsg = self.arrMsg;
        authorizeVC.txref = [userInfo valueForKey:@"ref"];
        authorizeVC.distance = distance;
        authorizeVC.clongitude = clongitude;
        authorizeVC.clatitude = clatitude;
        [(UINavigationController*)self.window.rootViewController pushViewController:authorizeVC animated:NO];
    }
    
    NSString *str = [NSString stringWithFormat:@"### end didReceiveRemoteNotification ###"];
    [SharedData writeToLogFile:str];
}
*/

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if (error.code == 3010) {
        NSLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        NSLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
        
        UIAlertView *alertView =  [[UIAlertView alloc] initWithTitle:@"Push Notification Error"
                                                     message:@"This Device is not registered for Push Notificatoin. Please close the app and re-open to work properly"
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

    //this works by dismiss the notification center
    if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  )
    {
        //NSLog(@"this is in didFinishLaunchingWithOptions once push receive application.applicationState %d", application.applicationState);
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
        [UIApplication sharedApplication].applicationIconBadgeNumber=0;
        
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//check distance
//the minimum distance we are considering is 25meters.
- (double) measureDistance:(double)lat1 long1:(double)lon1 latit2:(double)lat2 long2:(double)lon2
{  // generally used geo measurement function
    double R = 6378.137; // Radius of earth in KM
    
    //convert to radians by multiplying by PI/180
    double dlat1 = lat1 * M_PI / 180;
    double dlat2 = lat2 * M_PI / 180;
    double dlon1 = lon1 * M_PI / 180;
    double dlon2 = lon2 * M_PI / 180;
    
    double dLat = (dlat2 - dlat1) * M_PI / 180;
    double dLon = (dlon2 - dlon1) * M_PI / 180;
    double a = sin(dLat/2) * sin(dLat/2) +
    //cos(lat1 * M_PI / 180) * cos(lat2 * Math.M_PI / 180) * sin(dLon/2) * sin(dLon/2);
    cos(lat1 * M_PI / 180) * cos(lat2 * M_PI / 180) * sin(dLon/2) * sin(dLon/2);
    double c = 2 * atan2(sqrt(a), sqrt(1-a));
    double d = R * c;
    return d * 1000; // meters
}

//------------ Current Location Address-----
-(void)CurrentLocationIdentifier
{
    //---- For getting current gps location
    self.locationManager = [[CLLocationManager alloc]init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager performSelector:@selector(requestAlwaysAuthorization)];
    }
    [self.locationManager startUpdatingLocation];
    clatitude = self.locationManager.location.coordinate.latitude;
    clongitude = self.locationManager.location.coordinate.longitude;

    
    SharedData *sharedData = [SharedData sharedData];
    sharedData.clLocation = self.locationManager;
    //NSLog(@"*************CurrentLocationIdentifier lat: %f long:%f **************",clatitude,clongitude);
    //    NSLog(@"CurrentLocation %f", self.locationManager.location.coordinate.latitude);
    //    NSLog(@"CurrentLocation %f", self.locationManager.location.coordinate.longitude);
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    SharedData *sharedData = [SharedData sharedData];
    sharedData.isLocationSet = NO;
    [sharedData newsaveAppStatus];
    NSLog(@"Appdelegate LocationError: %@",error.description);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [locations lastObject];
    self.currentLocation = [locations objectAtIndex:0];
    [self.locationManager stopUpdatingLocation];
 
    clatitude = self.currentLocation.coordinate.latitude;
    clongitude = self.currentLocation.coordinate.longitude;
    
//    SharedData *sharedData = [SharedData sharedData];
//    sharedData.arrayOfLocations = [[NSMutableArray alloc]init];
//    sharedData.arrayOfLocations = locations.lastObject;
    
    
    
    
    //NSLog(@"*************lOCATION LASTOBJECT:%@ **************",[locations lastObject]);
    // NSLog(@"*************lOCATION ARRAY:%@ **************",sharedData.arrayOfLocations);
}
//
//-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
//    
//    UINavigationController *navigationController = (UINavigationController*)self.window.rootViewController;
//    
//    id topViewController = navigationController.topViewController;
//    if ([topViewController isKindOfClass:[ViewController class]]) {
//        [(ViewController*)topViewController insertNewObjectForFetchWithCompletionHandler:completionHandler];
//    } else {
//        NSLog(@"Not the right class %@.", [topViewController class]);
//        completionHandler(UIBackgroundFetchResultFailed);
//    }
//}

@end
