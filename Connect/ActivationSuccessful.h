//
//  ActivationSuccessful.h
//  EzToken
//
//  Created by Apple on 15/10/15.
//  Copyright © 2015 Ezmcom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharedData.h"

@interface ActivationSuccessful : UIViewController
{
    SharedData *sharedData;
}
@property (retain, nonatomic) IBOutlet UIButton *btnSkip;

- (IBAction)btnOkTapped:(id)sender;
- (IBAction)bntSkipTapped:(id)sender;

@end
