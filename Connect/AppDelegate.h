//
//  AppDelegate.h
//  Connect
//
//  Created by Harsha on 8/27/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableString *combinedString;
@property (strong, nonatomic) NSMutableDictionary *dict;
@property (strong, nonatomic) NSArray *arrMsg;
@property (strong, nonatomic) UIAlertView *pushalertView;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (strong, nonatomic) NSMutableDictionary *dictSequence;
@property (nonatomic) BOOL isWithinRange;
@end
