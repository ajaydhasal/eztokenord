//
//  ActivationSuccessful.m
//  EzToken
//
//  Created by Apple on 15/10/15.
//  Copyright © 2015 Ezmcom. All rights reserved.
//

#import "ActivationSuccessful.h"

@interface ActivationSuccessful ()

@end

@implementation ActivationSuccessful

- (void)viewDidLoad {
    [super viewDidLoad];
    sharedData=[SharedData sharedData];
    UIColor * color = [UIColor colorWithRed:92.0 /255.0f green:171.0/255.0f blue:222.00/255.0f alpha:1.0f];
    self.navigationController.navigationBar.barTintColor = color;
    
    // self.navigationController.navigationItem.hidesBackButton=YES;
    self.btnSkip.selected = YES;
    [self.btnSkip setBackgroundImage:[UIImage imageNamed:@"checkek.png"] forState:UIControlStateNormal];

    
    [self.navigationItem setHidesBackButton:YES animated:YES]; // hide back button
    
    [self.navigationItem setBackBarButtonItem:nil]; // set as nil
    
    [self.navigationItem setLeftBarButtonItem:nil animated:NO];
    [self.navigationItem setTitle:@"Activation Successful"];
    
    // left bar item as nil

    
//    UIBarButtonItem *backbutton =  [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];
//    [backbutton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                        [UIColor whiteColor],UITextAttributeTextColor,[UIFont systemFontOfSize:17.0f],UITextAttributeFont,
//                                        nil] forState:UIControlStateNormal];
//    self.navigationItem.leftBarButtonItem=backbutton;
//    [backbutton setTarget:self];
//    [backbutton setAction:@selector(ClickCancel:)];
//    
    // Do any additional setup after loading the view.
}





- (void)ClickCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [_btnSkip release];
    [super dealloc];
}
- (IBAction)btnOkTapped:(id)sender {
    if(sharedData.dTiltValue!=nil){
             [self performSegueWithIdentifier:@"firstView" sender:self];
    }
    else{
         NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    if (self.btnSkip.isSelected == NO) {
       
        [defaults setObject:@"1" forKey:@"ACTIVATION_SUCCESS"];
        [defaults synchronize];
        [self performSegueWithIdentifier:@"settingvc" sender:self];
    }
    else
    {
         [defaults removeObjectForKey:@"ACTIVATION_SUCCESS"];
         [self performSegueWithIdentifier:@"firstView" sender:self];
     }
    }
}

- (IBAction)bntSkipTapped:(id)sender {
    if (self.btnSkip.selected == NO) {
        self.btnSkip.selected = YES;
        [self.btnSkip setBackgroundImage:[UIImage imageNamed:@"checkek.png"] forState:UIControlStateNormal];
    }
    else
    {
        self.btnSkip.selected = NO;
        [self.btnSkip setBackgroundImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
    }

}
@end
