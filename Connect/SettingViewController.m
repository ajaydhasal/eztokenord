//
//  SettingViewController.m
//  EZToken
//
//  Created by Harsha on 2/11/15.
//  Copyright (c) 2015 Ezmcom. All rights reserved.
//

#import "SettingViewController.h"
#import "Constant.h"
#import "SharedData.h"
@interface SettingViewController ()

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIColor * color = [UIColor colorWithRed:92.0 /255.0f green:171.0/255.0f blue:222.00/255.0f alpha:1.0f];
    self.navigationController.navigationBar.barTintColor = color;

    [self.navigationController.navigationBar setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor],UITextAttributeFont: [UIFont systemFontOfSize:17.0f]}];
    
//    self.txtGatewayUrl.font =[UIFont fontWithName:@"Coda-Regular" size:17.0f];
//    self.lblGateway.font = [UIFont fontWithName:@"Coda-Regular" size:17.0f];

    
    SharedData *sharedData = [SharedData sharedData];
    NSLog(@"URL: %@", sharedData.gatewayURL);
    if ([sharedData.gatewayURL length] == 0) {
        NSString *actualURL = [NSString stringWithFormat:@"%@",MAIN_GATEWAY_URL_DEFAULT];
        self.txtGatewayUrl.text = actualURL;
    }
    else
    {
        self.txtGatewayUrl.text = sharedData.gatewayURL;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.hidesBackButton = YES;
    self.txtGatewayUrl.delegate = self;
    self.txtGatewayUrl.adjustsFontSizeToFitWidth = YES;
    self.title = @"Setting";

}

- (IBAction)ClickSave:(id)sender {
    NSLog(@"Before Save Actual String: %@", self.txtGatewayUrl.text);

    NSString *actualURL = [NSString stringWithFormat:@"%@",self.txtGatewayUrl.text];
    NSString *gatewayURLString = [actualURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (gatewayURLString == nil || [gatewayURLString length] == 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Gateway Entry Error"
                                                            message:@"Invalid Gateway URL. Please check application settings."
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Ok", nil];
        [alertView show];
    }
    else
    {
        NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))"
                                                                                    options:NSRegularExpressionCaseInsensitive
                                                                                      error:NULL];
        self.correctURLString = [gatewayURLString substringWithRange:[expression rangeOfFirstMatchInString:gatewayURLString
                                                                                                   options:NSMatchingCompleted
                                                                                                     range:NSMakeRange(0, [gatewayURLString length])]];
        if ([self.correctURLString hasSuffix:@"/"]) {
            self.correctURLString = [self.correctURLString substringToIndex:[self.correctURLString length] - 1];
        }
    }

    actualURL = self.correctURLString;
//    if ([self.correctURLString length] < 10)
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Invalid Gateway URL entry"
//                                                            message:@"Please enter a valid gateway URL."
//                                                           delegate:self
//                                                  cancelButtonTitle:nil
//                                                  otherButtonTitles:@"Ok", nil];
//        [alertView show];
//    }
//    else
//    {
        SharedData *appInfoManager = [SharedData sharedData];
        NSLog(@"gatewayURLString: %@",gatewayURLString);
        NSLog(@"appInfoManager.gatewayURL: %@",appInfoManager.gatewayURL);
        NSLog(@"self.correctURLString: %@",self.correctURLString);
        
        if ([self.correctURLString isEqualToString:appInfoManager.gatewayURL])
        {
            // Nothing changed
            self.saveAlert =  [[UIAlertView alloc] initWithTitle:@"Gateway URL"
                                                             message:@"Gateway URL not changed."
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                                   otherButtonTitles:nil];
            [self.saveAlert show];
            [self performSelector:@selector(dismissAlert:) withObject:self.saveAlert afterDelay:2.0f];
            
        }
        else
        {
            self.resetAlert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Are you sure you want to save the new Gateway URL?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
            
            [self.resetAlert show];
            
        }
    //}
    //}
    
}

#pragma mark UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    SharedData *sharedData = [SharedData sharedData];
    if (alertView == self.resetAlert) {
        if (buttonIndex == 1) {
            // Event the little bit change should be saved
            sharedData.gatewayURL = self.correctURLString;
            [sharedData newsaveAppStatus];
            [self.navigationController popViewControllerAnimated:YES];
            //[baseDelegate goScreen:SCREEN_ID_ACTIVATION_1 animation:YES];
        }
        else if(buttonIndex == 0)
        {
            //leave it
            self.txtGatewayUrl.text = sharedData.gatewayURL;
        }
        
    }
    
}

-(void)dismissAlert:(UIAlertView *) alertView
{
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
    [self.saveAlert release];
    [self ClickBack:self];
}

- (IBAction)ClickBack:(id)sender {
    //[self dismissModalViewControllerAnimated:YES];
    SharedData *sharedData = [SharedData sharedData];
    self.txtGatewayUrl.text = sharedData.gatewayURL;
    [self.navigationController popViewControllerAnimated:YES];
    //[baseDelegate goScreen:SCREEN_ID_ACTIVATION_1 animation:YES];
}
- (void)dealloc {
    [_txtGatewayUrl release];
    [self.correctURLString release];
    [_lblGateway release];
    [_txtDescription release];
    [_btnCancel release];
    [_btnSave release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTxtGatewayUrl:nil];
    [self setCorrectURLString:nil];
    [super viewDidUnload];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
