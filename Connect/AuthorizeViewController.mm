//
//  AuthorizeViewController.m
//  EZToken
//
//  Created by Harsha on 9/2/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import "AuthorizeViewController.h"
#import "Constant.h"
#import <CoreMotion/CoreMotion.h>
#import "CaptureViewController.h"
#include <math.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import "AppDelegate.h"
#import "StoryBoardID.h"
#import "VVDemoRecognizerController.h"

@interface AuthorizeViewController ()
{
    CMMotionManager *motionManager;
    NSTimer *timer;
    float rotation;
    UIAlertView *changealertView;
    float currentV;
    float newV;
    BOOL animationStarted;
    
    UIAlertView *setalertView;
    UIAlertView *locationalert;
    
    UIAlertView *transerroralert;
    UIAlertView *incorectSAalertView;
    UIAlertView *noSecurityAlertView;
}
@property (nonatomic, strong) NSMutableArray *groupNames;

@end

#define degrees(x) (180 * x / M_PI)

@implementation AuthorizeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //NSLog(@"dict %@", self.dict);
    
    //self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0/255 green:137.0/255 blue:123.0/255 alpha:1];
    UIColor * color = [UIColor colorWithRed:92.0 /255.0f green:171.0/255.0f blue:222.00/255.0f alpha:1.0f];
    self.navigationController.navigationBar.barTintColor = color;

    self.navigationController.navigationBar.translucent = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView==erroralertView1){
        sharedData.isDeviceLocked=YES;
        [sharedData newsaveAppStatus];
        [sharedData newloadAppStatus];
        [self performSegueWithIdentifier:@"toHome" sender:nil];
    }
    
    if (alertView == transerroralert) {
        if (buttonIndex == 0) {
            [self viewWillAppear:YES];
        }
        
    }
    else if (alertView == locationalert) {
        if (buttonIndex == 0) {
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
        else if (buttonIndex == 1)
        {
            self.btnAutomation.selected = NO;
            [self.btnAutomation setBackgroundImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
        }
    }
    else if(alertView == incorectSAalertView)
    {
        if (buttonIndex == 0) {
            [incorectSAalertView release];
        }
    }
    if(alertView==noSecurityAlertView){
        [self performSegueWithIdentifier:@"toHome" sender:self];
    }
    
//    self.myCirleView.hidden = NO;
//    self.lblValue.hidden = NO;
//    self.btnSecureAlignment.hidden = NO;
//    [self setCircleView];
    
//    else if (buttonIndex == 0) {
//        //save
//        //set the tilt value like setting a pin
//        //NSLog(@"savetilt value %@",self.lblTiltNo.text);
//        //NSLog(@"savetilt value %@",self.draw2DView.lblAngle.text);
//        SharedData *sharedData = [SharedData sharedData];
//        sharedData.isOutSET = YES;
//        [motionManager stopDeviceMotionUpdates];
//        
//        sharedData.dTiltValue =self.lblValue.text;
//        //check the profile name, null it first to save the value then resave the profile name
////        NSString *tempProfile = sharedData.profileName;
////        sharedData.profileName = nil;
//        [sharedData newsaveAppStatus];
////        sharedData.profileName = tempProfile;
//
//     //   [self performSegueWithIdentifier:@"firstView" sender:self];
//    }
}

- (void) makeSigOTPStringReady:(NSString *)j1{
    /////////////////////////////////// BAD HACK ///////////////////////////////////
    
    //NSString *j = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//    NSString *first = @"\"msg\":{";
//    NSString *second = @"}";
//    NSRange one = [j rangeOfString:first];
//    NSRange two = [[j substringFromIndex:one.location + one.length] rangeOfString:second];
//    NSRange final = NSMakeRange(one.location + first.length, two.location);
//    NSString *j1 = [j substringWithRange:final];
//    NSLog(@"MSG --- %@", j1);
    
    
    NSMutableString *strStore = [[NSMutableString alloc] init];
    
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appdelegate.dictSequence = [[NSMutableDictionary alloc] init];
    NSInteger iCount = 0;
    
    NSString *tg = @"\":\"";
    NSString *tg2 = @"\"";
    while(1){
        NSRange o = [j1 rangeOfString:tg];
        if(o.location == NSNotFound){
            break;
        }
        NSRange o1 = [[j1 substringFromIndex:o.location+o.length] rangeOfString:tg2];
        NSRange f = NSMakeRange(o.location + tg.length, o1.location);
        NSString *s = [j1 substringWithRange:f];
        [strStore appendFormat:@"%@,",s];
        [appdelegate.dictSequence setObject:s forKey:[NSString stringWithFormat:@"%ld", (long)iCount]];
        ++iCount;
        NSRange o2 = [j1 rangeOfString:s];
        NSString *s2 = [j1 substringFromIndex:o2.location+o2.length];
        j1 = s2;
    }
    
    if(strStore.length > 0){
        [strStore deleteCharactersInRange:NSMakeRange([strStore length]-1, 1)];
    }
    
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [userdefaults setObject:strStore forKey:@"SIGOTPSTRING"];
    [userdefaults synchronize];
    
    /////////////////////////////////// BAD HACK ///////////////////////////////////
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    ////////////************************
    self.myCirleView.hidden = YES;
    self.lblValue.hidden = YES;
    self.btnSecureAlignment.hidden = YES;
    
    if(_bOfflineMode){
        self.btnSecureAlignment.hidden = NO;
        _btnApprove.hidden = YES;
        _btnDeny.hidden = YES;
        self.lblAutomation.hidden = YES;
        self.btnAutomation.hidden = YES;
        [self.btnSecureAlignment setTitle:@"Get Authentication Code" forState:UIControlStateNormal];
    }
    
    sharedData = [SharedData sharedData];
    [sharedData newloadAppStatus];
    self.groupNames = [sharedData load];
//    NSLog(@"AUVC isTiltEnable %d",sharedData.isTiltEnable);
//    NSLog(@"AUVC isOutSET %d",sharedData.isOutSET);
//    NSLog(@"AUVC isTouchIDDevice %d",sharedData.isTouchIDDevice);
//    NSLog(@"AUVC isTouchIDEnable %d",sharedData.isTouchIDEnable);
//    NSLog(@"AUVC isLocationSet %d",sharedData.isLocationSet);

    if (sharedData.isLocationSet) {
        if(sharedData.isAutomationEnable){
            if (self.distance < 2.0) {
                [self.btnAutomation setBackgroundImage:[UIImage imageNamed:@"checkek.png"] forState:UIControlStateNormal];
            }
            else{
                [self.btnAutomation setBackgroundImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
            }
            
        }
    }
    
    NSMutableDictionary *msgDict = [[NSMutableDictionary alloc] init];
    for (id key in [self.arrmsg valueForKey:@"msg"]){
        [msgDict setValue:[[self.arrmsg valueForKey:@"msg"] valueForKey:key] forKey:key];
    }
    
    //[self makeSigOTPStringReady:[self.arrmsg valueForKey:@"msg"]];
    
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = [self.arrmsg valueForKey:@"uid"];
   
    self.lblGroup.text = [sharedData getgnm:[[self.arrmsg valueForKey:@"gid"] stringValue]];//[self.groupNames valueForKey:@"gnm"];
    NSUInteger t = [[self.arrmsg valueForKey:@"t"] integerValue];
    BOOL bIsLogin = t == 1;
    self.lblApp.text = bIsLogin ? @"A login request has been made" : @"A Transaction request has been made";
    
    NSDictionary *keyDict = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).dictSequence;
    
    NSArray *arK = [keyDict allKeys];//[msgDict allKeys];
    int cnt = 0;
    self.lbl1.hidden = YES;self.lbl2.hidden = YES;self.lbl3.hidden = YES;self.lbl4.hidden = YES;self.lbl5.hidden = YES;self.lbl6.hidden = YES;self.lbl7.hidden = YES;
    self.lblTime.hidden = YES;self.lblLocation.hidden = YES;self.lblGeoCoord.hidden = YES;self.lblDeviceName.hidden = YES;self.lblClientIp.hidden = YES;self.lblServerIp.hidden = YES;self.lblTransMsg.hidden = YES;
    
    for(NSString *lbl in arK){
        if(cnt == 0) {
            lbl = [[msgDict allKeysForObject:[keyDict valueForKey:[NSString stringWithFormat:@"%d", cnt]]] objectAtIndex:0];
            self.lblTime.text = lbl;
            self.lbl1.text = [msgDict valueForKey:lbl];
            self.lblTime.hidden = NO;
            self.lbl1.hidden = NO;
        }
        if(cnt == 1){
            lbl = [[msgDict allKeysForObject:[keyDict valueForKey:[NSString stringWithFormat:@"%d", cnt]]] objectAtIndex:0];
            self.lblLocation.text = lbl;
            self.lbl2.text = [msgDict valueForKey:lbl];
            self.lblLocation.hidden = NO;
            self.lbl2.hidden = NO;
        }
        if(cnt == 2){
            lbl = [[msgDict allKeysForObject:[keyDict valueForKey:[NSString stringWithFormat:@"%d", cnt]]] objectAtIndex:0];
            self.lblGeoCoord.text = lbl;
            self.lbl3.text = [msgDict valueForKey:lbl];
            self.lblGeoCoord.hidden = NO;
            self.lbl3.hidden = NO;
        }
        if(cnt == 3){
            lbl = [[msgDict allKeysForObject:[keyDict valueForKey:[NSString stringWithFormat:@"%d", cnt]]] objectAtIndex:0];
            self.lblDeviceName.text = lbl;
            self.lbl4.text = [msgDict valueForKey:lbl];
            self.lblDeviceName.hidden = NO;
            self.lbl4.hidden = NO;
        }
        if(cnt == 4){
            lbl = [[msgDict allKeysForObject:[keyDict valueForKey:[NSString stringWithFormat:@"%d", cnt]]] objectAtIndex:0];
            self.lblClientIp.text = lbl;
            self.lbl5.text = [msgDict valueForKey:lbl];
            self.lblClientIp.hidden = NO;
            self.lbl5.hidden = NO;
        }
        if(cnt == 5){
            lbl = [[msgDict allKeysForObject:[keyDict valueForKey:[NSString stringWithFormat:@"%d", cnt]]] objectAtIndex:0];
            self.lblServerIp.text = lbl;
            self.lbl6.text = [msgDict valueForKey:lbl];
            self.lblServerIp.hidden = NO;
            self.lbl6.hidden = NO;
        }
        if(cnt == 6){
            lbl = [[msgDict allKeysForObject:[keyDict valueForKey:[NSString stringWithFormat:@"%d", cnt]]] objectAtIndex:0];
            self.lblTransMsg.text = lbl;
            self.lbl7.text = [msgDict valueForKey:lbl];
            self.lblTransMsg.hidden = NO;
            self.lbl7.hidden = NO;
        }
        ++cnt;
    }
    
    self.lblAutomation.hidden = YES;
    self.btnAutomation.hidden = YES;

//    self.lblTime.text = bIsLogin ? @"Date/Time" : @"From";
//    self.lblLocation.text = bIsLogin ? @"Host/IP" : @"To";
//    self.lblGeoCoord.text = bIsLogin ? @"" : @"Amount";
    
    self.lblApp.font = LABEL;
    self.lblTime.font = LABEL;
    self.lblLocation.font = LABEL;
    self.lblGeoCoord.font = LABEL;
    self.lblDeviceName.font = LABEL;
    self.lblClientIp.font = LABEL;
    self.lblServerIp.font = LABEL;
    self.lblTransMsg.font = LABEL;
    

    [self.navigationController.navigationBar setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor],UITextAttributeFont: [UIFont systemFontOfSize:20.0f]}];
    
    self.lbl1.font = LABEL;
    self.lbl2.font = LABEL;
    self.lbl3.font = LABEL;
    self.lbl4.font = LABEL;
    self.lbl5.font = LABEL;
    self.lbl6.font = LABEL;
    self.lbl7.font = LABEL;
    
    //generate SIGOTP
    //NSLog(@"signature: %@ - profile: %@", self.combStr, sharedData.profileName);
    sharedData.signatureParamString = self.combStr;

}
- (void)dealloc {
    [_lbl1 release];
    [_lbl2 release];
    [_lbl3 release];
    [_lbl4 release];
    [_lbl5 release];
    [_myScrollView release];
    [_lblGroup release];
    [_lblApp release];
    [_lbl6 release];
    [_lbl7 release];
    [_lblTime release];
    [_lblLocation release];
    [_lblGeoCoord release];
    [_lblDeviceName release];
    [_lblClientIp release];
    [_lblServerIp release];
    [_lblTransMsg release];
    [_myCirleView release];
    [_lblValue release];
    [_btnSecureAlignment release];
    [_btnAutomation release];
    [_lblAutomation release];
    [_btnDeny release];
    [_btnApprove release];
    [super dealloc];
}

- (void)rateView:(Draw2D *)rateView ratingDidChange:(int)rating {
    self.lblValue.text = [NSString stringWithFormat:@"%i", rating];
    //NSLog(@"z: %i, x=%i",self.z,rating);
    
}

-(void)setCircleView
{
    //call the draw2d view to allow user to set the value.
//    self.draw2DView = [[Draw2D alloc]initWithFrame:CGRectMake(0, 31, 320, 540)];
    
  self.draw2DView = [[Draw2D alloc]initWithFrame:CGRectMake(0, 31, [[UIScreen mainScreen] bounds].size.width, ([[UIScreen mainScreen] bounds].size.height) - 30)];
    self.draw2DView.delegate = self;
    self.draw2DView.backgroundColor = [UIColor clearColor];
    self.lblValue.text = [NSString stringWithFormat:@"%i",self.draw2DView.z];
    [self.myCirleView addSubview:self.draw2DView];
}


- (IBAction)ClickAllow:(id)sender {
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    sharedData = [SharedData sharedData];
    [sharedData newloadAppStatus];
    
    
    //NSLog(@"~~~~~~~~~~~~Authorize~~~~~Click ALLOW~~~Profilename: %@", sharedData.profileName);
    if (self.btnAutomation.selected == YES){
        //save the location
        //get the current account information.
        //update the data.
        sharedData.isAutomationEnable = YES;
        //get user current location
        sharedData.longitude = self.clongitude;
        sharedData.latitude = self.clatitude;
        //NSLog(@"!!!!!!!!!sharedData.latitude: %f",sharedData.latitude);
        //NSLog(@"!!!!!!!!!sharedData.longitude: %f",sharedData.longitude);
        //[sharedData saveGeoLocation:self.clatitude long:self.clongitude];
        //[sharedData saveGeoLocation:self.clatitude long:self.clongitude];
        [sharedData saveCLLocation:sharedData.clLocation gnm:sharedData.profileName];
        [sharedData newsaveAppStatus];
    }
    self.txstat = @"1";
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:self.txstat forKey:@"TRANSACTION_STATUS"];
    //check the profile name, null it first to save the value then resave the profile name
//    NSString *tempProfile = sharedData.profileName;
//    sharedData.profileName = nil;
//[sharedData newloadAppStatus];
    
  
//    sharedData.profileName = tempProfile;
    //NSLog(@"tiltValue- %@", sharedData.dTiltValue);
    //if(sharedData.isTouchIDEnable && sharedData.isSecuritySet)
    if([[defaults objectForKey:@"SECURITY_TYPE"] isEqualToString:@"SECURITY_TYPE_TOUCHID"] && [defaults objectForKey:@"IS_SECURITY_SET"])
    {
        //with touch id
        LAContext *context = [[LAContext alloc] init];
        NSError *error = nil;
        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
            // Authenticate User
            [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                    localizedReason:@"Are you the device owner?"
                              reply:^(BOOL success, NSError *error) {
                                  if (success) {
                                      dispatch_async (dispatch_get_main_queue(), ^{
                                          if(_bOfflineMode){
                                              [self showTransparentView];
                                          }else{
                                              [self approved:nil];
                                          }
                                          
                                        });
                                  }
                                  if (error) {
                                      if (error.code == LAErrorUserFallback) {
                                          //NSLog(@"LAErrorUserFallback");
                                          dispatch_async (dispatch_get_main_queue(), ^{
                                              //AlertView Code
                                              transerroralert = [[UIAlertView alloc] initWithTitle:@"Touch ID"
                                                                                                   message:@"It has been detected that you have already setup Touch ID and enrolled a finger on this device. This application enforces Touch ID for your security."
                                                                                                  delegate:self
                                                                                         cancelButtonTitle:@"Ok"
                                                                                         otherButtonTitles:nil];
                                              [transerroralert show];
                                          });
                                      }
                                  }
                              }
             ];
        }
    
        else if (error.code == LAErrorTouchIDNotEnrolled)
        {
            //NSLog(@"RVc LAErrorTouchIDNotEnrolled");
            sharedData.isTouchIDDevice = YES;
            //NSLog(@"########################Author allow %@ ", sharedData.profileName);
            //[sharedData newsaveAppStatus];
        }
    }
    
    else if ([defaults objectForKey:@"SECURE_TILT_VALUE"]!=nil && [[defaults objectForKey:@"SECURITY_TYPE"] isEqualToString:@"SECURITY_TYPE_SECUREALIGNMENT"] ){
        //if (sharedData.dTiltValue != nil && sharedData.isTiltEnable) {
        //show the circleView
        self.myCirleView.hidden = NO;
        self.lblValue.hidden = NO;
        self.btnSecureAlignment.hidden = NO;
        [self setCircleView];
        
    } else
        //if(sharedData.isFaceRecognised){
        if([[defaults objectForKey:@"SECURITY_TYPE"] isEqualToString:@"SECURITY_TYPE_FACEVOICE"]){
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            //Do authentication
            [defaults setObject:@"DoAuthenticate" forKey:@"FACE_REC"];
            //Dont Generate OTP
            [defaults removeObjectForKey:@"GENERATE_OTP"];
            //Dont OFF face switch
            [defaults setObject:@"NO" forKey:@"TURNOFFFACE"];
            //Approve Transaction
            
            if(_bOfflineMode){
               [defaults setObject:@"2" forKey:@"APPROVE_OR_DENY"];
            }else{
                [defaults setObject:@"1" forKey:@"APPROVE_OR_DENY"];
            }
            
    
            [defaults synchronize];
            
        VVDemoRecognizerController *vc = (VVDemoRecognizerController *)[self.storyboard instantiateViewControllerWithIdentifier:VVDemoRecognizer];
        vc.callbackMe = self;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else{
        if(_bOfflineMode){
            [self showTransparentView];
        }else{
            [self approved:nil];
        }
//        NSLog(@"else executed");
//
//        noSecurityAlertView=[[UIAlertView alloc]initWithTitle:@"No Security" message:@"Please Set  Touch ID,Set Secure Alignment or Voice/Face Recognition from settings " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] ;
//        [noSecurityAlertView show];
        
        
//
//        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//        [dict setObject:[self.arrmsg valueForKey:@"uid"] forKey:@"actionby"];
//        [dict setObject:@"" forKey:@"txtcomment"];
//        [dict setObject:@"" forKey:@"alsid"];
//        
//        NSDictionary *ar = [self.arrmsg valueForKey:@"msg"];
//        NSMutableString *sg = [[NSMutableString alloc] initWithString:@""];
//        
//        BOOL bIsTransaction = [[[self.arrmsg valueForKey:@"t"] stringValue] isEqualToString:@"2"];
//        
//        NSArray *ar2 = [ar allKeys];
//        for(NSString *strKey in ar2){
//            [sg appendString:[ar valueForKey:strKey]];
//            [sg appendString:@","];
//        }
//        [sg deleteCharactersInRange:NSMakeRange([sg length]-1, 1)];
//        
////        if(bIsTransaction){
////            [sg appendString:[ar valueForKey:@"From"]];
////            [sg appendString:@","];
////            [sg appendString:[ar valueForKey:@"To"]];
////            [sg appendString:@","];
////            [sg appendString:[ar valueForKey:@"Amount"]];
////        }else{
////            [sg appendString:[ar valueForKey:@"Date/Time"]];
////            [sg appendString:@","];
////            [sg appendString:[ar valueForKey:@"Host/IP"]];
////        }
//        NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
//        [sharedData setSignatureParamString:[userdefaults objectForKey:@"SIGOTPSTRING"]];
//        [dict setObject:[sharedData generateSIGOTP] forKey:@"otp"];
//
//        [dict setObject:[self.arrmsg valueForKey:@"ref"] forKey:@"txref"];
//        [dict setObject:self.txstat forKey:@"txstat"];
//        
//        NSLog(@"dict=>%@",dict);
//        [sharedData transactionApproval:dict];
////        sharedData.dUse = nil;
//        
//        //[sharedData transactionApproval:[sharedData generateSIGOTP] txref:self.txref txstat:self.txstat];
//        sharedData.dUse = nil;
//        [self performSegueWithIdentifier:@"loading" sender:self];
    }
    
}
-(void)generateOTP1:(id)sender{
    [self showTransparentView];
}


-(void)approved:(id)sender{
    
    sharedData = [SharedData sharedData];
    [sharedData newloadAppStatus];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[self.arrmsg valueForKey:@"uid"] forKey:@"actionby"];
    [dict setObject:@"" forKey:@"txtcomment"];
    [dict setObject:@"" forKey:@"alsid"];
    
    NSDictionary *ar = [self.arrmsg valueForKey:@"msg"];
    NSMutableString *sg = [[NSMutableString alloc] initWithString:@""];
    
    BOOL bIsTransaction = [[[self.arrmsg valueForKey:@"t"] stringValue] isEqualToString:@"2"];
    
    NSArray *ar2 = [ar allKeys];
    for(NSString *strKey in ar2){
        [sg appendString:[ar valueForKey:strKey]];
        [sg appendString:@","];
    }
    [sg deleteCharactersInRange:NSMakeRange([sg length]-1, 1)];
    
    //        if(bIsTransaction){
    //            [sg appendString:[ar valueForKey:@"From"]];
    //            [sg appendString:@","];
    //            [sg appendString:[ar valueForKey:@"To"]];
    //            [sg appendString:@","];
    //            [sg appendString:[ar valueForKey:@"Amount"]];
    //        }else{
    //            [sg appendString:[ar valueForKey:@"Date/Time"]];
    //            [sg appendString:@","];
    //            [sg appendString:[ar valueForKey:@"Host/IP"]];
    //        }
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [sharedData setSignatureParamString:[userdefaults objectForKey:@"SIGOTPSTRING"]];
    [dict setObject:[sharedData generateSIGOTP] forKey:@"otp"];
    
    [dict setObject:[self.arrmsg valueForKey:@"ref"] forKey:@"txref"];
    [dict setObject:self.txstat forKey:@"txstat"];
    
    NSLog(@"dict=>%@",dict);
    [sharedData transactionApproval:dict];
    //        sharedData.dUse = nil;
    
    //[sharedData transactionApproval:[sharedData generateSIGOTP] txref:self.txref txstat:self.txstat];
    sharedData.dUse = nil;
    [self performSegueWithIdentifier:@"loading" sender:nil];


}

- (IBAction)ClickDeny:(id)sender {
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
     //NSLog(@"*******ClickDeny******  ");
    if(_bOfflineMode){
        return;
        //[self showTransparentView];
    }
    
    sharedData = [SharedData sharedData];
    [sharedData newloadAppStatus];
    self.txstat = @"2";
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:self.txstat forKey:@"TRANSACTION_STATUS"];

    if (self.btnAutomation.selected == YES){
        //save the location
        //get the current account information.
        //NSLog(@"profilename: %@", sharedData.profileName);
        //update the data.
        sharedData.isAutomationEnable = YES;
        //get user current location
        sharedData.longitude = self.clongitude;
        sharedData.latitude = self.clatitude;
        //NSLog(@"!!!!!!!!!sharedData.latitude: %f",sharedData.latitude);
        //NSLog(@"!!!!!!!!!sharedData.longitude: %f",sharedData.longitude);
        [sharedData saveGeoLocation:self.clatitude long:self.clongitude];
        [sharedData newsaveAppStatus];
    }
    if([[defaults objectForKey:@"SECURITY_TYPE"] isEqualToString:@"SECURITY_TYPE_TOUCHID"] && [defaults objectForKey:@"IS_SECURITY_SET"])//if(sharedData.isTouchIDEnable)
    {
       //NSLog(@"*******ClickDeny isTouchIDEnable******  ");
        //with touch id
        LAContext *context = [[LAContext alloc] init];
        NSError *error = nil;
        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
            // Authenticate User
            [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                    localizedReason:@"Are you the device owner?"
                              reply:^(BOOL success, NSError *error) {
                                  if (success) {
                                      dispatch_async (dispatch_get_main_queue(), ^{
                                          [self denied:nil];
                                          
                                          
                                      });
                                  }
                                  if (error) {
                                      
                                      if (error.code == LAErrorUserFallback) {
                                          //NSLog(@"LAErrorUserFallback");
                                          dispatch_async (dispatch_get_main_queue(), ^{
                                              //AlertView Code
                                              transerroralert = [[UIAlertView alloc] initWithTitle:@"Touch ID"
                                                                                           message:@"It has been detected that you have already setup Touch ID and enrolled a finger on this device. This application enforces Touch ID for your security."
                                                                                          delegate:self
                                                                                 cancelButtonTitle:@"Ok"
                                                                                 otherButtonTitles:nil];
                                              [transerroralert show];
                                          });
                                          
                                      }
                                  }
                              }
             ];
        }
        
        else if (error.code == LAErrorTouchIDNotEnrolled)
        {
            //NSLog(@"RVc LAErrorTouchIDNotEnrolled");
            sharedData.isTouchIDEnable = NO;
            //NSLog(@"########################Author allow %@ ", sharedData.profileName);
            [sharedData newsaveAppStatus];
        }
    }
    else if ([defaults objectForKey:@"SECURE_TILT_VALUE"]!=nil && [[defaults objectForKey:@"SECURITY_TYPE"] isEqualToString:@"SECURITY_TYPE_SECUREALIGNMENT"] ){//if (sharedData.dTiltValue != nil && sharedData.isTiltEnable) {
        //show the circleView
        self.myCirleView.hidden = NO;
        self.lblValue.hidden = NO;
        self.btnSecureAlignment.hidden = NO;
        [self setCircleView];
        
    }else if([[defaults objectForKey:@"SECURITY_TYPE"] isEqualToString:@"SECURITY_TYPE_FACEVOICE"]){ //if(sharedData.isFaceRecognised){
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        //Do authentication
        [defaults setObject:@"DoAuthenticate" forKey:@"FACE_REC"];
        
        //Don't Approve Transaction
         [defaults setObject:@"0" forKey:@"APPROVE_OR_DENY"];
        //Dont Generate OTP
        [defaults removeObjectForKey:@"GENERATE_OTP"];
        //Dont OFF Face Swithc
        [defaults setObject:@"NO" forKey:@"TURNOFFFACE"];
        [defaults synchronize];
        
        VVDemoRecognizerController *vc = (VVDemoRecognizerController *)[self.storyboard instantiateViewControllerWithIdentifier:VVDemoRecognizer];
        vc.callbackMe = self;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else{
        [self denied:nil];
    

    }
    


}

-(void)denied:(id)sender{
    
        sharedData = [SharedData sharedData];
       [sharedData newloadAppStatus];
 
        //NSLog(@"DENY Secure Alignment NOT SET");
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:[self.arrmsg valueForKey:@"uid"] forKey:@"actionby"];
        [dict setObject:@"" forKey:@"txtcomment"];
        [dict setObject:@"" forKey:@"alsid"];
        NSDictionary *ar = [self.arrmsg valueForKey:@"msg"];
        NSMutableString *sg = [[NSMutableString alloc] initWithString:@""];
        BOOL bIsTransaction = [[[self.arrmsg valueForKey:@"t"] stringValue] isEqualToString:@"2"];
        NSArray *ar2 = [ar allKeys];
        for(NSString *strKey in ar2){
            [sg appendString:[ar valueForKey:strKey]];
            [sg appendString:@","];
        }
        [sg deleteCharactersInRange:NSMakeRange([sg length]-1, 1)];
        
        //        if(bIsTransaction){
        //            [sg appendString:[ar valueForKey:@"From"]];
        //            [sg appendString:@","];
        //            [sg appendString:[ar valueForKey:@"To"]];
        //            [sg appendString:@","];
        //            [sg appendString:[ar valueForKey:@"Amount"]];
        //        }else{
        //            [sg appendString:[ar valueForKey:@"Date/Time"]];
        //            [sg appendString:@","];
        //            [sg appendString:[ar valueForKey:@"Host/IP"]];
        //        }
        NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
        [sharedData setSignatureParamString:[userdefaults objectForKey:@"SIGOTPSTRING"]];
        [dict setObject:[sharedData generateSIGOTP] forKey:@"otp"];
        [dict setObject:[self.arrmsg valueForKey:@"ref"] forKey:@"txref"];
        [dict setObject:self.txstat forKey:@"txstat"];
        [sharedData transactionApproval:dict];
        //[sharedData transactionApproval:[sharedData generateSIGOTP] txref:self.txref txstat:self.txstat];
        sharedData.dUse = nil;
        [self performSegueWithIdentifier:@"loading" sender:self];
}

UIView *OTPView1;
UIProgressView *prg1;
int iTimeForOTP1 = 30; //sec
int iTimeCurrent1 = iTimeForOTP1;
- (void) showTransparentView{
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:@"GOTOHOME" forKey:@"GOTOHOME"];
    [defaults synchronize];
    
    OTPView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [OTPView1 setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6f]];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 120)];
    lbl.center = CGPointMake(OTPView1.frame.size.width/2, OTPView1.frame.size.height/2 - self.navigationController.navigationBar.frame.size.height);
    lbl.backgroundColor = [UIColor whiteColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.layer.borderColor = [UIColor colorWithRed:48/255.0f green:108/255.0f blue:164/255.0f alpha:1.0f].CGColor;//[UIColor colorWithRed:18/255.0f green:123/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    lbl.layer.borderWidth = 2.0f;
    lbl.layer.shadowRadius = 3.0;
    lbl.layer.shadowOpacity = 0.5;
    lbl.layer.masksToBounds = NO;
    lbl.layer.shouldRasterize = YES;
    //lbl.font = [UIFont systemFontOfSize:30.0f];//@"Coda-Regular" size:30.0f
    lbl.numberOfLines = 2;
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    sharedData = [SharedData sharedData];
    [sharedData newloadAppStatus];
    [sharedData setSignatureParamString:[userdefaults objectForKey:@"SIGOTPSTRING"]];
    NSString *str = [NSString stringWithFormat:@"One-Time Password\n%@", [sharedData generateSIGOTP]];
    lbl.text = str;
    
    
    prg1 = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    [prg1 setFrame:CGRectMake(4, lbl.frame.size.height-7, lbl.frame.size.width-4, 10)];
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 3.0f);
    prg1.transform = transform;
    
    [prg1 setProgress:0.0f animated:YES];
    
    //    [prg setProgressTintColor:[UIColor colorWithRed:18/255.0f green:123/255.0f blue:107/255.0f alpha:1.0f]];
    [prg1 setProgressTintColor:[UIColor colorWithRed:48/255.0f green:108/255.0f blue:164/255.0f alpha:1.0f]];
    [prg1 setTrackTintColor:[UIColor clearColor]];
    
    [lbl addSubview:prg1];
    [OTPView1 addSubview:lbl];
    
    [self.view addSubview:OTPView1];
    timer = nil;
    timer = [NSTimer scheduledTimerWithTimeInterval:1
                                             target:self
                                           selector:@selector(updateProgress)
                                           userInfo:nil
                                            repeats:YES];
    
}

- (void) removeTransparentView{
    [OTPView1 removeFromSuperview];
    [prg1 setProgress:0.0 animated:YES];
    [timer invalidate];
    timer = nil;
    iTimeCurrent1 = iTimeForOTP1;
    OTPView1 = nil;
    prg1 = nil;
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//    [defaults setObject:@"GOTOHOME" forKey:@"GOTOHOME"];
//    [defaults synchronize];
    
    if([defaults objectForKey:@"GOTOHOME"])
    {
        [defaults removeObjectForKey:@"GOTOHOME"];
        [self performSegueWithIdentifier:@"toHome" sender:nil];
    }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // dismiss keyboard when background is touched
    [self removeTransparentView];
}

-(void) updateProgress{
    --iTimeCurrent1;
    
    int iProgress = (iTimeForOTP1 - iTimeCurrent1);
    iProgress = iProgress*100/iTimeForOTP1;
    [prg1 setProgress:iProgress/100.0f animated:YES];
    
    if(iTimeCurrent1 <= 0){
        [self removeTransparentView];
    }
}

- (IBAction)ClickSecureAlignment:(id)sender {
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    if(_bOfflineMode && [[self.btnSecureAlignment titleForState:UIControlStateNormal] isEqualToString:@"Get Authentication Code"]){
        
        [self ClickAllow:sender];
        [self.btnSecureAlignment setTitle:@"Secure Alignment" forState:UIControlStateNormal];
//        [self showTransparentView];
        return;
    }
    
    
    //NSLog(@"SecureAlignment Allow/Deny = %@",self.txstat);
    sharedData = [SharedData sharedData];
    [sharedData newloadAppStatus];
    self.lblValue.text = [NSString stringWithFormat:@"%i",self.draw2DView.z];
    //check the profile name, null it first to save the value then resave the profile name
//    NSString *tempProfile = sharedData.profileName;
//    sharedData.profileName = nil;
    [sharedData newloadAppStatus];
//    sharedData.profileName = tempProfile;
    
    //if ([sharedData.dTiltValue isEqualToString:self.lblValue.text]) {
    if([[defaults objectForKey:@"SECURE_TILT_VALUE"] isEqualToString:self.lblValue.text]){
        if(_bOfflineMode){
            [self showTransparentView];
            return;
        }
        
        //check if automation is being selected
        //if yes save the location to the current account.
        if (self.btnAutomation.selected == YES){
            //save the location
            //get the current account information.
            //NSLog(@"profilename: %@", sharedData.profileName);
            //update the data.
            sharedData.isAutomationEnable = YES;
            
            //get user current location
            sharedData.longitude = self.clongitude;
            sharedData.latitude = self.clatitude;
            //NSLog(@"!!!!!!!!!sharedData.latitude: %f",sharedData.latitude);
            //NSLog(@"!!!!!!!!!sharedData.longitude: %f",sharedData.longitude);
            [sharedData saveGeoLocation:self.clatitude long:self.clongitude];
            [sharedData newsaveAppStatus];
        }
        //approve the transaction
        //send notice to the server its approved
        //go to processing screen
        //[sharedData resetoutPut];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:[self.arrmsg valueForKey:@"uid"] forKey:@"actionby"];
        [dict setObject:@"" forKey:@"txtcomment"];
        [dict setObject:@"" forKey:@"alsid"];
        
        NSDictionary *ar = [self.arrmsg valueForKey:@"msg"];
        NSMutableString *sg = [[NSMutableString alloc] initWithString:@""];
        BOOL bIsTransaction = [[[self.arrmsg valueForKey:@"t"] stringValue] isEqualToString:@"2"];
        
        NSArray *ar2 = [ar allKeys];
        for(NSString *strKey in ar2){
            [sg appendString:[ar valueForKey:strKey]];
            [sg appendString:@","];
        }
        [sg deleteCharactersInRange:NSMakeRange([sg length]-1, 1)];
        
        
//        if(bIsTransaction){
//            [sg appendString:[ar valueForKey:@"From"]];
//            [sg appendString:@","];
//            [sg appendString:[ar valueForKey:@"To"]];
//            [sg appendString:@","];
//            [sg appendString:[ar valueForKey:@"Amount"]];
//        }else{
//            [sg appendString:[ar valueForKey:@"Date/Time"]];
//            [sg appendString:@","];
//            [sg appendString:[ar valueForKey:@"Host/IP"]];
//        }
        
        NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
        NSLog(@"Signature String - %@", [userdefaults objectForKey:@"SIGOTPSTRING"]);
        [sharedData setSignatureParamString:[userdefaults objectForKey:@"SIGOTPSTRING"]];
        [dict setObject:[sharedData generateSIGOTP] forKey:@"otp"];
        
        [dict setObject:[self.arrmsg valueForKey:@"ref"] forKey:@"txref"];
        [dict setObject:self.txstat forKey:@"txstat"];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:self.txstat forKey:@"TRANSACTION_STATUS"];
           NSLog(@"TRANSACTION_STATUS=>%@",self.txstat);
        [userDefaults synchronize];
        
        
        
        [sharedData transactionApproval:dict];
        
        sharedData.outPut=5;
        [sharedData newsaveAppStatus];
        [sharedData newloadAppStatus];
        
        [defaults removeObjectForKey:@"LOCKED_COUNT"];
        [defaults synchronize];
        
//        [sharedData transactionApproval:[sharedData generateSIGOTP] txref:self.txref txstat:self.txstat];
        sharedData.dUse = nil;
        [self performSegueWithIdentifier:@"loading" sender:nil];
        
    }
    else
    {
        //tilt value not right
        //reduce count n show alert message
        //NSLog(@"number of attempt %d",sharedData.outPut);
        [sharedData decreaseoutPut];
        NSString *msg =[NSString stringWithFormat:@"Please tilt your device to correct alignment to proceed. You have %d attempts remaining.",sharedData.outPut];
        //NSLog(@"Reduce the number of attempt %d",sharedData.outPut);
        
        /*
         
         This code is for lock app if secure alignment is wrong
         
         */
        
        if(sharedData.outPut>0){
            
            NSLog(@"greater than Zero");
            msg=[NSString stringWithFormat:@"Please tilt your device to correct alignment to proceed. You have %d attempts remaining.",sharedData.outPut];
            erroralertView = [[UIAlertView alloc]initWithTitle:@"Incorrect Secure Alignment" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [erroralertView show];
        }else{
            
            NSTimeInterval timeInMiliseconds = [[NSDate date] timeIntervalSince1970];
            if([defaults objectForKey:@"LOCKED_COUNT"])
            {
                NSNumber *locked_count=[defaults objectForKey:@"LOCKED_COUNT"];
                int temp_lock_count=locked_count.intValue+1;
                
                timeInMiliseconds=timeInMiliseconds+(temp_lock_count*300);
                
                locked_count=[NSNumber numberWithInt:temp_lock_count];
                [defaults setObject:locked_count forKey:@"LOCKED_COUNT"];
                
                NSNumber *locked_value=[NSNumber numberWithLongLong:timeInMiliseconds];
                [defaults setObject:locked_value forKey:@"LOCKED_VALUE"];
                [defaults synchronize];
                
                NSString *message=[NSString stringWithFormat:@"The application is locked for %i minutes because you have exceeded the limit for providing your Secure Alignment ",temp_lock_count*5];
                
                erroralertView1=[[UIAlertView alloc]initWithTitle:@"Application Locked" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [erroralertView1 show];
            }else{
                NSNumber *locked_count=[NSNumber numberWithInt:1];
                [defaults setObject:locked_count forKey:@"LOCKED_COUNT"];
                
                NSNumber *locked_value=[NSNumber numberWithLongLong:timeInMiliseconds+300];
                [defaults setObject:locked_value forKey:@"LOCKED_VALUE"];
                [defaults synchronize];
                
                erroralertView1=[[UIAlertView alloc]initWithTitle:@"Application Locked" message:@"The application is locked for 5 minutes because you have exceeded the limit for providing your Secure Alignment " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [erroralertView1 show];
            }
        }
        
        
//        incorectSAalertView = [[UIAlertView alloc]initWithTitle:@"Incorrect Secure Alignment" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [incorectSAalertView show];
    }
}
- (IBAction)ClickAutomate:(id)sender {
    //intialy not set.
    //user check the box     //  'd':'51.5033,-0.1276',

    //if user check this box, save the location inside this account.
    //if the user has saved this location before
    sharedData = [SharedData sharedData];
    [sharedData newloadAppStatus];

    if (self.btnAutomation.selected == NO) {
        if (sharedData.isLocationSet == NO) {
            //NSLog(@"LOCATION DISABLED");
            locationalert = [[UIAlertView alloc] initWithTitle:@"Location Disabled"
                                                            message:@"Could not determine your location.Click Open Settings to open the location settings and enable location services Or Cancel to close this dialog."
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Open Settings",nil];
            [locationalert show];
        }
        self.btnAutomation.selected = YES;
        [self.btnAutomation setBackgroundImage:[UIImage imageNamed:@"checkek.png"] forState:UIControlStateNormal];
    }
    else
    {
        self.btnAutomation.selected = NO;
        [self.btnAutomation setBackgroundImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
    }
}

//- (void) enrollSuccess{
//
//}
//
//-(void) authenticateSuccess{
//    SharedData *sharedData = [SharedData sharedData];
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    [userDefaults setObject:self.txstat forKey:@"TRANSACTION_STATUS"];
//
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//    [dict setObject:[self.arrmsg valueForKey:@"uid"] forKey:@"actionby"];
//    [dict setObject:@"" forKey:@"txtcomment"];
//    [dict setObject:@"" forKey:@"alsid"];
//    NSDictionary *ar = [self.arrmsg valueForKey:@"msg"];
//    NSMutableString *sg = [[NSMutableString alloc] initWithString:@""];
//    BOOL bIsTransaction = [[[self.arrmsg valueForKey:@"t"] stringValue] isEqualToString:@"2"];
//    NSArray *ar2 = [ar allKeys];
//    for(NSString *strKey in ar2){
//        [sg appendString:[ar valueForKey:strKey]];
//        [sg appendString:@","];
//    }
//    [sg deleteCharactersInRange:NSMakeRange([sg length]-1, 1)];
//
//    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
//    [sharedData setSignatureParamString:[userdefaults objectForKey:@"SIGOTPSTRING"]];
//    [dict setObject:[sharedData generateSIGOTP] forKey:@"otp"];
//    [dict setObject:[self.arrmsg valueForKey:@"ref"] forKey:@"txref"];
//    [dict setObject:self.txstat forKey:@"txstat"];
//    [sharedData transactionApproval:dict];
//    sharedData.dUse = nil;
//    [self performSegueWithIdentifier:@"loading" sender:self];
//}


@end
