//
//  VVDemoSettingsController.h
//  IOSVVDemo
//
//  Created by Karl Ridgeway on 9/4/14.
//  Copyright (c) 2014 Sensory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VVDemoSettingsController : UIViewController
@property (weak, nonatomic) IBOutlet UISwitch *requireBothSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *faceMotionAnalysisSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *useAdaptiveEnrollmentSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *requireConfirmationSwitch;
@property (weak, nonatomic) IBOutlet UISlider *authTimeoutSlider;
@property (weak, nonatomic) IBOutlet UILabel *authTimeoutLabel;
@property (weak, nonatomic) IBOutlet UISlider *securityLevelSlider;

- (IBAction)requireFaceVoiceChanged:(id)sender;
- (IBAction)faceMotionAnalysisChanged:(id)sender;
- (IBAction)useAdaptiveEnrollmentChanged:(id)sender;
- (IBAction)requireConfirmationChanged:(id)sender;
- (IBAction)authenticationTimeoutChanged:(id)sender;
- (IBAction)securityLevelChanged:(id)sender;

@end
