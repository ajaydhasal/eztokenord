//
//  VVDemoAppDelegate.h
//  IOSVVDemo
//
//  Created by Karl Ridgeway on 8/28/14.
//  Copyright (c) 2014 Sensory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VVDemoAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
