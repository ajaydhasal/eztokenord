//
//  DebugViewController.h
//  EZToken
//
//  Created by Harsha on 12/3/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface DebugViewController : UIViewController <UITextViewDelegate, MFMailComposeViewControllerDelegate>
- (IBAction)ClickBack:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *txtView;
- (IBAction)ClearLogs:(id)sender;
@end
