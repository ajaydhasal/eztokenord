//
//  SettingViewController.h
//  EZToken
//
//  Created by Harsha on 2/11/15.
//  Copyright (c) 2015 Ezmcom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *txtGatewayUrl;
- (IBAction)ClickSave:(id)sender;
@property (strong, nonatomic) NSString *correctURLString;
@property (strong, nonatomic) UIAlertView *saveAlert;
@property (strong, nonatomic) UIAlertView *resetAlert;
- (IBAction)ClickBack:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *lblGateway;
@property (retain, nonatomic) IBOutlet UITextView *txtDescription;
@property (retain, nonatomic) IBOutlet UIButton *btnCancel;
@property (retain, nonatomic) IBOutlet UIButton *btnSave;
@end
