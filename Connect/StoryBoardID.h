//
//  StoryBoardID.h
//  IOSVVDemo
//
//  Created by Apple on 07/09/15.
//  Copyright (c) 2015 Sensory. All rights reserved.
//

#ifndef IOSVVDemo_StoryBoardID_h
#define IOSVVDemo_StoryBoardID_h

#define  VVOptionVC  @"VVOptionVC"

#define ACCOUNTS                      @"Accounts"
#define TRANSFER                      @"Transfer"
#define RECHARGE                      @"Recharge"
#define PAYBILL                       @"PayBill"
#define FAVOURITE                     @"Favourite"
#define UITABBAR                      @"UITABBAR"
#define ROOTTABBAR                    @"ROOTTABBAR"
#define VVDemoRecognizer              @"VVDemoRecognizer"
#define CameraViewController          @"CameraViewController"
#define activation                    @"activation"

#endif
