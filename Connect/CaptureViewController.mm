
//
//  CaptureViewController.m
//  EZToken
//
//  Created by Harsha on 9/2/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import "CaptureViewController.h"
#import <CoreMotion/CoreMotion.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import "VVDemoRecognizerController.h"

@interface CaptureViewController ()
{
    CMMotionManager *motionManager;
    UIAlertView *setalertView;
    UIAlertView *changealertView,*changealertView1;
    UIAlertView *skipalertView;
    UIAlertView *erroralertView,*erroralertView1;
    UIAlertView *disablealertView,*disablealertView1;
    
    UIAlertView *erroralert;
    UIAlertView *touchIdsetupalert;
    
    NSTimer *lockTimer;
    UIAlertView *timealertView;
    
    UIAlertController *changealert;
}
@property int z;
@property int time;
@property (nonatomic, strong)UIImageView * loading;


@end
BOOL isAngleMatched=NO;

@implementation CaptureViewController
#define degrees(x) (180 * x / M_PI)

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)rateView:(Draw2D *)rateView ratingDidChange:(int)rating {
    self.lblTiltNo.text = [NSString stringWithFormat:@"%i", rating];
    //NSLog(@"z: %i, x=%i",self.z,rating);
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *backbutton =  [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:nil action:nil];
    [backbutton setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor],UITextAttributeFont: [UIFont systemFontOfSize: 17.0f]} forState:UIControlStateNormal];
    
    
    self.navigationItem.leftBarButtonItem=backbutton;
    [backbutton setTarget:self];
    [backbutton setAction:@selector(ClickCancel:)];
    
    sharedData=[SharedData sharedData];
    self.txtDescription.hidden = YES;
    self.btnCapture.hidden = YES;
    self.btnSkip.enabled = NO;
    self.imgTilt.hidden = YES;
    self.lblTiltNo.hidden = YES;
    self.viewAlert.hidden = YES;
    self.myCircleView.hidden = YES;
    
}

-(void)lockApp
{
    //NSLog(@"%%%%%%%%%%%%%%%%%%%%%%LOCKAPP Timer %@", lockTimer);
    if (lockTimer < 0) {
        NSString *msg = [NSString stringWithFormat:@"Wait For %i sec for app to be unlocked.",self.time];
        timealertView = [[UIAlertView alloc]initWithTitle:@"App Locked" message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
        [timealertView show];
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [_btnCapture setTitle:@"Set Secure Alignment" forState:UIControlStateNormal];
    self.btnSkipTilt.selected = YES;
    [self.btnSkipTilt setBackgroundImage:[UIImage imageNamed:@"checkek.png"] forState:UIControlStateNormal];
    
    self.navigationItem.hidesBackButton = YES;
    self.time = 0;
    if (sharedData.isDeviceLocked == YES) {
        self.time = self.time*sharedData.outPut;
        [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(lockApp) userInfo:nil repeats:NO];
    }
    
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    if([defaults objectForKey:@"DISABLETOUCHID"]){
        // We first want to disable the touch id set
        [defaults removeObjectForKey:@"AUTHENTICATION_METHOD"];
        [defaults synchronize];
    }
    

        sharedData.isTouchIDDevice = NO;
        [sharedData newsaveAppStatus];
        [sharedData newloadAppStatus];
        [self NotTouchIDDevice];
   
}

- (IBAction)ClickCancel:(id)sender {
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"AUTHTOBETURNEDON"];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setAllignmentView
{
    [self.loading stopAnimating];
    [self.loading removeFromSuperview];
    self.viewAlert.hidden = NO;
    self.view.backgroundColor = [UIColor lightGrayColor];
    NSArray * imageArray  = [[NSArray alloc] initWithObjects:
                             [UIImage imageNamed:@"finger0.png"],
                             [UIImage imageNamed:@"finger1.png"],
                             [UIImage imageNamed:@"finger2.png"],
                             [UIImage imageNamed:@"finger3.png"],
                             [UIImage imageNamed:@"finger4.png"],
                             nil];
    NSLog(@"Width of screen %f", [self.view bounds].size.width/2);
    
    
    self.imgViewanimation = [[UIImageView alloc] initWithFrame:
                             CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 110, 100, 280, 280)];
    self.imgViewanimation.animationImages = imageArray;
    self.imgViewanimation.animationDuration = 2.5;
    self.imgViewanimation.contentMode = UIViewContentModeBottomLeft;
    [self.view addSubview:self.imgViewanimation];
    [self.imgViewanimation startAnimating];
}

-(void)setCircleView
{
    //call the draw2d view to allow user to set the value.
//    self.draw2DView = [[Draw2D alloc]initWithFrame:CGRectMake(0, 31, 320, 540)];
//    self.draw2DView.delegate = self;
//    self.draw2DView.backgroundColor = [UIColor clearColor];
//    self.lblTiltNo.text = [NSString stringWithFormat:@"%i",self.draw2DView.z];
//    [self.myCircleView addSubview:self.draw2DView];
    
    self.draw2DView = [[Draw2D alloc]initWithFrame:CGRectMake(0, 31, [[UIScreen mainScreen] bounds].size.width, ([[UIScreen mainScreen] bounds].size.height) - 30)];
    self.draw2DView.delegate = self;
    self.draw2DView.backgroundColor = [UIColor clearColor];
    [self.myCircleView addSubview:self.draw2DView];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ClickOKSet:(id)sender {
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSLog(@"sharedData.isTiltEnable =%i  sharedData.dTiltValue.length =%lu ",sharedData.isTiltEnable,(unsigned long)sharedData.dTiltValue.length);

    // if (!sharedData.isTiltEnable || sharedData.dTiltValue.length == 0) {
    //[defaults setObject:@"SECURITY_TYPE_SECUREALIGNMENT" forKey:@"SECURITY_TYPE"];

    if(![[defaults objectForKey:@"SECURITY_TYPE"]isEqualToString:@"SECURITY_TYPE_SECUREALIGNMENT"]||[[defaults objectForKey:@"SECURE_TILT_VALUE"] length]==0){
        NSLog(@"first time setup");
        
        if (self.btnSkipTilt.selected == YES) {
            skipalertView = [[UIAlertView alloc]initWithTitle:@"Tip" message:@"You can set your Secure Alignment at any other time. Select the Set Secure Alignment from the home screen menu." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [skipalertView show];
        }
        else
        {
            
            [self.imgViewanimation stopAnimating];
            [self.imgViewanimation removeFromSuperview];
            self.viewAlert.hidden = YES;
            self.view.backgroundColor = [UIColor whiteColor];
            self.btnCapture.hidden = NO;
            self.btnSkip.enabled = YES;
            self.imgTilt.hidden = NO;
            self.lblTiltNo.hidden = NO;
            self.myCircleView.hidden = NO;
            [self setCircleView];
        }
    }
    else{
        
        //  @"IS_SECURITY_SET"
        //[defaults setObject:@"IS_SECURITY_SET" forKey:@"IS_SECURITY_SET"];
        sharedData.isSecuritySet = YES;
        //[defaults setObject:@"SECURITY_TYPE_TOUCHID" forKey:@"SECURITY_TYPE"];
        
        sharedData.isTouchIDEnable = YES;
        sharedData.isTiltEnable=NO;
        sharedData.isFaceRecognised=NO;
        [defaults synchronize];
        [sharedData newsaveAppStatus];
    }
    
}

- (IBAction)ClickOnSkip:(id)sender {
    if (self.btnSkipTilt.selected == NO) {
        self.btnSkipTilt.selected = YES;
        [self.btnSkipTilt setBackgroundImage:[UIImage imageNamed:@"checkek.png"] forState:UIControlStateNormal];
    }
    else
    {
        self.btnSkipTilt.selected = NO;
        [self.btnSkipTilt setBackgroundImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
    }
}



- (void)dealloc {
    [_myCircleView release];
    [_myNavBar release];
    [super dealloc];
}


/////==========================>ToucID Device==========================================//
//
//-(void)touchIDDevice{
//    NSLog(@"TouchIDDevice");
//    
//     [_myNavBar setTitle:@"Touch ID"];
//    //if(sharedData.isSecuritySet){
//    
//        LAContext *context = [[LAContext alloc] init];
//        NSError *error = nil;
//        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error])
//        {
//            // Authenticate User
//            NSLog(@"*****There is TouchID*******");
//            NSLog(@"########################CaptureVC SAVE %@ ", sharedData.profileName);
//            [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
//                    localizedReason:@"Are you the device owner?"
//                              reply:^(BOOL success, NSError *error) {
//                                  if (error) {
//                                      
//                                      if (error.code == LAErrorUserFallback) {
//                                          NSLog(@"LAErrorUserFallback");
//                                          dispatch_async (dispatch_get_main_queue(), ^{
//                                              //AlertView Code
//                                              erroralert = [[UIAlertView alloc] initWithTitle:@"Touch ID"
//                                                                                      message:@"It has been detected that you have already setup Touch ID and enrolled a finger on this device. This application will use the feature for future authentications.On this screen you'll be asked to setup the Touch ID feature to protect your token account."
//                                                                                     delegate:self
//                                                                            cancelButtonTitle:@"Ok"
//                                                                            otherButtonTitles:nil];
//                                              [erroralert show];
//                                          });
//                                          
//                                      }
//                                      else if(error.code == LAErrorUserCancel){
//                                          
//                                          dispatch_async (dispatch_get_main_queue(), ^{
//                                              [self performSegueWithIdentifier:@"firstView" sender:self];
//                                          });
//                                      }
//                                  }
//                                  
//                                  else if (success) {
//                                      
//                                      
//                                      dispatch_async (dispatch_get_main_queue(), ^{
//                                          NSLog(@"*******Success TOUCHID******  ");
//                                          sharedData.isSecuritySet=YES;
//                                          if(sharedData.isTouchIDEnable){
//                                              sharedData.isTouchIDEnable=NO;
//                                          } else {
//                                              sharedData.isTouchIDEnable=YES;
//                                              sharedData.isFaceRecognised=NO;
//                                              sharedData.isTiltEnable=NO;
//                                          }
//                                          
//                                          [sharedData newsaveAppStatus];
//                                          [sharedData newloadAppStatus];
//                                          
//                                          //[self.navigationController popViewControllerAnimated:YES];
//                                           [self.navigationController popViewControllerAnimated:YES];
//                                          if(_callbackMe && [_callbackMe respondsToSelector:@selector(authenticateSuccess:)]){
//                                              [_callbackMe performSelector:@selector(authenticateSuccess:) withObject:nil afterDelay:0.1];
//                                          }
//                                          
//                                      });
//                                  }
//                                  
//                                  else {
//                                      dispatch_async (dispatch_get_main_queue(), ^{
//                                          
//                                          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                                                          message:@"You are not the device owner."
//                                                                                         delegate:self
//                                                                                cancelButtonTitle:@"Ok"
//                                                                                otherButtonTitles:nil];
//                                          [alert show];
//                                      });
//                                  }
//                                  
//                              }];
//            
//        }
//        else{
//            NSLog(@"TOUCHID Error %@",error);
//            if (error.code == LAErrorTouchIDNotEnrolled) {
//                dispatch_async (dispatch_get_main_queue(), ^{
//                    NSLog(@"TOUCHID Error LAErrorTouchIDNotEnrolled");
//                    touchIdsetupalert = [[UIAlertView alloc] initWithTitle:@"Touch ID Setup"
//                                                                   message:@"Your device supports Touch ID. Please enroll Touch ID."
//                                                                  delegate:self
//                                                         cancelButtonTitle:@"OK"
//                                                         otherButtonTitles:nil];
//                    [touchIdsetupalert show];
//                });
//            }
//            if (error.code == LAErrorTouchIDNotAvailable) {
//                dispatch_async (dispatch_get_main_queue(), ^{
//                    //no touch id
//                    //not set alignemnt
//                    NSLog(@"TOUCHID Error LAErrorTouchIDNotAvailable");
//                    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(setAllignmentView) userInfo:nil repeats:NO];
//                    self.navigationItem.hidesBackButton = YES;
//                });
//            }
//        }
////    }else{
////        
////        
////    }
//}
//
//

///==========================>Not ToucID Device==========================================//


-(void)NotTouchIDDevice{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    [_myNavBar setTitle:@"Set Secure Alignment"];
     self.navigationItem.hidesBackButton = NO;
    [sharedData newloadAppStatus];
    // if(sharedData.dTiltValue==nil){/// check nsuer value instead sharedData.dTiltValue
    
    if([defaults objectForKey:@"SECURE_TILT_VALUE"]==nil)//sharedData.dTiltValue=nil
    {
                 [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(setAllignmentView) userInfo:nil repeats:NO];
                  self.navigationItem.hidesBackButton = YES;
    }else{//else of sharedData.dTitlValue==nil
        
        
        if([defaults objectForKey:@"FLAG_FOR_MATCH_ANGLE"]||[defaults objectForKey:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT"])
        {
            // if(sharedData.flagForMatchAngle){
            if([defaults objectForKey:@"FLAG_FOR_MATCH_ANGLE"]){
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Change Secure Alignment" message:@"To change your Secure Alignment   first spin the pointer to current angle and Click OK to continue." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            //if(sharedData.flagForDisableSecureAligment)
            if([defaults objectForKey:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT"])
            {
                
                [_myNavBar setTitle:@"Disable Secure Alignment"];
                [_btnCapture setTitle:@"Disable Secure Alignment" forState:UIControlStateNormal];
                
                NSString *mes = [NSString stringWithFormat:@"To Disable your Secure Alignment   first spin the pointer to current angle and Click OK to continue."];
                UIAlertView *alert= [[UIAlertView alloc]initWithTitle:@"Disable Secure Alignment" message:mes delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        }
    }
    self.viewAlert.hidden = YES;
    [self.imgViewanimation stopAnimating];
    [self.imgViewanimation removeFromSuperview];
    self.view.backgroundColor = [UIColor whiteColor];
    self.txtDescription.hidden = YES;
    self.btnCapture.hidden = NO;
    self.btnSkip.enabled = YES;
    self.imgTilt.hidden = NO;
    self.lblTiltNo.hidden = NO;
    self.myCircleView.hidden = NO;
    [self setCircleView];
}


- (IBAction)ClickSetSecureAlignment:(id)sender {
    [motionManager stopDeviceMotionUpdates];
    self.lblTiltNo.text = [NSString stringWithFormat:@"%i",self.draw2DView.z];
    [sharedData newloadAppStatus];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    //if(sharedData.dTiltValue!=nil)
    if([defaults objectForKey:@"SECURE_TILT_VALUE"]!=nil)
    {
        //if(sharedData.flagForMatchAngle || sharedData.flagForDisableSecureAligment){
        if([defaults objectForKey:@"FLAG_FOR_MATCH_ANGLE"]||[defaults objectForKey:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT"]){
            
            // if([sharedData.dTiltValue isEqualToString:self.lblTiltNo.text] || isAngleMatched==YES)
            if([[defaults objectForKey:@"SECURE_TILT_VALUE"] isEqualToString:self.lblTiltNo.text]||isAngleMatched==YES)
            {
                //if(sharedData.flagForMatchAngle)
                if([defaults objectForKey:@"FLAG_FOR_MATCH_ANGLE"])
                  {
                            if(isAngleMatched==YES){
                            NSLog(@"Save Value");
                                [defaults setObject:self.lblTiltNo.text forKey:@"SECURE_TILT_VALUE"];
                                sharedData.dTiltValue=self.lblTiltNo.text;
        
                                [defaults setObject:@"SECURITY_TYPE_SECUREALIGNMENT" forKey:@"SECURITY_TYPE"];                                sharedData.isTiltEnable=YES;
                                
                                [sharedData newsaveAppStatus];
                                isAngleMatched=NO;
                                
                                [defaults removeObjectForKey:@"FLAG_FOR_MATCH_ANGLE"];
                                sharedData.flagForMatchAngle=NO;
                            
                                [defaults synchronize];
                                 NSString *mes = [NSString stringWithFormat:@"Secure Alignment Changed to %@",self.lblTiltNo.text];
                                changealertView= [[UIAlertView alloc]initWithTitle:@"Change Alignment" message:mes delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                [changealertView show];

                        }
                            else{
                                NSString *mes = [NSString stringWithFormat:@"Now spin the angle to an angle of your choice Click OK to continue."];
                                changealertView1= [[UIAlertView alloc]initWithTitle:@"Change Alignment" message:mes delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                [changealertView1 show];
                            }
                   }
                //if(sharedData.flagForDisableSecureAligment)
                if([defaults objectForKey:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT"])
                {
                NSString *mes = [NSString stringWithFormat:@"Secure Alignment has been disabled You can re-enable it at any time"];
                 disablealertView1= [[UIAlertView alloc]initWithTitle:@"Disable Alignment" message:mes delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [disablealertView1 show];
            }
                sharedData.outPut=5;
                [sharedData newsaveAppStatus];
                [sharedData newloadAppStatus];
                
                [defaults removeObjectForKey:@"LOCKED_COUNT"];
                [defaults synchronize];
                
        }
        else //if(![sharedData.dTiltValue isEqualToString:self.lblTiltNo.text])
        {
            NSLog(@"DOES NOT TITL match");
            //give error
            NSLog(@"number of attempt %d",sharedData.outPut);
            [sharedData decreaseoutPut];
            
            NSString *msg; //=[NSString stringWithFormat:@"Please tilt your device to correct alignment to proceed. You have %d attempts remaining.",sharedData.outPut];
            
            if(sharedData.outPut>0){
                
                NSLog(@"greater than Zero");
                msg=[NSString stringWithFormat:@"Please tilt your device to correct alignment to proceed. You have %d attempts remaining.",sharedData.outPut];
                erroralertView = [[UIAlertView alloc]initWithTitle:@"Incorrect Secure Alignment" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [erroralertView show];
            }else{
                
                 NSTimeInterval timeInMiliseconds = [[NSDate date] timeIntervalSince1970];
                if([defaults objectForKey:@"LOCKED_COUNT"])
                {
                    NSNumber *locked_count=[defaults objectForKey:@"LOCKED_COUNT"];
                    int temp_lock_count=locked_count.intValue+1;
                    
                    timeInMiliseconds=timeInMiliseconds+(temp_lock_count*300);
                    
                    locked_count=[NSNumber numberWithInt:temp_lock_count];
                    [defaults setObject:locked_count forKey:@"LOCKED_COUNT"];
                    
                    NSNumber *locked_value=[NSNumber numberWithLongLong:timeInMiliseconds];
                    [defaults setObject:locked_value forKey:@"LOCKED_VALUE"];
                    [defaults synchronize];
                    
                    NSString *message=[NSString stringWithFormat:@"The application is locked for %i minutes because you have exceeded the limit for providing your Secure Alignment ",temp_lock_count*5];
                    
                    erroralertView1=[[UIAlertView alloc]initWithTitle:@"Application Locked" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [erroralertView1 show];
                }else{
                    
                   
                    
                    NSNumber *locked_count=[NSNumber numberWithInt:1];
                    [defaults setObject:locked_count forKey:@"LOCKED_COUNT"];
                    
                    NSNumber *locked_value=[NSNumber numberWithLongLong:timeInMiliseconds+300];
                    [defaults setObject:locked_value forKey:@"LOCKED_VALUE"];
                    [defaults synchronize];
                    
                    erroralertView1=[[UIAlertView alloc]initWithTitle:@"Application Locked" message:@"The application is locked for 5 minutes because you have exceeded the limit for providing your Secure Alignment " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [erroralertView1 show];
                }
            }
        
            
        }
    }else{
        NSString *mes = [NSString stringWithFormat:@"Do you want to save this value (%i)? Click Save to store it or Capture Again to capture a different angle.",self.draw2DView.z];
        setalertView = [[UIAlertView alloc]initWithTitle:@"Save Capture Angle" message:mes delegate:self cancelButtonTitle:@"Save" otherButtonTitles:@"Capture Again", nil];
        [setalertView show];
    }
    }else{
    
        NSString *mes = [NSString stringWithFormat:@"Do you want to save this value (%i)? Click Save to store it or Capture Again to capture a different angle.",self.draw2DView.z];
        setalertView = [[UIAlertView alloc]initWithTitle:@"Save Capture Angle" message:mes delegate:self cancelButtonTitle:@"Save" otherButtonTitles:@"Capture Again", nil];
        [setalertView show];
    
    }
}
#pragma mark UIAlertViewDelegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    if(alertView==erroralertView1){
        sharedData.isDeviceLocked=YES;
        [sharedData newsaveAppStatus];
        [sharedData newloadAppStatus];
        
       [self performSegueWithIdentifier:@"firstView" sender:self];
    }
    
    if (alertView == touchIdsetupalert)
    {
        sharedData.isTouchIDDevice = YES;
        //[defaults removeObjectForKey:@"SECURITY_TYPE"];
        sharedData.isTouchIDEnable = NO;
        [defaults removeObjectForKey:@"IS_SECURITY_SET"];
        sharedData.isSecuritySet = NO;
        [defaults synchronize];
        
        NSLog(@"########################CaptureVC SAVE %@ ", sharedData.profileName);
        [sharedData newsaveAppStatus];
        [self performSegueWithIdentifier:@"firstView" sender:self];
    }
    if (alertView == erroralert)
        if (buttonIndex == 0) {
            [self viewWillAppear:YES];
            NSLog(@"TOUCHID ERROR");
        }
    if (alertView == setalertView) {
        if (buttonIndex == 0) {
            NSLog(@"savetilt value %@",self.lblTiltNo.text);
            
            [defaults setObject:@"IS_OUT_SET" forKey:@"IS_OUT_SET"];
            [defaults setObject:@"SECURITY_TYPE_SECUREALIGNMENT" forKey:@"SECURITY_TYPE"];
            [defaults setObject:self.lblTiltNo.text forKey:@"SECURE_TILT_VALUE"];
            [defaults synchronize];
            
            sharedData.isOutSET = YES;
            sharedData.isTiltEnable = YES;
            [motionManager stopDeviceMotionUpdates];
            sharedData.dTiltValue =self.lblTiltNo.text;
            NSLog(@"CAPTURE SETALERTVIEW-> SAVE -> PROFILENAME: %@",sharedData.profileName);
            [sharedData newsaveAppStatus];
            [sharedData newloadAppStatus];
            //sharedData=[SharedData sharedData];
            //
            // NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
            if([defaults objectForKey:@"GO_TO_HOME"]!=nil)
            {
                [defaults removeObjectForKey:@"GO_TO_HOME"];
                [defaults synchronize];
                
                if(_callbackMe && [_callbackMe respondsToSelector:@selector(authenticateSuccess:)]){
                    [_callbackMe performSelector:@selector(authenticateSuccess:) withObject:nil afterDelay:0.1];
                }else{
                    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"Settings"];
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }
            
            
            if([defaults objectForKey:@"ACTIVATION_SUCCESS"] !=nil)
            {
                if([[defaults objectForKey:@"ACTIVATION_SUCCESS"] isEqualToString:@"1"])
                {
                    [defaults removeObjectForKey:@"ACTIVATION_SUCCESS"];
                    
                    //[defaults setObject:@"0" forKey:@"ACTIVATION_SUCCESS"];
                [defaults synchronize];
                [self performSegueWithIdentifier:@"firstView" sender:nil];
                }
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        else {
            [self setCircleView];
            [setalertView release];
        }
    }
    if (alertView == changealertView) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    if(alertView==changealertView1){
        
        isAngleMatched=YES;
        
               
    }
    
    if (alertView == disablealertView)
    {
        if (buttonIndex == 0) {
            //the value need to be reset.
            [disablealertView release];
        }
    }
    if(alertView==disablealertView1){
        
        NSLog(@"isTiltEnable");
        [defaults removeObjectForKey:@"SECURITY_TYPE"];
        sharedData.isTiltEnable = NO;
        sharedData.flagForDisableSecureAligment=NO;
        sharedData.flagForMatchAngle=NO;
        [defaults removeObjectForKey:@"FLAG_FOR_MATCH_ANGLE"];
        [defaults removeObjectForKey:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT"];
        //  sharedData.dTiltValue=nil;
        [defaults synchronize];
        [sharedData newsaveAppStatus];
        [sharedData newloadAppStatus];
        NSLog(@"###########Value Disabled #############CaptureVC SAVE %@ ", sharedData.profileName);
        
        // [self.navigationController popViewControllerAnimated:YES];
        
        // [self performSegueWithIdentifier:@"firstView" sender:self];
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        if([defaults objectForKey:@"DISABLETOUCHID"]){
            // We first want to disable the touch id set
            [defaults removeObjectForKey:@"AUTHENTICATION_METHOD"];
            [defaults synchronize];
        }
        
        if(_callbackMe && [_callbackMe respondsToSelector:@selector(authenticateSuccess:)]){
            [_callbackMe performSelector:@selector(authenticateSuccess:) withObject:nil afterDelay:0.1];
        }else{
            UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"Settings"];
            [self.navigationController pushViewController:vc animated:YES];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if (alertView == skipalertView) {
        sharedData.isSecuritySet = YES;
        //@"IS_SECURITY_SET"
//        [defaults setObject:@"IS_SECURITY_SET" forKey:@"IS_SECURITY_SET"];
//        [defaults synchronize];
    
        NSLog(@"########################CaptureVC SAVE skipalertView %@ ", sharedData.profileName);
        // sharedData.dTiltValue=nil;
        [sharedData newsaveAppStatus];
        [self.navigationController popViewControllerAnimated:YES];
        //   [self performSegueWithIdentifier:@"firstView" sender:self];
    }
    
    if (alertView == erroralertView) {
        if (sharedData.isDeviceLocked == YES) {
            self.time = self.time*sharedData.outPut;
            lockTimer = (NSTimer*)self.time;
            
            lockTimer = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(lockApp) userInfo:nil repeats:NO];
            [erroralertView release];
        }
        else{
            [self setCircleView];
            [erroralertView release];
        }
    }
    
    if (alertView == timealertView) {
        NSLog(@"timeout locked go back first screen %@",sharedData.profileName);
        sharedData.isDeviceLocked = NO;
        [sharedData newsaveAppStatus];
    }
    
}

-(void)EnableFaceAfterDisableAlignment{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    
    if([[defaults objectForKey:@"IS_ENROLLED"]isEqualToString:@"Enrolled"])
    {
        // IF Already Enrolled
        sharedData.isFaceRecognised=YES;
       [defaults setObject:@"SECURITY_TYPE_FACEVOICE" forKey:@"SECURITY_TYPE"];
        [defaults synchronize];
        sharedData.isTiltEnable=NO;
        sharedData.isTouchIDEnable=NO;
        [sharedData newsaveAppStatus];
        [sharedData newloadAppStatus];
        [self.navigationController popViewControllerAnimated:YES];
        //[self performSegueWithIdentifier:@"firstView" sender:self];
    }else{
        // Do Enrollment
        // set NotEnrolled for Key IS_ENROLLED
        [defaults setObject:@"GO_TO_HOME" forKey:@"GO_TO_HOME"];
        [defaults synchronize];

        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:@"NotEnrolled" forKey:@"IS_ENROLLED"];
        // set DoEnrollment for Key DoEnrollment
        [defaults setObject:@"NO" forKey:@"TURNOFFFACE"];
        [defaults removeObjectForKey:@"APPROVE_OR_DENY"];
        [defaults removeObjectForKey:@"GENERATE_OTP"];
        //[defaults setObject:@"0" forKey:@"GENERATE_OTP"];
        
        // [defaults setObject:@"0" forKey:@"APPROVE_OR_DENY"];
        [defaults setObject:@"DoEnrollment" forKey:@"FACE_REC"];
        [defaults synchronize];
        
        VVDemoRecognizerController *vc = (VVDemoRecognizerController *)[self.storyboard instantiateViewControllerWithIdentifier:VVDemoRecognizer];
        vc.callbackMe = self;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
    

}

@end
