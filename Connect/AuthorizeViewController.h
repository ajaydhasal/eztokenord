//
//  AuthorizeViewController.h
//  EZToken
//
//  Created by Harsha on 9/2/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Draw2D.h"
#import <CoreLocation/CoreLocation.h>
#import "SharedData.h"

@interface AuthorizeViewController : UIViewController<Draw2DDelegate, UIAlertViewDelegate,CLLocationManagerDelegate>
{
    SharedData *sharedData;
     UIAlertView *erroralertView,*erroralertView1;
}
@property (nonatomic, strong)NSMutableDictionary *dict;
@property (nonatomic, strong)NSMutableString *combStr;
@property (nonatomic, strong)NSArray *arrmsg;
@property (nonatomic, strong)NSString *txref;
@property (nonatomic, strong)NSString *txstat;
@property (nonatomic) BOOL bOfflineMode;
@property (retain, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (retain, nonatomic) IBOutlet UILabel *lbl1;
@property (retain, nonatomic) IBOutlet UILabel *lbl2;
@property (retain, nonatomic) IBOutlet UILabel *lbl3;
@property (retain, nonatomic) IBOutlet UILabel *lbl4;
@property (retain, nonatomic) IBOutlet UILabel *lbl5;
@property (retain, nonatomic) IBOutlet UILabel *lblGroup;
@property (retain, nonatomic) IBOutlet UILabel *lblApp;
@property (retain, nonatomic) IBOutlet UILabel *lbl6;
@property (retain, nonatomic) IBOutlet UILabel *lbl7;
@property (retain, nonatomic) IBOutlet UILabel *lblTime;
@property (retain, nonatomic) IBOutlet UILabel *lblLocation;
@property (retain, nonatomic) IBOutlet UILabel *lblGeoCoord;
@property (retain, nonatomic) IBOutlet UILabel *lblDeviceName;
@property (retain, nonatomic) IBOutlet UILabel *lblClientIp;
@property (retain, nonatomic) IBOutlet UILabel *lblServerIp;
@property (retain, nonatomic) IBOutlet UILabel *lblTransMsg;
@property (retain, nonatomic) IBOutlet UIView *myCirleView;
@property (retain, nonatomic) IBOutlet UILabel *lblValue;
- (IBAction)ClickAllow:(id)sender;
- (IBAction)ClickDeny:(id)sender;
@property (nonatomic, strong)Draw2D *draw2DView;
- (IBAction)ClickSecureAlignment:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnSecureAlignment;
@property (retain, nonatomic) IBOutlet UIButton *btnAutomation;
- (IBAction)ClickAutomate:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *lblAutomation;
@property (strong, nonatomic)    CLLocationManager *locationManager;
@property (nonatomic) double distance;
@property (nonatomic) double clatitude;
@property (nonatomic) double clongitude;
@property (retain, nonatomic) IBOutlet UIButton *btnDeny;
@property (retain, nonatomic) IBOutlet UIButton *btnApprove;
-(void)approved:(id)sender;
-(void)denied:(id)sender;
-(void)generateOTP1:(id)sender;
//- (void) enrollSuccess;
//- (void) authenticateSuccess;

@end