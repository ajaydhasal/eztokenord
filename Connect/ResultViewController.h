//
//  ResultViewController.h
//  EZToken
//
//  Created by Harsha on 10/10/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController
@property (retain, nonatomic) IBOutlet UIImageView *imgStatus;
@property (retain, nonatomic) IBOutlet UILabel *lblStatus;
- (IBAction)ClickOk:(id)sender;
@property (strong, nonatomic)NSDictionary *dict;
@end
