//
//  settings.h
//  IOSVVDemo
//
//  Created by Karl Ridgeway on 9/4/14.
//  Copyright (c) 2014 Sensory. All rights reserved.
//

#ifndef IOSVVDemo_settings_h
#define IOSVVDemo_settings_h

#define KEY_DEFAULTS_INITIALIZED @"DefaultsInitialized"
#define KEY_REQUIRE_BOTH @"RequireBoth"
#define KEY_FACE_MOTION_ANALYSIS @"FaceMotionAnalysis"
#define KEY_USE_ADAPTIVE_ENROLLMENT @"UseAdaptiveEnrollment"
#define KEY_REQUIRE_CONFIRMATION @"RequireConfirmation"
#define KEY_AUTHENTICATION_TIMEOUT @"AuthenticationTimeout"
#define KEY_SECURITY_LEVEL @"SecurityLevel"

#endif
