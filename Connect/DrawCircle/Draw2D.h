//
//  Draw2D.h
//  Draw2D
//
//  Created by Venus Kim on 10/29/14.
//  Copyright (c) 2014 EZMCOM. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Draw2D;

@protocol Draw2DDelegate
- (void)rateView:(Draw2D *)rateView ratingDidChange:(int)rating;
@end

//@protocol Draw2DDelegate
//-(void)updateZ:(int)x;
//@end

@interface Draw2D : UIView
@property (strong, nonatomic) IBOutlet UILabel *lblAngle;
@property (nonatomic) int z;
@property (nonatomic, strong) id<Draw2DDelegate> delegate;
@end
