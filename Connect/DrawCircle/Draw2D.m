//
//  Draw2D.m
//  Draw2D
//
//  Created by Venus Kim on 10/29/14.
//  Copyright (c) 2014 EZMCOM. All rights reserved.
//

#import "Draw2D.h"
#import <Foundation/Foundation.h>

@implementation Draw2D

#define  NUM_SEGMENTS 36

bool coordsSet = NO;
float pointerStartX, pointerStartY, pointerEndX, pointerEndY;
float coords[NUM_SEGMENTS][2];
float cx, cy,needleHeadColorR = 0.0f,needleHeadColorG = 137.0f,needleHeadColorB = 123.0f;
bool isTouched = YES;
int colorFactor = 20;
NSTimer* timer;

@synthesize z;
@synthesize delegate;


- (void)baseInit {
    self.delegate = nil;
    self.z = 0;
}

//-(id)initWithCoder:(NSCoder *)decoder{
//    if(self = [super initWithCoder:decoder])
//    {
//        
//    }
//    return self;
//}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //[self baseInit];
        pointerStartX = 0;
        pointerStartY = 0;
        coordsSet = NO;
    }
    return self;
}

- (void)setRating:(int)rating {
    //NSLog(@"%i",rating);
    self.z = rating;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
//    if (delegate == nil) {
//        NSLog(@"delegate is nil");
//    }
//    else
//    NSLog(@"delegate ********");

    if(coordsSet == NO)
    {
        float width = self.bounds.size.width;
        float height = self.bounds.size.height;
        cx = width/2, cy=height/2-80;
        float radius = .85f * cx;
        
        //The array
        
        
        //NSLog(@"width is %.0f, height is %.0f radius is %.0f",width,height,radius);
        //NSLog(@"centerX is %.0f, centerY is %.0f",cx,cy);
        
        for (int i = 0; i < NUM_SEGMENTS; i++) {
            
            float theta = 2.0f * 3.1415926 * i / NUM_SEGMENTS;
            float x = radius * cos(theta);
            float y = radius * sin(theta);
            coords[i][0] = x + cx;
            coords[i][1] = y + cy;
        }
        coordsSet = YES;
        pointerStartX = coords[27][0];
        pointerStartY = coords[27][1];
    }
    
   // NSLog(@"pointerStartX %0.f, pointerStartY %.0f",pointerStartX,pointerStartY);
    
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 5.0);
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    CGFloat components[] = {48/255.0, 108/255.0, 164/255.0, 1.0};
    CGColorRef color = CGColorCreate(colorspace, components);
    CGContextSetStrokeColorWithColor(context, color);
    CGContextSetFillColorWithColor(context, color);
    
    for(int i=0;i<NUM_SEGMENTS;i++){
        //NSLog(@"%d: [%f],[%f]",i,coords[i][0],coords[i][1]);
        CGContextFillEllipseInRect(context, CGRectMake(coords[i][0], coords[i][1], 6, 6));
    }
    
    //needle
    CGContextMoveToPoint(context, cx, cy);
    CGContextAddLineToPoint(context, pointerStartX, pointerStartY);
    CGContextStrokePath(context);
 
    if(isTouched == YES){
        //needle head
        CGContextFillEllipseInRect(context, CGRectMake(pointerStartX-8, pointerStartY-8, 16, 16));
    }
    else
    {
        CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
        CGFloat components[] ={needleHeadColorR/255,needleHeadColorG/255,needleHeadColorB/255, 1.0};
        CGColorRef color = CGColorCreate(colorspace, components);
        CGContextSetStrokeColorWithColor(context, color);
        CGContextSetFillColorWithColor(context, color);
        CGContextFillEllipseInRect(context, CGRectMake(pointerStartX-8, pointerStartY-8, 16, 16));
        CGColorSpaceRelease(colorspace);
        CGColorRelease(color);
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSArray *touchArray = [touches allObjects];
    
    // Only run the following code if there is more than one touch
    
    if ([touchArray count] == 1)
    {
        // We're going to track the first two touches (i.e. first two fingers)
        // Create "UITouch" objects representing each touch
        UITouch *fingerOne = [touchArray objectAtIndex:0];
        
        // Convert each UITouch object to a CGPoint, which has x/y coordinates we can actually use
        CGPoint pointOne = [fingerOne locationInView:[fingerOne view]];
        // The touch points are always in UIKit coordinates
//        for(int i=0;i<NUM_SEGMENTS;i++){
//            if( (pointOne.x >= coords[i][0] && pointOne.x <= coords[i][0]+50) && (pointOne.y >= coords[i][1] && pointOne.y <= coords[i][1]+50))
//                    NSLog(@"touchBegan at %d: %f,%f",i,pointOne.x,pointOne.y);
//                }
    }
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSArray *touchArray = [touches allObjects];
    
    // Only run the following code if there is more than one touch
    
    if ([touchArray count] == 1)
    {
        // We're going to track the first two touches (i.e. first two fingers)
        // Create "UITouch" objects representing each touch
        UITouch *fingerOne = [touchArray objectAtIndex:0];
        
        // Convert each UITouch object to a CGPoint, which has x/y coordinates we can actually use
        CGPoint pointOne = [fingerOne locationInView:[fingerOne view]];
        
        // The touch points are always in UIKit coordinates
        for(int i=0;i<NUM_SEGMENTS;i++){
            if( (pointOne.x >= coords[i][0]-20 && pointOne.x <= coords[i][0]+20) && (pointOne.y >= coords[i][1]-20 && pointOne.y <= coords[i][1]+20)){
                //NSLog(@"touchMoved at %d: %f,%f",i,pointOne.x,pointOne.y);
                
                self.z = i >= 27 ? 360 % (360-abs(27-i)*10) :360-abs(i-27)*10;
                [self setRating:self.z];
                [self.delegate rateView:self ratingDidChange:self.z];
                //NSLog(@"points of angel z: %i", self.z);
                self.lblAngle.text = [NSString stringWithFormat:@"%i",self.z];
                pointerStartX = coords[i][0];
                pointerStartY = coords[i][1];
                [self setNeedsDisplay];
            }
        }
    }    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //NSLog(@"touchEnded");
    isTouched = NO;
    timer = [NSTimer scheduledTimerWithTimeInterval: 0.2
                                             target: self
                                           selector:@selector(onTick:)
                                           userInfo: nil repeats:YES];
    NSRunLoop *runner = [NSRunLoop currentRunLoop];
    [runner addTimer:timer forMode: NSDefaultRunLoopMode];
    //self.lblAngle.text = [NSString stringWithFormat:@"%i",self.z];
    //NSLog(@"touchesEnded points of angel z: %@", self.lblAngle.text);
    
    
}

-(void)onTick:(NSTimer *)timer {
    //update the color
    needleHeadColorR += colorFactor;
    needleHeadColorG += colorFactor;
    needleHeadColorB += colorFactor;
    
    if(needleHeadColorR >= 118)
    {
        needleHeadColorR =118;
        colorFactor = -20;
    }else if(needleHeadColorR <=0)
    {
        needleHeadColorR = 0;
        colorFactor = 20;
    }
    
    if(needleHeadColorG >= 255)
    {
        needleHeadColorG =255;
        colorFactor = -20;
    }else if(needleHeadColorG <=137)
    {
        needleHeadColorG = 137;
        colorFactor = 20;
    }
    
    if(needleHeadColorB >= 241)
    {
        needleHeadColorB =241;
        colorFactor = -20;
    }else if(needleHeadColorB <=123)
    {
        needleHeadColorB = 123;
        colorFactor = 20;
    }
    [self setNeedsDisplay];
}

@end
