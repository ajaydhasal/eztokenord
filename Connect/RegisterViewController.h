//
//  ViewController.h
//  Connect
//
//  Created by Harsha on 8/27/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoryboardID.h"
#import "MBProgressHUD.h"
#import "SharedData.h"
#import "VVDemoRecognizerController.h"
#import "Draw2D.h"

@interface RegisterViewController : UIViewController <UIActionSheetDelegate,Draw2DDelegate,UIAlertViewDelegate>{
    MBProgressHUD *hud;
    NSTimer *timer;
    UIView *OTPView;
    UIProgressView *prg;
    SharedData *sharedData;
    UIBarButtonItem *leftBtn;
    UIAlertView *erroralertView,*erroralertView1;
}
@property (retain, nonatomic) IBOutlet UIBarButtonItem *btnSetting;
@property (retain, nonatomic) IBOutlet UINavigationItem *myNavigationBar;
@property (strong, nonatomic) IBOutlet UITextField *txtPhoneNumber;

@property (strong, nonatomic) IBOutlet UIButton *btnActivation;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (IBAction)Menu:(id)sender;
@property (strong, nonatomic) UIActionSheet *popupMenu;
@property (strong, nonatomic) UIActionSheet *tableMenu;
@property (retain, nonatomic) IBOutlet UITableView *myTableView;
@property (retain, nonatomic) IBOutlet UITextView *txtDescription;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *btnEdit;
@property (nonatomic, strong)Draw2D *draw2DView;


@property (retain, nonatomic) IBOutlet UIView *myCirleView;
@property (retain, nonatomic) IBOutlet UILabel *lblValue;
@property (retain, nonatomic) IBOutlet UIButton *btnSecureAlignment;
@property (retain,nonatomic ) IBOutlet UIView *viewAppLocked;
@property (retain,nonatomic ) IBOutlet UILabel *lblMessage;


- (IBAction)ClickSecureAlignment:(id)sender;

- (IBAction)btnSecureAligmentTapped:(id)sender;


- (IBAction)btnEditClicked:(id)sender;
-(void) generateOTP:(id)sender;

- (void)ClickAllow:(id)sender;
- (void)ClickDeny:(id)sender;

@end
