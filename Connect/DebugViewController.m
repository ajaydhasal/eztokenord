//
//  DebugViewController.m
//  EZToken
//
//  Created by Harsha on 12/3/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import "DebugViewController.h"

@interface DebugViewController ()

@end

@implementation DebugViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIColor * color = [UIColor colorWithRed:92.0 /255.0f green:171.0/255.0f blue:222.00/255.0f alpha:1.0f];
    self.navigationController.navigationBar.barTintColor = color;

    
    // Do any additional setup after loading the view.
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    NSString *fileDate = [dateFormatter stringFromDate:currDate];
    
    NSString *docPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *fileName = [NSString stringWithFormat:@"%@/EzTokenLog_%@.txt", docPath, fileDate];
    NSString *dataFile = [NSString stringWithContentsOfFile:fileName encoding:NSUTF8StringEncoding error:nil];
    
    self.txtView.text = dataFile;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.txtView.delegate = self;
    [self.txtView setScrollEnabled:YES];
    [self.txtView setUserInteractionEnabled:YES];
    [self.txtView setBackgroundColor:[UIColor clearColor]];
    //[textview setText:@"Hi,I'm working fine with this space"];
    [self.view addSubview:self.txtView];
    
    //send mail
    //[self sendMail];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ClickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

// in the text view's delegate
- (void)textViewDidChangeSelection:(UITextView *)textView
{
    [textView scrollRangeToVisible:textView.selectedRange];
}

- (void)sendMail
{
    //email subject
    NSString * subject = @"send mail test";
    //email body
    NSString * body = @"How did you find the Android-IOS-Tutorials Website ?";
    //recipient(s)
    NSArray * recipients = [NSArray arrayWithObjects:@"contact@androidiostutorials.com", nil];
    
    //create the MFMailComposeViewController
    MFMailComposeViewController * composer = [[MFMailComposeViewController alloc] init];
    if(composer == nil){
        NSLog(@"Mail NIL");
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"NO MAIL" message:@"Please setup you mail" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertview show];
    }
    else{
        composer.mailComposeDelegate = self;
        [composer setSubject:subject];
        [composer setMessageBody:body isHTML:NO];
        [composer setToRecipients:recipients];
        
        //get the filepath from resources
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"logo" ofType:@"png"];
        
        //read the file using NSData
        NSData * fileData = [NSData dataWithContentsOfFile:filePath];
        // Set the MIME type
        /*you can use :
         - @"application/msword" for MS Word
         - @"application/vnd.ms-powerpoint" for PowerPoint
         - @"text/html" for HTML file
         - @"application/pdf" for PDF document
         - @"image/jpeg" for JPEG/JPG images
         */
        NSString *mimeType = @"image/png";
        
        //add attachement
        [composer addAttachmentData:fileData mimeType:mimeType fileName:filePath];
        
        //present it on the screen
        
        [self presentViewController:composer animated:YES completion:NULL];
    }
    
    
}
#pragma mark - MFMailComposeViewControllerDelegate methods
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [super dealloc];
}
- (IBAction)ClearLogs:(id)sender {
    self.txtView.text = @"";
}

@end
