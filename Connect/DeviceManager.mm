//
//  DeviceManager.cpp
//  EZToken
//
//  Created by Harsha on 9/19/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#include "DeviceManager.h"
#import "SharedData.h"
#import <CommonCrypto/CommonDigest.h>
#import <UIKit/UIDevice.h>

static DeviceManager *_sharedDeviceManagerInstance = NULL;

DeviceManager* sharedDeviceManager()
{
	if (_sharedDeviceManagerInstance == NULL)
    {
		_sharedDeviceManagerInstance = new DeviceManager();
	}
	
	return _sharedDeviceManagerInstance;
}

DeviceManager::~DeviceManager () {}

DeviceManager::DeviceManager()
{
	// Secret byte array
	m_secret = NULL;
	
	m_cashMode = 0; // OTP=0, OTP_CR=1, OTP_SIG=2, OTP_CR_SIG=3, ALGO_NOTSUPPORTED=4
	m_cashType = 1; // HOTP=0, TOTP=1
	m_pinsupport = true; // PIN Support
	m_pbkdmode = 0; // 0:PIN REQUIRED 1:PRANK
	m_rcdisplay = true; // Show RC
    
	m_registrationcode = NULL;
	m_serialnumber = NULL;
	m_unlockchallenge = NULL;
}

const char* DeviceManager::getMacAddress()
{
    NSUUID *uuid = [[UIDevice currentDevice]identifierForVendor];
    NSString *deviceId = [uuid UUIDString];
    const char* cfilename = [deviceId UTF8String];
    return cfilename;
}

void DeviceManager::setSecretFilePath(const char *filePath)
{
	m_secret_file_path = new char[256];
	strcpy(m_secret_file_path, filePath);
    NSLog(@"setSecretFilePath [%s]",m_secret_file_path);
}

char *DeviceManager::generateSerialNo12(int tkExpType)
{
    int errorcode = 0;
    time_t curtime;
    curtime = time(NULL);
    
    char timestamp[30];
    sprintf(timestamp, "%lu", curtime);
    
	return ::generateSerialNo(tkExpType, timestamp, strlen(timestamp), &errorcode);
}

char *DeviceManager::generateSerialNo16(int tkExpType, const char *p1p2, const char *v1v2)
{
    int errorcode = 0;
    time_t curtime;
    curtime = time(NULL);
    
    char timestamp[30];
    sprintf(timestamp, "%lu", curtime);
    
    char *sn = ::generateNewSerialNo(tkExpType, timestamp, strlen(timestamp), (char *) p1p2, (char *) v1v2, &errorcode);
    
	return sn;
}


int DeviceManager::createDevice(const char* activeCode, const char* strEventsTimeSeed, const char *serialNumber)
{
	char *ac1 = const_cast< char* > (activeCode);
	int errorcode = 0;
	int ret = ::verifyActivationCode(ac1, &errorcode);
    
	if (ret == 0)
    {
		char buf[128];
		strcpy(buf, strEventsTimeSeed);
		
		char *sn1 = const_cast< char* > (serialNumber);
		
        deviceObj = ::activateToken(sn1, ac1, buf, &errorcode);
        
		if (deviceObj != NULL)
        {
            //set deviceRC for SharedData
            SharedData *sharedData = [SharedData sharedData];
            const char *rc = deviceObj->getRegistrationCode();
            if (sharedData.deviceRC !=nil) {
                sharedData.deviceRC = [NSString stringWithUTF8String:rc];
            }
            else{
                const char *rc = deviceObj->getRegistrationCode();
                sharedData.deviceRC = [NSString stringWithUTF8String:rc];
                NSLog(@"DEVICE RC IS NULL force %s",deviceObj->getRegistrationCode());
            }
            sharedData.isOutOn = deviceObj->getPinType() == 1;
            NSLog(@"DEVICE CREATED");
			//restoreDeviceInfo(deviceObj);
			//if (SHOULD_LOG_DEVICE_LOAD_INFO) showDeviceInfo(deviceObj);
            
            //m_unlockchallenge = deviceObj->getUnlockChallenge();
            //if (SHOULD_LOG_DEVICE_LOCK_INFO) printf("Unlock Challenge: [%s]\n", m_unlockchallenge);
           
//            if (!sharedData.isOutOn) {
//                //NSLog(@"Create Device persisting device Pin Not enabled");
//                char * fixPin =  const_cast< char* >((getMacAddress()));
//                const char *hash = getHash(fixPin, strlen(fixPin));
//                char * defaultDevicePin = const_cast< char* > (hash);
//                
//                char *def_pin = const_cast< char* > (defaultDevicePin);
                const char *duid1 = getMacAddress();
                char *duid = const_cast< char* > (duid1);
                m_secret = deviceObj->persist(duid, duid);
//            }
			return ret;
		}
        else
        {
			return -1;
		}
	}
	return -1;
}

bool DeviceManager::persistDeviceNew(Device *device, const char *pin)
{
	char *pin1 = const_cast< char* > (pin);
    //its new version

    const char *duid1 = getMacAddress();
    char *duid = const_cast< char* > (duid1);
    
    unsigned char *secret = device->persist(pin1, duid);
    
	if (secret != NULL)
    {
        NSLog(@"persistDeviceNew [%s]",m_secret_file_path);
		return writeSecretToFile(m_secret_file_path, (char *)secret);
	}
    else
    {
        NSLog(@"persistDeviceNew secret is null");
		deleteSecretFile(m_secret_file_path);
		return false;
	}
}

Device* DeviceManager::resurrectDeviceNew(const char *pin)
{
    char *pin1 = const_cast< char* > (pin);
    const char *duid1 = getMacAddress();
    char *duid = const_cast< char* > (duid1);
    
    NSLog(@"resurrectDeviceNew:: secret file [%s]",m_secret_file_path);
    
    unsigned char *secret = readSecretFromFile(m_secret_file_path);
    
    NSLog(@"duid %s  -  %s", duid, secret);
    if (secret != NULL)
    {
        Device *device = Device::resurrect(secret, duid, duid);
        if (device != NULL)
        {
            NSLog(@"resurrectDeviceNew");
            return device;
        }
        else
        {
            NSLog(@"resurrectDeviceNew returning NULL");
            return NULL;
        }
    }
    else
    {
        NSLog(@"resurrectDeviceNew secret is NULL");
        return NULL;
    }
    
    delete[]secret;
    secret = NULL;
    delete[] pin1;
    pin1 = NULL;
    delete[] duid;
    duid = NULL;
    
}

/*char * DeviceManager::getHash (const char *string, const int strLen)
{
    NSString *clear = [NSString stringWithFormat:@"%s",string];
    
    const char *s=[clear cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData=[NSData dataWithBytes:s length:strlen(s)];
    
    
    unsigned char digest[CC_SHA256_DIGEST_LENGTH];
    
    CC_SHA256(keyData.bytes,keyData.length, digest);
    
    NSData *out=[NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];
    NSString *hash=[out description];
    hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    char *hashStr = (char *) [hash UTF8String];
    
    //NSLog(@"hashStr -----------------------> : %s", hashStr);
    
    //    delete[] s;
    //    s = NULL;
    clear = nil;
    out = nil;
    hash = nil;
    keyData = nil;
    return hashStr;
}
*/
bool DeviceManager::saveDeviceWithPin(const char *pin)
{
//    char * fixPin =  const_cast< char* >((getMacAddress()));
//    const char *key = getMacAddress();
//    const char *duid1 = const_cast< char* > (key);
    const char *key = getMacAddress();
    char *duid = const_cast< char* > (key);
	Device *device = NULL;
    //char *duid = const_cast< char* > (duid);
    if (m_secret == NULL) {
        m_secret = readSecretFromFile(m_secret_file_path);
    }
    
    device = Device::resurrect(m_secret, duid, duid);
    device->reset();
    
	if (device != NULL)
    {
        return persistDeviceNew(device,duid);
	}
    else
    {
		return false;
	}
}

bool DeviceManager::saveDeviceWithoutPin()
{
    char * fixPin =  const_cast< char* >((getMacAddress()));
    const char *key = getMacAddress();
    char *duid = const_cast< char* > (key);
    Device *device = Device::resurrect(m_secret, duid, duid);
	if (device != NULL)
    {
        //NSLog(@"saveDeviceWithoutPin");
        return persistDeviceNew(device,duid);
	}
    else
    {
		return false;
	}
}

bool DeviceManager::changedOut(const char *pinOld, const char *pinNew)
{
	Device *device = resurrectDeviceNew(pinOld);
	if (device != NULL)
    {
        return persistDeviceNew(device,pinNew);
	}
    else
    {
		return false;
	}
}


void DeviceManager::removeDevice()
{
	deleteSecretFile(m_secret_file_path);
}

bool DeviceManager::loadDevice(const char *pin)
{
    Device *device = resurrectDeviceNew(pin);
    
	if (device != NULL)
    {
        //NSLog(@"loadDevice");
            const char * uFullStr =  pin;
            //const char *hash = getHash(uFullStr, strlen(uFullStr));
            persistDeviceNew(device,pin);
            //restoreDeviceInfo(device);
        return true;
	}
    else
    {
		return false;
	}
}


bool DeviceManager::loadDevice()
{
    char * fixPin =  const_cast< char* >((getMacAddress()));
	Device *device = resurrectDeviceNew(fixPin);
    if (device != NULL)
    {
        //NSLog(@"loadDevice NOPIN");
        //call d->reset() since this is the firsttime it will persist for a fresh installation
        device->reset();
        //restoreDeviceInfo(device);
        return true;
	}
    else
    {
		return false;
	}
}

unsigned char* DeviceManager::readSecretFromFile(const char *filePath)
{
	FILE *fd = fopen(filePath , "rb");
	if (fd != NULL)
    {
        fseek(fd, 0, SEEK_SET);
        unsigned char *secret = new unsigned char[1024];
        memset(secret, 0, 1024);
		
		int i = 0;
        while(i < 1024)
        {
			secret[i++] = (unsigned char)fgetc(fd);
		}
        
		fclose(fd);
		return secret;
	}
	return NULL;
}

bool DeviceManager::writeSecretToFile(const char *filePath, const char *bytes)
{
	FILE *fd = fopen(filePath , "wb");
	if (fd != NULL)
    {
		int i = 0;
		while(i < 1024)
        {
			unsigned char ch = bytes[i];
			fputc(ch, fd);
			i++;
		}
        
		fclose(fd);
        NSLog(@"writeSecretToFile returning true");
		return true;
	}
	return false;
}

bool DeviceManager::deleteSecretFile(const char *filePath)
{
	if (remove(filePath) == -1)
    {
        return false;
	}
    else
    {
        return true;
	}
}

/*char* DeviceManager::generateOTP(const char *pin)
{
    Device *device = resurrectDeviceNew(pin);
    if (device != NULL)
    {
        char *otp = device->generateOTP();
        persistDeviceNew(device,pin);
        
        device = nil;
        return otp;
	}
    else
    {
		return NULL;
	}
}*/

char* DeviceManager::generateOTP()
{
    const char *duid1 = getMacAddress();
    char *duid = const_cast< char* > (duid1);
    Device *device = resurrectDeviceNew(duid);
    
	if (device != NULL)
    {
		char *otp = device->generateOTP();
        persistDeviceNew(device,duid);
        device = nil;
		return otp;
	}
    else
    {
		return NULL;
	}
}

/*char* DeviceManager::generateSIGOTP(const char *pin, const char *signature)
{
    char *signature1 = const_cast< char* > (signature);
    Device *device = resurrectDeviceNew(pin);

    
    if (device != NULL)
    {
        char *otp = device->generateSIGOTP(signature1);
        persistDeviceNew(device,pin);
        
        device = nil;
        return otp;
    }
    else
    {
        return NULL;
    }
}*/

long DeviceManager::getClock(){
    const char *duid1 = getMacAddress();
    char *duid = const_cast< char* > (duid1);
    Device *device = resurrectDeviceNew(duid);
    return (long)device->getClock();
}

char* DeviceManager::generateSIGOTP(const char *signature)
{
    const char *duid1 = getMacAddress();
    char *duid = const_cast< char* > (duid1);
    char *signature1 = const_cast< char* > (signature);
    Device *device = resurrectDeviceNew(duid);
    
    if (device != NULL)
    {
        NSLog(@"DM **** generateSIGOTP");
        char *otp = device->generateSIGOTP(signature1);
        
        persistDeviceNew(device,duid);
        
        device = nil;
        return otp;
    }
    else
    {
        NSLog(@"DM NIL");
        return NULL;
    }
}
