//
//  VVDemoAlertView.h
//  IOSVVDemo
//
//  Created by Karl Ridgeway on 9/3/14.
//  Copyright (c) 2014 Sensory. All rights reserved.
//

#import <UIKit/UIKit.h>

// adapts a UIAlertView to be used with blocks instead of delegates
@interface VVDemoAlertView : UIAlertView <UIAlertViewDelegate>

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
            cancelButtonTitle:(NSString *)cancelButtonTitle
            otherButtonTitles:(NSArray *)otherButtonTitles
                cancelHandler:(void (^)(void))cancelHandler
          confirmationHandler:(void (^)(NSInteger otherButtonIndex))confirmationHandler;

@property (nonatomic, copy) void (^confirmationHandler)(NSInteger otherButtonIndex);
@property (nonatomic, copy) void (^cancelHandler)(void);
@end
