//
//  ViewController.m
//  Connect
//
//  Created by Harsha on 8/27/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import "RegisterViewController.h"
#import <Parse/Parse.h>
#import "AuthorizeViewController.h"
#import "CaptureViewController.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>

#define LABEL               [UIFont fontWithName:@"Coda-Regular" size:17]
#define LABEL2               [UIFont fontWithName:@"Coda-Regular" size:14]
@interface RegisterViewController ()
{
    UIAlertView *deletealertView;
    
    
    UIAlertView *setalertView;
    UIAlertView *locationalert;
    
    UIAlertView *transerroralert;
    UIAlertView *incorectSAalertView,*noSecurityAlertView;
}
@property (nonatomic, strong) NSArray *jsonArray;
@property (nonatomic, strong) NSMutableArray *groupNames;
@property (nonatomic,retain) NSArray *filelist;
@property (nonatomic,retain) NSString *otpString;
@end

@implementation RegisterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIColor * color = [UIColor colorWithRed:92.0 /255.0f green:171.0/255.0f blue:222.00/255.0f alpha:1.0f];
    self.navigationController.navigationBar.barTintColor = color;
   // [self.navigationController.navigationBar setBackgroundColor:[UIColor redColor]];//[UIColor colorWithRed:92.0 green:171.0 blue:11 alpha:1]];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{UITextAttributeTextColor : [UIColor whiteColor]}];
    
    sharedData=[SharedData sharedData];
    if(sharedData.isAppOpenedFirstTime==YES)
    {
        sharedData.tempIsAppOpenedFirstTimeFlag=1;
        sharedData.isAppOpenedFirstTime=NO;
        
    }
    else{
        sharedData.tempIsAppOpenedFirstTimeFlag=2;
        sharedData.isAppOpenedFirstTime=NO;
    }
    [sharedData newsaveAppStatus];
    // sharedData.dTiltValue = nil;
    //[self listOfDirectory];
    
    [self.myTableView setEditing:NO];
    

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)listOfDirectory
{
    
    self.groupNames = [sharedData load];
    if(self.groupNames.count > 0){
    }
    else if(self.groupNames.count == 0){
        
        sharedData.isOutSET = NO;
        [sharedData newsaveAppStatus];
    }
    
}
- (void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
   //    [self.navigationController.navigationBar setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor],UITextAttributeFont: [UIFont]}];//[UIFont fontWithName:@"Coda-Regular" size:17.0f]
    self.navigationItem.hidesBackButton = YES;
    
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        // Authenticate User
        sharedData.isTouchIDDevice = YES;
    }
    else if (error.code == LAErrorTouchIDNotAvailable)
    {
        NSLog(@"RVc LAErrorTouchIDNotAvailable");
        sharedData.isTouchIDDevice = NO;
        NSLog(@"########################RegisterVC SAVE viewWillAppear %@ ", sharedData.profileName);
        
    }
    else if (error.code == LAErrorTouchIDNotEnrolled)
    {
        NSLog(@"RVc LAErrorTouchIDNotEnrolled");
        sharedData.isTouchIDDevice = YES;
        NSLog(@"########################RegisterVC SAVE viewWillAppear %@ ", sharedData.profileName);
    }
    [sharedData newsaveAppStatus];
    
    [self listOfDirectory];
    if (self.groupNames.count > 0) {
        
        if(![self.myTableView isEditing]){
             [_btnEdit setTitle:@"Edit"];
            [_btnEdit setEnabled:YES];
        }else{
            [_btnEdit setTitle:@"Done"];
            [_btnEdit setEnabled:YES];
        }
        
         self.myTableView.hidden = NO;
         self.txtDescription.hidden = YES;
        [self.myTableView reloadData];
    }
    else
    {
        [_btnEdit setTitle:@""];
        [_btnEdit setEnabled:NO];
         self.myTableView.hidden = YES;
         self.txtDescription.hidden = NO;
    }
 
    
    self.myCirleView.hidden = YES;
    self.lblValue.hidden = YES;
    self.btnSecureAlignment.hidden = YES;
    
//    if(_bOfflineMode){
//        self.btnSecureAlignment.hidden = NO;
//        _btnApprove.hidden = YES;
//        _btnDeny.hidden = YES;
//        self.lblAutomation.hidden = YES;
//        self.btnAutomation.hidden = YES;
//        [self.btnSecureAlignment setTitle:@"Get Authentication Code" forState:UIControlStateNormal];
//    }

    
    //Check whether device is locked or not
    if (sharedData.isDeviceLocked == YES) {
        
        _viewAppLocked.hidden=NO;
        _myCirleView.hidden=YES;
        [_btnEdit setEnabled:NO];
        [_btnSetting setEnabled:NO];
       
        [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(lockApp) userInfo:nil repeats:YES];
    }
    else{
        _viewAppLocked.hidden=YES;
        [_btnEdit setEnabled:YES];
        [_btnSetting setEnabled:YES];
         sharedData.isDeviceLocked=NO;
        [sharedData newsaveAppStatus];
        //[self.navigationController.navigationBar setHidden:NO];
        
    }
    

   
    
}

- (IBAction)Menu:(id)sender {
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"Settings"];
    [self.navigationController pushViewController:vc animated:YES];
    NSLog(@"Setting Clicked");
    
    [self.popupMenu showFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // dismiss keyboard when background is touched
    [self removeTransparentView];
}

- (void) showSerialNumber{
    [sharedData newloadAppStatus];
    OTPView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [OTPView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6f]];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 240)];
    lbl.center = CGPointMake(OTPView.frame.size.width/2, OTPView.frame.size.height/2 - self.navigationController.navigationBar.frame.size.height);
    lbl.backgroundColor = [UIColor whiteColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.layer.borderColor = [UIColor colorWithRed:48/255.0f green:108/255.0f blue:164/255.0f alpha:1.0f].CGColor;//[UIColor colorWithRed:18/255.0f green:123/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    lbl.layer.borderWidth = 2.0f;
    lbl.layer.shadowRadius = 3.0;
    lbl.layer.shadowOpacity = 0.5;
    lbl.layer.masksToBounds = NO;
    lbl.layer.shouldRasterize = YES;
    //
  //  lbl.font = [UIFont fontWithName:@"Coda-Regular" size:25.0f];
    lbl.numberOfLines = 4;
    NSString *str = [NSString stringWithFormat:@"Serial Number\n%@\nEvent Counter\n%ld", [sharedData getHyphenSerialNumber], [sharedData getClock]];
    lbl.text = str;
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, lbl.frame.size.height-40, lbl.frame.size.width, 40)];
    [btn setTitle:@"OK" forState:UIControlStateNormal];
    [btn setBackgroundColor:[UIColor whiteColor]];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setUserInteractionEnabled:YES];
    [btn addTarget:self action:@selector(removeTransparentView) forControlEvents:UIControlEventTouchUpInside];
    btn.layer.borderColor = [UIColor grayColor].CGColor;
    btn.layer.borderWidth = 0.5f;
    
    [lbl setUserInteractionEnabled:YES];
    [OTPView setUserInteractionEnabled:YES];
    
    [lbl addSubview:btn];
    [OTPView addSubview:lbl];
    
    [self.view addSubview:OTPView];
}

#pragma mark UITableViewDataSource Methods

// Customize the number of sections in the table view
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    NSLog(@"-- numberOfSectionsInTableView");
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"Folder count %lu", (unsigned long)[self.groupNames count]);
    
    if([self.groupNames count]==0){
        [_btnEdit setTitle:@""];
        [_btnEdit setEnabled:NO];
    }
    else{
        [_btnEdit setEnabled:YES];
       
    }

    if (sharedData.isDeviceLocked == YES){
        [_btnEdit setEnabled:NO];
    }
    
    
    return [self.groupNames count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"-- cellForRowAtIndexPath");
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
    NSInteger rowIndex = [indexPath row];
    cell.textLabel.text = [[self.groupNames valueForKey:@"gnm"] objectAtIndex:rowIndex];
    cell.detailTextLabel.text = [[self.groupNames valueForKey:@"uid"] objectAtIndex:rowIndex];
//    cell.textLabel.font = LABEL;
//    cell.detailTextLabel.font = LABEL2;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.tag = indexPath.row + 99;
    [button addTarget:self action:@selector(menuShow:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(40, 5, 30, 30)];
   // [button setBackgroundColor:[UIColor grayColor]];
    
    
   [button setBackgroundImage:[UIImage imageNamed:@"mobile_menu72.png"] forState:UIControlStateNormal];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 40)];
    UITapGestureRecognizer *tapBtn = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(menuShowT:)];
    [view addGestureRecognizer:tapBtn];
    [view addSubview:button];
    
//        view.layer.borderWidth = 2.0f;
//        view.layer.borderColor = [UIColor redColor].CGColor;
    
    view.tag = indexPath.row + 991;
    cell.accessoryView = view;
    return cell;
}

- (void) menuShowT:(UITapGestureRecognizer *)gest{
    [self internalMenuShow:gest.view.tag - 991];
}

- (void) menuShow:(UIButton *)btn{
    [self internalMenuShow:btn.tag - 99];
    
}

- (void) internalMenuShow:(NSInteger) rowIndex{
    
    NSArray *selectedName = [self.groupNames objectAtIndex:rowIndex];
    NSString *profile = [NSString stringWithFormat:@"%@%@",[selectedName valueForKey:@"gnm"],[[selectedName valueForKey:@"uid"] lowercaseString]];
    sharedData.profileName = profile;
      NSLog(@"Shared Data Profile=>%@",sharedData.profileName);
    [sharedData newloadAppStatus];
    
    //need to pull the action sheet
    self.tableMenu = [[UIActionSheet alloc] initWithTitle:nil
                                                 delegate:self
                                        cancelButtonTitle:nil
                                   destructiveButtonTitle:nil
                                        otherButtonTitles:nil];
    self.tableMenu.actionSheetStyle = UIActionSheetStyleAutomatic;
    [self.tableMenu setTag:1];
    
    [self.tableMenu addButtonWithTitle:@"Scan QR Code"];
    [self.tableMenu addButtonWithTitle:@"Show Account Information"];
    [self.tableMenu addButtonWithTitle:@"Cancel"];
    [self.tableMenu showFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    NSLog(@"Shared Data Profile=>%@",sharedData.profileName);
    if(buttonIndex==0)
    {
        UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:CameraViewController];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (buttonIndex==1){
        [self showSerialNumber];
    }
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        // Delete an entry
        NSArray *selectedName = [self.groupNames objectAtIndex:indexPath.row];
        NSString *profile = [NSString stringWithFormat:@"%@%@",[selectedName valueForKey:@"gnm"],[[selectedName valueForKey:@"uid"] lowercaseString]];
        sharedData.profileName = profile;
        [sharedData newloadAppStatus];
        
        deletealertView = [[UIAlertView alloc]initWithTitle:@"Warning!" message:@"Are you sure you want to remove this account?\nThis action cannot be undone" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
        [deletealertView show];
    }
}

int iTimeForOTP = 30; //sec
int iTimeCurrent = iTimeForOTP;

-(void) updateProgress{
    --iTimeCurrent;
    
    int iProgress = (iTimeForOTP - iTimeCurrent);
    iProgress = iProgress*100/iTimeForOTP;
    [prg setProgress:iProgress/100.0f animated:YES];
    
    if(iTimeCurrent <= 0){
        [self removeTransparentView];
    }
}

- (UIImage *)imageWithColor:(UIColor *)color{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void) showTransparentView{
    OTPView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [OTPView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6f]];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 120)];
    lbl.center = CGPointMake(OTPView.frame.size.width/2, OTPView.frame.size.height/2 - self.navigationController.navigationBar.frame.size.height);
    lbl.backgroundColor = [UIColor whiteColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.layer.borderColor = [UIColor colorWithRed:48/255.0f green:108/255.0f blue:164/255.0f alpha:1.0f].CGColor;//[UIColor colorWithRed:18/255.0f green:123/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    lbl.layer.borderWidth = 2.0f;
    lbl.layer.shadowRadius = 3.0;
    lbl.layer.shadowOpacity = 0.5;
    lbl.layer.masksToBounds = NO;
    lbl.layer.shouldRasterize = YES;
   // lbl.font = [UIFont fontWithName:@"Coda-Regular" size:30.0f];
    lbl.numberOfLines = 2;
    NSString *str = [NSString stringWithFormat:@"One-Time Password\n%@", self.otpString];
    lbl.text = str;
    prg = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    [prg setFrame:CGRectMake(4, lbl.frame.size.height-7, lbl.frame.size.width-4, 10)];
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 3.0f);
    prg.transform = transform;
    [prg setProgress:0.0f animated:YES];
    //    [prg setProgressTintColor:[UIColor colorWithRed:18/255.0f green:123/255.0f blue:107/255.0f alpha:1.0f]];
    [prg setProgressTintColor:[UIColor colorWithRed:48/255.0f green:108/255.0f blue:164/255.0f alpha:1.0f]];
    [prg setTrackTintColor:[UIColor clearColor]];
    
    [lbl addSubview:prg];
    [OTPView addSubview:lbl];
    
    [self.view addSubview:OTPView];
    timer = nil;
    timer = [NSTimer scheduledTimerWithTimeInterval:1
                                             target:self
                                           selector:@selector(updateProgress)
                                           userInfo:nil
                                            repeats:YES];
    
}

- (void) removeTransparentView{
    [OTPView removeFromSuperview];
    [prg setProgress:0.0 animated:YES];
    [timer invalidate];
    timer = nil;
    iTimeCurrent = iTimeForOTP;
    OTPView = nil;
    prg = nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //toget the index selected
    NSArray *selectedName = [self.groupNames objectAtIndex:indexPath.row];
    NSLog(@"Selected Name in Did select: %@", selectedName);
    //toget profile name
    NSString *profile = [NSString stringWithFormat:@"%@%@",[selectedName valueForKey:@"gnm"],[[selectedName valueForKey:@"uid"] lowercaseString]];
    sharedData.profileName = profile;
    [sharedData newloadAppStatus];
    NSLog(@"Selected profilename in Did select: %@", sharedData.profileName);
    //generate otp
    
    [self chekSecurity];
    
    
//    self.otpString = [sharedData generateOTP];
//    [self showTransparentView];
}



- (void)hideClearButton:(BOOL)hide {
    
    if (hide) {
        leftBtn = self.navigationItem.leftBarButtonItem;
        self.navigationItem.leftBarButtonItem = nil;
    }
    else {
        self.navigationItem.leftBarButtonItem = leftBtn;
        
    }
}


-(void) generateOTP:(id)sender{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    //[self hideClearButton:YES];
    
    // [defaults setObject:@"0" forKey:@"GENERATE_OTP"];
    [defaults removeObjectForKey:@"GENERATE_OTP"];
     [defaults removeObjectForKey:@"TURNOFFFACE"];
    //[defaults setObject:@"NO" forKey:@"TURNOFFFACE"];
    //[defaults setObject:@"0" forKey:@"APPROVE_OR_DENY"];
    [defaults removeObjectForKey:@"APPROVE_OR_DENY"];
     [defaults removeObjectForKey:@"FACE_REC"];
    //[defaults setObject:@"DoAuthenticate" forKey:@"FACE_REC"];
    [defaults synchronize];
    self.otpString = [sharedData generateOTP];
    [self showTransparentView];
}

//- (void)ClickAllow:(id)sender {
//   
//    [self generateOTP];
//    
//}
//
//- (void)ClickDeny:(id)sender{
//    
//}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView==erroralertView1){
        sharedData.isDeviceLocked=YES;
        [sharedData newsaveAppStatus];
        [sharedData newloadAppStatus];
        [self viewWillAppear:YES];
    }
    
    [sharedData newloadAppStatus];
    if (alertView == deletealertView) {
        if (buttonIndex == 0) {
            NSLog(@"delete account %@", sharedData.profileName);
            [sharedData removeFromList];
            [self viewWillAppear:YES];
            [deletealertView release];
          
        }
        else
        {
            [sharedData newloadAppStatus];
            [deletealertView release];
        }
    }
    else
    if (alertView == transerroralert) {
        if (buttonIndex == 0) {
            [self viewWillAppear:YES];
        }
        
    }
    else if (alertView == locationalert) {
           }
    else if(alertView == incorectSAalertView)
    {
        if (buttonIndex == 0) {
            [incorectSAalertView release];
        }
    }
    
}

- (void)dealloc {
    [_myTableView release];
    [_txtDescription release];
    [_btnEdit release];
    [super dealloc];
}

- (IBAction)btnEditClicked:(id)sender {
    
    if([_btnEdit.title isEqualToString:@"Cancel"]){
        _myCirleView.hidden=YES;
        _btnSecureAlignment.hidden=YES;
        [_btnEdit setTitle:@"Edit"];
        return;
    
    }
    else{
        
    if(self.myTableView.editing)
        [self cancel:nil];
    else
        [self edit:nil];
    }
}

- (void)edit:(id)sender
{
    if([_btnEdit.title isEqualToString:@"Cancel"]){
         _myCirleView.hidden=YES;
         _btnSecureAlignment.hidden=YES;
        [_btnEdit setTitle:@"Edit"];
        return;
    }
    _btnEdit =
    [[[UIBarButtonItem alloc]
      initWithTitle:@"Edit"
      style:UIBarButtonItemStyleDone
      target:self
      action:@selector(edit:)]
     autorelease];
    [_btnEdit setTintColor:[UIColor whiteColor]];
    [self.navigationItem setLeftBarButtonItem:_btnEdit animated:NO];
    
    if(![self.myTableView isEditing]){
        [_btnEdit setTitle:@"Done"];
        [self.myTableView setEditing:YES animated:YES];}
    else{
        [_btnEdit setTitle:@"Edit"];
        [self.myTableView setEditing:NO animated:YES];
    }
    
    
}

- (void)cancel:(id)sender
{
    
    if([_btnEdit.title isEqualToString:@"Cancel"]){
        _myCirleView.hidden=YES;
          _btnSecureAlignment.hidden=YES;
        [_btnEdit setTitle:@"Edit"];
        return;
    }
  _btnEdit =
    [[[UIBarButtonItem alloc]
      initWithTitle:@"Edit"
      style:UIBarButtonItemStylePlain
      target:self
      action:@selector(edit:)]
     autorelease];
    [_btnEdit setTintColor:[UIColor whiteColor]];
    [self.navigationItem setLeftBarButtonItem:_btnEdit animated:NO];
    
    [self.myTableView setEditing:NO animated:YES];
}

-(IBAction)btnAddAccontTapped:(id)sender{
    sharedData.isFaceRecognised=NO;
    sharedData.isOutSET=NO;
    sharedData.isTouchIDEnable=NO;
    [sharedData newsaveAppStatus];
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:CameraViewController];
    [self.navigationController pushViewController:vc animated:YES];
}


- (void) chekSecurity{
    //SharedData *sharedData = [SharedData sharedData];
    //NSLog(@"~~~~~~~~~~~~Authorize~~~~~Click ALLOW~~~Profilename: %@", sharedData.profileName);
    [sharedData newloadAppStatus];
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    //[defaults setObject:@"SECURITY_TYPE_SECUREALIGNMENT" forKey:@"SECURITY_TYPE"];
    
    //if(sharedData.isTouchIDEnable && sharedData.isSecuritySet)
    if([[defaults objectForKey:@"SECURITY_TYPE"] isEqualToString:@"SECURITY_TYPE_TOUCHID"] && [defaults objectForKey:@"IS_SECURITY_SET"])
    {
        //with touch id
        LAContext *context = [[LAContext alloc] init];
        NSError *error = nil;
        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
            // Authenticate User
            [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                    localizedReason:@"Are you the device owner?"
                              reply:^(BOOL success, NSError *error) {
                                  if (success) {
                                      dispatch_async (dispatch_get_main_queue(), ^{
                                          //NSLog(@"*******Success TOUCHID******  ");
                                          [self generateOTP:nil];
                                         
                                      });
                                  }
                                  if (error) {
                                      if (error.code == LAErrorUserFallback) {
                                          //NSLog(@"LAErrorUserFallback");
                                          dispatch_async (dispatch_get_main_queue(), ^{
                                              //AlertView Code
                                              
                                          });
                                      }
                                  }
                              }
             ];
        }
        
        else if (error.code == LAErrorTouchIDNotEnrolled)
        {
            //NSLog(@"RVc LAErrorTouchIDNotEnrolled");
            sharedData.isTouchIDDevice = YES;
            //NSLog(@"########################Author allow %@ ", sharedData.profileName);
            //[sharedData newsaveAppStatus];
        }
    }
    // @"SECURE_TILT_VALUE"[defaults objectForKey:@"SECURITY_TYPE_SECUREALIGNMENT"]
    else if ([defaults objectForKey:@"SECURE_TILT_VALUE"]!=nil && [[defaults objectForKey:@"SECURITY_TYPE"] isEqualToString:@"SECURITY_TYPE_SECUREALIGNMENT"] ){//(sharedData.dTiltValue != nil && sharedData.isTiltEnable) {
        //show the circleView
        [_btnEdit setTitle:@"Cancel"];
         self.myCirleView.hidden = NO;
         self.lblValue.hidden = NO;
         self.btnSecureAlignment.hidden = NO;
        [self setCircleView];
        
    } else  if([[defaults objectForKey:@"SECURITY_TYPE"] isEqualToString:@"SECURITY_TYPE_FACEVOICE"]){//sharedData.isFaceRecognised){
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:@"DoAuthenticate" forKey:@"FACE_REC"];
        [defaults setObject:@"1" forKey:@"GENERATE_OTP"];
        //[defaults setObject:@"NO" forKey:@"TURNOFFFACE"];
        
        [defaults removeObjectForKey:@"TURNOFFFACE"];
        [defaults removeObjectForKey:@"APPROVE_OR_DENY"];
        //[defaults setObject:@"0" forKey:@"APPROVE_OR_DENY"];
        [defaults setObject:@"DoAuthenticate" forKey:@"FACE_REC"];
        
        [defaults synchronize];
        sharedData.flagForEnableSecureAlignAfterDisableFaceVoice=NO;
        VVDemoRecognizerController *vc = (VVDemoRecognizerController *)[self.storyboard instantiateViewControllerWithIdentifier:VVDemoRecognizer];
        vc.callbackMe = self;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else{
        [self generateOTP:nil];
        
//        noSecurityAlertView=[[UIAlertView alloc]initWithTitle:@"No Security Set" message:@"Please enable either of the security settings from Settings menu." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] ;
//        [noSecurityAlertView show];
    }
    
}


-(void)setCircleView
{
    //call the draw2d view to allow user to set the value.
    self.draw2DView = [[Draw2D alloc]initWithFrame:CGRectMake(0, 31, [[UIScreen mainScreen] bounds].size.width, ([[UIScreen mainScreen] bounds].size.height) - 30)];
    self.draw2DView.delegate = self;
    self.draw2DView.backgroundColor = [UIColor clearColor];
    self.lblValue.text = [NSString stringWithFormat:@"%i",self.draw2DView.z];
    [self.myCirleView addSubview:self.draw2DView];
    
 
}

- (void)rateView:(Draw2D *)rateView ratingDidChange:(int)rating {
    self.lblValue.text = [NSString stringWithFormat:@"%i", rating];
    //NSLog(@"z: %i, x=%i",self.z,rating);
    
}

- (IBAction)ClickSecureAlignment:(id)sender{

}

- (IBAction)btnSecureAligmentTapped:(id)sender {
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSLog(@"secure Aligment tapped");
    
    //    if(_bOfflineMode){
    //        [self showTransparentView];
    //        return;
    //    }
    
    
    //NSLog(@"SecureAlignment Allow/Deny = %@",self.txstat);
    SharedData *sharedData = [SharedData sharedData];
    self.lblValue.text = [NSString stringWithFormat:@"%i",self.draw2DView.z];
    //check the profile name, null it first to save the value then resave the profile name
    //    NSString *tempProfile = sharedData.profileName;
    //    sharedData.profileName = nil;
    [sharedData newloadAppStatus];
    //    sharedData.profileName = tempProfile;
    
    //if ([sharedData.dTiltValue isEqualToString:self.lblValue.text]) {
    if([[defaults objectForKey:@"SECURE_TILT_VALUE"] isEqualToString:self.lblValue.text]){
        //check if automation is being selected
        //if yes save the location to the current account.
        //approve the transaction
        //send notice to the server its approved
        //go to processing screen
        //[sharedData resetoutPut]
        self.myCirleView.hidden = YES;
        self.lblValue.hidden = YES;
        self.btnSecureAlignment.hidden = YES;
       [self generateOTP:nil];
        sharedData.outPut=5;
        [sharedData newsaveAppStatus];
        [sharedData newloadAppStatus];
        
        [defaults removeObjectForKey:@"LOCKED_COUNT"];
        [defaults synchronize];
        
    }
    else
    {
        //tilt value not right
        //reduce count n show alert message
        //NSLog(@"number of attempt %d",sharedData.outPut);
        [sharedData decreaseoutPut];
        NSString *msg =[NSString stringWithFormat:@"Please tilt your device to correct alignment to proceed. You have %d attempts remaining.",sharedData.outPut];
        //NSLog(@"Reduce the number of attempt %d",sharedData.outPut);
//        incorectSAalertView = [[UIAlertView alloc]initWithTitle:@"Incorrect Secure Alignment" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [incorectSAalertView show];
        
        if(sharedData.outPut>0){
            
            NSLog(@"greater than Zero");
            msg=[NSString stringWithFormat:@"Please tilt your device to correct alignment to proceed. You have %d attempts remaining.",sharedData.outPut];
            erroralertView = [[UIAlertView alloc]initWithTitle:@"Incorrect Secure Alignment" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [erroralertView show];
        }else{
            
            NSTimeInterval timeInMiliseconds = [[NSDate date] timeIntervalSince1970];
            
            if([defaults objectForKey:@"LOCKED_COUNT"])
            {
                NSNumber *locked_count=[defaults objectForKey:@"LOCKED_COUNT"];
                int temp_lock_count=locked_count.intValue+1;
                
                timeInMiliseconds=timeInMiliseconds+(temp_lock_count*300);
                
                locked_count=[NSNumber numberWithInt:temp_lock_count];
                [defaults setObject:locked_count forKey:@"LOCKED_COUNT"];
                
                NSNumber *locked_value=[NSNumber numberWithLongLong:timeInMiliseconds];
                [defaults setObject:locked_value forKey:@"LOCKED_VALUE"];
                [defaults synchronize];
                
                NSString *message=[NSString stringWithFormat:@"The application is locked for %i minutes because you have exceeded the limit for providing your Secure Alignment ",temp_lock_count*5];
                
                erroralertView1=[[UIAlertView alloc]initWithTitle:@"Application Locked" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [erroralertView1 show];
            }else{
                NSNumber *locked_count=[NSNumber numberWithInt:1];
                [defaults setObject:locked_count forKey:@"LOCKED_COUNT"];
                
                NSNumber *locked_value=[NSNumber numberWithLongLong:timeInMiliseconds+300];
                [defaults setObject:locked_value forKey:@"LOCKED_VALUE"];
                [defaults synchronize];
                
                erroralertView1=[[UIAlertView alloc]initWithTitle:@"Application Locked" message:@"The application is locked for 5 minutes because you have exceeded the limit for providing your Secure Alignment " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [erroralertView1 show];
            }
        }
        
    }
    
}
- (IBAction)ClickAutomate:(id)sender {
    //intialy not set.
    //user check the box     //  'd':'51.5033,-0.1276',
    
    //if user check this box, save the location inside this account.
    //if the user has saved this location before
    SharedData *sharedData = [SharedData sharedData];
    
//    if (self.btnAutomation.selected == NO) {
//        if (sharedData.isLocationSet == NO) {
//            //NSLog(@"LOCATION DISABLED");
//            locationalert = [[UIAlertView alloc] initWithTitle:@"Location Disabled"
//                                                       message:@"Could not determine your location.Click Open Settings to open the location settings and enable location services Or Cancel to close this dialog."
//                                                      delegate:self
//                                             cancelButtonTitle:@"Cancel"
//                                             otherButtonTitles:@"Open Settings",nil];
//            [locationalert show];
//        }
//        self.btnAutomation.selected = YES;
//        [self.btnAutomation setBackgroundImage:[UIImage imageNamed:@"checkek.png"] forState:UIControlStateNormal];
//    }
//    else
//    {
//        self.btnAutomation.selected = NO;
//        [self.btnAutomation setBackgroundImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
//    }


}


-(void)lockApp
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSTimeInterval timeInMiliseconds = [[NSDate date] timeIntervalSince1970];
    
    if([defaults objectForKey:@"LOCKED_COUNT"]!=nil&&[defaults objectForKey:@"LOCKED_VALUE"]!=nil)
    {
        //if(){
            NSNumber *locked_value=[defaults objectForKey:@"LOCKED_VALUE"];
            
            long lockedvalue=locked_value.longValue;
            
            // int time2=(timeInMiliseconds/60000);
            
            long difference=lockedvalue-timeInMiliseconds;
        
        
            
            NSString *message=@"You have to wait for 04:32 mins for the application to become unlocked";

            NSLog(@"difference=%ld  %ld %ld",difference,difference/60,difference%60);
        
         message=[NSString stringWithFormat:@"You have to wait for %li:%li mins for the application to become unlocked",difference/60,difference%60];
        
        if(difference/60<10){
        message=[NSString stringWithFormat:@"You have to wait for 0%li:%li mins for the application to become unlocked",difference/60,difference%60];
        }
        if(difference%60<10)
        {
        message=[NSString stringWithFormat:@"You have to wait for %li:0%li mins for the application to become unlocked",difference/60,difference%60];
        }
        if(difference/60<10 && difference%60<10){
        
            message=[NSString stringWithFormat:@"You have to wait for 0%li:0%li mins for the application to become unlocked",difference/60,difference%60];
        }
            _lblMessage.text=message;
            if(difference<=0){
                sharedData.isDeviceLocked=NO;
                [sharedData newsaveAppStatus];
                [defaults removeObjectForKey:@"LOCKED_VALUE"];
                [defaults synchronize];
                _viewAppLocked.hidden=YES;
                //[self.navigationController.navigationBar setHidden:NO];
                [self viewWillAppear:YES];
                
                //  }
        }
    }else{
        _viewAppLocked.hidden=YES;
        sharedData.isDeviceLocked=NO;
        [sharedData newsaveAppStatus];
        [self.navigationController.navigationBar setHidden:NO];
         [self viewWillAppear:YES];
    }
    
}



@end
