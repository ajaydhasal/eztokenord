//
//  DeviceManager.h
//  EZToken
//
//  Created by Harsha on 9/19/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

//#ifndef __EZToken__DeviceManager__
//#define __EZToken__DeviceManager__

//#include <iostream>
#include <stdlib.h>
#include "Device.h"
//#endif /* defined(__EZToken__DeviceManager__) */

class DeviceManager {
public:
	char *m_secret_file_path;
	unsigned char *m_secret;
	
	char *m_serialnumber;
	char *m_registrationcode;
	char *m_unlockchallenge;
	bool m_pinsupport;
	bool m_rcdisplay;
	int m_cashMode;
	int m_cashType;
	int m_pbkdmode;

private:

    //Device *resurrectDevice(const char *pin, const char *duid);
	Device *resurrectDevice(const char *duid);

	unsigned char* readSecretFromFile(const char *filePath);
	bool writeSecretToFile(const char *filePath, const char *bytes);
	bool deleteSecretFile(const char *filePath);
	Device* privateDevice;
    
    bool persistDeviceNew(Device *device,const char *pin);
    Device *resurrectDeviceNew(const char *pin);
    const char* getDuid ();
    Device *deviceObj;

public:
	DeviceManager();
	~DeviceManager();
    
	void setSecretFilePath(const char *);
	
	char *generateSerialNo12(int tkExpType);
	char *generateSerialNo16(int tkExpType, const char *p1p2, const char *v1v2);
    
	int createDevice(const char* activeCode, const char* strEventsTimeSeed, const char *serialNumber);
    
    bool saveDeviceWithPin(const char *pin);
    bool saveDeviceWithoutPin();
	
    bool changedOut(const char *pinOld, const char *pinNew);
    
    bool loadDevice(const char *pin);
    bool loadDevice();
	void removeDevice();
	
//    long getClock(const char *pin);
	long getClock();
//	
//    long getEventCounter(const char *pin);
//    long getEventCounter();
   
	
    //char *generateOTP(const char *pin);
    char *generateOTP();
    
//    char *generateCROTP(const char *pin, const char *challenge);
//    char *generateCROTP(const char *challenge);
//    
//    char *generateSIGOTP(const char *pin, const char *signature);
    char *generateSIGOTP(const char *signature);

    const char* getMacAddress();
    const char* getVersionNumber();
    //char * getHash (const char *string, const int strLen);
    
};


extern DeviceManager* sharedDeviceManager();
