//
//  VVDemoSettingsController.m
//  IOSVVDemo
//
//  Created by Karl Ridgeway on 9/4/14.
//  Copyright (c) 2014 Sensory. All rights reserved.
//

#import "VVDemoSettingsController.h"
#include "settings.h"


@implementation VVDemoSettingsController

-(void) updateUI:(BOOL)firstTime {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        if( firstTime == YES ) {
            [self.authTimeoutSlider setMinimumValue:1000.0f];
            [self.authTimeoutSlider setMaximumValue:30000.0f];
            [self.authTimeoutSlider setContinuous:YES];
            [self.securityLevelSlider setMinimumValue:0.0f];
            [self.securityLevelSlider setMaximumValue:4.0f];
            [self.securityLevelSlider setContinuous:YES];
        }
        
        [self.requireBothSwitch setOn: [defaults boolForKey:KEY_REQUIRE_BOTH]];
        [self.faceMotionAnalysisSwitch setOn: [defaults boolForKey:KEY_FACE_MOTION_ANALYSIS]];
        [self.useAdaptiveEnrollmentSwitch setOn: [defaults boolForKey:KEY_USE_ADAPTIVE_ENROLLMENT]];
        [self.requireConfirmationSwitch setOn: [defaults boolForKey:KEY_REQUIRE_CONFIRMATION]];
        [self.requireConfirmationSwitch setEnabled:[defaults boolForKey:KEY_USE_ADAPTIVE_ENROLLMENT]];
        [self.authTimeoutSlider setValue:(float)[defaults integerForKey:KEY_AUTHENTICATION_TIMEOUT]];
        [self.securityLevelSlider setValue:(float)[defaults integerForKey:KEY_SECURITY_LEVEL]];
        [self.authTimeoutLabel setText:[NSString stringWithFormat:@"%ld s", (long)[defaults integerForKey:KEY_AUTHENTICATION_TIMEOUT]/1000]];
    });
}

-(void) viewDidLoad {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if( [defaults boolForKey:KEY_DEFAULTS_INITIALIZED] == NO ) {
        [defaults setBool:YES forKey:KEY_DEFAULTS_INITIALIZED];
        [defaults setBool:NO forKey:KEY_REQUIRE_BOTH];
        [defaults setBool:NO forKey:KEY_FACE_MOTION_ANALYSIS];
        [defaults setBool:NO forKey:KEY_USE_ADAPTIVE_ENROLLMENT];
        [defaults setBool:NO forKey:KEY_REQUIRE_CONFIRMATION];
        [defaults setInteger:10000 forKey:KEY_AUTHENTICATION_TIMEOUT];
        [defaults setInteger:2 forKey:KEY_SECURITY_LEVEL];
    }
    [defaults synchronize];
    
    [self updateUI:YES];
}

- (IBAction)requireFaceVoiceChanged:(id)sender {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:self.requireBothSwitch.on forKey:KEY_REQUIRE_BOTH];
    [defaults synchronize];
}

- (IBAction)faceMotionAnalysisChanged:(id)sender {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:self.faceMotionAnalysisSwitch.on forKey:KEY_FACE_MOTION_ANALYSIS];
    [defaults synchronize];
}

- (IBAction)useAdaptiveEnrollmentChanged:(id)sender {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:self.useAdaptiveEnrollmentSwitch.on forKey:KEY_USE_ADAPTIVE_ENROLLMENT];
    [defaults setBool:self.requireConfirmationSwitch.on && self.useAdaptiveEnrollmentSwitch.on
               forKey:KEY_REQUIRE_CONFIRMATION];
    [defaults synchronize];
    [self updateUI:NO];
}

- (IBAction)requireConfirmationChanged:(id)sender {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:self.requireConfirmationSwitch.on forKey:KEY_REQUIRE_CONFIRMATION];
    [defaults synchronize];
}

- (IBAction)authenticationTimeoutChanged:(id)sender {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    int newvalue = ( (int)self.authTimeoutSlider.value );
    newvalue = newvalue - (newvalue % 1000);
    [defaults setInteger:newvalue forKey:KEY_AUTHENTICATION_TIMEOUT];
    [self updateUI:NO];
    [defaults synchronize];
}

- (IBAction)securityLevelChanged:(id)sender {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:(int)self.securityLevelSlider.value forKey:KEY_SECURITY_LEVEL];
    [self updateUI:NO];
    [defaults synchronize];
}
@end
