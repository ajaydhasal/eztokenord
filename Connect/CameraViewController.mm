//
//  CameraViewController.m
//  EZToken
//
//  Created by Harsha on 9/17/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import "CameraViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "Constant.h"
#import "SharedData.h"
#import "AuthorizeViewController.h"
#import "AppDelegate.h"
//#include "base64.h"


@interface CameraViewController ()<AVCaptureMetadataOutputObjectsDelegate, UIAlertViewDelegate>
{
    BOOL isPendingProcessResult;
    BOOL isWaitingDelayTimeOver;
    
    NSTimer *loadingDelayTimer;
    BOOL isAllLoadingProcessDone;
    float fLoadingDelayTime;
    
    UIAlertView *activateAlertView;
    
    UIAlertView *erroralert;
    UIAlertView *cameraAlert1;
    UIAlertView *networkErroralertView;
    UIAlertView *cameraAlert;
}
@property (nonatomic,strong)NSString *acToken;
@property (nonatomic,strong)NSString *userId;
@property (nonatomic,strong)NSString *gid;
@property (nonatomic,strong)NSString *gnm;
@property (nonatomic,strong)NSArray *jsonData;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, retain) UIButton *flashlightButton;

@end

@implementation CameraViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backbutton =  [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:nil action:nil];
    [backbutton setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor],UITextAttributeFont: [UIFont systemFontOfSize: 17.0f]} forState:UIControlStateNormal];
    
    
    self.navigationItem.leftBarButtonItem=backbutton;
    [backbutton setTarget:self];
    [backbutton setAction:@selector(ClickCancel1:)];
    
    
//    UIBarButtonItem *backbutton =  [[UIBarButtonItem alloc] initWithTitle:@"Backa" style:UIBarButtonItemStyleBordered target:nil action:nil];
//    [backbutton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                        [UIColor whiteColor],UITextAttributeTextColor,[UIFont systemFontOfSize: 16.0f],NSFontAttributeName,
//                                        nil] forState:UIControlStateNormal];
//    self.navigationItem.leftBarButtonItem=backbutton;
//    [backbutton setTarget:self];
//    
//    [backbutton setAction:@selector(ClickCancel:)];
//    [self.navigationController.navigationBar setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor],UITextAttributeFont:[UIFont systemFontOfSize:17.0f]}];
//
//    // Do any additional setup after loading the view.
    //alertview
    
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    NSString *strMsg = [userdefaults objectForKey:@"SCAN_FIRST_TIME_CAMERA_MESSAGE"];
    if(strMsg != nil && strMsg.length > 0){
        [self scanner];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Scan a QR-Code" message:@"Use your device to scan a valid/supported QR-code to activate the account" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        [userdefaults setObject:@"FIRST_TIME" forKey:@"SCAN_FIRST_TIME_CAMERA_MESSAGE"];
        [userdefaults synchronize];
    }
    
    }
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIAlertViewDelegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == erroralert) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    if (alertView==cameraAlert1) {
        [self scanner];
    }
    if(alertView == networkErroralertView)
    {
        SharedData *sharedData = [SharedData sharedData];
        NSLog(@"CameraV networkErroralertView profilename -------> %@", sharedData.profileName);
        [sharedData removeFromList];
    }
    if(alertView==cameraAlert){
        //         BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
        //        if (canOpenSettings)
        //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)scanner
{
    self.captureSession = [[AVCaptureSession alloc] init];
    AVCaptureDevice *videoCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    AVCaptureDeviceInput *videoInput = [AVCaptureDeviceInput deviceInputWithDevice:videoCaptureDevice error:&error];
    if(videoInput)
    {
        [self.captureSession addInput:videoInput];
        AVCaptureMetadataOutput *metadataOutput = [[AVCaptureMetadataOutput alloc] init];
        [self.captureSession addOutput:metadataOutput];
        [metadataOutput setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
        [metadataOutput setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
        
        AVCaptureVideoPreviewLayer *previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
        previewLayer.frame = self.view.layer.bounds;
        [self.view.layer addSublayer:previewLayer];
        [self.captureSession startRunning];
        
    }
    else
    {
        NSLog(@"Error: %@", error);
        cameraAlert=[[UIAlertView alloc]initWithTitle:@"Camera" message:@"Please Check Camera Settings And Restart Application " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [cameraAlert show];
        
    }
    
}
//-(void)scanner
//{
//    self.captureSession = [[AVCaptureSession alloc] init];
//    AVCaptureDevice *videoCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
//    NSError *error = nil;
//    AVCaptureDeviceInput *videoInput = [AVCaptureDeviceInput deviceInputWithDevice:videoCaptureDevice error:&error];
//    if(videoInput)
//        [self.captureSession addInput:videoInput];
//    else
//        NSLog(@"Error: %@", error);
//    
//    AVCaptureMetadataOutput *metadataOutput = [[AVCaptureMetadataOutput alloc] init];
//    [self.captureSession addOutput:metadataOutput];
//    [metadataOutput setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
//    [metadataOutput setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
//    
//    AVCaptureVideoPreviewLayer *previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
//    previewLayer.frame = self.view.layer.bounds;
//    [self.view.layer addSublayer:previewLayer];
//    [self.captureSession startRunning];
//    
//}
#pragma mark AVCaptureMetadataOutputObjectsDelegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    for(AVMetadataObject *metadataObject in metadataObjects)
    {
        AVMetadataMachineReadableCodeObject *readableObject = (AVMetadataMachineReadableCodeObject *)metadataObject;
        if([metadataObject.type isEqualToString:AVMetadataObjectTypeQRCode])
        {
            //NSLog(@"captureOutput QR Code = %@", readableObject.stringValue);
            if (self.isViewLoaded) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //NSLog(@"captureOutput QR Code %@", readableObject.stringValue);
                    //need to base64 first before send to json parser
                    const char *charString = [readableObject.stringValue UTF8String];
                    int strLen = [readableObject.stringValue length]+1;
                    int base64DataLen;
                    //unsigned char* base64Data = unbase64( charString, strLen, &base64DataLen ) ;
                    
                    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:readableObject.stringValue options:0];
                    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
                    //NSLog(@"%@", decodedString); // foo
                    
                    [self.captureSession stopRunning];
                    [self jsonParser:decodedData];
                    [self dismissViewControllerAnimated:NO completion:nil];
                    if(!bIsTransaction)
                        [self activateOnline];
                    else
                        [self transactOnline];
                    
                });
            }
        }
    }
}

-(void) transactOnline{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AuthorizeViewController *authorizeVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"authorizeView"];
    authorizeVC.arrmsg = self.jsonData;
    authorizeVC.txref = [self.jsonData valueForKey:@"ref"];
    authorizeVC.bOfflineMode = TRUE;
    [(UINavigationController*)[[UIApplication sharedApplication] delegate].window.rootViewController pushViewController:authorizeVC animated:NO];
}


-(void) makeSigOTPString:(NSString *)j{
    /////////////////////////////////// BAD HACK ///////////////////////////////////
    
    //NSString *j = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *first = @"\"msg\":{";
    NSString *second = @"}";
    NSRange one = [j rangeOfString:first];
    NSRange two = [[j substringFromIndex:one.location + one.length] rangeOfString:second];
    NSRange final = NSMakeRange(one.location + first.length, two.location);
    NSString *j1 = [j substringWithRange:final];
    NSLog(@"MSG --- %@", j1);
    
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appdelegate.dictSequence = [[NSMutableDictionary alloc] init];
    NSInteger iCount = 0;
    
    NSMutableString *strStore = [[NSMutableString alloc] init];
    
    NSString *tg = @"\":\"";
    NSString *tg2 = @"\"";
    while(1){
        NSRange o = [j1 rangeOfString:tg];
        if(o.location == NSNotFound){
            break;
        }
        NSRange o1 = [[j1 substringFromIndex:o.location+o.length] rangeOfString:tg2];
        NSRange f = NSMakeRange(o.location + tg.length, o1.location);
        NSString *s = [j1 substringWithRange:f];
        [strStore appendFormat:@"%@",s];
        [appdelegate.dictSequence setObject:s forKey:[NSString stringWithFormat:@"%ld", (long)iCount]];
        ++iCount;
        
        NSRange o2 = [j1 rangeOfString:s];
        NSString *s2 = [j1 substringFromIndex:o2.location+o2.length];
        j1 = s2;
    }
    
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [userdefaults setObject:strStore forKey:@"SIGOTPSTRING"];
    [userdefaults synchronize];
    
    /////////////////////////////////// BAD HACK ///////////////////////////////////
}

BOOL bIsTransaction = TRUE;

-(void)jsonParser:(NSData *)data
{
    NSError *e = nil;
    bIsTransaction = FALSE;
    if (data != nil) {
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
        //NSLog(@"Details: %@",jsonArray);
        if (!jsonArray) {
            NSLog(@"Error parsing JSON: %@", e);
            return;
        }
        else if([jsonArray valueForKey:@"gid"] && [jsonArray valueForKey:@"gnm"] && [jsonArray valueForKey:@"tac"] && [jsonArray valueForKey:@"uid"]){
            if ([jsonArray valueForKey:@"gid"] != nil) {
                //NSLog(@"Value: %@", [jsonArray valueForKey:@"gid"]);
                self.gid =[jsonArray valueForKey:@"gid"];
            }
            if ([jsonArray valueForKey:@"gnm"] != nil) {
                //NSLog(@"Value: %@", [jsonArray valueForKey:@"gnm"]);
                self.gnm =[jsonArray valueForKey:@"gnm"];
            }
            if ([jsonArray valueForKey:@"tac"] != nil) {
                //NSLog(@"Value: %@", [jsonArray valueForKey:@"tac"]);
                self.acToken =[jsonArray valueForKey:@"tac"];
            }
            if ([jsonArray valueForKey:@"uid"] != nil) {
                //NSLog(@"Value: %@", [jsonArray valueForKey:@"uid"]);
                self.userId =[[jsonArray valueForKey:@"uid"] lowercaseString];
            }
            self.jsonData = jsonArray;
        }
        else if([jsonArray valueForKey:@"gid"] && [jsonArray valueForKey:@"msg"] && [jsonArray valueForKey:@"rc"] && [jsonArray valueForKey:@"uid"] && [jsonArray valueForKey:@"ref"] && [jsonArray valueForKey:@"t"]){
            // New code to validate the QR Code
            NSLog(@"=== Transaction call ====");
            ////// BAD HACK
            [self makeSigOTPString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
            
            self.jsonData = jsonArray;
            bIsTransaction = TRUE;
        }
        else{
            erroralert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"A problem has occurred while trying to activate your device. Click OK and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [erroralert show];
        }
    }
    else if (data == nil)
    {
        NSLog(@"error in Json");
        erroralert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Activation denied. Please scan a valid QR-Code." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [erroralert show];
    }
}

- (void)activateOnline
{
    //NSLog(@"activateOnline");
    ///need to check if the gnm n uid exist in the folder if yes throw error already activated
    SharedData *sharedData = [SharedData sharedData];
    sharedData.profileName = nil;
    NSMutableArray *array =[sharedData checkGnmUid];
    NSString *strJoin = [NSString stringWithFormat:@"%@%@",self.gnm,self.userId];
    //NSLog(@"array :%@  - STRJOIN: %@",array,strJoin);
    BOOL identicalStringFound = NO;
    for(int i=0; i<array.count; i++)
    {
        NSArray* ar = [array objectAtIndex:i];
        NSString* gnmId =[NSString stringWithFormat:@"%@",ar];
        //NSLog(@"%d gnm %@",i,gnmId);
        if([strJoin isEqualToString: gnmId])
        {
            identicalStringFound = YES;
            break;
        }
    }
    //NSLog(@"identicalStringFound :----> %hhd", identicalStringFound);
    if (!identicalStringFound) {
        //doesnt exist.
        //create device and activate token
        NSDate *prevDate = [NSDate date];
        NSString *seedString = [NSString stringWithFormat:@"%f", getCurrentTimeInMilliseconds()];
        NSNumber *timeNumber = [NSNumber numberWithLong:getCurrentTimeInMillisecondsFromDate(prevDate)];
        NSString *tempString = [seedString stringByAppendingFormat:@"%@", [timeNumber stringValue]];
        seedString = tempString;
        [self.acToken stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (self.acToken.length > 0) {
            //NSLog(@"activateOnline1");
            if (seedString.length > SEED_STRING_MAX_LENGTH) {
                if (seedString.length > SEED_STRING_MAX_LENGTH) {
                    NSString *tempString = [seedString substringFromIndex:SEED_STRING_MIN_LENGTH];
                    seedString = tempString;
                }
            }
            if ([sharedData createDevice:self.acToken seed:seedString]){
                //save the scan details in a file (gnm,gid,uid n tac)
                [sharedData save:self.jsonData];
                //proceed to activate the device
                sharedData.processTarget = PROCESS_TARGET_ACTIVATE_TOKEN;
                [self startLoadingProcess];
                
                //[self createDirectory:strJoin];
            }
        }
    }else{
        //this exist throw error
        erroralert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Account already activated. Please check that you have scanned the correct QR-code." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [erroralert show];
    }
}


-(void)dismissAlert:(UIAlertView *) alertView
{
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
    [activateAlertView release];
}
//activate using cert via online


- (IBAction)ClickFlashLight:(id)sender {
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    [device lockForConfiguration:nil];
    if (device.torchMode == AVCaptureTorchModeOff)
    {
        [self.flashlightButton setBackgroundImage:[UIImage imageNamed:@"flash_on.png"] forState:UIControlStateNormal];
        
        [device setTorchMode:AVCaptureTorchModeOn];
        [device setFlashMode:AVCaptureFlashModeOn];
        //torchIsOn = YES;
    }
    else
    {
        [self.flashlightButton setBackgroundImage:[UIImage imageNamed:@"flash_off.png"] forState:UIControlStateNormal];
        [device setTorchMode:AVCaptureTorchModeOff];
        [device setFlashMode:AVCaptureFlashModeOff];
        // torchIsOn = NO;
    }
    [device unlockForConfiguration];
    
}
//check for internet availabilty
- (void) startLoadingProcess
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    int networkConnectionStatus = NETWORK_REACHABILITY_STATUS_NOT_REACHABLE;
    //NSLog(@"===========================> internet not available %d",internetStatus);
    SharedData *sharedData = [SharedData sharedData];
    
    //NSLog(@"internet avaialble %d", internetStatus);
    if(internetStatus != NotReachable)
    {
        isPendingProcessResult = YES;
        //NSLog(@"LVC processTarget %d",sharedData.processTarget);
        if (sharedData.processTarget == PROCESS_TARGET_ACTIVATE_TOKEN)
        {
            fLoadingDelayTime = LOADING_PROGRESS_DELAY_TIME_LOAD_ACTIVATION_CODE;
            //activate online, send detail to server.
            [sharedData onlineActivationToken:self.acToken userid:self.userId groupid:self.gid];
            [self performSegueWithIdentifier:@"loading" sender:self];
        }
    }
    
    else
    {
        //no internet
        //NSLog(@"********NO internet avaialble %d", internetStatus);
        NSString *title = [NSString stringWithFormat:@"Activation Failed [%d]",networkConnectionStatus];
        networkErroralertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:@"A network connectivity error has occured. Please try again later. If the problem persists, please contact your local system administrator."
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"OK", nil];
        [networkErroralertView show];
    }
    
}

- (IBAction)ClickCancel1:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}
@end
