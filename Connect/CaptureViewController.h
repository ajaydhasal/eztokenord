//
//  CaptureViewController.h
//  EZToken
//
//  Created by Harsha on 9/2/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Draw2D.h"
#import "SharedData.h"
#import "SettingVC.h"
@interface CaptureViewController : UIViewController <Draw2DDelegate,UIAlertViewDelegate>{
    SharedData *sharedData;
}
@property (strong, nonatomic) IBOutlet UITextView *txtDescription;
@property (strong, nonatomic) IBOutlet UILabel *lblTiltNo;
@property (strong, nonatomic) IBOutlet UIImageView *imgTilt;
@property (strong, nonatomic) IBOutlet UIButton *btnCapture;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnSkip;
@property (strong, nonatomic) IBOutlet UIView *viewAlert;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewanimation;
- (IBAction)ClickOKSet:(id)sender;
- (IBAction)ClickOnSkip:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSkipTilt;
- (IBAction)ClickSetSecureAlignment:(id)sender;
@property (retain, nonatomic) IBOutlet UIView *myCircleView;
@property (nonatomic, strong)Draw2D *draw2DView;

@property (nonatomic, strong)id callbackMe;
@property (retain, nonatomic) IBOutlet UINavigationItem *myNavBar;

@end
