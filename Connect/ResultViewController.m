//
//  ResultViewController.m
//  EZToken
//
//  Created by Harsha on 10/10/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import "ResultViewController.h"
#import "SharedData.h"

@interface ResultViewController ()

@end

@implementation ResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    UIColor * color = [UIColor colorWithRed:92.0 /255.0f green:171.0/255.0f blue:222.00/255.0f alpha:1.0f];
    self.navigationController.navigationBar.barTintColor = color;

    [super viewWillAppear:YES];
    self.navigationItem.hidesBackButton = YES;
    NSString *str = nil;
    SharedData *sharedData = [SharedData sharedData];
    
    self.dict = sharedData.jsonDictionary;
    //NSLog(@"dictionary: !!!! %@", sharedData.jsonDictionary);
    //NSLog(@"********RESULTVC AUTH STATUS:%@ ******", [self.dict valueForKey:@"authStatus"]);
    NSString *check = [[self.dict valueForKey:@"rc"]stringValue];
    
    NSLog(@"check stirng=>%@",check);
    
    if([check isEqualToString:@"0"])
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSString *strSt = [userDefaults objectForKey:@"TRANSACTION_STATUS"];
        str = [NSString stringWithFormat:@"Request to %@ successful", [strSt isEqualToString:@"1"] ? @"Accept" : @"Deny" ];
        if(   [strSt isEqualToString:@"1"]){
        _imgStatus.image=[UIImage imageNamed:@"success.png"];
        }
        else{
        _imgStatus.image=[UIImage imageNamed:@"access-denied1.png"];
        }
        [userDefaults removeObjectForKey:@"TRANSACTION_STATUS"];
        [userDefaults synchronize];
    }
    else if([check isEqualToString:@"-1"])
    {
        str = @"System error";
    }
    else if([check isEqualToString:@"-2"])
    {
        str = @"Bad parameter";
    }
    else if([check isEqualToString:@"-5"])
    {
        str = @"Transaction status is in an invalid state for this API call (expecting to be in pending state).";
    }
    else if([check isEqualToString:@"-7"])
    {
        str = @"Invalid OTP.";
    }
    else if([check isEqualToString:@"-18"])
    {
        str = @"Token is locked";
    }
    else if([check isEqualToString:@"-75"])
    {
        str = @"Transaction expired.";
    }
    else if([check isEqualToString:@"-104"])
    {
        str = @"Transaction not found.";
    }
    
    else if([check isEqualToString:@"-105"])
    {
        str = @" Transaction data corrupted.";
    }else{
        str = @"General Exception";
    }
    
    
    self.lblStatus.text = str;
//    self.lblStatus.text = [NSString stringWithFormat:@"Request to %@ successful",[check isEqualToString:@"0"] ? @"];//[self.dict valueForKey:@"returnMessage"]];//[NSString stringWithFormat:@"Request %@",str];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [_imgStatus release];
    [_lblStatus release];
    [super dealloc];
}
- (IBAction)ClickOk:(id)sender {
    //goes back home
    //NSLog(@"CLICK OK TO BACK HOME VIEW");
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
