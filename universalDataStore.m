//
//  universalDataStore.m
//  LICClaims
//
//  Created by VijayKumar Dogra on 27/08/15.
//  Copyright (c) 2015 Mdindia. All rights reserved.
//

#import "universalDataStore.h"

@implementation universalDataStore

+ (void) storeData:(NSString *) key withValue:(NSString *)val{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setValue:val forKey:key];
    [standardUserDefaults synchronize];
}

+ (NSString *) getStringValFor:(NSString *)key{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    return ([standardUserDefaults valueForKey:key] == nil) ? @"" : [NSString stringWithFormat:@"%@", [standardUserDefaults objectForKey:key]];
}

+ (NSString *) getDateForMillesecond:(NSString *)strDate{
    NSString *nDate = [[[[strDate componentsSeparatedByString:@"("] objectAtIndex:1] componentsSeparatedByString:@")"] objectAtIndex:0];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:([nDate doubleValue] / 1000)];
    NSDateFormatter *dtfrm = [[NSDateFormatter alloc] init];
    [dtfrm setDateFormat:@"dd/MM/yyyy"];
    // nDate = [dtfrm stringFromDate:date];
    return [dtfrm stringFromDate:date];
}
@end
