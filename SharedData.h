//
//  SharedData.h
//  EZToken
//
//  Created by Harsha on 9/19/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Downloader.h"
#import "ResponseXMLParser.h"
#import "DownloaderFactory.h"
#import "Reachability.h"
#import "Constant.h"
#import <CoreLocation/CoreLocation.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import "settings.h"

typedef enum AuthenticationMethods
{
    AUTHENTICATE_SECUREALIGNMENT,
    AUTHENTICATE_FACEVOICE,
    AUTHENTICATE_TOUCHID
} AuthenticationMethod;


@interface SharedData : NSObject<ResponseXMLParserDelegate, SmallFileDownloaderDelegate>
{
    NSString *dNum;
	NSString *dOut;
	NSString *deviceSN;
	NSString *deviceRC;
	NSString *dUse;
	NSString *deviceUC;
	NSString *deviceUCR;
    
	BOOL isDeviceLocked;
	int outPut;
	BOOL isDeviceExist;
	BOOL isOutOn;
	BOOL isRCDisplay;
	int cashMode;
	int pbkdMode;
	int cashType;
	int iPutOutMode;
	int makeUseType;
	double startOTPCountTimeMillis;
    
	NSString *challengeParamString;
	NSString *signatureParamString;
	NSString *appStatusFilePath;
    
    int currentScreen;
    int previousScreen;
}
+ (SharedData *)sharedData;
@property (nonatomic, retain) NSString *profileName;
@property (nonatomic, retain) NSString *dNum;
@property (nonatomic, retain) NSString *dOut;
@property (nonatomic, retain) NSString *deviceSN;
@property (nonatomic, retain) NSString *deviceRC;
@property (nonatomic, retain) NSString *dUse;
@property (nonatomic, retain) NSString *deviceUC;
@property (nonatomic, retain) NSString *devicePID;
@property (nonatomic, retain) NSString *deviceUCR;

@property NSInteger tempIsAppOpenedFirstTimeFlag;
@property (nonatomic) BOOL isDeviceLocked;
@property(nonatomic)BOOL  isAppOpenedFirstTime;
@property (nonatomic) int outPut;
@property (nonatomic) int isLocationSet;
@property (nonatomic) BOOL isDeviceExist;
@property (nonatomic) BOOL isOutOn;
@property (nonatomic) BOOL isRCDisplay;
@property (nonatomic) int cashMode;
@property (nonatomic) int pbkdMode;
@property (nonatomic) int cashType;
@property (nonatomic) BOOL isOutSET;
@property (nonatomic) BOOL isActivated;
@property (nonatomic) BOOL isSecuritySet;
@property (nonatomic) BOOL isTouchIDEnable;
@property (nonatomic) BOOL isTouchIDDevice;
@property (nonatomic) BOOL isTiltChanged;

@property(nonatomic)BOOL flagForDisableSecureAligment;
@property(nonatomic)BOOL  flagForMatchAngle;
@property(nonatomic)BOOL  flagFroEnableFaceAfterDisableAlignment;

@property(nonatomic)BOOL  flagForEnableSecureAlignAfterDisableFaceVoice;

@property(nonatomic)BOOL  isSecureAligned;
@property(nonatomic,retain)NSString *securedAlignedValue;

@property(nonatomic)BOOL isFaceRecognised;

@property (nonatomic) int iPutOutMode;
@property (nonatomic) int makeUseType;
@property (nonatomic) double startOTPCountTimeMillis;
@property (nonatomic, retain) NSString *appStatusFilePath;

@property (nonatomic, retain) NSString *gatewayURL;
@property (nonatomic) BOOL backendConnectionError;
@property (nonatomic) BOOL invalidResponseError;
@property (nonatomic) int internetConnectionStatus;
@property (nonatomic) int returnCode;
@property (nonatomic) int processTarget;
@property (nonatomic) int processResult;



- (int) getInternetConnectionStatus;
-(void)onlineActivationToken:(NSString *)activationCode userid:(NSString *)uid groupid:(NSString *)gid;
- (BOOL) createDevice:(NSString *)acString seed:(NSString *)seedString;
- (NSString *) getSerialNumber;
- (NSString *) generateSerialNumber;
- (void)saveList :(NSString *)gid gnms:(NSString*)gnm uIDs:(NSString*)uid;
- (NSString *) getHyphenSerialNumber;
- (long) getClock;

@property (nonatomic, retain) NSString *groupID;
@property (nonatomic, retain) NSString *groupName;
@property (nonatomic, retain) NSString *userID;

-(void)save :(NSArray*)data;
-(NSMutableArray*)load;
-(NSMutableArray*)checkGnmUid;
- (BOOL) saveDeviceWithPin:(NSString *)pin;
- (BOOL) loadDeviceWithPin:(NSString *)pin;
- (BOOL) changedOut:(NSString *)pin;
@property (nonatomic, retain) NSString *dTiltValue;
- (void) decreaseoutPut;
- (void) resetoutPut;
- (NSString *) generateOTP;
- (NSString *) generateSIGOTP;
@property (nonatomic, retain) NSString *signatureParamString;
- (BOOL) saveDeviceWithoutPin;
-(void)transactionApproval:(NSString *)sigOtp txref:(NSString *)reference txstat:(NSString *)txstatus;
-(void) transactionApproval:(NSDictionary *)jsonDict;
@property (nonatomic, retain) NSDictionary *jsonDictionary;
- (void) newsaveAppStatus;
@property (nonatomic) BOOL isTiltEnable;
-(void)removeFromList;
-(void)loadList;
- (void) setDeviceSecretFilePath:(NSString *)filePath;
- (void)removeAppStatus;
-(NSMutableArray*)removeNotSaved;
-(NSString*)getgnm:(NSString*)gid;
@property(nonatomic)double latitude;
@property(nonatomic)double longitude;

@property (nonatomic) BOOL isAutomationEnable;
- (void) newloadAppStatus;
- (void)saveGeoLocation:(double)lat long:(double)longit;
-(NSString *) onlineLogin:(NSString *)uid otp:(NSString *)otp ref:(NSString *)txtRef;
+(void) writeToLogFile:(NSString*)content;

@property (nonatomic, retain)NSMutableArray *arrayOfLocations;
@property (nonatomic, retain) CLLocationManager *clLocation;
-(void)saveCLLocation :(CLLocationManager*)location gnm:(NSString*)groudId;
-(NSMutableArray*)loadLocation:(NSString*)groupId;
+ (NSString *)getGatewayURL;

-(BOOL)isTouchIdDevice1;


-(BOOL)isSecuredAlignedORNot;
@end
