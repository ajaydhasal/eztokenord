//
//  SettingVC.m
//  EzToken
//
//  Created by Apple on 14/09/15.
//  Copyright (c) 2015 Ezmcom. All rights reserved.
//
/*if(sharedData.flagForMatchAngle || sharedData.flagForDisableSecureAligment)
 [defaults setObject:@"IS_TILT_CHANGED" forKey:@"IS_TILT_CHANGED"];
 [defaults setObject:@"SECURITY_TYPE_SECUREALIGNMENT" forKey:@"SECURITY_TYPE"];
 [defaults setObject:@"FLAG_FOR_MATCH_ANGLE" forKey:@"FLAG_FOR_MATCH_ANGLE"];
 [defaults setObject:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT" forKey:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT"];
 [defaults synchronize];*/
//
//            if([defaults objectForKey:@"IS_SECURITY_SET"]){
//                [defaults setObject:@"IS_SECURITY_SET" forKey:@"IS_SECURITY_SET"];
//                [defaults setObject:@"SECURITY_TYPE_TOUCHID" forKey:@"SECURITY_TYPE"];
//            }else{
//                [defaults removeObjectForKey:@"IS_SECURITY_SET"];
//                [defaults removeObjectForKey:@"SECURITY_TYPE"];
//
//            }

//[defaults removeObjectForKey:@"IS_SECURITY_SET"];
//@"SECURE_TILT_VALUE"

/*
 [defaults setObject:@"IS_TILT_CHANGED" forKey:@"IS_TILT_CHANGED"];
 [defaults setObject:@"SECURITY_TYPE_SECUREALIGNMENT" forKey:@"SECURITY_TYPE"];
 [defaults setObject:@"FLAG_FOR_MATCH_ANGLE" forKey:@"FLAG_FOR_MATCH_ANGLE"];
 [defaults setObject:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT" forKey:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT"];
 [defaults synchronize];
 
 */

//[_switchEnableDisableAlignment setOn:YES animated:YES];
//if(sharedData.dTiltValue == nil){

#import "SettingVC.h"
#import "VVDemoRecognizerController.h"
#import "CaptureViewController.h"


@implementation SettingVC
-(void)viewDidLoad{
    [self.navigationItem setTitle:@"Settings"];
    sharedData=[SharedData sharedData];
    
    
    self.navigationItem.hidesBackButton = NO;
    UIBarButtonItem *backbutton =  [[UIBarButtonItem alloc] initWithTitle:@"< Home" style:UIBarButtonItemStyleBordered target:nil action:nil];
    [backbutton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],UITextAttributeTextColor,[UIFont systemFontOfSize:16.0f],UITextAttributeFont,
                                        nil] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=backbutton;
    [backbutton setTarget:self];
    [backbutton setAction:@selector(ClickCancel:)];
    }

-(void)viewWillAppear:(BOOL)animated{
    
     //[self setSwitch];
    [self setUpView];
}

- (IBAction)ClickCancel:(id)sender {
    
    [self performSegueWithIdentifier:@"home" sender:nil];
    //[self.navigationController popViewControllerAnimated:YES];
}


-(void)setUpView{
    _lableCopyRight.text=@"© 2014. EZMCOM Inc.\n All rights reserved.";
    _labelVersion.text=[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    _lablePushID.text=sharedData.devicePID;
    
    if ([sharedData.gatewayURL length] == 0) {
        NSString *actualURL = [NSString stringWithFormat:@"%@",MAIN_GATEWAY_URL_DEFAULT];
        [_btnGatewayURL setTitle:actualURL forState:UIControlStateNormal];
    }
    else
    {
        [_btnGatewayURL setTitle:sharedData.gatewayURL forState:UIControlStateNormal];
    }
    [self deviceSpecificSetup];
//    // VJ code
//    //[_switchEnableDisableFaceVoiceRec setOn:sharedData.isFaceRecognised animated:YES];
//    
//    // ICICI bank demo
//    [_switchEnableDisableFaceVoiceRec setOn:NO animated:YES];
//    [_switchEnableDisableFaceVoiceRec setUserInteractionEnabled:NO];
//    //~ ICICI Bank demo
//    
//    
//    [_btnDeleteVoiceProfile setUserInteractionEnabled:sharedData.isFaceRecognised];
//    
//    [_btnChangeSecureAlignment setUserInteractionEnabled:sharedData.isTiltEnable];
//    [_switchEnableDisableAlignment setOn:sharedData.isTiltEnable animated:YES];
//    
//    _viewTouchID.hidden = ![sharedData isTouchIdDevice1];
//    if([sharedData isTouchIdDevice1]){
//        if(sharedData.isSecuritySet)
//        {
//            NSLog(@"switchEnableDisable=%i",sharedData.isTouchIDEnable);
//           [_switchEnableDisable setOn:sharedData.isTouchIDEnable animated:YES];
//        }
//        else
//            [_switchEnableDisable setOn:NO];
//    }
}
-deviceSpecificSetup{
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    // VJ code
    //[_switchEnableDisableFaceVoiceRec setOn:sharedData.isFaceRecognised animated:YES];
    
    // ICICI bank demo
    //  [_switchEnableDisableFaceVoiceRec setOn:NO animated:YES];
    // [_switchEnableDisableFaceVoiceRec setUserInteractionEnabled:NO];
    ///~ ICICI Bank demo
    
    
    //[_btnDeleteVoiceProfile setUserInteractionEnabled:sharedData.isFaceRecognised];
    
     _viewTouchID.hidden = ![sharedData isTouchIdDevice1];
    if([sharedData isTouchIdDevice1]){
        //if(sharedData.isSecuritySet)
        if([defaults objectForKey:@"IS_SECURITY_SET"])
        {
            if([[defaults objectForKey:@"SECURITY_TYPE"] isEqualToString:@"SECURITY_TYPE_TOUCHID"]){
                [_switchEnableDisable setOn:YES];
            }else{
                [_switchEnableDisable setOn:NO];
            }
        }
        if([[defaults objectForKey:@"SECURITY_TYPE"] isEqualToString:@"SECURITY_TYPE_SECUREALIGNMENT"])
        {
                    [_switchEnableDisableAlignment setOn:YES];
                    [_btnChangeSecureAlignment setUserInteractionEnabled:YES];
        }
        else{
                   [_switchEnableDisableAlignment setOn:NO];
                   [_btnChangeSecureAlignment setUserInteractionEnabled:NO];
        }
        if([[defaults objectForKey:@"SECURITY_TYPE"] isEqualToString:@"SECURITY_TYPE_FACEVOICE"]){
            [_switchEnableDisableFaceVoiceRec setOn:YES];
        }
        else{
            [_switchEnableDisableFaceVoiceRec setOn:NO];
        }
        
        
    }
    else{
        
        
    if([defaults objectForKey:@"SECURITY_TYPE"]){
        
        
        
        if([[defaults objectForKey:@"SECURITY_TYPE"] isEqualToString:@"SECURITY_TYPE_FACEVOICE"]){
            [_switchEnableDisableFaceVoiceRec setOn:YES];
            
        }else{
          [_switchEnableDisableFaceVoiceRec setOn:NO];
        }
        if([[defaults objectForKey:@"SECURITY_TYPE"] isEqualToString:@"SECURITY_TYPE_SECUREALIGNMENT"])
        {   [_btnChangeSecureAlignment setUserInteractionEnabled:YES];
            [_switchEnableDisableAlignment setOn:YES];
        }else{
            [_btnChangeSecureAlignment setUserInteractionEnabled:NO];
            [_switchEnableDisableAlignment setOn:NO];
        }
    }
    
    else{
        [_btnChangeSecureAlignment setUserInteractionEnabled:NO];
        [_switchEnableDisableAlignment setOn:NO];
        [_switchEnableDisableFaceVoiceRec setOn:NO];
    }
}
}


- (IBAction)btnChangeSecureAlignmentTapped:(id)sender {
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:@"FLAG_FOR_MATCH_ANGLE" forKey:@"FLAG_FOR_MATCH_ANGLE"];
    sharedData.flagForMatchAngle=YES;
     [defaults removeObjectForKey:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT"];
    sharedData.flagForDisableSecureAligment=NO;
    
    [defaults synchronize];
     sharedData.isTouchIDDevice = NO;
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"captureView"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)btnDeleteVoiceProfileTapped:(id)sender {
//    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//    [defaults setObject:@"NotEnrolled" forKey:@"IS_ENROLLED"];
//    [defaults removeObjectForKey:@"SECURITY_TYPE"];
//    [defaults synchronize];

    sharedData.isFaceRecognised=NO;
    [sharedData newsaveAppStatus];
    [sharedData newloadAppStatus];
    [self viewWillAppear:YES];
    
    
}

- (IBAction)btnSetGatewayTapped:(id)sender {
    NSLog(@"Set Gateway URL");
    
    UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"setGateWay"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)btnAboutTapped:(id)sender {
    //NSLog(@"Click ABOUT");
    //NSLog(@"******buttonIndex**** %ld", (long)buttonIndex);
    NSString *version = [NSString stringWithFormat:@"Version %@ \n© Copyright 2014. EZMCOM Inc.\n All rights reserved.\nPush ID: %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"], sharedData.devicePID];
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"About EZMCOM ORD" message:version delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
    

}

- (IBAction)btnCancelTapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}



////////////////////////TOUCH//////////////////////////////////////////////////
- (IBAction)swtichValueChanged:(id)sender {
    
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:@"TOUCHID" forKey:@"AUTHENTICATION_METHOD"];
     if([sender isOn]){
        [defaults setObject:@"TOUCHID" forKey:@"AUTHTOBETURNEDON"];
         [defaults removeObjectForKey:@"DisableTouch"];
         [defaults synchronize];
         //defaults setObject:@"DisableTouch" forKey:@"DisableTouch"];
     }else
//     {
//        [defaults removeObjectForKey:@"AUTHTOBETURNEDON"];
//     }
    [defaults synchronize];
    
    if([_switchEnableDisableAlignment isOn]){
        // Secure Alignment
        // [defaults setObject:@"SECUREALIGNMENT" forKey:@"AUTHENTICATION_METHOD"];
    
        [defaults setObject:@"IS_TILT_CHANGED" forKey:@"IS_TILT_CHANGED"];
        [defaults setObject:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT" forKey:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT"];
        [defaults removeObjectForKey:@"FLAG_FOR_MATCH_ANGLE"];
        [defaults synchronize];
        
        sharedData.isTiltChanged = YES;
        sharedData.flagForDisableSecureAligment=YES;
        sharedData.flagFroEnableFaceAfterDisableAlignment=YES;
        sharedData.flagForMatchAngle=NO;
        
        CaptureViewController *vc = (CaptureViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"captureView"];
        vc.callbackMe = self;
        
        [self.navigationController pushViewController:vc animated:NO];
        [sender setOn:NO];
         return;
    }
//    if([_switchEnableDisableFaceVoiceRec isOn]){
//        //Ajay Code
//        //Need to authenticate
//        [defaults setObject:@"DoAuthenticate" forKey:@"FACE_REC"];
//        // turn of face switch yes
//        [defaults setObject:@"YES" forKey:@"TURNOFFFACE"];
//        //Dont generate otp
//        [defaults removeObjectForKey:@"GENERATE_OTP"];
//        //Dont do any approve or deny task
//        [defaults removeObjectForKey:@"APPROVE_OR_DENY"];
//        
//        [defaults synchronize];
//        VVDemoRecognizerController *vc= (VVDemoRecognizerController *)[self.storyboard instantiateViewControllerWithIdentifier:VVDemoRecognizer];
//        vc.callbackMe = self;
//        [self.navigationController pushViewController:vc animated:YES];
//        [sender setOn:NO];
//        return;
//    }
    
    if([sender isOn]){
        [self enableTouchID];
    }
    else
    {
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:@"DisableTouch" forKey:@"DisableTouch"];
        [defaults synchronize];
        [self disableTouchID];
    }
}


///////////////////////ALIGNMENT//////////////////////////////////

- (IBAction)swithAlignmentValueChanged:(id)sender {

    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:@"SECUREALIGNMENT" forKey:@"AUTHENTICATION_METHOD"];
    
     if([sender isOn]){
        [defaults setObject:@"ALIGNMENT" forKey:@"AUTHTOBETURNEDON"];
         [defaults removeObjectForKey:@"DisableAligment"];
         [defaults synchronize];
     }
//     else{
//         [defaults removeObjectForKey:@"AUTHTOBETURNEDON"];
//     }
    [defaults synchronize];

    
//    if([_switchEnableDisableFaceVoiceRec isOn]){
//        //Need to authenticate
//        [defaults setObject:@"DoAuthenticate" forKey:@"FACE_REC"];
//        // turn of face switch yes
//        [defaults setObject:@"YES" forKey:@"TURNOFFFACE"];
//        //Dont generate otp
//        [defaults removeObjectForKey:@"GENERATE_OTP"];
//        //Dont do any approve or deny task
//        [defaults removeObjectForKey:@"APPROVE_OR_DENY"];
//        
//        [defaults synchronize];
//        VVDemoRecognizerController *vc= (VVDemoRecognizerController *)[self.storyboard instantiateViewControllerWithIdentifier:VVDemoRecognizer];
//        vc.callbackMe = self;
//        [self.navigationController pushViewController:vc animated:YES];
//        [sender setOn:NO];
//        return;
//    }
    
    if([_switchEnableDisable isOn]){
        [defaults setObject:@"DIS" forKey:@"DISABLETOUCHID"];
        [defaults setObject:@"FromAlignment" forKey:@"DisableTouchIDFrom"];
        [defaults synchronize];
        
        [self disableTouchID];

        return;
    }
    
    // We make the device non touch ID
    
    if([sender isOn])
    {
        // [self turnAlignmentOn];
        //if(sharedData.dTiltValue == nil){
        if([defaults objectForKey:@"SECURE_TILT_VALUE"]==nil){
            UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"captureView"];
            [self.navigationController pushViewController:vc animated:NO];
        } else {
            

//Device specific setting
            [defaults setObject:@"IS_TILT_CHANGED" forKey:@"IS_TILT_CHANGED"];
            [defaults setObject:@"SECURITY_TYPE_SECUREALIGNMENT" forKey:@"SECURITY_TYPE"];
            [defaults setObject:@"FLAG_FOR_MATCH_ANGLE" forKey:@"FLAG_FOR_MATCH_ANGLE"];
            [defaults setObject:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT" forKey:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT"];
            [defaults synchronize];
            
            sharedData.isTiltChanged = YES;
            sharedData.isTiltEnable = YES;
            sharedData.flagForMatchAngle = NO;
            sharedData.flagForDisableSecureAligment = NO;
            [sharedData newsaveAppStatus];
            [sharedData newloadAppStatus];
        }
    }else{
        
        sharedData.isTouchIDDevice = NO;
        [sharedData newsaveAppStatus];
        [sharedData newloadAppStatus];
        sharedData.flagForMatchAngle=NO;
        [defaults removeObjectForKey:@"FLAG_FOR_MATCH_ANGLE"];
        
        [defaults setObject:@"SECURITY_TYPE_SECUREALIGNMENT" forKey:@"SECURITY_TYPE"];
        [defaults synchronize];
        sharedData.flagForDisableSecureAligment=YES;
        // sharedData.flagFroEnableFaceAfterDisableAlignment=YES;
        [defaults setObject:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT" forKey:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT"];
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:@"DisableAligment" forKey:@"DisableAligment"];
        [defaults synchronize];
         
        CaptureViewController *vc = (CaptureViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"captureView"];
        vc.callbackMe = self;
        [self.navigationController pushViewController:vc animated:NO];
        [sender setOn:NO];
        return;

    }
    [self viewWillAppear:YES];
}

/* face voice not for ORD

////////////////////FACE REC//////////////////////////////////////////////////////
- (IBAction)switchFaceRecChanged:(id)sender {
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:@"VOICEFACE" forKey:@"AUTHENTICATION_METHOD"];
    
     if([sender isOn]){
        [defaults setObject:@"VOICEFACE" forKey:@"AUTHTOBETURNEDON"];
     }
//     else{
//         [defaults removeObjectForKey:@"AUTHTOBETURNEDON"];
//     }
    [defaults synchronize];
    
    if([_switchEnableDisable isOn]){
        //Touch ID
//        [defaults setObject:@"DIS" forKey:@"DISABLETOUCHID"];
//        [defaults synchronize];
//        
//        CaptureViewController *vc = (CaptureViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"captureView"];
//        vc.callbackMe = self;
//        [self.navigationController pushViewController:vc animated:NO];
//        [sender setOn:NO];
        [self disableTouchID];
        return;
    }
    
    if([_switchEnableDisableAlignment isOn]==YES){
        
        // Secure Alignment
        
        [defaults setObject:@"IS_TILT_CHANGED" forKey:@"IS_TILT_CHANGED"];
        [defaults setObject:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT" forKey:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT"];
        [defaults removeObjectForKey:@"FLAG_FOR_MATCH_ANGLE"];
        [defaults synchronize];
        
        sharedData.isTiltChanged = YES;
        sharedData.flagForDisableSecureAligment=YES;
        sharedData.flagFroEnableFaceAfterDisableAlignment=YES;
        sharedData.flagForMatchAngle=NO;
        
        CaptureViewController *vc = (CaptureViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"captureView"];
        vc.callbackMe = self;
        
        [self.navigationController pushViewController:vc animated:NO];
        [sender setOn:NO];
        return;
    }
    if([sender isOn]){
        //[self turnFaceVoiceOn];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if([[defaults objectForKey:@"IS_ENROLLED"]isEqualToString:@"Enrolled"]) {
            [defaults setObject:@"SECURITY_TYPE_FACEVOICE" forKey:@"SECURITY_TYPE"];
            [defaults synchronize];
            sharedData.isFaceRecognised=YES;
            sharedData.isTiltEnable=NO;
            sharedData.isTouchIDEnable=NO;
            [sharedData newsaveAppStatus];
            [sharedData newloadAppStatus];
            
        }else{
            // Do Enrollment
            // set NotEnrolled for Key IS_ENROLLED
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            [defaults setObject:@"NotEnrolled" forKey:@"IS_ENROLLED"];
            [defaults setObject:@"DoEnrollment" forKey:@"FACE_REC"];
            // set DoEnrollment for Key DoEnrollment
            // Turn on face switch
            [defaults setObject:@"NO" forKey:@"TURNOFFFACE"];
            
            //Dont Generate OTP
            [defaults removeObjectForKey:@"GENERATE_OTP"];
            //  [defaults setObject:@"0" forKey:@"GENERATE_OTP"];
            
            //Dont do any approve or deny task
            [defaults removeObjectForKey:@"APPROVE_OR_DENY"];
            
            //[defaults setObject:@"0" forKey:@"APPROVE_OR_DENY"];
            
            // [defaults setObject:@"GO_TO_HOME" forKey:@"GO_TO_HOME"];
            
            [defaults synchronize];
            
            VVDemoRecognizerController *vc = (VVDemoRecognizerController *)[self.storyboard instantiateViewControllerWithIdentifier:VVDemoRecognizer];
            vc.callbackMe = self;
            [self.navigationController pushViewController:vc animated:YES];
        }

    } else {
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:@"DisableFaceVoice" forKey:@"DisableFaceVoice"];
        [defaults synchronize];
        // Authenticate First
        [defaults setObject:@"DoAuthenticate" forKey:@"FACE_REC"];
        // Turn off face switch
        [defaults setObject:@"YES" forKey:@"TURNOFFFACE"];
        //Dont Generate otp
        //[defaults setObject:@"0" forKey:@"GENERATE_OTP"];
        [defaults removeObjectForKey:@"GENERATE_OTP"];
        //Dont do any approve or deny task
        [defaults removeObjectForKey:@"APPROVE_OR_DENY"];
        //[defaults setObject:@"0" forKey:@"APPROVE_OR_DENY"];
        
        [defaults synchronize];
        VVDemoRecognizerController *vc = (VVDemoRecognizerController *)[self.storyboard instantiateViewControllerWithIdentifier:VVDemoRecognizer];
        vc.callbackMe = self;
        [self.navigationController pushViewController:vc animated:YES];

    }
}*/

- (void)enrollSuccess:(id)sender{
    NSLog(@"enroll success called from vvdemorecognizer");
}


-(void) turnAlignmentOn{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
   
    if([defaults objectForKey:@"SECURE_TILT_VALUE"]==nil){
        UIViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"captureView"];
        [self.navigationController pushViewController:vc animated:NO];
    } else {
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        if([defaults objectForKey:@"DisableAligment"]!=nil){
            [defaults removeObjectForKey:@"DisableAligment"];
              sharedData.isTiltEnable = NO;
            [defaults removeObjectForKey:@"SECURITY_TYPE"];
        }else{
            sharedData.isTiltEnable = YES;
            [defaults setObject:@"SECURITY_TYPE_SECUREALIGNMENT" forKey:@"SECURITY_TYPE"];
        }
        [defaults setObject:@"FLAG_FOR_MATCH_ANGLE" forKey:@"FLAG_FOR_MATCH_ANGLE"];
        [defaults setObject:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT" forKey:@"FLAG_FOR_DISABLE_SECURE_ALIGNMENT"];
        sharedData.isTiltChanged = NO;
        sharedData.flagForMatchAngle = NO;
        sharedData.flagForDisableSecureAligment = NO;
        [sharedData newsaveAppStatus];
        [sharedData newloadAppStatus];
        [defaults synchronize];
    }
    // [_btnChangeSecureAlignment setUserInteractionEnabled:YES];
    [self viewWillAppear:YES];
}

- (void) turnFaceVoiceOn{
 
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([[defaults objectForKey:@"IS_ENROLLED"]isEqualToString:@"Enrolled"]) {
        if([defaults objectForKey:@"DisableFaceVoice"]!=nil){
            [defaults removeObjectForKey:@"DisableFaceVoice"];
             sharedData.isFaceRecognised=NO;
            [defaults removeObjectForKey:@"SECURITY_TYPE"];
        }else{
             sharedData.isFaceRecognised=YES;
            [defaults setObject:@"SECURITY_TYPE_FACEVOICE" forKey:@"SECURITY_TYPE"];
        }
        [defaults synchronize];
        sharedData.isTiltEnable=NO;
        sharedData.isTouchIDEnable=NO;
        [sharedData newsaveAppStatus];
        [sharedData newloadAppStatus];
        
    }else{
        // Do Enrollment
        // set NotEnrolled for Key IS_ENROLLED
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:@"NotEnrolled" forKey:@"IS_ENROLLED"];
        [defaults setObject:@"DoEnrollment" forKey:@"FACE_REC"];
        // set DoEnrollment for Key DoEnrollment
        // Turn on face switch
        [defaults setObject:@"NO" forKey:@"TURNOFFFACE"];
        
        //Dont Generate OTP
        [defaults removeObjectForKey:@"GENERATE_OTP"];
        //  [defaults setObject:@"0" forKey:@"GENERATE_OTP"];
        
        //Dont do any approve or deny task
        [defaults removeObjectForKey:@"APPROVE_OR_DENY"];
        
        //[defaults setObject:@"0" forKey:@"APPROVE_OR_DENY"];
        
        // [defaults setObject:@"GO_TO_HOME" forKey:@"GO_TO_HOME"];
        
        [defaults synchronize];
        
        VVDemoRecognizerController *vc = (VVDemoRecognizerController *)[self.storyboard instantiateViewControllerWithIdentifier:VVDemoRecognizer];
        vc.callbackMe = self;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void) turnTouchOn{
    if(sharedData.isSecuritySet) {
        // DisableTouch
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if([[defaults objectForKey:@"IS_ENROLLED"]isEqualToString:@"Enrolled"]) {
            if([defaults objectForKey:@"DisableTouch"]!=nil){
                [defaults removeObjectForKey:@"DisableTouch"];
                sharedData.isTouchIDEnable=NO;
                [defaults removeObjectForKey:@"SECURITY_TYPE"];

            }else{
               sharedData.isTouchIDEnable=YES;
            [defaults setObject:@"SECURITY_TYPE_TOUCHID" forKey:@"SECURITY_TYPE"];
        }
        sharedData.isFaceRecognised=NO;
        sharedData.isTiltEnable=NO;
        [sharedData newsaveAppStatus];
        [sharedData newloadAppStatus];
            [defaults synchronize];
        [_switchEnableDisable setOn:YES];
     
        }
    }else{
        
        
        CaptureViewController *vc = (CaptureViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"captureView"];
        vc.callbackMe = self;
        [self.navigationController pushViewController:vc animated:NO];
        
        
    }
   
}
-(void) authenticateSuccess:(id)sender{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strAuth = [defaults objectForKey:@"AUTHTOBETURNEDON"];
    [defaults removeObjectForKey:@"AUTHTOBETURNEDON"];
    [defaults synchronize];
    if(strAuth == nil){
        [defaults removeObjectForKey:@"DISABLETOUCHID"];
        [defaults synchronize];
        [self viewWillAppear:YES];
        strAuth = @"";
    }
    
    if([strAuth isEqualToString:@"ALIGNMENT"]){
        [defaults removeObjectForKey:@"DISABLETOUCHID"];
        [defaults synchronize];
        [self turnAlignmentOn];
        [self viewWillAppear:YES];
    }
    
    if([strAuth isEqualToString:@"TOUCHID"]){
        
        //[self turnTouchOn];
        [self enableTouchID];
        [self viewWillAppear:YES];
        
    }
    
    if([strAuth isEqualToString:@"VOICEFACE"]){
        [defaults removeObjectForKey:@"DISABLETOUCHID"];
        [defaults synchronize];
        [self turnFaceVoiceOn];
        [self viewWillAppear:YES];
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    if (alertView==enableErrorAlert) {
        
        [defaults removeObjectForKey:@"SECURITY_TYPE"];
        [defaults synchronize];
        [_switchEnableDisable setOn:NO];
        sharedData.isTouchIDEnable=NO;
        //sharedData.isSecuritySet=NO;
        [sharedData newsaveAppStatus];
        [sharedData newloadAppStatus];
        [self viewWillAppear:YES];
    }
    if(alertView==disableErrorAlert){
        sharedData.isTouchIDEnable=YES;
        [_switchEnableDisable setOn:YES];
//        [defaults setObject:@"SECURITY_TYPE_TOUCHID" forKey:@"SECURITY_TYPE"];
//        [defaults synchronize];
        //sharedData.isSecuritySet=YES;
        [sharedData newsaveAppStatus];
        [sharedData newloadAppStatus];
        [self viewWillAppear:YES];
    }
}


- (void)dealloc {
    [_switchEnableDisable release];
    [_switchEnableDisableAlignment release];
    [_btnGatewayURL release];
    [_switchEnableDisableFaceVoiceRec release];
    [_lablePushID release];
    [_labelVersion release];
    [_lableCopyRight release];
    [_viewGetwayURL release];
    [_viewSecureAlignment release];
    [_viewFaceVoiceRec release];
    [_viewTouchID release];
    [_viewAbout release];
    [_btnChangeSecureAlignment release];
    [_btnDeleteVoiceProfile release];
    [super dealloc];
}


////////////// Function for touch authenticate & set security

-(void)enableTouchID{
    
NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"IS_SECURITY_SET"])
    {//sharedData.isSecuritySet
        if([defaults objectForKey:@"DisableTouch"])
        {
            [defaults removeObjectForKey:@"DisableTouch"];
            [defaults removeObjectForKey:@"SECURITY_TYPE"];
            sharedData.isTouchIDEnable=NO;
        }else{
            sharedData.isTouchIDEnable=YES;
            [defaults setObject:@"SECURITY_TYPE_TOUCHID" forKey:@"SECURITY_TYPE"];
        }
        sharedData.isFaceRecognised=NO;
        sharedData.isTiltEnable=NO;
         [defaults synchronize];
        [self viewWillAppear:YES];
       
    }else{
    
    NSLog(@"TouchIDDevice");
    //if(sharedData.isSecuritySet){
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error])
    {
        // Authenticate User
        NSLog(@"*****There is TouchID*******");
        NSLog(@"########################CaptureVC SAVE %@ ", sharedData.profileName);
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                localizedReason:@"Are you the device owner?"
                          reply:^(BOOL success, NSError *error) {
                              if (error) {
                                  
                                  if (error.code == LAErrorUserFallback) {
                                      NSLog(@"LAErrorUserFallback");
                                      dispatch_async (dispatch_get_main_queue(), ^{
                                          //AlertView Code
                                        enableErrorAlert = [[UIAlertView alloc] initWithTitle:@"Touch ID"
                                                                                  message:@"It has been detected that you have already setup Touch ID and enrolled a finger on this device. This application will use the feature for future authentications.On this screen you'll be asked to setup the Touch ID feature to protect your token account."
                                                                                 delegate:self
                                                                        cancelButtonTitle:@"Ok"
                                                                        otherButtonTitles:nil];
                                          [enableErrorAlert show];
                                        
                                      });
                                      
                                  }
                                  else if(error.code == LAErrorUserCancel){
                                      
                                      dispatch_async (dispatch_get_main_queue(), ^{
                                          //[self performSegueWithIdentifier:@"firstView" sender:self];
                                          // [self.navigationController popViewControllerAnimated:YES];
                                          
                                          //This code is for device specific setting
                                          // NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                          //[defaults removeObjectForKey:@"AUTHTOBETURNEDON"];
                                          if([defaults objectForKey:@"IS_SECURITY_SET"]){
                                             [defaults setObject:@"IS_SECURITY_SET" forKey:@"IS_SECURITY_SET"];
                                             [defaults setObject:@"SECURITY_TYPE_TOUCHID" forKey:@"SECURITY_TYPE"];
                                              [_switchEnableDisable setOn:YES];
                                              [defaults synchronize];
                                              [self viewWillAppear:YES];
                                          }else{
                                              [defaults removeObjectForKey:@"IS_SECURITY_SET"];
                                              [defaults removeObjectForKey:@"SECURITY_TYPE"];
                                              [_switchEnableDisable setOn:NO];
                                              [defaults synchronize];
                                              [self viewWillAppear:YES];
                                          }
                                          
                                          
                                          
                                          if(sharedData.isSecuritySet){
                                          sharedData.isSecuritySet=YES;
                                          sharedData.isTouchIDEnable=YES;
                                          [sharedData newsaveAppStatus];
                                          [sharedData newloadAppStatus];
                                              [self viewWillAppear:YES];
                                          }
                                          else{
                                              sharedData.isSecuritySet=NO;
                                              sharedData.isTouchIDEnable=NO;
                                              [sharedData newsaveAppStatus];
                                              [sharedData newloadAppStatus];
                                              [self viewWillAppear:YES];
                                          }
                                      });
                                  }
                              }
                              
                              else if (success) {
                                  
                                  
                                  dispatch_async (dispatch_get_main_queue(), ^{
                                      NSLog(@"*******Success TOUCHID******  ");
                                      // Here store values to NSuerdefaults
                                      
                                      NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                                      [defaults setObject:@"IS_SECURITY_SET" forKey:@"IS_SECURITY_SET"];
                                      [defaults setObject:@"SECURITY_TYPE_TOUCHID" forKey:@"SECURITY_TYPE"];
                                      [defaults synchronize];
                                      
                                      sharedData.isSecuritySet=YES;
                                      sharedData.isTouchIDEnable=YES;
                                      sharedData.isFaceRecognised=NO;
                                      sharedData.isTiltEnable=NO;
                                      [sharedData newsaveAppStatus];
                                      [sharedData newloadAppStatus];
                                      [self authenticateSuccess:nil];
                                      
                                  });
                              }
                              
                              else {
                                  dispatch_async (dispatch_get_main_queue(), ^{
                                      
                                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                      message:@"You are not the device owner."
                                                                                     delegate:nil
                                                                            cancelButtonTitle:@"Ok"
                                                                            otherButtonTitles:nil];
                                      [alert show];
                                  });
                              }
                              
                          }];
        
    }
    else{
        NSLog(@"TOUCHID Error %@",error);
        if (error.code == LAErrorTouchIDNotEnrolled) {
            dispatch_async (dispatch_get_main_queue(), ^{
                NSLog(@"TOUCHID Error LAErrorTouchIDNotEnrolled");
                touchIdsetupalert = [[UIAlertView alloc] initWithTitle:@"Touch ID Setup"
                                                               message:@"Your device supports Touch ID. Please enroll Touch ID."
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
                [touchIdsetupalert show];
            });
        }
        if (error.code == LAErrorTouchIDNotAvailable) {
            dispatch_async (dispatch_get_main_queue(), ^{
                //no touch id
                //not set alignemnt
                NSLog(@"TOUCHID Error LAErrorTouchIDNotAvailable");
                [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(setAllignmentView) userInfo:nil repeats:NO];
                self.navigationItem.hidesBackButton = YES;
            });
        }
    }
    
    }
}

-(void)disableTouchID{
    
    NSLog(@"TouchIDDevice");
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error])
    {
        // Authenticate User
        NSLog(@"*****There is TouchID*******");
        NSLog(@"########################CaptureVC SAVE %@ ", sharedData.profileName);
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                localizedReason:@"Are you the device owner?"
                          reply:^(BOOL success, NSError *error) {
                              if (error) {
                                  
                                  if (error.code == LAErrorUserFallback) {
                                      NSLog(@"LAErrorUserFallback");
                                      
                                      
                                      dispatch_async (dispatch_get_main_queue(), ^{
                                          //AlertView Code
                                          disableErrorAlert = [[UIAlertView alloc] initWithTitle:@"Touch ID"
                                                                                  message:@"It has been detected that you have already setup Touch ID and enrolled a finger on this device. This application will use the feature for future authentications.On this screen you'll be asked to setup the Touch ID feature to protect your token account."
                                                                                 delegate:self
                                                                        cancelButtonTitle:@"Ok"
                                                                        otherButtonTitles:nil];
                                          [disableErrorAlert show];
                                      });
                                      
                                  }
                                  else if(error.code == LAErrorUserCancel){
                                      
                                      dispatch_async (dispatch_get_main_queue(), ^{
                                          NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                          [defaults setObject:@"SECURITY_TYPE_TOUCHID" forKey:@"SECURITY_TYPE"];
                                          [defaults setObject:@"TOUCHID" forKey:@"AUTHTOBETURNEDON"];
                                          
                                          [defaults synchronize];
                                          [defaults setObject:@"FromAlignment" forKey:@"DisableTouchIDFrom"];

                                          if([[defaults objectForKey:@"DisableTouchIDFrom"] isEqualToString:@"FromAlignment"])
                                          {
                                              [defaults removeObjectForKey:@"DisableTouchIDFrom"];
                                              [_switchEnableDisableAlignment setOn:NO];
                                          }
                                          sharedData.isTouchIDEnable=YES;
                                          [_switchEnableDisable setOn:YES];
                                          [sharedData newsaveAppStatus];
                                          [sharedData newloadAppStatus];
                                          
                                          [self viewWillAppear:YES];
        
                                          //[self authenticateSuccess:nil];
                                          //[self performSegueWithIdentifier:@"firstView" sender:self];
                                          //[self.navigationController popViewControllerAnimated:YES];
                                      });
                                  }
                              }
                              
                              else if (success) {
                                  dispatch_async (dispatch_get_main_queue(), ^{
                                      NSLog(@"*******Success TOUCHID******  ");
                                      
                                      NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                                      [defaults removeObjectForKey:@"SECURITY_TYPE"];
                                      [defaults synchronize];
                                      
                                      sharedData.isTouchIDEnable=NO;
                                      [sharedData newsaveAppStatus];
                                      [sharedData newloadAppStatus];
                                      
                                      [self authenticateSuccess:nil];
                                      
                                      
                                  });
                              }
                              
                              else {
                                  dispatch_async (dispatch_get_main_queue(), ^{
                                      
                                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                      message:@"You are not the device owner."
                                                                                     delegate:self
                                                                            cancelButtonTitle:@"Ok"
                                                                            otherButtonTitles:nil];
                                      [alert show];
                                  });
                              }
                              
                          }];
        
    }
    else{
        NSLog(@"TOUCHID Error %@",error);
        if (error.code == LAErrorTouchIDNotEnrolled) {
            dispatch_async (dispatch_get_main_queue(), ^{
                NSLog(@"TOUCHID Error LAErrorTouchIDNotEnrolled");
                touchIdsetupalert = [[UIAlertView alloc] initWithTitle:@"Touch ID Setup"
                                                               message:@"Your device supports Touch ID. Please enroll Touch ID."
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
                [touchIdsetupalert show];
            });
        }
        if (error.code == LAErrorTouchIDNotAvailable) {
            dispatch_async (dispatch_get_main_queue(), ^{
                //no touch id
                //not set alignemnt
                NSLog(@"TOUCHID Error LAErrorTouchIDNotAvailable");
                [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(setAllignmentView) userInfo:nil repeats:NO];
                self.navigationItem.hidesBackButton = YES;
            });
        }
    }
}




@end
