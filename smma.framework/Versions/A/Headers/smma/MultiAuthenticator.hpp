#pragma once

#include "MultiRecognizer.hpp"
#include "AuthenticationResult.hpp"
#include "CombinationMethod.hpp"

#include <memory>

namespace sensory {

    class AuthenticationCombiner;
    class AuthenticatorPlugin;
    template <class P> class PluginRegistry;

    /**
     * Authenticates a User using both face and voice information.
     *
     * Before a user can be authenticated, he must be enrolled via the MultiEnroller.
     */
    class MultiAuthenticator : public MultiRecognizer {
    public:
        MultiAuthenticator();
        virtual ~MultiAuthenticator();

        /**
         * @return true if authentication is finished.
         */
        virtual bool hasAuthenticationResult();

        /**
         * @return the authentication result of this session. This value is invalid if hasAuthenticationResult() 
         * returns false.
         */
        virtual AuthenticationResult getAuthenticationResult();

        virtual void start();

        virtual void load(const std::vector<std::path> &modelPaths, const std::path &enrollPath, const std::path &logPath = "");

        /**
         * Sets the security level of the authenticator. Security levels are in the range [0,4].
         * The default is 2.
         */
        virtual void setSecurityLevel(int level);
        virtual int getSecurityLevel();

        /**
         * How the authentication modes are combined:
         *    COMBINE_ANY: At least one of the modes must pass (ORing).
         *    COMBINE_ALL: All of the modes must pass (ANDing). Not recommended.
         *    COMBINE_SMART: Intelligently combines all the modes.  Recommended when multiple modes are required.
         * Default: COMBINE_ANY
         */
        virtual void setCombinationMethod(int combinationMethod);
        virtual int getCombinationMethod();
        
        /**
         * If the last authentication attempt resulted in a false reject, this method can be used
         * to update the enrollment using the data collected from the attempt and improve accuracy
         * in future sessions. Returns true if any part of the enrollment was enhanced.
         */
        virtual bool enhanceEnrollment(const std::string &userId);

    protected:
        virtual void appendLogMetadata(YAML::Emitter &emitter);
        virtual AuthenticationCombiner *makeCombiner();

    protected:
        virtual RecognizerPlugin *createRecognizer(const std::string &name);
        virtual RecognizerPlugin *loadRecognizer(const std::shared_ptr<RecognizerModel> &model);
        virtual std::string getRecognizerType() const;

        std::auto_ptr<AuthenticationCombiner> _combiner;
        
        static std::auto_ptr< PluginRegistry<AuthenticatorPlugin> > _registry;
    };
}
