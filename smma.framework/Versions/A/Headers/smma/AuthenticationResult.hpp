#pragma once

#include "Mode.hpp"
#include "User.hpp"

#include <vector>
#include <sstream>
#include <algorithm>
#include <map>

namespace sensory  {
    
    class AuthenticationResult {
        
    public:
        AuthenticationResult() : _allModesValid(false) { }
        AuthenticationResult(const User &user) : _user(user), _allModesValid(false) { }
        virtual ~AuthenticationResult() { }
        
        User &getUser() { return _user; }
        void setUser(User &user) { _user = user; }
        
        void setScore(int modes, float score) {
            _scores[modes] = score;
        }
        float getScore(int modes) const {
            std::map<int,float>::const_iterator it;
            it = _scores.find(modes);
            if (it == _scores.end())
                return 1.0f;
            else
                return it->second;
        }
        
        void setIsAuthentic(int modes, bool isAuthentic) {
            _authentics[modes]= isAuthentic;
        }
        bool getIsAuthentic(int modes) const {
            std::map<int,bool>::const_iterator it= _authentics.find(modes);
            if (it == _authentics.end())
                return false;
            else
                return it->second;
        }
        void setIsValidForRequireAll(Mode mode, bool isValid) {
            _validRequireAll[mode]= isValid;
        }
        bool getIsValidForRequireAll(Mode mode) const {
            std::map<Mode,bool>::const_iterator it= _validRequireAll.find(mode);
            if (it == _validRequireAll.end())
                return false;
            else
                return it->second;
        }

        std::vector<int> getModes() const {
            std::vector<int> modes;
            for (std::map<int,float>::const_iterator it= _scores.begin(); it != _scores.end(); it++) {
                modes.push_back(it->first);
            }
            return modes;
        }

        float getMinModeScore() const {
            float minScore = 1.0f;
            for (std::map<int,float>::const_iterator it= _scores.begin(); it != _scores.end(); it++) {
                if (it->second < minScore)
                    minScore = it->second;
            }
            return minScore;
        }

        void setAllModesValid(bool val) { _allModesValid= val; }
        bool getAllModesValid() const { return _allModesValid; }
        
        std::string toString() const {
            std::ostringstream os;
            os  << "User '" << _user.getId() << "': ";
            for (std::map<int,bool>::const_iterator it= _authentics.begin(); it != _authentics.end(); it++) {
                os << modeToString(it->first) << " = " << it->second << " (" << _scores.find(it->first)->second << "), ";
            }
            return os.str();
        }
        
    protected:
        User _user;
        bool _allModesValid;
        std::map<int,float> _scores;
        std::map<int,bool> _authentics;
        std::map<Mode,bool> _validRequireAll;
    };
    
}
