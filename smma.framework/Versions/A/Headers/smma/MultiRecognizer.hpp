#pragma once

#include "RecognizerState.hpp"
#include "FaceRecognizerState.hpp"
#include "VoiceRecognizerState.hpp"
#include "Recognizer.hpp"
#include "Mode.hpp"
#include "User.hpp"
#include "data/DataDescriptor.hpp"
#include "util/Version.hpp"

#include <cstddef>
#include <vector>
#include <map>
#include <memory>

namespace std {
    typedef string path;
}

namespace YAML {
    class Emitter;
}

namespace sensory {

    class RecognizerPlugin;
    class RecognizerModel;

    /**
     * A MultiRecognizer is an abstract class defining a process of working with multi-modal 
     * real-time media data.
     */
    class MultiRecognizer : public Recognizer {
    public:
        MultiRecognizer();
        virtual ~MultiRecognizer();

        /**
         * Begins the data-intake process. This must be invoked before processData
         */
        virtual void start();

        /**
         * Ends the data-intake process. No more processData calls may follow until start() has been called again.
         */
        virtual void stop();

        /**
         * @return True if start() has been called.
         */
        virtual bool isStarted();

        /**
         * Initializes the MultiRecognizer with a given list of model paths, enrollment directory, and optional log directory.
         *
         * Note that if you're interested in only operating with one biometric (e.g., face) you only need to pass in that one model path.
         *
         * If you are using multiple biometrics (e.g., face+voice) you'll also need to pass in the "combo" model path in addition
         * to the face and voice model paths.
         */
        virtual void load(const std::vector<std::path> &modelPaths, const std::path &enrollPath, const std::path &logPath = "");

        /**
         * @return a list of possible Modes for this recognizer
         */
        virtual std::vector<int> getModes();

        /**
         * Sets the data format for the given recognition mode
         */
        virtual void setModeDescriptor(Mode mode, const DataDescriptor &descriptor);

        /**
         * Process the given media data. 
         *
         * @param mode The modality of the input data
         * @param timestampMS The timestamp, in milliseconds, of the given input data. For an image, this 
         * represents the time at which the image was captured. For audio, it represents the timestamp at 
         * the end of the audio samples. The absolute values of the timestamps do not matter, they are
         * interpreted only as values relative to each other.
         * @param data The data buffer
         * @param len The length, in bytes, of data.
         */
        virtual void processData(Mode mode, long long timestampMS, const unsigned char data[], size_t len);

        /**
         * Get the FaceRecognizerState, which represents the current face recognizer
         */
        virtual sensory::FaceRecognizerState& getFaceRecognizerState() const;

        /**
         * Get the VoiceRecognizerState, which represents the current voice recognizer
         */
        virtual sensory::VoiceRecognizerState& getVoiceRecognizerState() const;

    public:

        /**
         * @return the User object that is identified by the given user ID
         */
        virtual User getUser(const std::string &userId);

        /**
         * @return a list of all enrolled users for this recognizer
         */
        virtual std::vector<User> getUsers() const;

        /**
         * Get metadata about the last session (e.g., a start/process/stop usage)
         */
        virtual std::string getSessionMetadata();

    public:
        const std::map<Mode,RecognizerPlugin*> &getPlugins() const;
        
    protected:
        virtual RecognizerPlugin *createRecognizer(const std::string &name)= 0;
        virtual RecognizerPlugin *loadRecognizer(const std::shared_ptr<RecognizerModel> &model)= 0;
        static std::string getPluginName(const std::string &name);

        virtual std::path getUserPath(const User& user, const std::path &relPath) const;
        
        void assertLicenseGood() const;
        virtual void assertLoaded() const;
        virtual void assertStarted() const;
        virtual void assertStopped() const;
        virtual void assertHasUsers() const;
        virtual void assertHasUser(const User& user) const;
        virtual void assertHasMode() const;

        virtual RecognizerPlugin *getRecognizerPlugin(Mode mode);
        virtual const RecognizerPlugin *getConstRecognizerPlugin(Mode mode) const;
        virtual sensory::RecognizerState& getRecognizerState( Mode mode ) const ;

        virtual std::string getRecognizerType() const = 0;
        void appendModesMetadata(YAML::Emitter &emitter);
        virtual void appendLogMetadata(YAML::Emitter &emitter);

        void clearRecognizers();

    protected:
        char const * const TAG;

        std::map<Mode,RecognizerPlugin*> _recognizers;
        std::vector< std::shared_ptr<RecognizerModel> > _models;
        std::vector<std::path> _modelPaths;
        std::path _enrollPath, _logPath, _sessionLogPath;
        bool _loaded, _started;
        long long _startTimeMS;
        const int MAX_USERS;
        const int MAX_LOG_DIRS;
    };

}
