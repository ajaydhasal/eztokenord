#pragma once

#include <string>
#include <vector>

namespace sensory {

    #define combination_method_elem(entry) entry,
    #define combination_method_list(entry)   \
        entry(COMBINE_ANY) \
        entry(COMBINE_ALL) \
        entry(COMBINE_SMART)


    enum CombinationMethod {
        combination_method_list(combination_method_elem)
    };
    std::vector<CombinationMethod> getAllCombinationMethods();
    char const * combinationMethodToString(CombinationMethod combinationMethod);
    CombinationMethod combinationMethodFromString(const std::string &combinationMethod);
}
