#pragma once

namespace sensory {
    
    class Recognizer {
    public:
        virtual ~Recognizer() { }
        
        virtual void start() = 0;
        virtual void stop() = 0;
        virtual bool isStarted() = 0;
    };
    
}