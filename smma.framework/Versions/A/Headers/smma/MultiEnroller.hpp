#pragma once

#include "MultiRecognizer.hpp"
#include <memory>

namespace sensory {
    class EnrollerPlugin;
    template <class P> class PluginRegistry;

    /** \brief MultiEnroller is used for enrolling with both voice and face data.
     *
     * Before starting enrollment,
     *  * the enroller needs to be loaded
     *  * a user has to be set
     *  * the enrollment mode(s) must be set.
     */
    class MultiEnroller : public MultiRecognizer {
    public:
        MultiEnroller();
        virtual ~MultiEnroller();
        
        /**
         * Begins the enrollment process - must be called before processData().
         */
        virtual void start();
        /**
         * Stops the enrollment process. 
         */
        virtual void stop();
        
        /**
         * @return true if the enrollment is complete.
         */
        virtual bool isEnrollmentDone();

        /**
         * Saves the enrollment to disk.
         */
        virtual void save();
        
        /**
         * @return the User that is currently being enrolled
         */
        virtual User getCurrUser() const;

        /**
         * Sets the user to be enrolled
         */
        virtual void setCurrUser(const User &user);

        /**
         * Recreates all existing enrollments from disk. This can be useful in the case when SMMA has been
         * upgraded and has changed its file formats. Warning: This process is potentially very expensive.
         */
        static void regenerate(const std::path &modelPath, const std::path &enrollPath);
        
    public:
        /**
         * Removes the enrollment for a given User.
         */
        virtual void unEnrollUser(User &user);

    protected:
        virtual RecognizerPlugin *createRecognizer(const std::string &name);
        virtual RecognizerPlugin *loadRecognizer(const std::shared_ptr<RecognizerModel> &model);
        virtual std::string getRecognizerType() const;
        
        void assertCurrUser();
        
    protected:
        char const * const TAG;
        User _currUser;

        static std::auto_ptr< PluginRegistry<EnrollerPlugin> > _registry;
    };
    
}
