#pragma once

#include "Mode.hpp"

#include <string>
#include <vector>

namespace std{
    typedef string path;
}

namespace sensory {
    class User {
    public:
        User();
        User(const std::string &userId);
        User(const User &from) : _userId(from._userId), _saveDir(from._saveDir) {
        }
        virtual ~User();

        virtual void load(const std::path &dir);
        
        virtual std::string getId() const;
        virtual std::string getSaveDir() const;
        
        virtual std::string getProperty(const std::string &property) const;
        virtual void setProperty(const std::string &property, const std::string &value);
        virtual std::vector<std::path> getEnrollmentFiles(Mode mode) const;
        
        static User loadUser(const std::path &dir);
        
    protected:
        std::string _userId, _saveDir;
    };

    /// \cond HIDDEN_SYMBOLS
    struct UserComparator : std::binary_function<const User&, const User&, bool> {
        bool operator ()(const User &user1, const User &user2) {
            return user1.getId().compare(user2.getId()) < 0;
        }
    };
    
    class UserMatcher : public std::unary_function<const User&, bool> {
    public:
        UserMatcher(const User &user) : _user(user) {
        }
        bool operator ()(const User &user) {
            return _user.getId() == user.getId();
        }
    protected:
        const User &_user;
    };
    /// \endcond
}
