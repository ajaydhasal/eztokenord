#pragma once

namespace sensory {
    
    enum SecurityLevel {
        LOWEST,
        LOW,
        MEDIUM,
        HIGH,
        HIGHEST
    };
    
}
