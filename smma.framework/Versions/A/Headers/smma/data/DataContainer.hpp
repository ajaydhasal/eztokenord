#pragma once

#include <string>
#include <stdexcept>

#include "Mode.hpp"
#include "data/DataDescriptor.hpp"

namespace std {
    typedef std::string path;
}

namespace sensory {
    /**
     * Wraps a block of media data with a sensory::DataDescriptor containing metadata
     */
    class DataContainer {
    public:
        virtual ~DataContainer() { }
        virtual const DataDescriptor &getDataDescriptor() const = 0;
        virtual unsigned char* getData() const = 0;
        virtual size_t getDataSize() const = 0;
    };
    
    class ImageContainer : public DataContainer {
    public:
        virtual ~ImageContainer() { }
        virtual const ImageDescriptor &getImageDescriptor() const = 0;

        virtual ImageContainer *copy() { return NULL; }
    };
    
    class AudioContainer : public DataContainer {
    public:
        virtual ~AudioContainer() { }
        virtual const AudioDescriptor &getAudioDescriptor() const = 0;

        virtual AudioContainer *copy() { return NULL; }
        virtual size_t getLengthInMS() const {
            const AudioDescriptor& format = getAudioDescriptor();
            return ( ((float)getDataSize()) / (format.getSampleSize() * format.getSampleRate() * format.getNumChannels() ) ) * 1000.0f;
        }
    };

    /** 
     * Convenience method for reading images. This function relies on [OpenCV's `imread` function](http://docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video.html#imread), and supports the same file formats it does.
     * The invoker of this function is responsible for calling delete on the result ImageContainer.
     * \param path The file system path of the image file
     */
    ImageContainer *readImage(const std::path &path) throw(std::runtime_error);

    /**
     * Convenience method for reading audio files from disk. Supports RIFF-format wave files at any sample rate.
     * In the case of stereo, only the left channel is used.
     *
     * \param path The file system path of the audio file
     * \param desiredSampleRate The sample rate, in samples per second, of the audio in the result AudioContainer. 
     * Setting this parameter might result in resampling.
     */
    AudioContainer *readAudio(const std::path &path, int desiredSampleRate= 0) throw(std::runtime_error);

    /**
     * Convenience method for reading multiple audio files from disk into memory.
     *
     * \param paths Vector of file system paths.
     * \param desiredSampleRate The sample rate, in samples per second, of the audio in the result AudioContainer.
     * Setting this parameter might result in resampling.
     */
    AudioContainer *readAudio(const std::vector<std::path> &paths, int paddingMS= 500, int desiredSampleRate= 16000) throw(std::runtime_error);
}
