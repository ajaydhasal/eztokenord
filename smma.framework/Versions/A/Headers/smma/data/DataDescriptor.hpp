#pragma once

#include <string>

namespace sensory {
    
    class DataDescriptor {
    public:
        virtual ~DataDescriptor() { }
    };
    
    #define imagetype_enum_elem(entry) entry,
    #define imagetype_enum_list(entry)   \
        entry(IMAGETYPE_UNKNOWN)         \
        entry(IMAGETYPE_FEAT_8)          \
        entry(IMAGETYPE_GRAY_8)          \
        entry(IMAGETYPE_BGR_8)           \
        entry(IMAGETYPE_BGRA_8)          \
        entry(IMAGETYPE_RGB_8)           \
        entry(IMAGETYPE_RGBA_8)          \
        entry(IMAGETYPE_NV21)

    class ImageDescriptor : public DataDescriptor {
    public:
        enum ImageType {
            imagetype_enum_list(imagetype_enum_elem)
        };
        
        ImageDescriptor() : _rows(0), _cols(0), _orientation(0), _type(IMAGETYPE_BGR_8) {
        }
        ImageDescriptor(int rows, int cols, int orientation= 0, ImageType type= IMAGETYPE_BGR_8) : _rows(rows), _cols(cols), _orientation(orientation), _type(type) {
        }
        ImageDescriptor(const ImageDescriptor &from) {
            copy(from);
        }
        ImageDescriptor &operator =(const ImageDescriptor &from) {
            return copy(from);
        }
        
        virtual ~ImageDescriptor() { }
        
        virtual int getRows() const { return _rows; }
        virtual int getCols() const { return _cols; }
        virtual int getOrientation() const { return _orientation; }
        virtual ImageType getType() const { return _type; }

        virtual void setRows( int rows ) { _rows = rows; }
        virtual void setCols( int cols ) { _cols = cols; }
        virtual void setOrientation( int orientation ) { _orientation = orientation; }
        virtual void setImageType( ImageType type ) { _type = type; }
    protected:
        ImageDescriptor &copy(const ImageDescriptor &from) {
            _rows= from.getRows();
            _cols= from.getCols();
            _orientation= from.getOrientation();
            _type= from.getType();
            return *this;
        }
        
    protected:
        int _rows, _cols, _orientation;
        ImageType _type;
    };
    
    class AudioDescriptor : public DataDescriptor {
    public:
        AudioDescriptor()
        : _sampleRate(0), _sampleSize(0), _numChannels(1)
        {
        }
        AudioDescriptor(int sampleRate, size_t sampleSize, int numChannels)
        : _sampleRate(sampleRate), _sampleSize(sampleSize), _numChannels(numChannels)
        {
        }
        AudioDescriptor(const AudioDescriptor &from) {
            copy(from);
        }
        AudioDescriptor &operator =(const AudioDescriptor &from) {
            return copy(from);
        }
        virtual ~AudioDescriptor() { }
        
        virtual int getSampleRate() const { return _sampleRate; }
        virtual size_t getSampleSize() const { return _sampleSize; }
        virtual int getNumChannels() const { return _numChannels; }

        virtual void setSampleRate( int sampleRate ) { _sampleRate = sampleRate; }
        virtual void setSampleSize( size_t sampleSize ) { _sampleSize = sampleSize; }
        virtual void setNumChannels( int numChannels ) { _numChannels = numChannels; }

        bool isInitialized() const {
            return _sampleRate != 0 && _sampleSize != 0;
        }
        
    protected:
        AudioDescriptor &copy(const AudioDescriptor &from) {
            _sampleRate= from.getSampleRate();
            _sampleSize= from.getSampleSize();
            _numChannels= from.getNumChannels();
            return *this;
        }
        
    protected:
        int _sampleRate;
        size_t _sampleSize;
        int _numChannels;
    };
}
