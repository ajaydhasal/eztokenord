#pragma once

#include <vector>
#include <string>
#include <cstdlib>
#include <stdexcept>
#include <map>

#include "DataContainer.hpp"
#include "Mode.hpp"

struct sfs_s;
typedef struct sfs_s sfs_t;

namespace sensory {
    
    class AVDataStore {
    public:
        AVDataStore(const std::path &dataStorePath, const std::string &imageExt = "", const std::string &audioExt = "");
        virtual ~AVDataStore();
        
        const std::path &getPath() { return _dataStorePath; }
        
        // For using a subset of the datastore. One line of text for each session to be
        // included. Line format: user_name/session_name
        void loadUserSessions(const std::path &userSessionsFile);
        
        std::vector<std::string> getUsers();
        std::vector<std::string> getSessions(const std::string &user);
        std::vector<std::path> getSessionImages(const std::string &user, const std::string &session);
        std::vector<std::path> getSessionAudio(const std::string &user, const std::string &session);
        std::path getSessionPath(const std::string &user, const std::string &session);
        
        std::path getMediaPropertyFile(const std::path &mediaPath);
        void writeMediaProperty(const std::path &mediaPath, const std::string &property, const std::string &value);
        std::string readMediaProperty(const std::path &mediaPath, const std::string &property);

        std::string getRandomUser(const std::string notuser= "") {
            std::vector<std::string> users= getUsers();
            int loops= 0;
            const int max= 50;
            
            int idx= rand() % users.size();
            while (!validUser(users[idx]) || users[idx] == notuser) {
                idx= rand() % users.size();
                if (loops++ > max) throw std::runtime_error("cannot get valid random user");
            }
            
            return users[idx];
        }
        
        std::string getRandomSession(const std::string &user, const std::string &notsession= "") {
            std::vector<std::string> sessions= getSessions(user);
            int loops= 0;
            const int max= 50;
            
            int idx= rand() % sessions.size();
            while (!validSession(user, sessions[idx]) || sessions[idx] == notsession) {
                idx= rand() % sessions.size();
                if (loops++ > max) throw std::runtime_error("cannot get valid random session");
            }
            
            return sessions[idx];
        }
        
        bool validUser(const std::string &user) {
            int numValidSessions= 0;
            std::vector<std::string> sessions= getSessions(user);
            
            for (std::vector<std::string>::const_iterator it= sessions.begin(); it != sessions.end(); it++) {
                if (validSession(user, *it)) numValidSessions++;
                
                // we need more than one valid session for a valid user
                if (numValidSessions > 1) return true;
            }
            
            return false;
        }
        
        bool validSession(const std::string &user, const std::string &session) {
            return getSessionImages(user, session).size() > 0 &&
            getSessionAudio(user, session).size() > 0;
        }    
        
    protected:
        std::path _dataStorePath;
        std::string _imageExt;
        std::string _audioExt;
        std::map< std::string, std::vector<std::string> > _userSessions;
        sfs_t *_sfs;
    };
    
}
