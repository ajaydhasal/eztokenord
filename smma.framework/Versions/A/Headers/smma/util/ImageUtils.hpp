#pragma once

#include <string>

namespace std { typedef string path; }

namespace sensory {
    
    class ImageUtils {
    public:
        static void makeCenterGradientImage(size_t rows, size_t cols, int gradientRGB, int centerARGB, const std::path &path);
        
    protected:
        ImageUtils() { }
    };
    
}
