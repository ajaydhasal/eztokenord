#pragma once

namespace sensory {
    char const * getSmmaVersion();
    char const * getSmmaCopyright();
    long long getSmmaExpiration();
    bool isSmmaExpired();
}
