#pragma once
#include <string>
#include "RecognizerState.hpp"

#define face_status_list(enum_entry) \
    enum_entry(FACE_UNKNOWN) \
    enum_entry(FACE_OK)   \
    enum_entry(FACE_POOR) \
    enum_entry(FACE_NONE)

#define light_status_list(enum_entry) \
    enum_entry(LIGHT_UNKNOWN) \
    enum_entry(LIGHT_OK)  \
    enum_entry(LIGHT_LOW) \
    enum_entry(LIGHT_NONE)

namespace sensory {
    /**
     * An interface to a face recognizer. Supports reading the current
     * state of the recognizer as well as setting common properties.
     */
    class FaceRecognizerState : public RecognizerState {
    public:
        /**
         * The state of the face detection
         */
        enum FaceStatus {
            face_status_list(enum_elem)
        };
        enum LightStatus {
            light_status_list(enum_elem)
        };
        virtual ~FaceRecognizerState() { }

        /**
         * Whether to use "liveness" features in face recognition
         * Defaults to false
         */
        virtual void setUseLiveness( bool useLiveness )  = 0;
        virtual bool getUseLiveness() const = 0;

        /**
         * The minimum number of frames with valid faces necessary for recognition
         * The default is 2.
         */
        virtual void setTrackMinFrames( int minFrames )  = 0;

        /**
         * @return the minimum number of frames with valid faces necessary for recognition.
         */
        virtual int getTrackMinFrames() const = 0;

        /**
         * The number of frames to skip when data first comes in.
         */
        virtual void setInitialSkipFrames( int numFrames )  = 0;
        
        /**
         * @return the number of frames to skip when data first comes in.
         */
        virtual int getInitialSkipFrames() const = 0;
        
        /**
         * The last image's face status
         */
        virtual FaceStatus getFaceStatus() const = 0;

        /**
         * The last image's lighting status
         */
        virtual LightStatus getLightStatus() const = 0;

        /*
         * The last image's brightness (raw light status).
         */
        virtual float getBrightness() const = 0;
        
        virtual char const * getFaceStatusString() const;
        static char const * getFaceStatusString(FaceStatus status);

        virtual char const * getLightStatusString() const;
        static char const * getLightStatusString(LightStatus status);
    };
}
