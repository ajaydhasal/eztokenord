#pragma once

#include "RecognizerState.hpp"

#include <string>
#include <vector>

#define noise_status_list(enum_entry) \
    enum_entry(NOISE_UNKNOWN)         \
    enum_entry(NOISE_OK)              \
    enum_entry(NOISE_MED)             \
    enum_entry(NOISE_HIGH)

namespace sensory {
    /**
     * An interface to a voice recognizer. Supports reading the current state of
     * the recognizer and setting some common properties on it.
     */
    class VoiceRecognizerState : public RecognizerState {
    public:
        enum NoiseStatus {
            noise_status_list( enum_elem )
        };
        virtual ~VoiceRecognizerState() { }

        /**
         * The current energy level in the sound. In the range [0,1]
         */
        virtual float getEnergy() const = 0;

        /**
         * The current noise status
         */
        virtual NoiseStatus getNoiseStatus() const = 0;

        /**
         * Timestamps of speech "end-points" detected so far.
         */
        virtual std::vector<long long> getEndpoints() const = 0;

        /**
         * Number of speech "end-points" detected so far.
         */
        virtual size_t getNumEndpoints() const = 0;

        /**
         * Whether the voice recognizer has finished
         */
        virtual bool isDone() const { return false; }

        /**
         * The maximum number of enrollments for voice
         */
        virtual int getMaxEnrollments() const = 0;
        
        /**
         * For testing on whole wav files
         */
        virtual void setExpectBatchAudio(bool val) = 0;

        virtual char const * getNoiseStatusString() const;
        static char const * getNoiseStatusString(NoiseStatus status);

        /**
         * Sets the quality-check level. Levels are in the range [0,4], where 0 is off.
         * Depending on the model that's been loaded, this may be used differently.
         * It may be ignored, or used for phrase-verification.
         */
        virtual void setQualityCheckLevel(int level) = 0;
        virtual int getQualityCheckLevel() = 0;
    };
}
