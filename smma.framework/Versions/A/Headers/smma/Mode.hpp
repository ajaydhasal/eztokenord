#pragma once

#include <string>
#include <vector>

namespace sensory {
    
    #define mode_enum_elem(entry, val) entry=val,
    #define mode_enum_list(entry)   \
        entry(NONE  , 0     )       \
        entry(VOICE , (1<<0))       \
        entry(FACE  , (1<<1))


    enum Mode {
        mode_enum_list(mode_enum_elem)
    };
    std::vector<Mode> getAllModes();
    std::string modeToString(int mode);
    int modeFromString(const std::string &mode);
    bool isSingleMode(int modeCombo);
}
