#pragma once

#define enum_elem(val)    val,
#define string_elem(val) #val,

namespace sensory {
class RecognizerState {
public:
    virtual ~RecognizerState() { }
    virtual int getMaxEnrollments() const = 0;
};
}
