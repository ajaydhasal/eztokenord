
//Rijndael.h

#ifndef __RIJNDAEL_H__
#define __RIJNDAEL_H__

#include <cstring>

using namespace std;

class CRijndael {
public:

    enum {
        ECB = 0
    };

private:

    enum {
        DEFAULT_BLOCK_SIZE = 16
    };

    /* multiply two elements of GF(2^m)
     * needed for MixColumn and InvMixColumn
     */
    static int Mul(int a, int b) {
        return (a != 0 && b != 0) ? AntilogTable[(LogTable[a & 0xFF] + LogTable[b & 0xFF]) % 255] : 0;

    }


    //Convenience method used in generating Transposition Boxes

    static int Mul4(int a, char b[]) {
        if (a == 0)
            return 0;
        a = LogTable[a & 0xFF];
        int a0 = (b[0] != 0) ? AntilogTable[(a + LogTable[b[0] & 0xFF]) % 255] & 0xFF : 0;
        int a1 = (b[1] != 0) ? AntilogTable[(a + LogTable[b[1] & 0xFF]) % 255] & 0xFF : 0;
        int a2 = (b[2] != 0) ? AntilogTable[(a + LogTable[b[2] & 0xFF]) % 255] & 0xFF : 0;
        int a3 = (b[3] != 0) ? AntilogTable[(a + LogTable[b[3] & 0xFF]) % 255] & 0xFF : 0;
        return a0 << 24 | a1 << 16 | a2 << 8 | a3;
    }

public:
    //CONSTRUCTOR
    CRijndael();

    //DESTRUCTOR
    virtual ~CRijndael();

    //Expand a user-supplied key material into a session key.
    // key        - The 128-bit user-key to use.
    // keylength  - 16bytes
    // blockSize  - The block size in bytes of this Rijndael (16 bytes).
    int MakeKey(char const* key);

private:

    //Convenience method to encrypt exactly one block of plaintext, assuming
    //Rijndael's default block size (128-bit).
    // in         - The plaintext
    // result     - The ciphertext generated from a plaintext using the key
    int DefEncryptBlock(char const* in, char* result);

    //Convenience method to decrypt exactly one block of plaintext, assuming
    //Rijndael's default block size (128-bit).
    // in         - The ciphertext.
    // result     - The plaintext generated from a ciphertext using the session key.
    int DefDecryptBlock(char const* in, char* result);

public:
    //Encrypt exactly one block of plaintext.
    // in           - The plaintext.
    // result       - The ciphertext generated from a plaintext using the key.
    int EncryptBlock(char const* in, char* result);

    //Decrypt exactly one block of ciphertext.
    // in         - The ciphertext.
    // result     - The plaintext generated from a ciphertext using the session key.
    int DecryptBlock(char const* in, char* result);

    int Encrypt(char const* in, char* result, size_t n);

    int Decrypt(char const* in, char* result, size_t n);

    //Get Key Length

    int GetKeyLength() {
        if (false == m_bKeyInit)
            return -1;
        return m_keylength;
    }

    //Block Size

    int GetBlockSize() {
        if (false == m_bKeyInit)
            return -1;
        return m_blockSize;
    }

    //Number of Rounds

    int GetRounds() {
        if (false == m_bKeyInit)
            return -1;
        return 10;
    }


public:
    //Null chain
    static char const* sm_chain0;

private:
    static const int AntilogTable[256];
    static const int LogTable[256];
    static const char SBox[256];
    static const char SBoxInv[256];
    static const int sm_T1[256];
    static const int sm_T2[256];
    static const int sm_T3[256];
    static const int sm_T4[256];
    static const int sm_T5[256];
    static const int sm_T6[256];
    static const int sm_T7[256];
    static const int sm_T8[256];
    static const int sm_U1[256];
    static const int sm_U2[256];
    static const int sm_U3[256];
    static const int sm_U4[256];
    static const char sm_rcon[30];
    static const int sm_shifts[3][4][2];
    //Error Messages
    static char const* sm_szErrorMsg1;
    static char const* sm_szErrorMsg2;
    //Key Initialization Flag
    bool m_bKeyInit;
    //Encryption (encryptKeys) round key
    int** encryptKeys;
    //Decryption (decryptKeys) round key
    int** decryptKeys;
    //Key Length
    int m_keylength;
    //Block Size
    int m_blockSize;


};

#endif // __RIJNDAEL_H__

