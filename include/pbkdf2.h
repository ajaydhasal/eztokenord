#ifndef ___PBKDF2_HDR___
#define ___PBKDF2_HDR___

#include <stdlib.h>

extern int pbkdf2_generateDerivedKey(char *password, size_t Plen, const char *salt,
			size_t Slen, unsigned int c, char *derivedKey,
			size_t dkLen);

#endif
