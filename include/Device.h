/* Device.h */
/* Copyright (C) 2006-2012 ActivIdentity
 * All rights reserved.
 *
 * This header file is part of the SDK for iOS package written
 * by ActivIdentity.
 * 
 * Copyright remains ActivIdentity's, and as such any Copyright notices in
 * the code are not to be removed.
 * If this package is used in a product, a valid licensing agreement with 
 * ActivIdentity must be established.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, is not permitted.
 * 
 * THIS SOFTWARE IS PROVIDED BY ACTIVIDENTITY ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 */

#ifndef ___DEVICE_HDR___
#define ___DEVICE_HDR___

#include <stdlib.h>

/**
* \defgroup SDKGlobalDefinitions Global variables and definitions for Device 
* @{
*/


/*! 
   A constant identifying the version of the Device. 
*/
#define VERSION "4.0.0.9"
/*! 
   A constant used internally by Device object to identify itself as Version-1 or a Version-2
*/
#define V1_MODE 1
/*! 
   A constant used internally by Device object to identify itself as Version-1 or a Version-2
*/
#define V2_MODE 2

#ifndef BYTE
typedef unsigned char BYTE;
#endif
/*! 
   A constant to evaluate against return error code value indicating an invalid state of Device object to perform the requested method
*/
#define INVALID_STATE -5
/*! 
   A constant to evaluate against return error code value indicating a bad parameter is passed to the requested method
*/
#define BAD_PARAMETER -2
/*! 
   A constant to evaluate against return error code value indicating an incorrect PIN is passed in as an argument to  resurrect(unsigned char* bdev, char* pin, char* env)
*/
#define PIN_ERROR -3
/*! This enum defines constants for evaluating various modes of operation supported by Device */
typedef enum {
    OTP = 0, /*!< OTP mode only. value 0 */
    OTP_CR, /*!< OTP and Challenge Response mode only. value 1 */
    OTP_SIG, /*!< OTP and Plain Signature mode only. value 2 */
    OTP_CR_SIG, /*!< OTP, Challenge Response and Plain Signature modes. value 3 */
    ALGO_NOTSUPPORTED /*!< Error state. No modes supported. value 4 */
} OtpAlgo;
/*! This enum defines constants for evaluating the type of Device */
typedef enum {
    HOTP = 0, /*!< Event Based OTP mode. value 0 */
    TOTP /*!< Time Based OTP mode. value 0 */
} _TokenType;
/*! This enum defines constants for evaluating the type of SHA algorithms that might be supported by Device */
typedef enum {
    SHA_1, /*!< SHA-1. value 0 */
    SHA_256, /*!< SHA-256 (Not supported in VERSION 1.X). value 1 */
    SHA_512, /*!< SHA-256 (Not supported in VERSION 1.X). value 2 */
    SHA_NOTSUPPORTED /*!< Undefined Unspecified Hashing allgorithm. value 3 */
} ShaAlgo;
/*! This enum defines constants for evaluating the type of symmetric encryption algorithms that might be supported by Device */
typedef enum {
    TDES = 0, /*!< 192-bit Triple DES. value 0 */
    AES /*!< 256-bit AES (Not supported in VERSION 1.X). value 0 */
} _EncType;
#define RANDOMNOLENGTH        9
#define OTP_TEXT_LEN  8
#define MAX_OTP_LEN  10
#define OTP_DIGEST_LENGTH 20
#define OTP_SECRET_KEY_LENGTH 20
#define SHA1_HASH_SIZE 20


/**@}*/


/*! \class Device Device.h "include/Device.h"
 *  \brief This is the OTP Token Device class
 *
 * Device class allows integrating mobile applications in iOS to implement RFC 4226 - Event-Based One-Time Password Algorithm, RFC 6238 - TOTP: Time-Based One-Time Password Algorithm and RFC 6287 - OCRA: OATH Challenge-Response Algorithm for Challenge Response and Plain Signature (OCRA-1:HOTP-SHA1-8:C-QAXX, OCRA-1:HOTP-SHA1-8:QAXX-TXXX)  
 */

class Device {
public:
    /**
    * Constructor of a Version-1 or Version-2 Device. Usage of this method is recommended only to advance users of the Device library 
    * @param wszSNO initialize the Serial Number
    * @param wszActCode the activation code 
    * @param wszRegCodeSeed up to 512 bytes of randomness for a Version-2 Device or the Check Code for Version-1 Device
    * @param mode Version-1 (V1_MODE) or Version-2 (V2_MODE)
    */
    Device(char* wszSNO, char* wszActCode, const char* wszRegCodeSeed, int mode);
    /**
    * Constructor of Device. It is internally invoked by the global method activateToken(char* wszSNO, char* wszActCode, char* wszRegCodeSeed, int* errcode). Usage of this method is recommended only to advance users of the Device library 
    * @param lDateCreated a long value to initialize the creation date 
    * @param lLastUsed initialize the last usage date for OTP generation
    * @param iTokenType initialize the type of Device (Event or Time based)
    * @param nOtpAlgo initialize the OTP algorithms to be supported by Device (OTP, Challenge Response, Plain Signature)
    * @param iSHAType initialize SHA hash algorithms to be supported
    * @param iOTPLength initialize the length of OTP digits to be generated (6 | 8 | 10)
    * @param iTimeStepInSecs initialize the time step in seconds to be used for TOTP and OCRA algorithms (30 | 60)
    * @param lInitialEventCounter initialize the initial event counter
    * @param lEventCounter initialize the current event counter
    * @param strSerialNumber initialize the Serial Number
    * @param strRegistrationCode initialize the Registration Code
    * @param strHexRND 5 bytes (40 Bits) of randomness
    * @param abRawSecret HOTP secret for the Device
    * @param strVersion version
    * @param bLocked the status of the Device (locked or unlocked). The abRawSecret is in encrypted form if the status is locked
    * @param bPBKD initialize the PIN policy mode. Depending on this the Device will either block incorrect PIN values during resurrect() or generate an invalid HOTP secret and spoof One-Time Passwords
    * @param bPinType PIN enabled or without PIN 
    * @param iRCdisplay whether to allow display of registration code or not 
    * @param pinHash optionally the hash of PIN 
    * @param strActivationCode the activation code 
    * @param abB64EncPIN optionally an encrypted and base64 encoded PIN
    * @see resurrect(unsigned char* bdev, char* pin, char* env)
    */
    Device(unsigned long int lDateCreated, long lLastUsed, int iTokenType, OtpAlgo nOtpAlgo, int iSHAType,
        int iOTPLength, int iTimeStepInSecs, long lInitialEventCounter, long lEventCounter,
        char* strSerialNumber, char* strRegistrationCode, char* strHexRND,
        BYTE* abRawSecret, char*strVersion, bool bLocked, bool bPBKD, bool bPinType, int iRCdisplay,
        unsigned char* pinHash, char* strActivationCode, char* abB64EncPIN);
    /**
    * This method unlocks the Device. It is internally invoked by the resurrect method. Usage of this method is recommended only to advance users of the Device library 
    * @param pin a user defined PIN for protected access to the Device
    * @param env a container fingerprint. A unique key identifying the electronic device that contains the Device
    * @see resurrect(unsigned char* bdev, char* pin, char* env)
    * @return 0 for SUCCESS and negative integer value (error code) in case of FAILURE
    */
    int unlock(char* pin, char* env);
    /**
    * This method clears the state of ENCRYPTED SECRET. The PIN and ENV parameter of very next call to persist(...) will be used to ENCRYPT the SECRET
    * @see persist(char* pin, char* env)
    * @see resurrect(unsigned char* bdev, char* pin, char* env)
    * @return 0 for SUCCESS and negative integer value (error code) in case of FAILURE
    */
    int reset();
    /**
    * This method allows persisting of the Device object for a future use. It preserves the state of the Device and returns encrypted bytes representation of the Device
    * @param pin a user defined PIN for protected access to the Device
    * @param env a container fingerprint. A unique key identifying the electronic device that contains the Device
    * @see resurrect(unsigned char* bdev, char* pin, char* env)
    * @see size()
    * @return The secured bytes representation of the Device object
    */
    unsigned char* persist(char* pin, char* env);
    /**
    * This method allows persisting of the Device object for a future use. It preserves the state of the Device and returns encrypted bytes representation of the Device
    * @see resurrect(unsigned char* bdev)
    * @see size()
    * @return The secured bytes representation of the Device object
    */
    unsigned char* persist(); 
    /**
    * This method instantiates a Device object from an earlier persisting of the Device object 
    * @param bdev the byte array containing a persisted Device object. The byte array must be from a prior call to persist method without encryption parameters
    * @see persist()
    * @return The Device object
    */
    static Device* resurrect(unsigned char* bdev); 
    /**
    * This method instantiates a Device object from an earlier persisting of the Device object 
    * @param bdev the byte array containing a persisted Device object. The byte array must be from a prior call to persist method with encryption parameters
    * @param pin a user defined PIN for protected access to the Device
    * @param env a container fingerprint. A unique key identifying the electronic device that contains the Device
    * @see persist(char* pin, char* env)
    * @return The Device object
    */
    static Device* resurrect(unsigned char* bdev, char* pin, char* env); 
    /**
    * This method calculates the buffer size required storing the device object in bytes
    * @see persist(char* pin, char* env)
    * @see persist()
    * @return The number of bytes required required in memory allocation 
    */
    int size(); 
    /**
    * This method returns the version of SDK
    * @return The version of SDK in major.minor.build 
    */
    char const* const getVersion();
    /**
    * This method returns the Serial Number
    * @return The Serial Number of the Device object
    */
    char const* const getSerialNumber(); 
    /**
    * This method returns the Registration Code
    * @return The Registration Code of the Device object
    */
    char const* const getRegistrationCode(); 
    /**
    * This method returns the Activation Code
    * @return The Activation Code that was used during activation of the Device object
    */
    char const* const getActivationCode(); 
    /**
    * This method returns the Initial Event Counter
    * @return The Initial Event Counter of the Device object
    */
    long getIEC(); 
    /**
    * This method returns the current Event counter. The method returns a meaningful value only in case of an Event based Device
    * @see getTokenType()
    * @return The current Event Counter of the Device object
    */
    long getEC(); 
    /**
    * This method returns the current Clock value. The method returns a meaningful value only in case of an Time based Device
    * @see getTokenType()
    * @return The current Clock Counter value of the Device object
    */
    unsigned long long int getClock(); 
    /**
    * This method returns the date when the Token device activated
    * @return The creation date of the Device object (Time in seconds since EPOCH)
    */
    unsigned long int getCreatedDate();
    /**
    * This method returns the date when the Token device was last used for OTP | CR | SIG generation
    * @return The last use date of OTP generation from Device object (Time in seconds since EPOCH)
    */
    long getLatUsedDate(); 
    /**
    * This method returns the length of OTP generated by this Device
    * @return The length as an int for number of digits in OTP generated by this Device. 6 | 8 | 10
    */
    int getOTPLength();
    /**
    * This method returns the SHA algorithm used by this Device
    * @return The int value representing SHA algorithm. SHA_1 = 0 | SHA_256 = 1 | SHA_512 = 3. Current version only supports SHA_1
    */
    int getSHAType();
    /**
    * This method returns the Time Step used for time based OTP/ CR/ SIG generation
    * @return The time step value in seconds (30 | 60) 
    */
    int getTimeStep();
    /**
    * This method returns the type (Event | Time) of Device 
    * @return the int value identifying Device as Event based or Time based. HOTP = 0, TOTP = 1
    */
    int getTokenType();
    /**
    * This method returns the operational modes supported by the Device 
    * @return the int value identifying the various operational modes of the Device. OTP only = 0, OTP and CR = 1, OTP and SIG = 2, OTP, CR and SIG = 3
    */
    int getTokenMode();
    /**
    * This method provides information to integrating application if Registration Code of the Device must be displayed/ made accessible or not
    * @return the int value YES = 1, NO = 0
    */
    int isRCDisplayEnabed();
    /**
    * This method provides information on the encryption algorithm used by Device
    * @return the int value identifying Triple DES with 192 bit = 0 | AES 256 bit = 1
    */
    int getEncType();
    /**
    * This method provides information on the PIN enforcement requirement of the Device
    * @return the int value identifying PIN policy. NO PIN = 0 | PIN ENABLED = 1
    */
    int getPinType();
    /**
    * This method provides information on the behavior of Device in the event of a wrong PIN input patameter during resurrection of PIN ENABLED Device. In case of an incorrect PIN parameter the Device will either return an ERROR keeping count of consecutive errors that might lead to LOCKING of the Device OR simply return an invalid Device object that will produce incorrect OTP/ CR/ SIG values 
    * @see resurrect(unsigned char* bdev, char* pin, char* env)
    * @see getPinType()
    * @return the int value identifying this PIN policy. BLOCKING BEHAVIOR = 0 | PASSWORD BASED KEY DERIVATION = 1
    */
    int getPinMode();
    /**
    * This method generates a Plain Signature for a given data. For EVENT Based Device the OCRA suite string used is: OCRA-1:HOTP-SHA1-8:C-QAXX. For TIME Based Device the OCRA suite string used is: OCRA-1:HOTP-SHA1-8:QAXX-TXXX
    * @param data the (not NULL) data aginst which Plain Signature must be generated
    * @see generateCROTP(char* challenge)
    * @see generateOTP()
    * @return the Plain Signature value on SUCCESS or NULL for FAILURE
    */
    char* generateSIGOTP(char* data);
    /**
    * This method generates a Response for a given Challenge data. For EVENT Based Device the OCRA suite string used is: OCRA-1:HOTP-SHA1-8:C-QAXX. For TIME Based Device the OCRA suite string used is: OCRA-1:HOTP-SHA1-8:QAXX-TXXX
    * @param challenge the (not NULL) challenge data aginst which Response must be generated
    * @see generateSIGOTP(char* data)
    * @see generateOTP()
    * @return the Plain Signature value on SUCCESS or NULL for FAILURE
    */
    char* generateCROTP(char* challenge);
    /**
    * This method generates an OTP. Depending on thd typd of Device, it generates an EVENT Based OTP (HOTP) or a TIME Based OTP (TOTP)
    * @see generateCROTP(char* challenge)
    * @see generateSIGOTP(char* data)
    * @return the Plain Signature value on SUCCESS or NULL for FAILURE
    */
    char* generateOTP();
    //char* generateSIGOTP(unsigned char*); // generate SIG OTP
    /**
    * This method provides the last encountered error information. On receiving a NULL value for failure from other methods, this method must be invoked to gather the error code for troubleshooting purpose
    * @see generateCROTP(char* challenge)
    * @see generateSIGOTP(char* data)
    * @return the int value of last encountered errror code 
    */
    int getErrorCode();
    /**
    * This method allows validation of a checkdigit in the end of a numeric value
    * @param actcode must be a numeric value representation in a character array 
    * @return true (1) on SUCCESS and false (0) on FAILURE to validate checkdigit of the provided activation code 
    */
    static bool verifyCheckDigit(char* actcode);
    /**
    * This method allows reset of all counter values in the Device. It is recommended for use only to advanced users of the Device library
    */
    void resetInitialCounters();
    /**
    * Once the Device as declared locked an unlock challenge code is displayed to the user, use this method to unlock the Device
    * @param bdevice the bytes represening a locked Device object
    * @param unlockcode the unlock code for the Device
    * @see resurrect(unsigned char* bdev, char* pin, char* env)
    * @see persist(char* pin, char* env)
    * @return a pointer to the unlocked Device object
    */
    static Device* unlockDevice(unsigned char* bdevice, char* unlockcode);
    /**
    * After consuctive failed attempts to resurrect the Device is made with incorrect PIN, the integrating application can decide to declare the Device as locked and use this method to obtain an unlock code that used to shown to user 
    * @return a null terminated string containing the unlock challenge code 
    */
    char* getUnlockChallenge();
    char* getLockCode(char*); // IMPORTANT: Must be made PRIVATE

    bool getLockStatus();

    /**
    * Obtains version of previously persisted Device object
    * @param bdevice the bytes represening a locked Device object
    * @see persist(char* pin, char* env)
    * @return a pointer to string containing version. Caller must release memory
    */
    static char * getVersion(unsigned char* buf);

    void resetDateCreated();
    
private:
    static int doubleDigits[10];
    static unsigned long long int DIGITS_POWER[10];
    bool m_bLocked;
    int errorcode;
    char m_strVersion[16];
    long m_lLastUsed;
    unsigned long int m_lDateCreated;
    OtpAlgo m_nOtpAlgo;
    int m_iIC;
    int m_iOTPLength;
    int m_iTokenType;
    bool m_bPinType;
    bool m_bPBKD; 
    _EncType m_enctype;
    int m_iRCdisplay; 
    int m_iTimeStepInSecs; 
    int m_iSpoof; 
    unsigned char m_pinHash[20]; 
    unsigned char m_abRawSecret[320]; 
    unsigned char m_abHolder[320]; 
    char m_strActivationCode[20];
    char m_strSerialNumber[20]; 
    char m_strRegistrationCode[20]; 
    unsigned char m_strHexRND[16]; 
    long m_lInitialEventCounter; 
    long m_lEventCounter; 
    int m_iSHAType; 
    int m_iTruncationOffset; 
    bool m_bAddChecksum; 
    char m_abB64EncPIN[256]; 

    void initV1(char*, char*, const char*);
    char* generateOTP_Custom(BYTE*, long, int);
    //char* getLockCode(char*);
    int unlockTheDevice(char*);
    void parseActivationCode(char*);
    long to_network_byte_order(long);
    void Int32ToUInt8Arr(long, BYTE*);
    int calcChecksum(long, int);
    unsigned long long int getCurrentTimeinMiliSecond();
    unsigned long long int StringToU64(const char *);
    BYTE* activateToken(char*, char*, unsigned char*);
    BYTE* generateRawSecret(char*, char*, BYTE*);
    long getInitialEventCounter(char*);
    char* generateCROTP_Time(char *);
    char* generateCROTP_Event(char*);
    char* generateSIGOTP_Time(char*);
    char* generateSIGOTP_Event(char*);
    char* generateTOTP(BYTE*, char*, long, char *);
    char* generateOTP_Time(); 
    char* generateOTP_Event();
    char *generateOCRA(char*, BYTE*, char*, char*, char*, char*, char*);
};

/**
* \defgroup SDKGlobalMethods Global methods for Device operations
* @{
*/

/**
* This is a utility method for generating a random long value. Integrating applications are recommended against using this method to generate randomness. It is recommended that stronger APIs, algorithms be used for generating randomness
* @return a random long value
*/
extern "C" unsigned long randomize();

/**
* This method validates an Activation Code for the Device
* @param wszActCode should contain the activation code
* @param errcode returns an error code if encountered
* @return 0 for SUCCESS and negative value for FAILURE (error code)
*/
extern "C" int verifyActivationCode(char* wszActCode, int* errcode);
/**
* This method activates the Device
* @param wszSNO a null terminated Serial Number to be associated with the Device. Must be generated using generateSerialNo(int iTokenType, char* inRndSeed, int inRndSeedLen, int* errcode)
* @param wszActCode a null terminated valid activation code
* @param wszRegCodeSeed a null terminated radomness source for the Device. The length of buffer must not exceed 512 bytes
* @param errcode returns an error code if encountered
* @see generateSerialNo(int iTokenType, char* inRndSeed, int inRndSeedLen, int* errcode)
* @return a pointer to a Device object on SUCCESS. NULL on FAILURE
*/
extern "C" Device* activateToken(char* wszSNO, char* wszActCode, char* wszRegCodeSeed, int* errcode);
/**
* This method activates a Version-1 Device
* @param wszSNO a null terminated Serial Number to be associated with the Device. Must be generated using generateSerialNo(int iTokenType, char* inRndSeed, int inRndSeedLen, int* errcode)
* @param wszActCode a null terminated valid activation code
* @param wszCheckCode a null terminated check code for the Device
* @param errcode returns an error code if encountered
* @see generateSerialNo(int iTokenType, char* inRndSeed, int inRndSeedLen, int* errcode)
* @return a pointer to a Device object on SUCCESS. NULL on FAILURE
*/
extern "C" Device* activateV1Token(char* wszSNO, char* wszActCode, char* wszCheckCode, int* errcode);
/**
* A utility method implementing the PBKDF2. It may be used by developers for generating randomness (keys) of specififed key length
* @param password a null terminated passphrase, seed for the key generation
* @param Plen the length of password parameter
* @param salt a null terminated salt value to the PDKDF2 algorithm
* @param Slen the length of salt parameter
* @param c the iteration count value to the PDKDF2 algorithm. The value determines the response time of the method. For weaker CPU devices, a value of 100 is recommended and for higher specs CPU devices 1500 is recommended
* @param derivedKey the buffer to receive generated key (randomness)
* @param dkLen length of the buffer derivedKey
* @return 0 on SUCCESS. Negative integer value (error code) on FAILURE
*/
int pbkd2_generateKey(char *password, size_t Plen, const char *salt,
        size_t Slen, unsigned int c, char *derivedKey,
        size_t dkLen);

/**
 * This method generates a 16-Digit Serial Number.
 * @param iTokenType embeds the token type. In this VERSION a constant integer value "2" must be passed
 * @param inRndSeed is a pointer to a buffer containing randomness generated by the calling application. Must be at most 512 bytes in size
 * @param inRndSeedLen informs the method of the length of the buffer size (inRndSeed) containing the randomness. Must be at most 512
 * @param P1P2 is a pointer to a null terminates character array containing the Platform identifier for the Token (e.g. iOS Token = "03")
 * @param V1V2 is a pointer to a null terminates character array containing the Version Code identifier for this Token release (e.g. "01")
 * @param errcode returns an error code if encountered. A NULL value is returned and this errorcode value is set by the method in case of failure
 * @return the 16 digit Serial Number to be associated with this Device
 */
extern "C" char* generateNewSerialNo(int iTokenType, char* inRndSeed, int inRndSeedLen, char* P1P2, char* V1V2, int* errcode);


/**
 * This method generates a unique 12 digit Serial Number for the Device.
 * Generate a serial no. using a UPC-A algorithm.
 * First 2 digits of the serial number is the exponent of the token type. Next 9 digits are the random generated number/ Last digit is the check digit of the 11 digits using the UPC-A check digit algorithm
 * @param iTokenType embeds the token type. In this VERSION a constant integer value "2" must be passed
 * @param inRndSeed is a pointer to a buffer containing randomness generated by the calling application. Must be at most 512 bytes in size
 * @param inRndSeedLen informs the method of the length of the buffer size (inRndSeed) containing the randomness. Must be at most 512
 * @param errcode returns an error code if encountered. A NULL value is returned and this errorcode value is set by the method in case of failure
 * @return the 12 digit Serial Number to be associated with this Device
 */
extern "C" char* generateSerialNo(int iTokenType, char* inRndSeed, int inRndSeedLen, int* errcode);





/**@}*/
#endif
