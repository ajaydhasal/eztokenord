//
//  universalDataStore.h
//  LICClaims
//
//  Created by VijayKumar Dogra on 27/08/15.
//  Copyright (c) 2015 Mdindia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface universalDataStore : NSObject
+ (void) storeData:(NSString *) key withValue:(NSString *)val;
+ (NSString *) getStringValFor:(NSString *)key;
+ (NSString *) getDateForMillesecond:(NSString *)strDate;
@end
