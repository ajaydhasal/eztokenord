//
//  Constant.h
//  EZToken
//
//  Created by Harsha on 9/22/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#ifndef EZToken_Constant_h
#define EZToken_Constant_h



#endif

//font name
#define LABEL              [UIFont systemFontOfSize:15.0f] /// [UIFont fontWithName:@"Coda-Regular" size:17]
#define ENTER_PIN_MODE_LOGIN		0
// Enter pin attempts
#define ENTER_PIN_ATTEMPTS_MAX 5

// Logout time limit by milli-seconds
#define SESSION_TIME_OUT_LIMIT_BY_MILLISECONDS		75000

// Save file names
#define APP_STATUS_FILE_NAME				@"token_status.txt"
#define DEVICE_SECRET_FILE_NAME				@"device_secret.txt"

#define SHOULD_USE_16_DIGITS_SN             YES
//TKEXPTYPE
#define ITERATIONFACTOR                     2

// Platform & Version code
#define TOKEN_PLATFORM_CODE_12      @"03"
#define TOKEN_VERSION_CODE_12       @"01"

//RC Display
#define HID_RCDISPLAY                     YES

// Save property names
#define APP_STATUS_SERIAL_NUMBER_KEY		@"serial_number"

#define APP_STATUS_VERSION                  @"version"
#define APP_STATUS_VERSION_NO               @"4.0.1.4"





#define APP_STATUS_IS_ACTIVATED                               @"is_activated"
#define APP_STATUS_IS_LOCKED                                     @"is_app_locked"
#define APP_STATUS_MAX_TILT_ATTEMPTS                 @"max_tilt_attemps"
#define APP_STATUS_IS_TILT_ENABLED                         @"is_tilt_enabled"
#define APP_STATUS_IS_LOCATION_ENABLED             @"is_location_enabled"
#define APP_STATUS_LOCKED_DURATION                     @"locked_duration"
#define APP_STATUS_LOCKED_TIME                                @"locked_time"
#define APP_STATUS_LOCKED_INCREMENT                  @"locked_increment"
#define APP_STATUS_PID_KEY                                           @"pid_key"
#define APP_STATUS_SECURE_VALUE                             @"secure_value"
#define APP_STATUS_LATITUDE_VALUE                         @"latitude_value"
#define APP_STATUS_LONGITUDE_VALUE                      @"longitude_value"
#define APP_STATUS_IS_AUTOMATION_ENABLE          @"is_automation"
#define APP_STATUS_IS_SECURITY_SET                          @"is_security_set"
#define APP_STATUS_IS_TOUCH_ID_SET                          @"is_touchid_set"
#define APP_STATUS_IS_TOUCH_ID_DEVICE                  @"is_touchid_device"
#define APP_STATUS_GATEWAY_URL                               @"gatewayurl"
/**************ajay*******************/
#define IS_APP_OPENED_FIRST_TIME                               @"isAppOpenedFirstTime"

#define IS_SECURE_ALIGNED                                               @"isSecuredAlinged"

#define IS_FACE_RECGNSED                                              @"isFaceRecgnised"

///////// ONLINE MODE harsha/////////////

#define SEED_STRING_MAX_LENGTH	128
#define SEED_STRING_MIN_LENGTH	64



#define MAIN_GATEWAY_URL_DEFAULT             @"https://2fa.ooredoo.qa"//@"https://211.25.18.246:8444"
//#define GATEWAY_URL_TRANS               @"https://211.25.18.246/ezwsrest/rest/engine/auth/m/resauth"
#define GATEWAY_URL_DEFAULT                         @"/ezwsrest/rest/engine/softtk/m/actmp"
#define GATEWAY_URL_TRANS                             @"/ezwsrest/rest/engine/auth/m/resauth"

//new gateway provide by ashish on 11th February 2015
//#define GATEWAY_URL_DEFAULT             @"https://182.160.96.244:8443/ezwsrest/rest/engine/softtk/m/actmp"
//#define GATEWAY_URL_TRANS               @"https://182.160.96.244:8443/ezwsrest/rest/engine/auth/m/resauth"


//local http
//#define GATEWAY_URL_DEFAULT            @"http://192.168.2.90:8080/ezwsrest/rest/engine/softtk/m/actmp"
//#define GATEWAY_URL_TRANS             @"http://192.168.2.90:8080/ezwsrest/rest/engine/auth/m/resauth"

//#define GATEWAY_URL_DEFAULT             @"https://192.168.2.161:8443/ezwsrest/rest/engine/softtk/m/actmp"
//#define GATEWAY_URL_TRANS               @"https://192.168.2.161:8443/ezwsrest/rest/engine/auth/m/resauth"


#define ISPUSH_ENABLED                  YES
#define ISONLINE                        YES
// HTTP Response Header Name
#define RESPONSE_HEADER_NAME_ENCRYPT_MODE		@"attc_encrypt_mode"

// HTTP Request Time-out
#define HTTP_REQUEST_TIME_OUT	10.0

// HTTP Request parameter name
#define REQUEST_PARAMETER_NAME	@"data"

#define CHECK_DOWNLOADING_PROGRESS_BY_PERCENT	YES

// Checking internet connections
#define NETWORK_REACHABILITY_STATUS_NOT_REACHABLE		                   100
#define NETWORK_REACHABILITY_STATUS_REACHABLE_VIA_WWAN	           101
#define NETWORK_REACHABILITY_STATUS_REACHABLE_VIA_WIFI	               102


#define INTERNET_STATUS_NOT_REACHABLE			                                                110
#define INTERNET_STATUS_REACHABLE_VIA_WWAN		                                        111
#define INTERNET_STATUS_REACHABLE_VIA_WIFI		                                            112


#define LOCAL_WIFI_STATUS_NOT_REACHABLE                                                      120
#define LOCAL_WIFI_STATUS_REACHABLE_VIA_WWAN	                                        121
#define LOCAL_WIFI_STATUS_REACHABLE_VIA_WIFI	                                            122

// Process target flag
#define PROCESS_TARGET_NONE                                                                                 10
#define PROCESS_TARGET_ACTIVATE_TOKEN                                                          11
#define PROCESS_TARGET_TRANSACTION_SEND                                                    12
#define PROCESS_TARGET_ONLINE_LOGIN                                                               13

// Process result flag
#define PROCESS_RESULT_NONE                                                                                  200
#define PROCESS_RESULT_ACTIVATE_TOKEN_FAILED                                           201
#define PROCESS_RESULT_ACTIVATE_TOKEN_SUCCESS                                       202
#define PROCESS_RESULT_TRANSACTION_SEND_FAILED                                    203
#define PROCESS_RESULT_TRANSACTION_SEND_SUCCESS                                 204
#define PROCESS_RESULT_ONLINE_LOGIN_SUCCESS                                             205
#define PROCESS_RESULT_ONLINE_LOGIN_FAILED                                                206

// Local notification names
#define NOTIFICATION_NAME_ACTIVATE_TOKEN_DONE		@"done_activation"
#define NOTIFICATION_NAME_TRANSACTION_SEND_DONE		@"done_transaction"
#define NOTIFICATION_NAME_ONLINE_LOGIN_DONE         @"done_online_login"
//#define NOTIFICATION_NAME_ACTIVATE_TOKEN_DONE1		@"done_activation1"


// Response Return Code
#define RESPONSE_RETURN_CODE_SUCCESS									0
#define RESPONSE_RETURN_CODE_NONE										-999

#define LOADING_PROGRESS_DELAY_TIME_LOAD_ACTIVATION_CODE	2.0f
#define LOADING_PROGRESS_DELAY_TIME_DEFAULT					2.0f


#define ALERT_INTERNET_CONNECTION_ERROR_TITLE		@"Connection Error"
#define ALERT_INTERNET_CONNECTION_ERROR_MSG			@"Unable to activate due to network issue. Please check your Internet connection and try again."
#define ALERT_BACKEND_CONNECTION_ERROR_TITLE		@"Connection Error"
#define ALERT_BACKEND_CONNECTION_ERROR_MSG			@"Unable to activate due to network issue. Please try again later. If the problem continues, contact support@ezmcom.com/ +60 (0)12 570 1114"
#define ALERT_INVALID_RESPONSE_ERROR_TITLE			@"Invalid Response Error"
#define ALERT_INVALID_RESPONSE_ERROR_MSG			@"An error has occurred, please try again later. If the problem continues, contact support@ezmcom.com/ +60 (0)12 570 1114."


#pragma Static Methods

// Get current time in milli-seconds (from 1970)
static double getCurrentTimeInMilliseconds()
{
    NSTimeInterval timePassed_ms = [[NSDate date] timeIntervalSince1970] * 1000;
    return timePassed_ms;
}

// Get current elapsed time from date in milli-seconds
static double getCurrentTimeInMillisecondsFromDate(NSDate *fromDate)
{
    NSTimeInterval timePassed_ms = [[NSDate date] timeIntervalSinceDate:fromDate] * 1000;
    return timePassed_ms;
}

