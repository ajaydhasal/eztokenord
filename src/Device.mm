#include "Device.h"
#include "SHA1.h"
#include "Base32.h"
#include "HMAC_SHA1.h"
#include "Rijndael.h"
#include "base64.h"

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sys/timeb.h>

using namespace std;


/*! 
   A constant used internally by Device object. It MUST HAVE VALUE 0 for all HID BUILDS 
 */
#define GLOBAL_RC_PERSIST_COMMAND 0

int Device::doubleDigits[] = {
    0, 2, 4, 6, 8, 1, 3, 5, 7, 9
}
;
unsigned long long int Device::DIGITS_POWER[] = {
    1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000
}
;
//unsigned long long Device::DIGITS_POWER2[] = {
//	1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000,10000000000
//}
//;

int hexCtoI(char n);
char* getEffectiveSN(const char* SN);

unsigned long long int Device::getCurrentTimeinMiliSecond() {

    struct timeb tp;
    ftime(&tp);
    //printf("time = %ld.%d\n", tp.time, tp.millitm);
    char szTimeStampMiliSecond[20];
    char szTimeStampms[5];
    memset(szTimeStampMiliSecond, 0, 20);
    memset(szTimeStampms, 0, 5);
    sprintf(szTimeStampMiliSecond, "%ld", tp.time);

    // TOTP Oscillation fix
    sprintf(szTimeStampms, "%03d", tp.millitm);
    strcat(szTimeStampMiliSecond, szTimeStampms);

    //printf("\n Time in miliseconds - %s \n", szTimeStampMiliSecond);
    unsigned long long int li_timestamp = StringToU64(szTimeStampMiliSecond);
    //printf("\n Time in miliseconds in long int- %llu \n", li_timestamp);
    return li_timestamp;
}

long Device::to_network_byte_order(long value) {
    long result = 0;
    uint8_t *temp = (uint8_t *) (&result);

    *temp = (uint8_t) ((value >> 24) & 0x000000ff);
    temp++;
    *temp = (uint8_t) ((value >> 16) & 0x000000ff);
    temp++;
    *temp = (uint8_t) ((value >> 8) & 0x000000ff);
    temp++;
    *temp = (uint8_t) (value & 0x000000ff);

    return result;
}

void Device::Int32ToUInt8Arr(long val, BYTE *pBytes) {
    pBytes[0] = (unsigned int) val;
    pBytes[1] = (unsigned int) (val >> 8);
    pBytes[2] = (unsigned int) (val >> 16);
    pBytes[3] = (unsigned int) (val >> 24);
}

unsigned long long int Device::StringToU64(const char * sz) {
    unsigned long long int u64Result = 0;

    while (*sz != '\0') {
        //printf(
        u64Result *= 10;
        u64Result += *sz - '0';
        sz++;
    }
    return u64Result;
}

int Device::calcChecksum(long num, int digits) {
    bool doubleDigit = true;
    int total = 0;

    while (0 < digits--) {
        int digit = (int) (num % 10);
        num /= 10;

        if (doubleDigit) {
            digit = doubleDigits[digit];
        }
        total += digit;
        doubleDigit = !doubleDigit;
    }

    int result = total % 10;
    if (result > 0) {
        result = 10 - result;
    }
    return result;
}

char const* const Device::getVersion() {
    char const* const ver = m_strVersion;
    return ver;
}

Device::Device(unsigned long int lDateCreated, long lLastUsed, int iTokenType, OtpAlgo nOtpAlgo, int iSHAType,
        int iOTPLength, int iTimeStepInSecs, long lInitialEventCounter, long lEventCounter,
        char* strSerialNumber, char* strRegistrationCode, char* strHexRND,
        BYTE* abRawSecret, char*strVersion, bool bLocked, bool bPBKD, bool bPinType, int iRCdisplay,
        unsigned char* pinHash, char* strActivationCode, char* abB64EncPIN) {
    strcpy(this->m_strVersion, VERSION);
    this->m_lDateCreated = lDateCreated;
    this->m_lLastUsed = lLastUsed;
    this->m_iTokenType = iTokenType;
    this->m_nOtpAlgo = nOtpAlgo;
    this->m_iSHAType = iSHAType;
    this->m_iOTPLength = iOTPLength;
    this->m_iTimeStepInSecs = iTimeStepInSecs;
    this->m_lInitialEventCounter = lInitialEventCounter;
    this->m_lEventCounter = lEventCounter;
    memset(this->m_strSerialNumber, 0, sizeof (m_strSerialNumber));
    memcpy(this->m_strSerialNumber, strSerialNumber, sizeof (m_strSerialNumber));
    memset(this->m_strRegistrationCode, 0, sizeof (m_strRegistrationCode));
    memcpy(this->m_strRegistrationCode, strRegistrationCode, sizeof (m_strRegistrationCode));
    memset(this->m_strHexRND, 0, sizeof (m_strHexRND));
    memcpy(this->m_strHexRND, strHexRND, sizeof (m_strHexRND));
    memset(this->m_abRawSecret, 0, sizeof (m_abRawSecret));
    memcpy(this->m_abRawSecret, abRawSecret, sizeof (m_abRawSecret));
    memset(this->m_abHolder, 0, sizeof (m_abHolder));
    memset(this->m_strVersion, 0, sizeof (m_strVersion));
    memcpy(this->m_strVersion, strVersion, sizeof (m_strVersion));
    this->m_bLocked = bLocked;
    this->m_bPBKD = bPBKD;
    this->m_bPinType = bPinType;
    this->m_iRCdisplay = iRCdisplay;
    memcpy(this->m_pinHash, pinHash, sizeof (m_pinHash));
    memset(this->m_strActivationCode, 0, sizeof (m_strActivationCode));
    memcpy(this->m_strActivationCode, strActivationCode, sizeof (m_strActivationCode));
    memcpy(this->m_abB64EncPIN, abB64EncPIN, sizeof (m_abB64EncPIN));

    this->m_bAddChecksum = false;
    this->m_iTruncationOffset = 16;
    this->m_iSpoof = false;
}

Device::Device(char* wszSNO, char* wszActCode, const char* wszRegCodeSeed, int MODE) {
    strcpy(this->m_strVersion, VERSION);
    errorcode = 0;
    if (wszSNO == NULL || wszActCode == NULL || wszRegCodeSeed == NULL) {
        errorcode = BAD_PARAMETER;
        return;
    }

    char* wszEffSNO = NULL;
    if (strlen(wszSNO) == 16) {
        // New 16-Digit SN
        // Lets get effective SN
        wszEffSNO = getEffectiveSN(wszSNO);
    } else {
        wszEffSNO = new char[20];

        /*
        memset(SN, 0, 20);
        // Copy the 11 Digits after the P1P2, V1V2 prefix
        memcpy(SN, &NEWSN[4], 11);
        
         */

        memset(wszEffSNO, 0, 20); //sizeof (wszEffSNO));

        strcpy(wszEffSNO, wszSNO);
        printf("\nserial number %s\n", wszEffSNO);
    }

    if (MODE == V2_MODE) {

        if (wszEffSNO[0] == '0') {
            // ITC = 5000
            m_iIC = 5000;
        } else if (wszEffSNO[0] == '1') {
            // ITC = 500
            m_iIC = 500;
        } else if (wszEffSNO[0] == '2') {
            // ITC = 50
            m_iIC = 50;
        } else {
            errorcode = -2;
        }

        unsigned char abEventsTimeSeed[20];
        CSHA1 sha1;
        sha1.Reset();
        sha1.Update((unsigned char*) wszRegCodeSeed, strlen(wszRegCodeSeed));
        sha1.Final();
        sha1.GetHash(abEventsTimeSeed);

        parseActivationCode(wszActCode);

        unsigned char* secret = activateToken(wszEffSNO, wszActCode, abEventsTimeSeed);
        if (secret == NULL) {
            return;
        }
        memcpy(m_abRawSecret, secret, 20);
        delete secret;
        //printf("\n Secret Value in Hex - %x \n", m_secret);
        // Generate the Otp
        // Get the initial event counter
        m_lInitialEventCounter = getInitialEventCounter(wszEffSNO);
        m_lEventCounter = m_lInitialEventCounter;
        m_iTruncationOffset = 16;
        m_bAddChecksum = false;
        //printf("\n Initial event counter - %d \n", m_initEventCounter );
        char *pOTPValue = NULL;
        //printf("\n One Time password length - %d \n", m_nOTPDigits);
        int type = (int) m_iTokenType;
        m_iTokenType = HOTP;
        pOTPValue = generateOTP();
        m_iTokenType = (_TokenType) type;
        //printf("\n One Time password generated - %s \n", pOTPValue);
        // Append the 1st two bytes of the generated otp to encoded string [ m_HexRandEncodeString  ] ( 8 bytes) to get
        // 10 bytes Registration code
        memset(m_strRegistrationCode, 0, sizeof (m_strRegistrationCode));
        memcpy(m_strRegistrationCode, m_strHexRND, 8);
        m_strRegistrationCode[8] = pOTPValue[0];
        m_strRegistrationCode[9] = pOTPValue[1];

        delete [] pOTPValue;

        memset(m_strSerialNumber, 0, 20); //sizeof (m_strSerialNumber));
        strcpy(m_strSerialNumber, wszSNO);

        //printf("\nm_strSerialNumber -> %s\n", m_strSerialNumber);

        //23-APRIL-2013:
        // Since all use of ACT CODE is already done, why should we save it
        // The code has been parsed and states updated into member variables
        // The code has been used to get registration code as well
        // So lets comment out the copy of ACT CODE into member variable
        memset(m_strActivationCode, 0, sizeof (m_strActivationCode));
        //strcpy(m_strActivationCode, wszActCode); // Commented by Anupam on 23-APRIL-2013 to prevent any recording of ACT CODE in persisted device object

//        char szSecLog[24];
//        memset(szSecLog, 0, sizeof (szSecLog));
//        ofstream a_file("/opt/dev/logs/cplusdevice.log", ios::app);
//        a_file << "-- activateToken() --\n";
//        a_file << "SN: " << m_strSerialNumber << "\n";
//        a_file << "ACT CODE: " << m_strActivationCode << "\n";
//        a_file << "ITC: " << m_iIC << "\n";
//        a_file << "SECRET: ";
//        for (int j = 0; j < 20; j++) {
//            sprintf(szSecLog, "[%02x]", m_abRawSecret[j]);
//            a_file << szSecLog;
//        }
//        a_file << "\n";
//        a_file.close();

        //added 22-10-2013
        memset(m_abHolder, 0, sizeof (m_abHolder));
        
        m_lDateCreated = getCurrentTimeinMiliSecond() / 1000;
        errorcode = 0;
        m_bLocked = false;
        m_iSpoof = false;
    } else if (MODE == V1_MODE) {
        initV1(wszEffSNO, wszActCode, wszRegCodeSeed);
    }
    return;
}

void Device::initV1(char* wszSNO, char* wszActCode, const char* wszRegCodeSeed) {
    m_iTruncationOffset = 16;
    m_bAddChecksum = false;
    m_iOTPLength = strlen(wszRegCodeSeed);

    BYTE *pSerialHash = new BYTE [ OTP_SECRET_KEY_LENGTH ];
    char *pOTPValue = NULL;
    CSHA1 csha1;

    // COMPUTE SHA-1 HASH OF 'SERIAL NUMBER' (wszSNO).
    // It is passed as Param#1 to this function
    // & generate the OTP : this is done in one step by calling generateOTP()

    csha1.Reset();
    csha1.Update((UINT_8 *) wszSNO, strlen(wszSNO));
    csha1.Final();
    csha1.GetHash((UINT_8 *) pSerialHash);

    long lActCode = atol(wszActCode);

    pOTPValue = generateOTP_Custom(pSerialHash,
            lActCode,
            strlen(wszRegCodeSeed));

    memset(pSerialHash, 0, OTP_SECRET_KEY_LENGTH);

    // generate the Hash of the OTP value again.
    csha1.Reset();
    csha1.Update((UINT_8 *) pOTPValue, strlen(pOTPValue));
    csha1.Final();
    csha1.GetHash((UINT_8 *) pSerialHash);

    // Cleanup
    delete pOTPValue;


    // Now we have the SECRET. Lets verify Check Code and if SUCCESS, initialize the Device object
    lActCode = lActCode + 1;

    // Lets verify if the Check Code corresponds to this SECRET (SN, AC)
    char* compareOTP = generateOTP_Custom(pSerialHash, lActCode, strlen(wszRegCodeSeed));
    if (compareOTP != NULL) {
        if (memcmp(compareOTP, wszRegCodeSeed, strlen(wszRegCodeSeed)) != 0) {
            errorcode = BAD_PARAMETER;
            delete compareOTP;
            return;
        }
    }
    delete compareOTP;

    memcpy(m_abRawSecret, pSerialHash, OTP_SECRET_KEY_LENGTH);
    delete pSerialHash;

    // Lets initialize all other state variables
    m_bLocked = false;
    memset(m_strRegistrationCode, 0, sizeof (m_strRegistrationCode));
    m_bPinType = true;
    m_bPBKD = false;
    m_enctype = TDES;
    m_iIC = 5000;
    m_iRCdisplay = false;
    m_iSHAType = SHA_1;
    m_iSpoof = false;
    m_iTimeStepInSecs = 30;
    m_iTokenType = HOTP;
    m_lDateCreated = getCurrentTimeinMiliSecond() / 1000;
    m_lEventCounter = lActCode + 1;
    m_lInitialEventCounter = lActCode + 1;
    m_lLastUsed = m_lDateCreated;
    m_nOtpAlgo = OTP;
    strcpy(m_strRegistrationCode, wszRegCodeSeed);
    strcpy(m_strSerialNumber, wszSNO);
    errorcode = 0;
}

Device* Device::resurrect(unsigned char* buf) {
    //printf(@"Device **** resurrect with buf ");
    unsigned long int lDateCreated = 0;
    long lLastUsed = 0;
    int iTokenType;
    OtpAlgo nOtpAlgo;
    int iSHAType;
    int iOTPLength;
    int iTimeStepInSecs;
    long lInitialEventCounter = 0;
    long lEventCounter = 0;
    char strSerialNumber[20];
    char strRegistrationCode[20];
    char strHexRND[16];
    unsigned char abRawSecret[320];
    char strVersion[16];
    bool bLocked;
    bool bPBKD;
    bool bPinType;
    int iRCDisplay;
    unsigned char pinHash[20];
    char strActivationCode[20];
    char abB64EncPIN[256];

    int lenSoFar = 0;
    memcpy(&lDateCreated, buf, sizeof (lDateCreated));
    lenSoFar += sizeof (lDateCreated);
    memcpy(&lLastUsed, buf + lenSoFar, sizeof (lLastUsed));
    lenSoFar += sizeof (lLastUsed);
    memcpy(&iTokenType, buf + lenSoFar, sizeof (iTokenType));
    lenSoFar += sizeof (iTokenType);
    memcpy(&nOtpAlgo, buf + lenSoFar, sizeof (nOtpAlgo));
    lenSoFar += sizeof (nOtpAlgo);
    memcpy(&iSHAType, buf + lenSoFar, sizeof (iSHAType));
    lenSoFar += sizeof (iSHAType);
    memcpy(&iOTPLength, buf + lenSoFar, sizeof (iOTPLength));
    lenSoFar += sizeof (iOTPLength);
    memcpy(&iTimeStepInSecs, buf + lenSoFar, sizeof (iTimeStepInSecs));
    lenSoFar += sizeof (iTimeStepInSecs);
    memcpy(&lInitialEventCounter, buf + lenSoFar, sizeof (lInitialEventCounter));
    lenSoFar += sizeof (lInitialEventCounter);
    memcpy(&lEventCounter, buf + lenSoFar, sizeof (lEventCounter));
    lenSoFar += sizeof (lEventCounter);
    memcpy(strSerialNumber, buf + lenSoFar, sizeof (strSerialNumber));
    lenSoFar += sizeof (strSerialNumber);
    memcpy(strRegistrationCode, buf + lenSoFar, sizeof (strRegistrationCode));
    lenSoFar += sizeof (strRegistrationCode);
    memcpy(strHexRND, buf + lenSoFar, sizeof (strHexRND));
    lenSoFar += sizeof (strHexRND);
    memcpy(abRawSecret, buf + lenSoFar, sizeof (abRawSecret));
    lenSoFar += sizeof (abRawSecret);
    memcpy(strVersion, buf + lenSoFar, sizeof (strVersion));
    lenSoFar += sizeof (strVersion);
    
    //----- START: ADDED 15-APR-2015 by Musa to handle incorrect resurrct on 64 bit devices -------//
    size_t verLen = strlen(strVersion);
    
    if( (verLen < 5) || (iOTPLength != 6 && iOTPLength != 8 && iOTPLength != 10) || strSerialNumber[0] == '\0' || strcmp(strSerialNumber,"")==0 || (strlen(strSerialNumber) != 12 && strlen(strSerialNumber) != 16) ||
       (iTimeStepInSecs != 30 && iTimeStepInSecs != 60))
    {
        //printf("Resurrect:: parsing error, restart parsing with 4 bytes.\n");
        lenSoFar = 0;
        memcpy(&lDateCreated, buf, 4);//sizeof (lDateCreated));
        lenSoFar += 4;//sizeof (lDateCreated);
        memcpy(&lLastUsed, buf + lenSoFar, 4);//sizeof (lLastUsed));
        lenSoFar += 4;//sizeof (lLastUsed);
        memcpy(&iTokenType, buf + lenSoFar, sizeof (iTokenType));
        lenSoFar += sizeof (iTokenType);
        memcpy(&nOtpAlgo, buf + lenSoFar, sizeof (nOtpAlgo));
        lenSoFar += sizeof (nOtpAlgo);
        memcpy(&iSHAType, buf + lenSoFar, sizeof (iSHAType));
        lenSoFar += sizeof (iSHAType);
        memcpy(&iOTPLength, buf + lenSoFar, sizeof (iOTPLength));
        lenSoFar += sizeof (iOTPLength);
        memcpy(&iTimeStepInSecs, buf + lenSoFar, sizeof (iTimeStepInSecs));
        lenSoFar += sizeof (iTimeStepInSecs);
        //printf("--Resurrect:: Time step %d\n",iTimeStepInSecs);
        
        memcpy(&lInitialEventCounter, buf + lenSoFar, 4);//sizeof (lInitialEventCounter));
        lenSoFar += 4;//sizeof (lInitialEventCounter);
        memcpy(&lEventCounter, buf + lenSoFar, 4);//sizeof (lEventCounter));
        lenSoFar += 4;//sizeof (lEventCounter);
        memcpy(strSerialNumber, buf + lenSoFar, sizeof (strSerialNumber));
        lenSoFar += sizeof (strSerialNumber);
        
        memcpy(strRegistrationCode, buf + lenSoFar, sizeof (strRegistrationCode));
        lenSoFar += sizeof (strRegistrationCode);
        memcpy(strHexRND, buf + lenSoFar, sizeof (strHexRND));
        lenSoFar += sizeof (strHexRND);
        memcpy(abRawSecret, buf + lenSoFar, sizeof (abRawSecret));
        lenSoFar += sizeof (abRawSecret);
        memcpy(strVersion, buf + lenSoFar, sizeof (strVersion));
        lenSoFar += sizeof (strVersion);
        //printf("Resurrect::  version [%s], %lu\n",strVersion,strlen(strVersion));
    }
    //----- END: ADDED 15-APR-2015 by Musa to handle incorrect resurrct on 64 bit devices -------//

    memcpy(&bLocked, buf + lenSoFar, sizeof (bLocked));
    lenSoFar += sizeof (bLocked);
    memcpy(&bPBKD, buf + lenSoFar, sizeof (bPBKD));
    lenSoFar += sizeof (bPBKD);
    memcpy(&bPinType, buf + lenSoFar, sizeof (bPinType));
    lenSoFar += sizeof (bPinType);
    memcpy(&iRCDisplay, buf + lenSoFar, sizeof (iRCDisplay));
    lenSoFar += sizeof (iRCDisplay);
    memcpy(pinHash, buf + lenSoFar, sizeof (pinHash));
    lenSoFar += sizeof (pinHash);
    memcpy(strActivationCode, buf + lenSoFar, sizeof (strActivationCode));
    lenSoFar += sizeof (strActivationCode);
    memcpy(abB64EncPIN, buf + lenSoFar, sizeof (abB64EncPIN));
    lenSoFar += sizeof (abB64EncPIN);

//    char szSecLog[24];
//    memset(szSecLog, 0, sizeof (szSecLog));
//    ofstream a_file("/opt/dev/logs/cplusdevice.log", ios::app);
//    a_file << "-- resurrect() --\n";
//    a_file << "b64 ENC SECRET: ";
//    sprintf(szSecLog, "%s", abRawSecret);
//    a_file << szSecLog;
//    a_file << "\n";
//    a_file << "BASE64 ENC PIN: " << abB64EncPIN << "\n";
//    a_file.close();

    return new Device(lDateCreated, lLastUsed, iTokenType, nOtpAlgo, iSHAType,
            iOTPLength, iTimeStepInSecs, lInitialEventCounter, lEventCounter,
            strSerialNumber, strRegistrationCode, strHexRND, abRawSecret,
            strVersion, bLocked, bPBKD, bPinType, iRCDisplay, pinHash, strActivationCode, abB64EncPIN);
}

Device* Device::resurrect(unsigned char* buf, char* pin, char* env) {
    //printf(@"Device **** resurrect with buf,pin,env");
    unsigned long int lDateCreated = 0;
    long lLastUsed = 0;
    int iTokenType;
    OtpAlgo nOtpAlgo;
    int iSHAType;
    int iOTPLength;
    int iTimeStepInSecs;
    long lInitialEventCounter = 0;
    long lEventCounter = 0;
    char strSerialNumber[20];
    char strRegistrationCode[20];
    char strHexRND[16];
    unsigned char abRawSecret[320];
    char strVersion[16];
    bool bLocked;
    bool bPBKD;
    bool bPinType;
    int iRCDisplay;
    unsigned char pinHash[20];
    char strActivationCode[20];
    char abB64EncPIN[256];

    int lenSoFar = 0;
    memcpy(&lDateCreated, buf, sizeof (lDateCreated));
    lenSoFar += sizeof (lDateCreated);
    memcpy(&lLastUsed, buf + lenSoFar, sizeof (lLastUsed));
    lenSoFar += sizeof (lLastUsed);
    memcpy(&iTokenType, buf + lenSoFar, sizeof (iTokenType));
    lenSoFar += sizeof (iTokenType);
    memcpy(&nOtpAlgo, buf + lenSoFar, sizeof (nOtpAlgo));
    lenSoFar += sizeof (nOtpAlgo);
    memcpy(&iSHAType, buf + lenSoFar, sizeof (iSHAType));
    lenSoFar += sizeof (iSHAType);
    memcpy(&iOTPLength, buf + lenSoFar, sizeof (iOTPLength));
    lenSoFar += sizeof (iOTPLength);
    memcpy(&iTimeStepInSecs, buf + lenSoFar, sizeof (iTimeStepInSecs));
    lenSoFar += sizeof (iTimeStepInSecs);
    memcpy(&lInitialEventCounter, buf + lenSoFar, sizeof (lInitialEventCounter));
    lenSoFar += sizeof (lInitialEventCounter);
    memcpy(&lEventCounter, buf + lenSoFar, sizeof (lEventCounter));
    lenSoFar += sizeof (lEventCounter);
    memcpy(strSerialNumber, buf + lenSoFar, sizeof (strSerialNumber));
    lenSoFar += sizeof (strSerialNumber);
    memcpy(strRegistrationCode, buf + lenSoFar, sizeof (strRegistrationCode));
    lenSoFar += sizeof (strRegistrationCode);
    memcpy(strHexRND, buf + lenSoFar, sizeof (strHexRND));
    lenSoFar += sizeof (strHexRND);
    memcpy(abRawSecret, buf + lenSoFar, sizeof (abRawSecret));
    lenSoFar += sizeof (abRawSecret);
    memcpy(strVersion, buf + lenSoFar, sizeof (strVersion));
    lenSoFar += sizeof (strVersion);
    
      //----- START: ADDED 15-APR-2015 by Musa to handle incorrect resurrct on 64 bit devices -------//

    size_t verLen = strlen(strVersion);
    
    if( (verLen < 5) || (iOTPLength != 6 && iOTPLength != 8 && iOTPLength != 10) || strSerialNumber[0] == '\0' || strcmp(strSerialNumber,"")==0 || (strlen(strSerialNumber) != 12 && strlen(strSerialNumber) != 16) ||
       (iTimeStepInSecs != 30 && iTimeStepInSecs != 60))
    {
        //printf("Resurrect:: parsing error, restart parsing with 4 bytes.\n");
        lenSoFar = 0;
        memcpy(&lDateCreated, buf, 4);//sizeof (lDateCreated));
        lenSoFar += 4;//sizeof (lDateCreated);
        memcpy(&lLastUsed, buf + lenSoFar, 4);//sizeof (lLastUsed));
        lenSoFar += 4;//sizeof (lLastUsed);
        memcpy(&iTokenType, buf + lenSoFar, sizeof (iTokenType));
        lenSoFar += sizeof (iTokenType);
        memcpy(&nOtpAlgo, buf + lenSoFar, sizeof (nOtpAlgo));
        lenSoFar += sizeof (nOtpAlgo);
        memcpy(&iSHAType, buf + lenSoFar, sizeof (iSHAType));
        lenSoFar += sizeof (iSHAType);
        memcpy(&iOTPLength, buf + lenSoFar, sizeof (iOTPLength));
        lenSoFar += sizeof (iOTPLength);
        memcpy(&iTimeStepInSecs, buf + lenSoFar, sizeof (iTimeStepInSecs));
        lenSoFar += sizeof (iTimeStepInSecs);
        //printf("--Resurrect:: Time step %d\n",iTimeStepInSecs);
        
        memcpy(&lInitialEventCounter, buf + lenSoFar, 4);//sizeof (lInitialEventCounter));
        lenSoFar += 4;//sizeof (lInitialEventCounter);
        memcpy(&lEventCounter, buf + lenSoFar, 4);//sizeof (lEventCounter));
        lenSoFar += 4;//sizeof (lEventCounter);
        memcpy(strSerialNumber, buf + lenSoFar, sizeof (strSerialNumber));
        lenSoFar += sizeof (strSerialNumber);
        
        memcpy(strRegistrationCode, buf + lenSoFar, sizeof (strRegistrationCode));
        lenSoFar += sizeof (strRegistrationCode);
        memcpy(strHexRND, buf + lenSoFar, sizeof (strHexRND));
        lenSoFar += sizeof (strHexRND);
        memcpy(abRawSecret, buf + lenSoFar, sizeof (abRawSecret));
        lenSoFar += sizeof (abRawSecret);
        memcpy(strVersion, buf + lenSoFar, sizeof (strVersion));
        lenSoFar += sizeof (strVersion);
        //printf("Resurrect::  version [%s], %lu\n",strVersion,strlen(strVersion));
    }
    //----- END: ADDED 15-APR-2015 by Musa to handle incorrect resurrct on 64 bit devices -------//

    memcpy(&bLocked, buf + lenSoFar, sizeof (bLocked));
    lenSoFar += sizeof (bLocked);
    memcpy(&bPBKD, buf + lenSoFar, sizeof (bPBKD));
    lenSoFar += sizeof (bPBKD);
    memcpy(&bPinType, buf + lenSoFar, sizeof (bPinType));
    lenSoFar += sizeof (bPinType);
    memcpy(&iRCDisplay, buf + lenSoFar, sizeof (iRCDisplay));
    lenSoFar += sizeof (iRCDisplay);
    memcpy(pinHash, buf + lenSoFar, sizeof (pinHash));
    lenSoFar += sizeof (pinHash);
    memcpy(strActivationCode, buf + lenSoFar, sizeof (strActivationCode));
    lenSoFar += sizeof (strActivationCode);
    memcpy(abB64EncPIN, buf + lenSoFar, sizeof (abB64EncPIN));
    lenSoFar += sizeof (abB64EncPIN);


 //   char szSecLog[24];
 //   memset(szSecLog, 0, sizeof (szSecLog));
 //   ofstream a_file("/opt/dev/logs/cplusdevice.log", ios::app);
//    a_file << "-- resurrect(unsigned char*, char*, char*) --\n";
//    a_file << "b64 ENC SECRET: ";
//    sprintf(szSecLog, "%s", abRawSecret);
//    a_file << szSecLog;
//    a_file << "\n";
//    a_file << "BASE64 ENC PIN: " << abB64EncPIN << "\n";
//    a_file.close();

    Device* d = new Device(lDateCreated, lLastUsed, iTokenType, nOtpAlgo, iSHAType,
            iOTPLength, iTimeStepInSecs, lInitialEventCounter, lEventCounter, strSerialNumber,
            strRegistrationCode, strHexRND, abRawSecret, strVersion, bLocked,
            bPBKD, bPinType, iRCDisplay, pinHash, strActivationCode, abB64EncPIN);
    int ret = d->unlock(pin, env);
    if (ret == 0) {
        return d;
    } else {
        return NULL;
    }
}

// 29-APRIL-2013
int Device::reset() {
    
    // DO SOME DEBUG LOGGING
//    char szSecLog[24];
//    memset(szSecLog, 0, sizeof (szSecLog));
//    ofstream a_file("/opt/dev/logs/cplusdevice.log", ios::app);
//    a_file << "-- reset() --\n";
//    a_file << "THE SECRET WILL BE ENCRYPTED WITH PIN AND ENV PARAMETER OF THE VERY NEXT CALL to: persist(...)";
//    a_file << "\n";
//    a_file.close();

    // STEP-1, Empty out the m_abHolder variable indicating that in the next persist(...)
    // the provided PIN and ENV will be used to ENCRYPT the SECRET
    memset(m_abHolder, 0, sizeof(m_abHolder));
    //NSLog(@"Device **** RESET");
    return 0;
}

int Device::unlock(char* pin, char* env) {
    errorcode = 0;
    if (!m_bLocked) {
        errorcode = INVALID_STATE;
        return errorcode;
    }

    if (pin == NULL || env == NULL) {
        errorcode = BAD_PARAMETER;
        return errorcode;
    }

    // 23-APRIL-2013
    int iIC = 0;
    char strIC[6];
    memset(strIC, '0', sizeof (strIC));
    // STEP-1a, Calculate the PIN Hash
    unsigned char pinHash[20];
    CSHA1 sha1;
    sha1.Reset();
    sha1.Update((unsigned char*) pin, strlen(pin));
    sha1.Final();
    sha1.GetHash(pinHash);
    // STEP-1b, SAVE the BASE64 encrypted secret in a holding variable
    memset(m_abHolder, 0, sizeof(m_abHolder));
    memcpy(m_abHolder, m_abRawSecret, sizeof (m_abRawSecret));


    // STEP-2, Check if this is PBKD mode or not
    if (!m_bPBKD) {
        // If not: Check if PIN supplied is correct or not
        if (memcmp(pinHash, m_pinHash, sizeof (pinHash)) != 0) {
            errorcode = PIN_ERROR;
            return PIN_ERROR;
        }
    }

    // STEP-3, lets  proceed to decrypt the secret
    // The encrypted secret stays in its holding variable 'm_abHolder'
    // A. BASE64 DECODE THE SECRET
    std::string encodedSecret((const char*) m_abRawSecret);
    std::string decodedSecret = base64_decode(encodedSecret);
    const char* encSecret = decodedSecret.c_str();

    int keyholderlen = sizeof (m_pinHash) + strlen(env);
    char* key_holder = new char[keyholderlen];
    char* key = new char[24];

    memset(key_holder, 0, keyholderlen);
    memcpy(key_holder, pinHash, sizeof (pinHash));
    memcpy(key_holder + sizeof (pinHash), env, strlen(env));

    // B. DECRYPT THE SECRET
    // (i) If version is < 4.0.0.0, then use ITC = 50 (TODO)
    if (m_strVersion[0] == '1' || m_strVersion[0] == '2' || m_strVersion[0] == '3') {
        //@ 24 bytes is hardcoded. - Length of Encryption key. 50 is hardcoded as the ITERATION COUNT for PBKDF2
        pbkd2_generateKey(key_holder, sizeof (pinHash) + strlen(env), m_strSerialNumber, strlen(m_strSerialNumber), 50, key, 24);
        strcpy(strIC, "50");
    } else {
        // (ii) If version is >= 4.0.0.0, then use stronger ITC
        if (m_strSerialNumber[0] == '0') {
            // ITC = 5000 DIVIDE BY 5
            strcpy(strIC, "1000");
            iIC = 5000/5;
        } else if (m_strSerialNumber[0] == '1') {
            // ITC = 500 DIVIDE BY 5
            strcpy(strIC, "100");
            iIC = 500/5;
        } else if (m_strSerialNumber[0] == '2') {
            // ITC = 50 DIVIDE BY 5
            strcpy(strIC, "10");
            iIC = 50/5;
        } else {
            strcpy(strIC, "1000");
            iIC = 1000;
        }
        //@ 24 bytes is hardcoded. - Length of Encryption key. IC for PBKDF2 is derived from SERIAL NUMBER
        pbkd2_generateKey(key_holder, sizeof (pinHash) + strlen(env), m_strSerialNumber, strlen(m_strSerialNumber), iIC, key, 24);
    }

    CRijndael decryptor;
    decryptor.MakeKey((const char*) key);
    decryptor.Decrypt(encSecret, (char*) m_abRawSecret, decodedSecret.length());


    // DO SOME DEBUG LOGGING
//    char szSecLog[24];
//    memset(szSecLog, 0, sizeof (szSecLog));
//    ofstream a_file("/opt/dev/logs/cplusdevice.log", ios::app);
//    a_file << "-- unlock(pin, env) --\n";
//    a_file << "PIN: " << pin << "\n";
//    a_file << "ENV: " << env << "\n";
//    a_file << "ITC: " << strIC << "\n";
//    a_file << "SECRET: ";
//    for (int j = 0; j < 20; j++) {
//        sprintf(szSecLog, "[%02x]", m_abRawSecret[j]);
//        a_file << szSecLog;
//    }
//    a_file << "\n";
//    a_file << "b64 ENC SECRET (HOLDING): ";
//    a_file << m_abHolder;
//    a_file << "\n";
//
//    a_file.close();
    m_bLocked = false;

    if (key_holder) {
        delete[] key_holder;
    }
    if (key) {
        delete[] key;
    }

    return 0;
}

unsigned char* Device::persist() {
    //NSLog(@"Device **** persist() ");
    errorcode = 0;
//    ofstream a_file("/opt/dev/logs/cplusdevice.log", ios::app);
//    a_file << "-- persist() --\n";
//    char szSecLog[24];
//    memset(szSecLog, 0, sizeof (szSecLog));
//    a_file << "SECRET: ";
//    for (int j = 0; j < 20; j++) {
//        sprintf(szSecLog, "[%02x]", m_abRawSecret[j]);
//        a_file << szSecLog;
//    }

    int len = sizeof (m_lDateCreated) +
            sizeof (m_lLastUsed) +
            sizeof (m_iTokenType) +
            sizeof (m_nOtpAlgo) +
            sizeof (m_iSHAType) +
            sizeof (m_iOTPLength) +
            sizeof (m_iTimeStepInSecs) +
            sizeof (m_lInitialEventCounter) +
            sizeof (m_lEventCounter) +
            sizeof (m_strSerialNumber) +
            sizeof (m_strRegistrationCode) +
            sizeof (m_strHexRND) +
            sizeof (m_abRawSecret) +
            sizeof (m_strVersion) +
            sizeof (m_bLocked) +
            sizeof (m_bPBKD) +
            sizeof (m_bPinType) +
            sizeof (m_iRCdisplay) +
            sizeof (m_pinHash) +
            sizeof (m_strActivationCode) +
            sizeof (m_abB64EncPIN);

    int lenSoFar = 0;
    unsigned char* buf = new unsigned char[len];
    memcpy(buf, &m_lDateCreated, sizeof (m_lDateCreated));
    lenSoFar += sizeof (m_lDateCreated);
    memcpy(buf + lenSoFar, &m_lLastUsed, sizeof (m_lLastUsed));
    lenSoFar += sizeof (m_lLastUsed);
    memcpy(buf + lenSoFar, &m_iTokenType, sizeof (m_iTokenType));
    lenSoFar += sizeof (m_iTokenType);
    memcpy(buf + lenSoFar, &m_nOtpAlgo, sizeof (m_nOtpAlgo));
    lenSoFar += sizeof (m_nOtpAlgo);
    memcpy(buf + lenSoFar, &m_iSHAType, sizeof (m_iSHAType));
    lenSoFar += sizeof (m_iSHAType);
    memcpy(buf + lenSoFar, &m_iOTPLength, sizeof (m_iOTPLength));
    lenSoFar += sizeof (m_iOTPLength);
    memcpy(buf + lenSoFar, &m_iTimeStepInSecs, sizeof (m_iTimeStepInSecs));
    lenSoFar += sizeof (m_iTimeStepInSecs);
    memcpy(buf + lenSoFar, &m_lInitialEventCounter, sizeof (m_lInitialEventCounter));
    lenSoFar += sizeof (m_lInitialEventCounter);
    memcpy(buf + lenSoFar, &m_lEventCounter, sizeof (m_lEventCounter));
    lenSoFar += sizeof (m_lEventCounter);
    memcpy(buf + lenSoFar, m_strSerialNumber, sizeof (m_strSerialNumber));
    lenSoFar += sizeof (m_strSerialNumber);
    // 23-APRIL-2013
    // If GLOABL OVERRIDE TO ALWAYS IGNORE RC PERSIST,
    // AND
    // If RC DISPLAY configuration is 1 (ON), then only save the RC
    if (m_iRCdisplay == 1 && GLOBAL_RC_PERSIST_COMMAND == 1) {
        memcpy(buf + lenSoFar, m_strRegistrationCode, sizeof (m_strRegistrationCode));
        lenSoFar += sizeof (m_strRegistrationCode);
        memcpy(buf + lenSoFar, m_strHexRND, sizeof (m_strHexRND));
        lenSoFar += sizeof (m_strHexRND);
    } else {
        // Initialize '0' character into the RC holding variables
        memset(buf + lenSoFar, 0, sizeof (m_strRegistrationCode));
        lenSoFar += sizeof (m_strRegistrationCode);
        memset(buf + lenSoFar, 0, sizeof (m_strHexRND));
        lenSoFar += sizeof (m_strHexRND);
    }
    memcpy(buf + lenSoFar, m_abRawSecret, sizeof (m_abRawSecret));
    lenSoFar += sizeof (m_abRawSecret);
    strcpy(m_strVersion,VERSION);
    memcpy(buf + lenSoFar, m_strVersion, sizeof (m_strVersion));
    lenSoFar += sizeof (m_strVersion);
    memcpy(buf + lenSoFar, &m_bLocked, sizeof (m_bLocked));
    lenSoFar += sizeof (m_bLocked);
    memcpy(buf + lenSoFar, &m_bPBKD, sizeof (m_bPBKD));
    lenSoFar += sizeof (m_bPBKD);
    memcpy(buf + lenSoFar, &m_bPinType, sizeof (m_bPinType));
    lenSoFar += sizeof (m_bPinType);
    memcpy(buf + lenSoFar, &m_iRCdisplay, sizeof (m_iRCdisplay));
    lenSoFar += sizeof (m_iRCdisplay);


    if (!m_bPBKD) {
        memcpy(buf + lenSoFar, m_pinHash, sizeof (m_pinHash));
        lenSoFar += sizeof (m_pinHash);
    } else {
        // PBKD mode, DO NOT save PIN HASH
        memset(buf + lenSoFar, 0, sizeof (m_pinHash));
        lenSoFar += sizeof (m_pinHash);
    }
    // NEVER persist ACTIVATION CODE
    memset(buf + lenSoFar, 0, sizeof (m_strActivationCode));
    memcpy(buf + lenSoFar, m_abB64EncPIN, sizeof (m_abB64EncPIN));
    lenSoFar += sizeof (m_abB64EncPIN);
    
//    a_file << "EVENT COUNTER: ";
//    sprintf(szSecLog, "%ld", m_lEventCounter);
//    a_file << szSecLog << "\n";
//    a_file << "SIZEOF DEVICE OBJECT: " << len << "\n";
//    a_file.close();
    return buf;
}

unsigned char* Device::persist(char* pin, char* env) {
    //NSLog(@"Device **** persist with pin and env ");
    char strIC[6];
    int iIC = 0;

    errorcode = 0;
    if (m_bLocked) {
        errorcode = INVALID_STATE;
        return NULL;
    }

    if (pin == NULL || env == NULL) {
        errorcode = BAD_PARAMETER;
        return NULL;
    }

    int keyholderlen = sizeof (m_pinHash) + strlen(env);
    char* key_holder = new char[keyholderlen];
    char* key = new char[24];

//    ofstream a_file("/opt/dev/logs/cplusdevice.log", ios::app);
//    a_file << "-- persist(char*, char*) --\n";
//    a_file << "PIN: [" << pin << "] \n";
//    a_file << "CF: [" << env << "] \n";


    // Lets prepare the LOCK CODE for later use of encrypting the PIN
    //1. First 8 characters of DATE CREATED
    char strTemp[20];
    char strUnlockChallenge[10];
    memset(strTemp, 0, sizeof (strTemp));
    memset(strUnlockChallenge, 0, sizeof (strUnlockChallenge));
    sprintf(strTemp, "%lu", m_lDateCreated);
    memcpy(strUnlockChallenge, strTemp, 8);
//    a_file << "DATE CREATED [long]: " << m_lDateCreated << "\n";
//    a_file << "DATE CREATED [string]: " << strTemp << "\n";
//    a_file << "UNLOCK CHALLENGE: " << strUnlockChallenge << "\n";
    char* strLockCode = getLockCode(strUnlockChallenge);
  //  a_file << "LOCK CODE: " << strLockCode << "\n";


    char encryptedSecret[320];

    // 23-APRIL-2013
    // LETS ENCRYPT THE RAW SECRET USED FOR OTP GENERATION
//    char szSecLog[24];
//    memset(szSecLog, 0, sizeof (szSecLog));
//    a_file << "SECRET: ";
//    for (int j = 0; j < 20; j++) {
//        sprintf(szSecLog, "[%02x]", m_abRawSecret[j]);
//        a_file << szSecLog;
//    }

    unsigned char pinHash[20];
    CSHA1 sha;
    sha.Reset();
    sha.Update((unsigned char*) pin, strlen(pin));
    sha.Final();
    sha.GetHash(pinHash);

    memset(key_holder, 0, keyholderlen);
    memcpy(key_holder, pinHash, sizeof (pinHash));
    memcpy(key_holder + sizeof (pinHash), env, strlen(env));


    //For version >= 4.0.0.0, then use stronger ITC
    if (m_strSerialNumber[0] == '0') {
        // ITC = 5000 DIVIDE BY 5
        strcpy(strIC, "1000");
        iIC = 5000/5;
    } else if (m_strSerialNumber[0] == '1') {
        // ITC = 500 DIVIDE BY 5
        strcpy(strIC, "100");
        iIC = 500/5;
    } else if (m_strSerialNumber[0] == '2') {
        // ITC = 50 DIVIDE BY 5
        strcpy(strIC, "10");
        iIC = 50/5;
    } else {
        strcpy(strIC, "1000");
        iIC = 5000/5;
    }
//    a_file << "VER >= 4.X.X.X, ITC TO ENCRYPT SECRET: " << strIC << "\n";
    //@ 24 bytes is hardcoded. - Length of Encryption key. IC for PBKDF2 is derived from SERIAL NUMBER
    pbkd2_generateKey(key_holder, sizeof (pinHash) + strlen(env), m_strSerialNumber, strlen(m_strSerialNumber), iIC, key, 24);

    // ANUPAM: SECURITY FIX FOR VISA 21-MAY-2013: START
    //char cplaindata[32];
    //memset(encryptedSecret, 0, 32);
    //memcpy(cplaindata, m_abRawSecret, 20);
    //memset(encryptedSecret, 0, 300);
    //printf("SECRET BEFORE ENCRYPTION\n");
    
    //for(int i=0;i<320;i++)
    //{
        //printf("%x ",m_abRawSecret[i]);
    //}
    //printf("\n");
    
    unsigned char cplaindata[32];
    memcpy(cplaindata, m_abRawSecret, 20);
    /////////////////////////////////////
    //printf("\nGenerating random 12 digits\n");
    srand((unsigned int)time(NULL));
    unsigned char extras[12];
    for(int i=0;i<12;i++)
    {
        extras[i] = ((unsigned int)rand());
        //printf("%x",extras[i]);
    }
    ////printf("\n\n");
    /////////////////////////////////////
    
    
    //char strTempRnd[32];
    
    //sprintf(strTempRnd,"%x",&cplaindata);
    
    //printf("RANDOM BYTES TO BE ADDED TO SECRET\n");
    
    //for(int i=0;i<32;i++)
    //{
        ////printf("%x ",strTempRnd[i]);
    //}
    //printf("\n");
    //a_file << "USE [" << strTempRnd << "] FOR RANDOM BYTE PADDING IN SECRET\n";
    //memcpy(cplaindata+20, strTempRnd, 12);
    memcpy(cplaindata+20, extras, 12);
    memset(encryptedSecret, 0, 300);
    // ANUPAM: SECURITY FIX FOR VISA 21-MAY-2013: END

    //printf("SECRET BEFORE ENCRYPTION + PADDED BYTES (32)\n");
    
    //for(int i=0;i<32;i++)
    //{
        //printf("%x ",cplaindata[i]);
    //}
    //printf("\n");

    CRijndael encryptor1;
    // 24 Bytes from the 'key' will get for making the AES encryption key
    encryptor1.MakeKey((const char*) key);
    encryptor1.Encrypt((const char*) cplaindata, encryptedSecret, 32);

    //printf("SECRET AFTER ENCRYPTION\n");

    //for(int i=0;i<320;i++)
    //{
        //printf("%x ",encryptedSecret[i]);
    //}
    //printf("\n");
    
    std::string strb64 = base64_encode((const unsigned char*) encryptedSecret, 32);
    memcpy(m_abRawSecret, strb64.c_str(), strb64.length());
 //   a_file << "ENCRYPYTED THE SECRET WITH PROVIDED PIN, ENV AND B64 ENCODED FOR PERSIST\n";
    m_bLocked = true;
    // Save PIN Hash
    CSHA1 sha1;
    sha1.Update((unsigned char*) pin, strlen(pin));
    sha1.Final();
    sha1.GetHash(m_pinHash);
  //  a_file << "HASHED THE NEW PIN\n";

    // If, PBKD mode, just persist the BASE64 encrypted secret in holding variable m_abHolder IFF AVAILABLE
    if (m_bPBKD) {
  //      a_file << "PBKD MODE\n";
        if (strlen((const char*)m_abHolder) == 0) {
  //          a_file << "BASE64 ENC SECRET HOLDER EMPTY, USE THE CURRENT b64 ENC SECRET TO PERSIST\n";
        } else {
            // Copy the encrypted secret from holding variable m_abHolder to m_abRawSecret
            ////Added 23th October 2013 - Harsha///
            memset(m_abRawSecret, 0, sizeof (m_abRawSecret));
            /////
            memcpy(m_abRawSecret, m_abHolder, sizeof (m_abRawSecret));
            memset(m_abHolder, 0, sizeof (m_abHolder));
   //         a_file << "USE BASE64 ENC SECRET IN HOLDER TO PERSIST\n";
        }
    }
  //  a_file << "PERSISTING SECRET [B64(ENC)]: " << m_abRawSecret << "\n";



    // Lets encrypt the PIN
    // 1. Use the lock code as the encryption key
    char *lockkey = new char [24];
    memset(lockkey, 0, 24);
    //printf("\n Password - %s \n", password);
    //printf("\n strSerialNumber: [%s]",strSerialNumber);
    //printf("\n m_iIterationCount: [%d]",m_iIterationCount);
    //printf("\n GOING FOR PBKDF2\n");
    //@ 24 bytes is hardcoded. - Length of Encryption key. 50 is hardcoded as the ITERATION COUNT for PBKDF2
    pbkd2_generateKey(strLockCode, strlen(strLockCode), m_strSerialNumber, strlen(m_strSerialNumber), 50, lockkey, 24);
    //printf("\n SECRET Generated using PBKDF2\n");

//    memset(szSecLog, 0, sizeof (szSecLog));
//    a_file << "PARAMS FOR LOCK KEY - PARAM(1): ";
//    a_file << strLockCode;
//    a_file << "\n";
//    a_file << "PARAMS FOR LOCK KEY - PARAM(2): ";
//    a_file << m_strSerialNumber;
//    a_file << "\n";
//    a_file << "LOCK PIN (24 bytes): ";
//    for (int j = 0; j < 24; j++) {
//        sprintf(szSecLog, "[%02x]", lockkey[j]);
//        a_file << szSecLog;
//    }
//    a_file << "\n";


    CRijndael encryptor2;

    memset(encryptedSecret, 0, 300);
    encryptor2.MakeKey((const char*) lockkey);
    encryptor2.Encrypt((const char*) key, encryptedSecret, 32);
    std::string strb642 = base64_encode((const unsigned char*) encryptedSecret, 32);
    memset(m_abB64EncPIN, 0, sizeof (m_abB64EncPIN));
    memcpy(m_abB64EncPIN, strb642.c_str(), strb642.length());


 //   a_file << "BASE64 ENC PIN: " << m_abB64EncPIN << "\n";


    int len = sizeof (m_lDateCreated) +
            sizeof (m_lLastUsed) +
            sizeof (m_iTokenType) +
            sizeof (m_nOtpAlgo) +
            sizeof (m_iSHAType) +
            sizeof (m_iOTPLength) +
            sizeof (m_iTimeStepInSecs) +
            sizeof (m_lInitialEventCounter) +
            sizeof (m_lEventCounter) +
            sizeof (m_strSerialNumber) +
            sizeof (m_strRegistrationCode) +
            sizeof (m_strHexRND) +
            sizeof (m_abRawSecret) +
            sizeof (m_strVersion) +
            sizeof (m_bLocked) +
            sizeof (m_bPBKD) +
            sizeof (m_bPinType) +
            sizeof (m_iRCdisplay) +
            sizeof (m_pinHash) +
            sizeof (m_strActivationCode) +
            sizeof (m_abB64EncPIN);

  //  a_file << "EVENT COUNTER: ";
//    sprintf(szSecLog, "%ld", m_lEventCounter);
//    a_file << szSecLog << "\n";
//    a_file << "SIZEOF DEVICE OBJECT: " << len << "\n";
//    a_file.close();
    int lenSoFar = 0;
    unsigned char* buf = new unsigned char[len];
    memcpy(buf, &m_lDateCreated, sizeof (m_lDateCreated));
    lenSoFar += sizeof (m_lDateCreated);
    memcpy(buf + lenSoFar, &m_lLastUsed, sizeof (m_lLastUsed));
    lenSoFar += sizeof (m_lLastUsed);
    memcpy(buf + lenSoFar, &m_iTokenType, sizeof (m_iTokenType));
    lenSoFar += sizeof (m_iTokenType);
    memcpy(buf + lenSoFar, &m_nOtpAlgo, sizeof (m_nOtpAlgo));
    lenSoFar += sizeof (m_nOtpAlgo);
    memcpy(buf + lenSoFar, &m_iSHAType, sizeof (m_iSHAType));
    lenSoFar += sizeof (m_iSHAType);
    memcpy(buf + lenSoFar, &m_iOTPLength, sizeof (m_iOTPLength));
    lenSoFar += sizeof (m_iOTPLength);
    memcpy(buf + lenSoFar, &m_iTimeStepInSecs, sizeof (m_iTimeStepInSecs));
    lenSoFar += sizeof (m_iTimeStepInSecs);
    memcpy(buf + lenSoFar, &m_lInitialEventCounter, sizeof (m_lInitialEventCounter));
    lenSoFar += sizeof (m_lInitialEventCounter);
    memcpy(buf + lenSoFar, &m_lEventCounter, sizeof (m_lEventCounter));
    lenSoFar += sizeof (m_lEventCounter);
    memcpy(buf + lenSoFar, m_strSerialNumber, sizeof (m_strSerialNumber));
    lenSoFar += sizeof (m_strSerialNumber);
    // 23-APRIL-2013
    // If GLOABL OVERRIDE TO ALWAYS IGNORE RC PERSIST,
    // AND
    // If RC DISPLAY configuration is 1 (ON), then only save the RC
    if (m_iRCdisplay == 1 && GLOBAL_RC_PERSIST_COMMAND == 1) {
        memcpy(buf + lenSoFar, m_strRegistrationCode, sizeof (m_strRegistrationCode));
        lenSoFar += sizeof (m_strRegistrationCode);
        memcpy(buf + lenSoFar, m_strHexRND, sizeof (m_strHexRND));
        lenSoFar += sizeof (m_strHexRND);
    } else {
        // Initialize '0' character into the RC holding variables
        memset(buf + lenSoFar, 0, sizeof (m_strRegistrationCode));
        lenSoFar += sizeof (m_strRegistrationCode);
        memset(buf + lenSoFar, 0, sizeof (m_strHexRND));
        lenSoFar += sizeof (m_strHexRND);
    }
    memcpy(buf + lenSoFar, m_abRawSecret, sizeof (m_abRawSecret));
    lenSoFar += sizeof (m_abRawSecret);
    strcpy(m_strVersion,VERSION);
    memcpy(buf + lenSoFar, m_strVersion, sizeof (m_strVersion));
    lenSoFar += sizeof (m_strVersion);
    memcpy(buf + lenSoFar, &m_bLocked, sizeof (m_bLocked));
    lenSoFar += sizeof (m_bLocked);
    memcpy(buf + lenSoFar, &m_bPBKD, sizeof (m_bPBKD));
    lenSoFar += sizeof (m_bPBKD);
    memcpy(buf + lenSoFar, &m_bPinType, sizeof (m_bPinType));
    lenSoFar += sizeof (m_bPinType);
    memcpy(buf + lenSoFar, &m_iRCdisplay, sizeof (m_iRCdisplay));
    lenSoFar += sizeof (m_iRCdisplay);
    if (!m_bPBKD) {
        memcpy(buf + lenSoFar, m_pinHash, sizeof (m_pinHash));
        lenSoFar += sizeof (m_pinHash);
    
    } else {
        // PBKD mode, DO NOT save PIN HASH
        memset(buf + lenSoFar, 0, sizeof (m_pinHash));
        lenSoFar += sizeof (m_pinHash);
    }
    // NEVER persist ACTIVATION CODE
    memset(buf + lenSoFar, 0, sizeof (m_strActivationCode));
    lenSoFar += sizeof (m_strActivationCode);
    memcpy(buf + lenSoFar, m_abB64EncPIN, sizeof (m_abB64EncPIN));
    lenSoFar += sizeof (m_abB64EncPIN);

    if (key_holder)delete [] key_holder;
    if (key)delete[] key;
    if (lockkey) delete[] lockkey;

    return buf;
}

/**
 * To generate the initial event counter from the token serial number
 * @param tsn String   - The token serial number (UPC-A serial no.)
 * @return long        - The initial event counter
 */
long Device::getInitialEventCounter(char *tsn) {
    errorcode = 0;
    long lEventCounter = 0;
    char *pEventCounter = NULL;
    if (tsn == NULL) {
        return lEventCounter;
    }
    char* sn = tsn;
    if (strlen(tsn) == 16) {
        // Seems to be NEW SN
        sn = getEffectiveSN(tsn);
    }

    // Event counter is the sub-string of the serial number of length 9
    pEventCounter = new char [10];
    memset(pEventCounter, 0, 10);
    memcpy(pEventCounter, sn + 2, 9);
    lEventCounter = atol(pEventCounter);
    delete [] pEventCounter;
    return lEventCounter;
}


// Calling application must FREE the memory

char const* const Device::getSerialNumber() {
    errorcode = 0;
    /*
    char* sn = new char[strlen(m_strSerialNumber) + 1];
     */
    char const* const sn = m_strSerialNumber;
    if (m_strSerialNumber != NULL && strlen(m_strSerialNumber) != 0) {

        /*
        memset(sn, 0, strlen(m_strSerialNumber) + 1);
        strcpy(sn, m_strSerialNumber);
         */
        return sn;
    } else {
        errorcode = INVALID_STATE;
        return NULL;
    }
}

// Calling application must FREE the memory

char const* const Device::getRegistrationCode() {
    errorcode = 0;
    //char* rc = new char[strlen(m_strRegistrationCode) + 1];
    char const* const rc = m_strRegistrationCode;

    if (m_strRegistrationCode != NULL && strlen(m_strRegistrationCode) != 0) {

        /*
        memset(rc, 0, strlen(m_strRegistrationCode) + 1);
        strcpy(rc, m_strRegistrationCode);
         */

        return rc;
    } else {
        errorcode = INVALID_STATE;
        return NULL;
    }
}

// Calling application must FREE the memory

char const* const Device::getActivationCode() {
    errorcode = 0;
    //char* ac = new char[strlen(m_strActivationCode) + 1];
    char const* const ac = m_strActivationCode;

    if (m_strActivationCode != NULL && strlen(m_strActivationCode) != 0) {

        /*
                 memset(ac, 0, strlen(m_strActivationCode) + 1);
                 strcpy(ac, m_strActivationCode);
         */

        return ac;
    } else {
        errorcode = INVALID_STATE;
        return NULL;
    }
}

/**
 * To retrieve the initial event counter from the initialized token
 * @return long        - The initial event counter
 */
long Device::getIEC() {
    errorcode = 0;
    return m_lInitialEventCounter;
}

/**
 * To retrieve the current event counter from the initialized token
 * @return long        - The event counter
 */
long Device::getEC() {
    errorcode = 0;
    return m_lEventCounter;
}

unsigned long long int Device::getClock() {
    errorcode = 0;
    unsigned long long int li_timestamp = getCurrentTimeinMiliSecond();
    li_timestamp = li_timestamp / 1000; // Convert to seconds
    // update the time step factor :: as the time is in milisecond : multiply by 1000;
    long m_timestepfactor = 2;
    // Time in half seconds since 1/1/1995 GMT+7
    li_timestamp = (li_timestamp - 788893200) * m_timestepfactor;
    return li_timestamp;
}

unsigned long int Device::getCreatedDate() {
    errorcode = 0;
    return m_lDateCreated;
}

long Device::getLatUsedDate() {
    errorcode = 0;
    return m_lLastUsed;
}

int Device::getOTPLength() {
    errorcode = 0;
    return m_iOTPLength;
}

int Device::getSHAType() {
    errorcode = 0;
    return m_iSHAType;
}

int Device::getTimeStep() {
    errorcode = 0;
    return m_iTimeStepInSecs;
}

int Device::getTokenType() {
    errorcode = 0;
    return m_iTokenType;
}

int Device::getTokenMode() {
    errorcode = 0;
    return m_nOtpAlgo;
}

int Device::isRCDisplayEnabed() {
    errorcode = 0;
    return m_iRCdisplay;
}

int Device::getEncType() {
    errorcode = 0;
    return m_enctype;
}

int Device::getPinType() {
    errorcode = 0;
    if (m_bPinType) return 1;
    else return 0;
}

int Device::getPinMode() {
    errorcode = 0;
    if (m_bPinType == true) {
        if (m_bPBKD) return 1;
        else return 0;
    } else {
        return INVALID_STATE;
    }
}

int Device::getErrorCode() {
    return errorcode;
}

bool Device::getLockStatus() {
    return m_bLocked;
}

void Device::resetDateCreated()
{
    m_lDateCreated = getCurrentTimeinMiliSecond() / 1000;
}

void Device::parseActivationCode(char* actcode) {
    errorcode = 0;
    int c1 = 0, c2 = 0, c3 = 0;
    c3 = actcode[strlen(actcode) - 2] - 48;
    c2 = actcode[strlen(actcode) - 3] - 48;
    c1 = actcode[strlen(actcode) - 4] - 48;

    // Get the bit 0 of c1
    //printf("\n Value of 7 & 8th digit - %d : %d \n", digit7, digit8);
    if (c1 % 2 == 0) {
        // even
        m_bPinType = false;
    } else {
        // odd
        m_bPinType = true;
    }
    // Check 2nd and 3rd bit of c1 [Right Shift 1-bit and mask]
    int appmode = c1 >> 1;
    appmode = appmode & 0x3;
    if (appmode == 0) {
        // OTP Mode only
        m_nOtpAlgo = OTP;
        //strAppMode = "1";
    } else if (appmode == 1) {
        // OTP + CR
        m_nOtpAlgo = OTP_CR;
        //strAppMode = "2";
    } else if (appmode == 2) {
        // OTP + SIG
        m_nOtpAlgo = OTP_SIG;
        //strAppMode = "3";
    } else if (appmode == 3) {
        // OTP + CR + SIG
        m_nOtpAlgo = OTP_CR_SIG;
        //strAppMode = "4";
    }

    // Check 3rd Bit of c2 [Right Shift 2-bits]
    int tktype = c2 >> 2;
    tktype = tktype & 0x1;
    if (tktype % 2 == 0) {
        // EVEN
        // HOTP Type
        m_iTokenType = HOTP;
    } else {
        // ODD
        // TOTP Type
        m_iTokenType = TOTP;
    }

    // c2 1st and 2nd bit = OTP Length
    int otplen = c2 & 0x03;
    if (otplen == 0) {
        m_iOTPLength = 4;
    } else if (otplen == 1) {
        m_iOTPLength = 6;
    } else if (otplen == 2) {
        m_iOTPLength = 8;
    } else if (otplen == 3) {
        m_iOTPLength = 10;
    }

    // 1st bit of c3 = PBKD ON or OFF
    if (c3 % 2 == 0) {
        // even
        m_bPBKD = 0;
    } else {
        // odd
        m_bPBKD = 1;
    }

    // 2nd bit of c3 = RC display ON or OFF
    int rcdisplay = c3 >> 1;
    if (rcdisplay % 2 == 0) {
        // EVEN
        // Implies that RC will not be displayed
        m_iRCdisplay = 0;
    } else {
        // ODD
        // Implies that RC will be displayed for 7days
        m_iRCdisplay = 1;
    }

    int shatype = c3 >> 2;
    if (shatype % 2 == 0) {
        // EVEN
        // SHA-1
        m_iSHAType = SHA_1;
        m_enctype = TDES;
    } else {
        // ODD
        // TOTP Type
        m_enctype = AES;
        m_iSHAType = SHA_256;
    }

    // Time Step Factor
    m_iTimeStepInSecs = 30;
}

BYTE *Device::activateToken(char *strSerialNumber, char *strActivationCode, unsigned char * strEventsTimeSeed) {

    errorcode = 0;
    BYTE *abHexRND = NULL;
    //printf("\n Inside OTPLib::activateToken \n");
    // Add the serialnumber & actcode verification code
    //
    //
    if (!verifyCheckDigit(strActivationCode))
        return NULL;
    if (strEventsTimeSeed == NULL || strlen((const char*) strEventsTimeSeed) < 5)
        return NULL;
    // Read only 5 bytes : 40 bits
    abHexRND = new BYTE[5];
    memset(abHexRND, 0, 5);
    for (int iCount = 0; iCount < 5; iCount++) {
        abHexRND[iCount] = strEventsTimeSeed[iCount];
    }
    //char buffer[32];
    //memset(buffer, 0, sizeof(buffer));
    //memcpy(buffer,abHexRND,5);
    // Do the Base32 encoding
    struct Base32 base32;
    int encodeLength = base32.GetEncode32Length(5);
    base32.Encode32(abHexRND, 5, m_strHexRND);
    if (m_strHexRND == NULL) {
        errorcode = -1;
        return NULL;
    }
    //unsigned char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
    unsigned char alphabet[] = "ABCDEFGHJKMNPQRSTVWXYZ0123456789";
    base32.Map32(m_strHexRND, encodeLength, alphabet);
    //memset(buffer, 0, sizeof(buffer));
    //memcpy(buffer,m_strHexRND,encodeLength);
    //printf("\n B32 ENCODING OUTPUT: [%s]\n" , buffer);
    BYTE *abRawSecret = generateRawSecret(strSerialNumber, strActivationCode, abHexRND);

    delete [] abHexRND;
    return abRawSecret;
}

BYTE *Device::generateRawSecret(char *strSerialNumber, char *strActivationCode, BYTE *abRND) {
    errorcode = 0;
    char *password = NULL;
    char *secret = new char [21];
    memset(secret, 0, 21);
    int pwdlen = strlen(strActivationCode) + 5;
    password = new char [pwdlen + 1];
    memset(password, 0, pwdlen + 1);
    memcpy(password, strActivationCode, strlen(strActivationCode));
    //printf("\n Password - %s \n", password);
    memcpy(password + strlen(strActivationCode), abRND, 5);
    //printf("\n Password - %s \n", password);
    //printf("\n strSerialNumber: [%s]",strSerialNumber);
    //printf("\n m_iIterationCount: [%d]",m_iIterationCount);
    //printf("\n GOING FOR PBKDF2\n");
    pbkd2_generateKey(password, pwdlen, strSerialNumber, strlen(strSerialNumber), m_iIC, secret, 20);
    //@ 20 bytes is hardcoded. - Length of HOTP secret
    //printf("\n SECRET Generated using PBKDF2\n");
    delete [] password;
    return (BYTE *) secret;
}

bool Device::verifyCheckDigit(char *actcode) {
    int iSum = 0, iDigit = 0, iCheckSum = 0;
    char number[3];
    char szactcode[15];
    memset(szactcode, 0, 15);
    strncpy(szactcode, actcode, strlen(actcode) - 1);
    // get the last digit of the act code
    char ckdigit[2];
    memset(ckdigit, 0, 2);
    ckdigit[0] = actcode[strlen(actcode) - 1];
    int ickdigit = atoi(ckdigit);
    int j = 0;
    for (int i = 0; i < strlen(szactcode); i++) {
        j = i + 1;
        number[0] = szactcode[i];
        number[1] = '\0';
        iDigit = atoi(number);
        if (j % 2 == 0) {
            // Even Position
            iSum += iDigit * 1;
        } else {
            iSum += iDigit * 3;
        }
    }
    iCheckSum = (10 - (iSum % 10)) % 10;
    if (iCheckSum == ickdigit) {
        return true;
    } else {
        return false;
    }
    // always correct actcode
}

char *Device::generateOTP() {
    errorcode = 0;
    m_lLastUsed = getCurrentTimeinMiliSecond() / 1000;
    //return generateOTP_Time();
    if (m_iTokenType == HOTP) {
        // EVENT MODE
        return generateOTP_Event();
    } else if (m_iTokenType == TOTP) {
        return generateOTP_Time();
    }
    return NULL;
}

char* Device::generateOTP_Event() {
    errorcode = 0;
    BYTE* secret = m_abRawSecret;
    long lEventCounter = m_lEventCounter;
    bool addChecksum = false;
    int iTruncationOffset = m_iTruncationOffset;
    int iOtpLength = m_iOTPLength;
    // put movingFactor value into text BYTE array
    char *result;
    BYTE text[OTP_TEXT_LEN];
    BYTE *hash;
    //int digits = addChecksum ? (iOtpLength + 1) : iOtpLength;
    memset(text, 0, OTP_TEXT_LEN);
    for (int i = OTP_TEXT_LEN - 1; i >= 0; i--) {
        text[i] = (BYTE) (lEventCounter & 0xff);
        lEventCounter >>= OTP_TEXT_LEN;
    }
    // compute hmac hash
    CHMAC_SHA1 hmc_obj;
    hash = new BYTE [20];
    hmc_obj.HMAC_SHA1(text, OTP_TEXT_LEN, secret, OTP_SECRET_KEY_LENGTH, hash);
    // put selected BYTEs into result int
    int offset = hash[OTP_DIGEST_LENGTH - 1] & 0xf;
    if ((0 <= iTruncationOffset) && (iTruncationOffset < (OTP_DIGEST_LENGTH - 4))) {
        offset = iTruncationOffset;
    }
    int binary =
            ((hash[offset] & 0x7f) << 24)
            | ((hash[offset + 1] & 0xff) << 16)
            | ((hash[offset + 2] & 0xff) << 8)
            | (hash[offset + 3] & 0xff);
    int otp = 0;
    if (iOtpLength == 10) {
        char tt[20];
        memset(tt, 0, 20);
        strcpy(tt, "10000000000");
        unsigned long long int tendigits = StringToU64(tt);
        //otp = binary % DIGITS_POWER2[m_iOTPLength];
        otp = binary % tendigits;
    } else {
        otp = binary % DIGITS_POWER[iOtpLength];
    }
    if (addChecksum) {
        otp = (otp * 10) + calcChecksum(otp, iOtpLength);
    }
    // Allocate memory for result
    result = new char [ MAX_OTP_LEN + 1];
    memset(result, 0, MAX_OTP_LEN + 1);
    switch (iOtpLength) {
        case 4:
            sprintf(result, "%04d", otp);
            break;
        case 6:
            sprintf(result, "%06d", otp);
            break;
        case 8:
            sprintf(result, "%08d", otp);
            break;
        case 10:
            sprintf(result, "%010d", otp);
            break;
        default:
            sprintf(result, "%d", otp);
    }
    // clean up
    m_lEventCounter++;
    delete [] hash;
    return result;
}

// For v1 Token usage

char* Device::generateOTP_Custom(BYTE* pSecret, long lCounter, int iOTPLen) {
    errorcode = 0;
    BYTE* secret = pSecret;
    long lEventCounter = lCounter;
    bool addChecksum = false;
    int iTruncationOffset = m_iTruncationOffset;
    int iOtpLength = iOTPLen;
    // put movingFactor value into text BYTE array
    char *result;
    BYTE text[OTP_TEXT_LEN];
    BYTE *hash;
    //int digits = addChecksum ? (iOtpLength + 1) : iOtpLength;
    memset(text, 0, OTP_TEXT_LEN);
    for (int i = OTP_TEXT_LEN - 1; i >= 0; i--) {
        text[i] = (BYTE) (lEventCounter & 0xff);
        lEventCounter >>= OTP_TEXT_LEN;
    }
    // compute hmac hash
    CHMAC_SHA1 hmc_obj;
    hash = new BYTE [20];
    hmc_obj.HMAC_SHA1(text, OTP_TEXT_LEN, secret, OTP_SECRET_KEY_LENGTH, hash);
    // put selected BYTEs into result int
    int offset = hash[OTP_DIGEST_LENGTH - 1] & 0xf;
    if ((0 <= iTruncationOffset) && (iTruncationOffset < (OTP_DIGEST_LENGTH - 4))) {
        offset = iTruncationOffset;
    }
    int binary =
            ((hash[offset] & 0x7f) << 24)
            | ((hash[offset + 1] & 0xff) << 16)
            | ((hash[offset + 2] & 0xff) << 8)
            | (hash[offset + 3] & 0xff);
    int otp = 0;
    if (iOtpLength == 10) {
        char tt[20];
        memset(tt, 0, 20);
        strcpy(tt, "10000000000");
        unsigned long long int tendigits = StringToU64(tt);
        //otp = binary % DIGITS_POWER2[m_iOTPLength];
        otp = binary % tendigits;
    } else {
        otp = binary % DIGITS_POWER[iOtpLength];
    }
    if (addChecksum) {
        otp = (otp * 10) + calcChecksum(otp, iOtpLength);
    }
    // Allocate memory for result
    result = new char [ MAX_OTP_LEN + 1];
    memset(result, 0, MAX_OTP_LEN + 1);
    switch (iOtpLength) {
        case 4:
            sprintf(result, "%04d", otp);
            break;
        case 6:
            sprintf(result, "%06d", otp);
            break;
        case 8:
            sprintf(result, "%08d", otp);
            break;
        case 10:
            sprintf(result, "%010d", otp);
            break;
        default:
            sprintf(result, "%d", otp);
    }
    // clean up
    m_lEventCounter++;
    delete [] hash;
    return result;
}

char* Device::generateOTP_Time() {
    errorcode = 0;
    char strCryptoString[10];
    memset(strCryptoString, 0, 10);

    unsigned long long int li_timestamp = getCurrentTimeinMiliSecond();
    char log[24];
    sprintf(log, "%llu", li_timestamp);
    // update the time step factor :: as the time is in milisecond : multiply by 1000;
    int timestep = m_iTimeStepInSecs * 1000;
    sprintf(log, "%ld", m_lInitialEventCounter);
    li_timestamp -= m_lInitialEventCounter;
    sprintf(log, "%lld", li_timestamp);
    li_timestamp = li_timestamp / timestep;
    sprintf(log, "%lld", li_timestamp);

    strcpy(strCryptoString, "HmacSHA");
    char szSHAType[5];
    memset(szSHAType, 0, 5);
    sprintf(szSHAType, "%d", m_iSHAType);
    strcat(strCryptoString, szSHAType);
    char tt[20];
    memset(tt, 0, 20);

    char strTimeStamp[20];
    memset(strTimeStamp, 0, 20);
    sprintf(strTimeStamp, "%llx", li_timestamp);
    char strTimeStamp1[17];
    memset(strTimeStamp1, 0, 17);
    int len = strlen(strTimeStamp);
    if (len < 16) {
        int dlen = 16 - len;
        // pad with dlen number of zeros
        strcpy(strTimeStamp1, "0");
        while (dlen > 1) {
            strcat(strTimeStamp1, "0");
            dlen--;
        }
        dlen = 16 - len;
        strcat(strTimeStamp1, strTimeStamp);
        //sprintf(strTimeStamp + dlen , "%llu", li_timestamp);
    }
    char *pOTP = generateTOTP(m_abRawSecret, strTimeStamp1, m_iOTPLength, strCryptoString);
    return pOTP;
}

char *Device::generateTOTP(BYTE *key, char *timestamp, long otplength, char *cryptostring) {
    errorcode = 0;
    BYTE *msg = new BYTE[8];
    BYTE *msg1 = new BYTE[8];

    // Convert the string to long value & then convert it to the BYTE *
    long tt = strtoul(timestamp, NULL, 16);
    long ntt = to_network_byte_order(tt);
    Int32ToUInt8Arr(ntt, msg);

    msg1[0] = 0;
    msg1[1] = 0;
    msg1[2] = 0;
    msg1[3] = 0;
    msg1[4] = msg[0];
    msg1[5] = msg[1];
    msg1[6] = msg[2];
    msg1[7] = msg[3];

    CHMAC_SHA1 hmc_obj;
    BYTE *hash = new BYTE [20];
    hmc_obj.HMAC_SHA1(msg1, 8, key, 20, hash);
    //hmc_obj.HMAC_SHA1(timestamp,4, (BYTE *)"12345678901234567890", 20 ,hash);


    int offset = hash[OTP_DIGEST_LENGTH - 1] & 0xf;
    int binary =
            ((hash[offset] & 0x7f) << 24)
            | ((hash[offset + 1] & 0xff) << 16)
            | ((hash[offset + 2] & 0xff) << 8)
            | (hash[offset + 3] & 0xff);

    int otp = 0;
    if (m_iOTPLength == 10) {
        char tt[20];
        memset(tt, 0, 20);
        strcpy(tt, "10000000000");
        unsigned long long int tendigits = StringToU64(tt);
        otp = binary % tendigits;
    } else {
        otp = binary % DIGITS_POWER[m_iOTPLength];
    }


    // Allocate memory for result
    char *tempresult = new char [10];
    memset(tempresult, 0, 10);
    char *result = new char [ MAX_OTP_LEN + 1];
    memset(result, 0, MAX_OTP_LEN + 1);
    switch (m_iOTPLength) {
        case 4:
            sprintf(result, "%04d", otp);
            break;
        case 6:
            sprintf(result, "%06d", otp);
            break;
        case 8:
            sprintf(result, "%08d", otp);
            break;
        case 10:
            sprintf(result, "%010d", otp);

            break;
        default:
            sprintf(result, "%d", otp);
    }

    // clean up
    delete hash;

    return result;
}

char *Device::generateCROTP(char* szChallenge) {
    errorcode = 0;
    m_lLastUsed = getCurrentTimeinMiliSecond() / 1000;
    if (m_iTokenType == HOTP) {
        // EVENT MODE
        return generateCROTP_Event(szChallenge);
    } else if (m_iTokenType == TOTP) {
        return generateCROTP_Time(szChallenge);
    }
    return NULL;
}

char *Device::generateCROTP_Time(char *strChallenge) {
    errorcode = 0;
    return generateSIGOTP_Time(strChallenge);
} // MEMORY LEAK REVIEW- OK (11-JAN-2010)

char *Device::generateCROTP_Event(char *strChallenge) {
    errorcode = 0;
    // strChallenge is the string which is inputed by the user as challange
    // OCRA-1:HOTP-SHA1-8:C-QAXX
    //char *ocraSuite = "OCRA-1:HOTP-SHA1-" + m_iOTPLength + ":C-QA" + strChallenge.length();
    // Construct the OCRA suite based on the activation code
    char *ocrasuite1 = "OCRA-1:HOTP-SHA1-";
    char ocrasuite[128];
    memset(ocrasuite, 0, 128);
    char strOtpLen[3];
    memset(strOtpLen, 0, 3);
    sprintf(strOtpLen, "%d", m_iOTPLength);
    char strChallangeLength[5];
    memset(strChallangeLength, 0, 5);
    sprintf(strChallangeLength, "%02ld", strlen(strChallenge));
    // Concatenate the above input to create the ocrasuite string
    strcpy(ocrasuite, ocrasuite1);
    strcat(ocrasuite, strOtpLen);
    strcat(ocrasuite, ":C-QA");
    strcat(ocrasuite, strChallangeLength);
    BYTE *key = m_abRawSecret;
    char *question = NULL;
    question = new char [ strlen(strChallenge) + 1];
    memset(question, 0, strlen(strChallenge) + 1);
    strcpy(question, strChallenge);
    char *password = NULL;
    // Not used
    char *sessionInformation = NULL;
    // Not used
    char *timestamp = NULL;
    // Not used
    char strCounter[128];
    memset(strCounter, 0, 128);
    sprintf(strCounter, "%ld", m_lEventCounter);
    char *pOTP = generateOCRA(ocrasuite, key, strCounter, question, password, sessionInformation, timestamp);
    // Cleanup stuffs
    m_lEventCounter++;

    delete [] question;
    return pOTP;
} // MEMORY LEAK REVIEW- OK (11-JAN-2010)

char *Device::generateSIGOTP(char* szChallenge) {
    errorcode = 0;
    m_lLastUsed = getCurrentTimeinMiliSecond() / 1000;
    if (m_iTokenType == HOTP) {
        // EVENT MODE
        return generateSIGOTP_Event(szChallenge);
    } else if (m_iTokenType == TOTP) {
        return generateSIGOTP_Time(szChallenge);
    }
    return NULL;
}

char *Device::generateSIGOTP_Time(char *astrData) {
    errorcode = 0;
    //OCRA-1:HOTP-SHA1-8:QAXX-TXXX

    char *ocrasuite1 = "OCRA-1:HOTP-SHA1-";
    char ocrasuite[128];
    memset(ocrasuite, 0, 128);
    char strOtpLen[3];
    memset(strOtpLen, 0, 3);
    sprintf(strOtpLen, "%d", m_iOTPLength);
    char strChallangeLength[5];
    memset(strChallangeLength, 0, 5);
    sprintf(strChallangeLength, "%02ld", strlen(astrData));
    // Concatenate the above input to create the ocrasuite string
    strcpy(ocrasuite, ocrasuite1);
    strcat(ocrasuite, strOtpLen);
    strcat(ocrasuite, ":QA");
    strcat(ocrasuite, strChallangeLength);
    strcat(ocrasuite, "-T");
    if (m_iTimeStepInSecs == 60) {
        strcat(ocrasuite, "1M");
    } else if (m_iTimeStepInSecs == 30) {
        strcat(ocrasuite, "30S");
    }
    BYTE *key = m_abRawSecret;
    char *question = NULL;
    question = new char [ strlen(astrData) + 1];
    memset(question, 0, strlen(astrData) + 1);
    strcpy(question, astrData);
    char *password = NULL;
    // Not used
    char *sessionInformation = NULL;
    // Not used
    char *timestamp = NULL;
    // Not used
    char *counter = NULL;
    // Time calculation
    unsigned long long int lTimeMiliSec = 0;
    lTimeMiliSec = getCurrentTimeinMiliSecond();
    // convert into time step
    lTimeMiliSec = lTimeMiliSec / 1000;
    lTimeMiliSec = lTimeMiliSec / m_iTimeStepInSecs;
    char strTimeStamp[20];
    memset(strTimeStamp, 0, 20);
    sprintf(strTimeStamp, "%llu", lTimeMiliSec);
    // Converting timestamp in Hex
    // timeStamp = new String(StringUtils.longToHex(lToUse));

    char *pOTP = generateOCRA(ocrasuite, key, counter, question, password, sessionInformation, strTimeStamp);
    // Cleanup stuffs
    delete [] question;
    return pOTP;
} // MEMORY LEAK REVIEW- OK (11-JAN-2010)

char *Device::generateSIGOTP_Event(char *astrData) {

    errorcode = 0;
    //OCRA-1:HOTP-SHA1-8:C-QAXX
    char *ocrasuite1 = "OCRA-1:HOTP-SHA1-";
    char ocrasuite[128];
    memset(ocrasuite, 0, 128);
    char strOtpLen[3];
    memset(strOtpLen, 0, 3);
    sprintf(strOtpLen, "%d", m_iOTPLength);
    char strChallangeLength[5];
    memset(strChallangeLength, 0, 5);
    sprintf(strChallangeLength, "%02ld", strlen(astrData));
    // Concatenate the above input to create the ocrasuite string
    strcpy(ocrasuite, ocrasuite1);
    strcat(ocrasuite, strOtpLen);
    strcat(ocrasuite, ":C-QA");
    strcat(ocrasuite, strChallangeLength);
    char *question = new char [ strlen(astrData) + 1];
    memset(question, 0, strlen(astrData) + 1);
    strcpy(question, astrData);
    BYTE *key = m_abRawSecret;
    char *password = NULL;
    // Not used
    char *sessionInformation = NULL;
    // Not used
    char *timestamp = NULL;
    // Not used
    char strCounter[128];
    memset(strCounter, 0, 128);
    sprintf(strCounter, "%ld", m_lEventCounter);
    char *pOTP = generateOCRA(ocrasuite, key, strCounter, question, password, sessionInformation, timestamp);
    m_lEventCounter++;

    // Cleanup Stuff
    delete [] question;
    return pOTP;
}

char *Device::generateOCRA(
        char* ocrasuite, // OCRA Suite: OCRA-1:HMAC-SHA1-6:C:QN08:P:S:T [ where C, P, S is not being used ]
        BYTE *key, // KEY
        char *counter,
        char *question, // 00000000-[ padded with zerosss ]
        char *password,
        char *session,
        char *timestamp) {

    errorcode = 0;
    /*
    int index = 0;
    printf("\n------- ocrasuite: [%s]\n", ocrasuite);
    printf("------- key: \n");
    for (index = 0; index < 20; index++) {
        printf("%x", key[index]);
    }
    printf("\n");
    if (counter != NULL) {
        printf("------- counter: [%s]\n", counter);
    }
    if (question != NULL) {
        printf("------- question: [%s]\n", question);
    }
    if (timestamp != NULL) {
        printf("------- timestamp (LONG): [%s]\n", timestamp);
    }
     */

    int iOcraOTPlen = m_iOTPLength;
    int iCounterLength = 0;
    int iQuestionLength = 0;
    int iPwdLength = 0;
    int iSessionLength = 0;
    int iTimeStampLength = 0;
    int iOcraLength = strlen(ocrasuite);
    int i = 0;
    int iTotalLength = 0;
    char str[3];
    if (counter != NULL)
        iCounterLength = 8;
    // Question
    if (question != NULL)
        iQuestionLength = 128;
    // Check for password.
    if (password != NULL) {
        // Parse the ocrasuite to get the password hash algorithm
        iPwdLength = 20;
    }
    // Check for session
    if (session != NULL) {
        // Read session info
        iSessionLength = strlen(session);
    }
    // Check for timestamp
    if (timestamp != NULL) {
        // get the timestamp length
        iTimeStampLength = 8;
    }
    BYTE *msg = new BYTE [ iOcraLength + iCounterLength + iQuestionLength + iPwdLength + iSessionLength + iTimeStampLength + 1];
    memset(msg, 0, iOcraLength + iCounterLength + iQuestionLength + iPwdLength + iSessionLength + iTimeStampLength + 1);
    iTotalLength = iOcraLength + iCounterLength + iQuestionLength + iPwdLength + iSessionLength + iTimeStampLength + 1;
    // Put the ocrasuite into the final msg buffer
    for (i = 0; i < strlen(ocrasuite); i++) {
        BYTE x = (BYTE) ocrasuite[i];
        msg[i] = x;
    }
    if (iCounterLength > 0) {
        BYTE *bytecounter = new BYTE[8];
        BYTE *msg1 = new BYTE[8];
        long tt = strtoul(counter, NULL, 0);
        long ntt = to_network_byte_order(tt);
        Int32ToUInt8Arr(ntt, msg1);
        //Int32ToUInt8Arr(tt, msg1);
        bytecounter[0] = 0;
        bytecounter[1] = 0;
        bytecounter[2] = 0;
        bytecounter[3] = 0;
        bytecounter[4] = msg1[0];
        bytecounter[5] = msg1[1];
        bytecounter[6] = msg1[2];
        bytecounter[7] = msg1[3];
        for (i = 0; i < iCounterLength; i++) {
            BYTE x = bytecounter[i];
            msg[i + 8 - iCounterLength + iOcraLength + 1] = x;
        }
        delete [] msg1;
        delete [] bytecounter;
    }

    if (iQuestionLength > 0) {
        //bArray = question.getBytes();
        for (int i = 0; i < 128 && i < strlen(question); i++) {
            BYTE x = question[i];
            msg[i + iOcraLength + 1 + iCounterLength] = x;
        }
    }

    /*
    if(iQuestionLength > 0)
    {
            for( i = 0; i < 128 && i < 8 ; i++) // question for us will be 8 byte length @ezmcom
            {
                    BYTE x = question[i];
                    msg[i + iOcraLength  + 1 + iCounterLength] = x;
            }
    }
     */

    if (iPwdLength > 0) {
        // if password is used it will be hashed using SHA1 - so length will be 20 bytes
        for (i = 0; i < 20 && i < iPwdLength; i++) {
            BYTE x = password[i];
            msg[i + iOcraLength + 1 + iCounterLength + iQuestionLength ] = x;
        }
    }

    // Fill session Information
    if (iSessionLength > 0) {
        for (i = 0; i < iSessionLength; i++) {
            BYTE x = session[i];
            msg[i + iOcraLength + 1 + iCounterLength + iQuestionLength + iPwdLength ] = x;
        }
    }
    if (iTimeStampLength > 0) {
        BYTE *bytecounter = new BYTE[8];
        BYTE *msg1 = new BYTE[8];
        long tt = strtoul(timestamp, NULL, 0);
        long ntt = to_network_byte_order(tt);
        Int32ToUInt8Arr(ntt, msg1);
        bytecounter[0] = 0;
        bytecounter[1] = 0;
        bytecounter[2] = 0;
        bytecounter[3] = 0;
        bytecounter[4] = msg1[0];
        bytecounter[5] = msg1[1];
        bytecounter[6] = msg1[2];
        bytecounter[7] = msg1[3];

        for (i = 0; i < iTimeStampLength; i++) {
            BYTE x = bytecounter[i];
            msg[i + 8 - iTimeStampLength + iOcraLength + 1 + iCounterLength + iQuestionLength + iPwdLength + iSessionLength] = x;
        }
        delete [] msg1;
        delete [] bytecounter;
    }
    // Msg read end
    CHMAC_SHA1 hmc_obj;
    BYTE *hash = new BYTE [20];
    if (key[0] == 0) {
        BYTE *key1 = new BYTE[19];
        for (int i = 0; i < 19; i++)
            key1[i] = key[i + 1];
        hmc_obj.HMAC_SHA1(msg, iTotalLength, key1, 19, hash);
        delete [] key1;
    } else {
        hmc_obj.HMAC_SHA1(msg, iTotalLength, key, 20, hash);
    }
    int offset = hash[OTP_DIGEST_LENGTH - 1] & 0xf;
    int binary =
            ((hash[offset] & 0x7f) << 24)
            | ((hash[offset + 1] & 0xff) << 16)
            | ((hash[offset + 2] & 0xff) << 8)
            | (hash[offset + 3] & 0xff);
    int otp = 0;
    if (m_iOTPLength == 10) {
        char tt[20];
        memset(tt, 0, 20);
        strcpy(tt, "10000000000");
        unsigned long long int tendigits = StringToU64(tt);
        otp = binary % tendigits;
    } else {
        otp = binary % DIGITS_POWER[m_iOTPLength];
    }
    // Allocate memory for result
    char *result = new char [ MAX_OTP_LEN + 1];
    memset(result, 0, MAX_OTP_LEN + 1);
    switch (iOcraOTPlen) {
        case 4:
            sprintf(result, "%04d", otp);
            break;
        case 6:
            sprintf(result, "%06d", otp);
            break;
        case 8:
            sprintf(result, "%08d", otp);
            break;
        case 10:
            sprintf(result, "%010d", otp);
            break;
        default:
            sprintf(result, "%d", otp);
    }
    // clean up
    delete [] hash;
    delete [] msg;
    return result;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////// NON-CLASS METHODS ///////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


int verifyActivationCode(char* wszActCode, int* errcode) {
    if (wszActCode == NULL || errcode == NULL) {
        *errcode = -1; // BAD PARAMETERS
        return -1;
    }

    std::string strActCode(wszActCode);
    int len = strActCode.length();
    if (len < 9 || len > 12) {
        return -1;
    }

    if (!Device::verifyCheckDigit(wszActCode)) {
        return -1;
    }
    return 0;
}

Device* activateToken(char* wszSNO, char* wszActCode, char* wszRegCodeSeed, int* errcode) {
    if (wszSNO == NULL || wszActCode == NULL || wszRegCodeSeed == NULL || errcode == NULL) {
        *errcode = -1; // BAD PARAMETERS
        return NULL;
    }

    Device* d = new Device(wszSNO, wszActCode, wszRegCodeSeed, V2_MODE);
    return d;
}

int pbkd2_generateKey(char *password, size_t Plen, const char *salt, size_t Slen,
        unsigned int c, char *derivedKey, size_t dkLen) {
    unsigned int hLen = 20;
    char U[20], T[20];
    unsigned int u, l, r, i, k;

    size_t tmplen = Slen + 4;
    char* tmp = new char[tmplen];

    if (!c || !dkLen || dkLen > 4294967295U)
        return 0;

    l = ((dkLen - 1) / hLen) + 1;
    r = dkLen - (l - 1) * hLen;

    //if (!(tmp = (char *) malloc (tmplen)))
    //  return 0;

    memcpy(tmp, salt, Slen);

    for (i = 1; i <= l; i++) {
        memset(T, 0, hLen);

        for (u = 1; u <= c; u++) {
            if (u == 1) {
                tmp[Slen + 0] = (i & 0xff000000) >> 24;
                tmp[Slen + 1] = (i & 0x00ff0000) >> 16;
                tmp[Slen + 2] = (i & 0x0000ff00) >> 8;
                tmp[Slen + 3] = (i & 0x000000ff) >> 0;

                CHMAC_SHA1 hmc_obj;
                //hmc_obj.HMAC_SHA1 ((BYTE*)password, Plen, (BYTE*)tmp, tmplen, (BYTE*)(&U));
                hmc_obj.HMAC_SHA1((BYTE*) tmp, tmplen, (BYTE*) password, Plen, (BYTE*) (&U));
            } else {
                CHMAC_SHA1 hmc_obj;
                //hmc_obj.HMAC_SHA1 ((BYTE*)password, Plen, (BYTE*)(&U), hLen, (BYTE*)(&U));
                hmc_obj.HMAC_SHA1((BYTE*) (&U), hLen, (BYTE*) password, Plen, (BYTE*) (&U));
            }

            for (k = 0; k < hLen; k++)
                T[k] ^= U[k];
        }

        memcpy(derivedKey + (i - 1) * hLen, T, i == l ? r : hLen);
    }

    free(tmp);
    return 1;
}

unsigned long randomize() {

    struct timeb tp;
    ftime(&tp);
    //printf("time = %ld.%d\n", tp.time, tp.millitm);
    char szTimeStampMiliSecond[20];
    char szTimeStampms[5];
    memset(szTimeStampMiliSecond, 0, 20);
    memset(szTimeStampms, 0, 5);
    sprintf(szTimeStampMiliSecond, "%ld", tp.time);

    // TOTP Oscillation fix
    sprintf(szTimeStampms, "%03d", tp.millitm);
    strcat(szTimeStampMiliSecond, szTimeStampms);

    //printf("\n Time in miliseconds - %s \n", szTimeStampMiliSecond);
    unsigned long long int u64Result = 0;

    char* sz = szTimeStampMiliSecond;

    while (*sz != '\0') {
        u64Result *= 10;
        u64Result += *sz - '0';
        sz++;
    }

    return u64Result;
}

void Device::resetInitialCounters() {
    m_lInitialEventCounter = 0;
    m_lEventCounter = 0;
}

// New API added for V1 Token compatibility

Device* activateV1Token(char* wszSNO, char* wszActCode, char* wszCheckCode, int* errcode) {
    if (wszSNO == NULL || wszActCode == NULL || wszCheckCode == NULL || errcode == NULL) {
        *errcode = -1; // BAD PARAMETERS
        return NULL;
    }

    Device* d = new Device(wszSNO, wszActCode, wszCheckCode, V1_MODE);
    if (d->getErrorCode() == BAD_PARAMETER) {
        return NULL;
    }
    return d;
}

char* Device::getLockCode(char* strUnlockChallenge) {
    // 1. Force mode as event counter
    // 2. Initial counter = 0
    long lInitialEventCounter = m_lInitialEventCounter;
    long lEventCounter = m_lEventCounter;

    m_lInitialEventCounter = 0;
    m_lEventCounter = 0;

    char* lockCode = generateCROTP_Event(strUnlockChallenge);


    m_lInitialEventCounter = lInitialEventCounter;
    m_lEventCounter = lEventCounter;
    return lockCode;
}

Device* Device::unlockDevice(unsigned char* buf, char* unlockCode) {
    unsigned long int lDateCreated = 0;
    long lLastUsed = 0;
    int iTokenType;
    OtpAlgo nOtpAlgo;
    int iSHAType;
    int iOTPLength;
    int iTimeStepInSecs;
    long lInitialEventCounter = 0;
    long lEventCounter = 0;
    char strSerialNumber[20];
    char strRegistrationCode[20];
    char strHexRND[16];
    unsigned char abRawSecret[320];
    char strVersion[16];
    bool bLocked;
    bool bPBKD;
    bool bPinType;
    int iRCDisplay;
    unsigned char pinHash[20];
    char strActivationCode[20];
    char abB64EncPIN[256];



    int lenSoFar = 0;
    memcpy(&lDateCreated, buf, sizeof (lDateCreated));
    lenSoFar += sizeof (lDateCreated);
    memcpy(&lLastUsed, buf + lenSoFar, sizeof (lLastUsed));
    lenSoFar += sizeof (lLastUsed);
    memcpy(&iTokenType, buf + lenSoFar, sizeof (iTokenType));
    lenSoFar += sizeof (iTokenType);
    memcpy(&nOtpAlgo, buf + lenSoFar, sizeof (nOtpAlgo));
    lenSoFar += sizeof (nOtpAlgo);
    memcpy(&iSHAType, buf + lenSoFar, sizeof (iSHAType));
    lenSoFar += sizeof (iSHAType);
    memcpy(&iOTPLength, buf + lenSoFar, sizeof (iOTPLength));
    lenSoFar += sizeof (iOTPLength);
    memcpy(&iTimeStepInSecs, buf + lenSoFar, sizeof (iTimeStepInSecs));
    lenSoFar += sizeof (iTimeStepInSecs);
    memcpy(&lInitialEventCounter, buf + lenSoFar, sizeof (lInitialEventCounter));
    lenSoFar += sizeof (lInitialEventCounter);
    memcpy(&lEventCounter, buf + lenSoFar, sizeof (lEventCounter));
    lenSoFar += sizeof (lEventCounter);
    memcpy(strSerialNumber, buf + lenSoFar, sizeof (strSerialNumber));
    lenSoFar += sizeof (strSerialNumber);
    memcpy(strRegistrationCode, buf + lenSoFar, sizeof (strRegistrationCode));
    lenSoFar += sizeof (strRegistrationCode);
    memcpy(strHexRND, buf + lenSoFar, sizeof (strHexRND));
    lenSoFar += sizeof (strHexRND);
    memcpy(abRawSecret, buf + lenSoFar, sizeof (abRawSecret));
    lenSoFar += sizeof (abRawSecret);
    memcpy(strVersion, buf + lenSoFar, sizeof (strVersion));
    lenSoFar += sizeof (strVersion);
    
    //----- START: ADDED 15-APR-2015 by Musa to handle incorrect resurrct on 64 bit devices -------//
    size_t verLen = strlen(strVersion);
    if( (verLen < 5) || (iOTPLength != 6 && iOTPLength != 8 && iOTPLength != 10) || strSerialNumber[0] == '\0' || strcmp(strSerialNumber,"")==0 || (strlen(strSerialNumber) != 12 && strlen(strSerialNumber) != 16) ||
       (iTimeStepInSecs != 30 && iTimeStepInSecs != 60))
    {
        //printf("Resurrect:: empty sn restart parsing with 4 bytes.\n");
        lenSoFar = 0;
        memcpy(&lDateCreated, buf, 4);//sizeof (lDateCreated));
        lenSoFar += 4;//sizeof (lDateCreated);
        memcpy(&lLastUsed, buf + lenSoFar, 4);//sizeof (lLastUsed));
        lenSoFar += 4;//sizeof (lLastUsed);
        memcpy(&iTokenType, buf + lenSoFar, sizeof (iTokenType));
        lenSoFar += sizeof (iTokenType);
        memcpy(&nOtpAlgo, buf + lenSoFar, sizeof (nOtpAlgo));
        lenSoFar += sizeof (nOtpAlgo);
        memcpy(&iSHAType, buf + lenSoFar, sizeof (iSHAType));
        lenSoFar += sizeof (iSHAType);
        memcpy(&iOTPLength, buf + lenSoFar, sizeof (iOTPLength));
        lenSoFar += sizeof (iOTPLength);
        memcpy(&iTimeStepInSecs, buf + lenSoFar, sizeof (iTimeStepInSecs));
        lenSoFar += sizeof (iTimeStepInSecs);
        memcpy(&lInitialEventCounter, buf + lenSoFar, 4);//sizeof (lInitialEventCounter));
        lenSoFar += 4;//sizeof (lInitialEventCounter);
        memcpy(&lEventCounter, buf + lenSoFar, 4);//sizeof (lEventCounter));
        lenSoFar += 4;//sizeof (lEventCounter);
        memcpy(strSerialNumber, buf + lenSoFar, sizeof (strSerialNumber));
        lenSoFar += sizeof (strSerialNumber);
        memcpy(strRegistrationCode, buf + lenSoFar, sizeof (strRegistrationCode));
        lenSoFar += sizeof (strRegistrationCode);
        memcpy(strHexRND, buf + lenSoFar, sizeof (strHexRND));
        lenSoFar += sizeof (strHexRND);
        memcpy(abRawSecret, buf + lenSoFar, sizeof (abRawSecret));
        lenSoFar += sizeof (abRawSecret);
        memcpy(strVersion, buf + lenSoFar, sizeof (strVersion));
        lenSoFar += sizeof (strVersion);
        //printf("Resurrect::  version [%s], %lu\n",strVersion,strlen(strVersion));
    }
    //----- END: ADDED 15-APR-2015 by Musa to handle incorrect resurrct on 64 bit devices -------//

    memcpy(&bLocked, buf + lenSoFar, sizeof (bLocked));
    lenSoFar += sizeof (bLocked);
    memcpy(&bPBKD, buf + lenSoFar, sizeof (bPBKD));
    lenSoFar += sizeof (bPBKD);
    memcpy(&bPinType, buf + lenSoFar, sizeof (bPinType));
    lenSoFar += sizeof (bPinType);
    memcpy(&iRCDisplay, buf + lenSoFar, sizeof (iRCDisplay));
    lenSoFar += sizeof (iRCDisplay);
    memcpy(pinHash, buf + lenSoFar, sizeof (pinHash));
    lenSoFar += sizeof (pinHash);
    memcpy(strActivationCode, buf + lenSoFar, sizeof (strActivationCode));
    lenSoFar += sizeof (strActivationCode);
    memcpy(abB64EncPIN, buf + lenSoFar, sizeof (abB64EncPIN));
    lenSoFar += sizeof (abB64EncPIN);


    Device* d = new Device(lDateCreated, lLastUsed, iTokenType, nOtpAlgo, iSHAType,
            iOTPLength, iTimeStepInSecs, lInitialEventCounter, lEventCounter, strSerialNumber,
            strRegistrationCode, strHexRND, abRawSecret, strVersion, bLocked,
            bPBKD, bPinType, iRCDisplay, pinHash, strActivationCode, abB64EncPIN);
    int ret = d->unlockTheDevice(unlockCode);
    if (ret == 0) {
        return d;
    } else {
        return NULL;
    }
}

int Device::unlockTheDevice(char* unlockcode) {

    errorcode = 0;
    if (!m_bLocked) {
        errorcode = INVALID_STATE;
        return errorcode;
    }

    if (unlockcode == NULL) {
        errorcode = BAD_PARAMETER;
        return errorcode;
    }

//    char szSecLog[24];
//    memset(szSecLog, 0, sizeof (szSecLog));
//    ofstream a_file("/opt/dev/logs/cplusdevice.log", ios::app);
//    a_file << "-- unlockTheDevice(unlockcode) --\n";
//    a_file << "UNLOCK CODE: " << unlockcode << "\n";

    // NOW lets decrypt the PIN
    // 1. BASE64 DECODE THE ENC PIN
    std::string encodedPIN((const char*) m_abB64EncPIN);
    std::string decodedPIN = base64_decode(encodedPIN);
    const char* encPIN = decodedPIN.c_str();
//    a_file << "BASE64 ENC PIN: " << m_abB64EncPIN << "\n";

    // 2. Use this lock code as the encryption key
    char *pinkey = new char [24];
    memset(pinkey, 0, 24);
    //printf("\n Password - %s \n", password);
    //printf("\n strSerialNumber: [%s]",strSerialNumber);
    //printf("\n m_iIterationCount: [%d]",m_iIterationCount);
    //printf("\n GOING FOR PBKDF2\n");
    //@ 24 bytes is hardcoded. - Length of Encryption key. 50 is hardcoded as the ITERATION COUNT for PBKDF2
    pbkd2_generateKey(unlockcode, strlen(unlockcode), m_strSerialNumber, strlen(m_strSerialNumber), 50, pinkey, 24);
    //printf("\n SECRET Generated using PBKDF2\n");

//    memset(szSecLog, 0, sizeof (szSecLog));
//    a_file << "PARAMS FOR LOCK KEY - PARAM(1): ";
//    a_file << unlockcode;
//    a_file << "\n";
//    a_file << "PARAMS FOR LOCK KEY - PARAM(2): ";
//    a_file << m_strSerialNumber;
//    a_file << "\n";
//
//    a_file << "LOCK PIN (24 bytes): ";
//    for (int j = 0; j < 24; j++) {
//        sprintf(szSecLog, "[%02x]", pinkey[j]);
//        a_file << szSecLog;
//    }
//    a_file << "\n";


    // 3. DECRYPT THE PIN
    char abDecPIN[256];
    CRijndael decryptor;
    decryptor.MakeKey((const char*) pinkey);
    decryptor.Decrypt(encPIN, (char*) abDecPIN, decodedPIN.length());


    // 4. Now lets DECRYPT THE SECRET
    // 4.1. BASE64 DECODE THE SECRET
    std::string encodedSecret((const char*) m_abRawSecret);
    std::string decodedSecret = base64_decode(encodedSecret);
    const char* encSecret = decodedSecret.c_str();


    CRijndael decryptor1;
    decryptor1.MakeKey((const char*) abDecPIN);
    decryptor1.Decrypt(encSecret, (char*) m_abRawSecret, decodedSecret.length());

//    memset(szSecLog, 0, sizeof (szSecLog));
//    a_file << "UNLOCKED SECRET: ";
//    for (int j = 0; j < 20; j++) {
//        sprintf(szSecLog, "[%02x]", m_abRawSecret[j]);
//        a_file << szSecLog;
//    }
//    a_file << "\n";
//    a_file.close();

    if (pinkey)delete[] pinkey;


    char strTemp[20];
    char strUnlockChallenge[10];
    memset(strTemp, 0, sizeof (strTemp));
    memset(strUnlockChallenge, 0, sizeof (strUnlockChallenge));
    sprintf(strTemp, "%lu", m_lDateCreated);
    memcpy(strUnlockChallenge, strTemp, 8);
    char* strLockCode = getLockCode(strUnlockChallenge);
    // Lets check if it was a correct unlock code or not
    if (0 != memcmp(strLockCode, unlockcode, strlen(strLockCode))) {
        return -1;
    }

    m_lDateCreated = getCurrentTimeinMiliSecond() / 1000;
    m_bLocked = false;
    return 0;
}

char* Device::getUnlockChallenge() {
    char strTemp[20];
    char* strUnlockChallenge = new char[20];
    memset(strTemp, 0, sizeof (strTemp));
    memset(strUnlockChallenge, 0, 20);
    sprintf(strTemp, "%lu", m_lDateCreated);
    memcpy(strUnlockChallenge, strTemp, 8);
    return strUnlockChallenge;
}

int Device::size() {
    int len = sizeof (m_lDateCreated) +
            sizeof (m_lLastUsed) +
            sizeof (m_iTokenType) +
            sizeof (m_nOtpAlgo) +
            sizeof (m_iSHAType) +
            sizeof (m_iOTPLength) +
            sizeof (m_iTimeStepInSecs) +
            sizeof (m_lInitialEventCounter) +
            sizeof (m_lEventCounter) +
            sizeof (m_strSerialNumber) +
            sizeof (m_strRegistrationCode) +
            sizeof (m_strHexRND) +
            sizeof (m_abRawSecret) +
            sizeof (m_strVersion) +
            sizeof (m_bLocked) +
            sizeof (m_bPBKD) +
            sizeof (m_bPinType) +
            sizeof (m_iRCdisplay) +
            sizeof (m_pinHash) +
            sizeof (m_strActivationCode) +
            sizeof (m_abB64EncPIN);
    return len;
}

// 23-APRIL-2013: New API to get VERSION from RAW stream of Device buffer

char * Device::getVersion(unsigned char* buf) {
    int SIZE_OF_VERSION_BUF = 16;
    long lDateCreated = 0;
    long lLastUsed = 0;
    int iTokenType;
    OtpAlgo nOtpAlgo;
    int iSHAType;
    int iOTPLength;
    int iTimeStepInSecs;
    long lInitialEventCounter = 0;
    long lEventCounter = 0;
    char strSerialNumber[20];
    char strRegistrationCode[20];
    char strHexRND[16];
    unsigned char abRawSecret[320];
    char* pstrVersion = new char[SIZE_OF_VERSION_BUF];
    bool bLocked;
    bool bPBKD;
    bool bPinType;
    int iRCDisplay;
    unsigned char pinHash[20];
    char strActivationCode[20];
    char abB64EncPIN[256];


    memset(pstrVersion, 0, SIZE_OF_VERSION_BUF);

    int lenSoFar = 0;
    memcpy(&lDateCreated, buf, sizeof (lDateCreated));
    lenSoFar += sizeof (lDateCreated);
    memcpy(&lLastUsed, buf + lenSoFar, sizeof (lLastUsed));
    lenSoFar += sizeof (lLastUsed);
    memcpy(&iTokenType, buf + lenSoFar, sizeof (iTokenType));
    lenSoFar += sizeof (iTokenType);
    memcpy(&nOtpAlgo, buf + lenSoFar, sizeof (nOtpAlgo));
    lenSoFar += sizeof (nOtpAlgo);
    memcpy(&iSHAType, buf + lenSoFar, sizeof (iSHAType));
    lenSoFar += sizeof (iSHAType);
    memcpy(&iOTPLength, buf + lenSoFar, sizeof (iOTPLength));
    lenSoFar += sizeof (iOTPLength);
    memcpy(&iTimeStepInSecs, buf + lenSoFar, sizeof (iTimeStepInSecs));
    lenSoFar += sizeof (iTimeStepInSecs);
    memcpy(&lInitialEventCounter, buf + lenSoFar, sizeof (lInitialEventCounter));
    lenSoFar += sizeof (lInitialEventCounter);
    memcpy(&lEventCounter, buf + lenSoFar, sizeof (lEventCounter));
    lenSoFar += sizeof (lEventCounter);
    memcpy(strSerialNumber, buf + lenSoFar, sizeof (strSerialNumber));
    lenSoFar += sizeof (strSerialNumber);
    memcpy(strRegistrationCode, buf + lenSoFar, sizeof (strRegistrationCode));
    lenSoFar += sizeof (strRegistrationCode);
    memcpy(strHexRND, buf + lenSoFar, sizeof (strHexRND));
    lenSoFar += sizeof (strHexRND);
    memcpy(abRawSecret, buf + lenSoFar, sizeof (abRawSecret));
    lenSoFar += sizeof (abRawSecret);
    memcpy(pstrVersion, buf + lenSoFar, SIZE_OF_VERSION_BUF);
    lenSoFar += SIZE_OF_VERSION_BUF;
    
    //----- START: ADDED 15-APR-2015 by Musa to handle incorrect resurrct on 64 bit devices -------//
    size_t verLen = strlen(pstrVersion);
    
    if( (verLen < 5) || (iOTPLength != 6 && iOTPLength != 8 && iOTPLength != 10) || strSerialNumber[0] == '\0' || strcmp(strSerialNumber,"")==0 || (strlen(strSerialNumber) != 12 && strlen(strSerialNumber) != 16) ||
       (iTimeStepInSecs != 30 && iTimeStepInSecs != 60))
    {
        //printf("Resurrect:: empty sn restart parsing with 4 bytes.\n");
        lenSoFar = 0;
        memcpy(&lDateCreated, buf, 4);//sizeof (lDateCreated));
        lenSoFar += 4;//sizeof (lDateCreated);
        memcpy(&lLastUsed, buf + lenSoFar, 4);//sizeof (lLastUsed));
        lenSoFar += 4;//sizeof (lLastUsed);
        memcpy(&iTokenType, buf + lenSoFar, sizeof (iTokenType));
        lenSoFar += sizeof (iTokenType);
        memcpy(&nOtpAlgo, buf + lenSoFar, sizeof (nOtpAlgo));
        lenSoFar += sizeof (nOtpAlgo);
        memcpy(&iSHAType, buf + lenSoFar, sizeof (iSHAType));
        lenSoFar += sizeof (iSHAType);
        memcpy(&iOTPLength, buf + lenSoFar, sizeof (iOTPLength));
        lenSoFar += sizeof (iOTPLength);
        memcpy(&iTimeStepInSecs, buf + lenSoFar, sizeof (iTimeStepInSecs));
        lenSoFar += sizeof (iTimeStepInSecs);
        memcpy(&lInitialEventCounter, buf + lenSoFar, 4);//sizeof (lInitialEventCounter));
        lenSoFar += 4;//sizeof (lInitialEventCounter);
        memcpy(&lEventCounter, buf + lenSoFar, 4);//sizeof (lEventCounter));
        lenSoFar += 4;//sizeof (lEventCounter);
        memcpy(strSerialNumber, buf + lenSoFar, sizeof (strSerialNumber));
        lenSoFar += sizeof (strSerialNumber);
        memcpy(strRegistrationCode, buf + lenSoFar, sizeof (strRegistrationCode));
        lenSoFar += sizeof (strRegistrationCode);
        memcpy(strHexRND, buf + lenSoFar, sizeof (strHexRND));
        lenSoFar += sizeof (strHexRND);
        memcpy(abRawSecret, buf + lenSoFar, sizeof (abRawSecret));
        lenSoFar += sizeof (abRawSecret);
        memcpy(pstrVersion, buf + lenSoFar, SIZE_OF_VERSION_BUF);
        lenSoFar += SIZE_OF_VERSION_BUF;
        //printf("Resurrect::  version [%s], %lu\n",pstrVersion,strlen(pstrVersion));
    }
    //----- END: ADDED 15-APR-2015 by Musa to handle incorrect resurrct on 64 bit devices -------//

    memcpy(&bLocked, buf + lenSoFar, sizeof (bLocked));
    lenSoFar += sizeof (bLocked);
    memcpy(&bPBKD, buf + lenSoFar, sizeof (bPBKD));
    lenSoFar += sizeof (bPBKD);
    memcpy(&bPinType, buf + lenSoFar, sizeof (bPinType));
    lenSoFar += sizeof (bPinType);
    memcpy(&iRCDisplay, buf + lenSoFar, sizeof (iRCDisplay));
    lenSoFar += sizeof (iRCDisplay);
    memcpy(pinHash, buf + lenSoFar, sizeof (pinHash));
    lenSoFar += sizeof (pinHash);
    memcpy(strActivationCode, buf + lenSoFar, sizeof (strActivationCode));
    lenSoFar += sizeof (strActivationCode);
    memcpy(abB64EncPIN, buf + lenSoFar, sizeof (abB64EncPIN));
    lenSoFar += sizeof (abB64EncPIN);

//    char szSecLog[24];
//    memset(szSecLog, 0, sizeof (szSecLog));
//    ofstream a_file("/opt/dev/logs/cplusdevice.log", ios::app);
//    a_file << "-- getVersion(char* devbuf) --\n";
//    a_file << "(RESURRECTED) DEVICE OBJECT VERSION: ";
//    sprintf(szSecLog, "[%s]", pstrVersion);
//    a_file << szSecLog;
//    a_file << "\n";
//    a_file.close();

    return pstrVersion;
}


int hexCtoI(char n) {
    if (n >= '0' && n <= '9')
        return (n - '0');
    else

        if (n >= 'A' && n <= 'F')
        return (n - 'A' + 10);
    else
        return 0;
}

char* getEffectiveSN(const char* NEWSN) {
    // This is the 16-Digit SN. Convert into effective 12-Digit SN
    if (NEWSN == NULL || strlen(NEWSN) != 16) {
        return NULL;
    }
    char* SN = new char[20];
    memset(SN, 0, 20);
    // Copy the 11 Digits after the P1P2, V1V2 prefix
    memcpy(SN, &NEWSN[4], 11);
    // Compute the Check-Digit
    char number[3];
    int iDigit = 0;
    int iSum = 0;
    int iCheckSum = 0;
    memset(number, 0, sizeof (number));
    int j = 0;
    for (int i = 0; i < 11; i++) {
        j = i + 1;
        number[0] = SN[i];
        number[1] = '\0';
        iDigit = atoi(number);
        if (j % 2 == 0) {
            //! Even Position
            iSum += iDigit * 1;
        } else {
            iSum += iDigit * 3;
        }
    }
    iCheckSum = (10 - (iSum % 10)) % 10;
    sprintf(number, "%d", iCheckSum);
    strcat(SN, number);
    //printf("\n-- (NEW) SERIAL NUMBER: %s", NEWSN);
    //printf("\n-- EFFECTIVE SERIAL NUMBER: %s", SN);
    return SN;
}

char* generateNewSerialNo(int iTokenType, char* inRndSeed, int inRndSeedLen, char* P1P2, char* V1V2, int* errcode) {
    if (inRndSeed == NULL || errcode == NULL) {
        *errcode = -2;
        return NULL;
    }
    if (inRndSeedLen > 512) {
        *errcode = -2;
        return NULL;
    }
    
    char *pTokenSerialNumber = NULL;
    char seg1Str[32];
    char strMobileType[5];
    char seg2Str[32];
    int iSum = 0, iDigit = 0, iCheckSum = 0;
    char number[3];
    memset(seg1Str, 0, 32);
    memset(seg2Str, 0, 32);
    if (iTokenType < 0)
        return pTokenSerialNumber;
    if (iTokenType > 99)
        return pTokenSerialNumber;
    // Append this to the mobile type
    memset(strMobileType, 0, 5);
    sprintf(strMobileType, "%02d", iTokenType);
    
    // Only 9 digits
    unsigned char strHash[32];
    memset(strHash, 0, sizeof (strHash));
    CSHA1 sha1;
    sha1.Reset();
    sha1.Update((unsigned char*) inRndSeed, inRndSeedLen);
    sha1.Final();
    sha1.GetHash(strHash);
    char strPrefix[64];
    memset(strPrefix, 0, sizeof (strPrefix));
    unsigned long num1 = 0;
    memcpy(&num1, &strHash[0], sizeof (num1));
    unsigned long num2 = 0;
    memcpy(&num2, &strHash[15], sizeof (num2));
    sprintf(strPrefix, "%s%lu%lu", strMobileType, num1, num2);
    //printf("-- SN strPrefix: %s\n", strPrefix);
    
    //    ofstream a_file("/opt/dev/logs/cplusdevice.log", ios::app);
    //    a_file << "-- generateSerialNo() --\n";
    //    a_file << "m_strEventsTimeSeed: [" << inRndSeed << "] \n";
    //    a_file << "RND seed length: [" << inRndSeedLen << "] \n";
    //    a_file << "strPrefix: " << strPrefix << "\n";
    //    a_file.close();
    
    memcpy(seg1Str, strPrefix, RANDOMNOLENGTH + 2);
    //printf("-- seg1Str: %s\n", seg1Str);
    if (strlen(seg1Str) == 10) {
        // Append 0 ahead
        sprintf(seg2Str, "%d", 0);
        strcat(seg2Str, seg1Str);
    } else {
        strcpy(seg2Str, seg1Str);
    }
    
    
    char xDigits[9];
    memset(xDigits, 0, sizeof (xDigits));
    
    // Convert P1 HEX to NUM
    char P1[3];
    iDigit = hexCtoI(P1P2[0]);
    sprintf(P1, "%d", iDigit);
    strcat(xDigits, P1);
    
    // Convert P2 HEX to NUM
    char P2[3];
    iDigit = hexCtoI(P1P2[1]);
    sprintf(P2, "%d", iDigit);
    strcat(xDigits, P2);
    
    // Convert V1 HEX to NUM
    char V1[3];
    //typo Change on 29th October 2013// using V1 instead of V1V2
    iDigit = hexCtoI(V1V2[0]);
    sprintf(V1, "%d", iDigit);
    strcat(xDigits, V1);
    
    // Convert V2 HEX to NUM
    char V2[3];
    iDigit = hexCtoI(V1V2[1]);
    sprintf(V2, "%d", iDigit);
    strcat(xDigits, V2);
    
    char* SNEXPANDED = new char[strlen(seg2Str) + sizeof (xDigits)];
    memset(SNEXPANDED, 0, strlen(seg2Str) + sizeof (xDigits));
    strcpy(SNEXPANDED, xDigits);
    strcat(SNEXPANDED, seg2Str);
    memset(number, 0, sizeof (number));
    int j = 0;
    for (int i = 0; i < strlen(SNEXPANDED); i++) {
        j = i + 1;
        number[0] = SNEXPANDED[i];
        number[1] = '\0';
        iDigit = atoi(number);
        if (j % 2 == 0) {
            //! Even Position
            iSum += iDigit * 1;
            //NSLog(@"EVEN addition => %d", iSum);
        } else {
            iSum += iDigit * 3;
            //NSLog(@"ODD addition => %d", iSum);
        }
    }
    delete SNEXPANDED;
    iCheckSum = (10 - (iSum % 10)) % 10;
    //NSLog(@"Modulos - 10  => %d", iCheckSum);
    memset(number, 0, 3);
    sprintf(number, "%d", iCheckSum);
    
    int newlen = 0;
    newlen += strlen(P1P2);
    newlen += strlen(V1V2);
    newlen += strlen(seg2Str);
    newlen += strlen(number);
    newlen += 1;
    pTokenSerialNumber = new char [newlen];
    memset(pTokenSerialNumber, 0, newlen);
    sprintf(pTokenSerialNumber, "%s%s%s%s", P1P2, V1V2, seg2Str, number);
    //printf("\n-- (NEW) SERIAL NUMBER: %s", pTokenSerialNumber);
    return pTokenSerialNumber;
}




char* generateSerialNo(int iTokenType, char* inRndSeed, int inRndSeedLen, int* errcode) {
    if (inRndSeed == NULL || errcode == NULL) {
        *errcode = -2;
        return NULL;
    }
    if (inRndSeedLen > 512) {
        *errcode = -2;
        return NULL;
    }
    
    char *pTokenSerialNumber = NULL;
    char seg1Str[32];
    char strMobileType[5];
    char seg2Str[32];
    int iSum = 0, iDigit = 0, iCheckSum = 0;
    char number[3];
    memset(seg1Str, 0, 32);
    memset(seg2Str, 0, 32);
    if (iTokenType < 0)
        return pTokenSerialNumber;
    if (iTokenType > 99)
        return pTokenSerialNumber;
    // Append this to the mobile type
    memset(strMobileType, 0, 5);
    sprintf(strMobileType, "%02d", iTokenType);
    
    // Only 9 digits
    unsigned char strHash[32];
    memset(strHash, 0, sizeof (strHash));
    CSHA1 sha1;
    sha1.Reset();
    sha1.Update((unsigned char*) inRndSeed, inRndSeedLen);
    sha1.Final();
    sha1.GetHash(strHash);
    char strPrefix[64];
    memset(strPrefix, 0, sizeof (strPrefix));
    unsigned long num1 = 0;
    memcpy(&num1, &strHash[0], sizeof (num1));
    unsigned long num2 = 0;
    memcpy(&num2, &strHash[15], sizeof (num2));
    sprintf(strPrefix, "%s%lu%lu", strMobileType, num1, num2);
    //printf("-- SN strPrefix: %s\n", strPrefix);
    
    //    ofstream a_file("/opt/dev/logs/cplusdevice.log", ios::app);
    //    a_file << "-- generateSerialNo() --\n";
    //    a_file << "m_strEventsTimeSeed: [" << inRndSeed << "] \n";
    //    a_file << "RND seed length: [" << inRndSeedLen << "] \n";
    //    a_file << "strPrefix: " << strPrefix << "\n";
    //    a_file.close();
    
    memcpy(seg1Str, strPrefix, RANDOMNOLENGTH + 2);
    //printf("-- seg1Str: %s\n", seg1Str);
    if (strlen(seg1Str) == 10) {
        // Append 0 ahead
        sprintf(seg2Str, "%d", 0);
        strcat(seg2Str, seg1Str);
    } else {
        strcpy(seg2Str, seg1Str);
    }
    memset(number, 0, 3);
    int j = 0;
    for (int i = 0; i < strlen(seg2Str); i++) {
        j = i + 1;
        number[0] = seg2Str[i];
        number[1] = '\0';
        iDigit = atoi(number);
        if (j % 2 == 0) {
            // Even Position
            iSum += iDigit * 1;
        } else {
            iSum += iDigit * 3;
        }
    }
    iCheckSum = (10 - (iSum % 10)) % 10;
    memset(number, 0, 3);
    sprintf(number, "%d", iCheckSum);
    //printf("\n-- iCheckSum: %s", number);
    strcat(seg2Str, number);
    //printf("\n-- seg2Str: %s", seg2Str);
    pTokenSerialNumber = new char [strlen(seg2Str) + 1];
    memset(pTokenSerialNumber, 0, strlen(seg2Str) + 1);
    strcpy(pTokenSerialNumber, seg2Str);
    //printf("\n-- SERIAL NUMBER: %s", pTokenSerialNumber);
    return pTokenSerialNumber;
}

