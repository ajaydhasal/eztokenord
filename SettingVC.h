//
//  SettingVC.h
//  EzToken
//
//  Created by Apple on 14/09/15.
//  Copyright (c) 2015 Ezmcom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharedData.h"
#import "Constant.h"
#import "universalDataStore.h"
#import "StoryboardID.h"
#import <CoreMotion/CoreMotion.h>
#import <LocalAuthentication/LocalAuthentication.h>


@interface SettingVC : UIViewController<UIAlertViewDelegate>{
    SharedData *sharedData ;
    UIAlertView *alertTouchId,*alertFaceRecogniton,*alertSecureAlignment,*alertSecureAlignmentChange;
    UIAlertView *erroralert,*enableErrorAlert,*disableErrorAlert;
    UIAlertView *touchIdsetupalert;
}
@property (retain, nonatomic) IBOutlet UISwitch *switchEnableDisable;
@property (retain, nonatomic) IBOutlet UISwitch *switchEnableDisableAlignment;
@property (retain, nonatomic) IBOutlet UIButton *btnGatewayURL;
@property (retain, nonatomic) IBOutlet UISwitch *switchEnableDisableFaceVoiceRec;

// These are viewfor change in setting

@property (retain, nonatomic) IBOutlet UIView *viewGetwayURL;
@property (retain, nonatomic) IBOutlet UIView *viewSecureAlignment;
@property (retain, nonatomic) IBOutlet UIView *viewFaceVoiceRec;
@property (retain, nonatomic) IBOutlet UIView *viewTouchID;
@property (retain, nonatomic) IBOutlet UIView *viewAbout;

@property (retain, nonatomic) IBOutlet UIButton *btnChangeSecureAlignment;
@property (retain, nonatomic) IBOutlet UIButton *btnDeleteVoiceProfile;
- (IBAction)btnChangeSecureAlignmentTapped:(id)sender;
- (IBAction)btnDeleteVoiceProfileTapped:(id)sender;

- (IBAction)btnSetGatewayTapped:(id)sender;
- (IBAction)btnAboutTapped:(id)sender;
- (IBAction)btnCancelTapped:(id)sender;
- (IBAction)swtichValueChanged:(id)sender;
- (IBAction)swithAlignmentValueChanged:(id)sender;
- (IBAction)switchFaceRecChanged:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *labelVersion;
@property (retain, nonatomic) IBOutlet UILabel *lableCopyRight;
@property (retain, nonatomic) IBOutlet UILabel *lablePushID;

- (void) enrollSuccess;
- (void) authenticateSuccess;

@end
