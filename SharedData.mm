//
//  SharedData.m
//  EZToken
//
//  Created by Harsha on 9/19/14.
//  Copyright (c) 2014 Ezmcom. All rights reserved.
//

#import "SharedData.h"
#import "Constant.h"
#import "NSMutableData-AES.h"
#import "SecKeyHelper.h"
#include "base64.h"

#include "DeviceManager.h"
#import <UIKit/UIDevice.h>
#import "AuthorizeViewController.h"
#import "AppDelegate.h"
#import "RegisterViewController.h"

@implementation SharedData
@synthesize dNum;
@synthesize dOut;
@synthesize deviceSN;
@synthesize deviceRC;
@synthesize dUse;
@synthesize deviceUC;
@synthesize devicePID;
@synthesize deviceUCR;
@synthesize isDeviceLocked;
@synthesize outPut;

@synthesize isDeviceExist;
@synthesize isOutOn;
@synthesize isRCDisplay;
@synthesize cashMode;
@synthesize pbkdMode;
@synthesize cashType;
@synthesize isOutSET;
@synthesize isActivated;

@synthesize iPutOutMode;
@synthesize makeUseType;

@synthesize startOTPCountTimeMillis;

@synthesize appStatusFilePath;
@synthesize dTiltValue;
@synthesize signatureParamString;
@synthesize jsonDictionary;
@synthesize isTiltEnable;
@synthesize latitude, longitude;
@synthesize isAutomationEnable;
@synthesize isSecuritySet, isTouchIDEnable, isTouchIDDevice;

static SharedData *_sharedDataInstance = nil;

+ (SharedData *)sharedData {
	if (_sharedDataInstance == nil) {
		_sharedDataInstance = [[self alloc] init];
	}
	return _sharedDataInstance;
}

- (void) setReturnCodeVal:(int)returnCodeVal
{
    self.returnCode = returnCodeVal;
}

- (void) setDeviceACString:(NSString *)deviceACString
{
    self.deviceRC = deviceACString;
}

- (id) init {
	if (self = [super init]) {
		// Initial process.
		[self initSharedData];
	}
	return self;
}

- (void) initSharedData
{
    //need to get the version but need to pass the buffer to it.
    //the buf consist of the secret file
    
	self.deviceUCR = nil;
	self.dOut = nil;
	self.deviceSN = nil;
	self.deviceRC = nil;
	self.dUse = nil;
	self.deviceUC = nil;
    self.devicePID = nil;
    self.outPut = ENTER_PIN_ATTEMPTS_MAX;
	self.isDeviceLocked = NO;
	self.isDeviceExist = NO;
	self.isOutOn = NO;
	self.isRCDisplay = NO;
	self.isOutSET = NO;
    self.isActivated = NO;
    self.isSecuritySet = NO;
    self.isLocationSet = YES;
    
    self.isTiltEnable = NO;
    self.isTiltChanged=NO;
    self.isSecureAligned=NO;
    self.isFaceRecognised=NO;
    self.flagForMatchAngle=NO;
    self.flagForDisableSecureAligment=NO;
    
	self.startOTPCountTimeMillis = 0;
    self.dTiltValue = nil;
    self.jsonDictionary = nil;
    self.latitude = 0.0;
    self.longitude = 0.0;
    self.isAutomationEnable = NULL;
    self.isTouchIDEnable = NO;
    self.isTouchIDDevice = [self isTouchIdDevice1];
    self.isAppOpenedFirstTime=YES;
    self.gatewayURL = MAIN_GATEWAY_URL_DEFAULT;
    
    
    [self checkList];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *docsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [docsDirectory stringByAppendingPathComponent:self.profileName];
	self.appStatusFilePath = [dataPath stringByAppendingPathComponent:APP_STATUS_FILE_NAME];
    
	// Set Device store file path
    if (self.profileName.length != 0) {
        NSString *deviceSecretFilePath = [dataPath stringByAppendingPathComponent:DEVICE_SECRET_FILE_NAME];
        [self setDeviceSecretFilePath:deviceSecretFilePath];
    }
    [self newloadAppStatus];

    //[self loadLocation:self.profileName];
    //NSLog(@"Attempts: %d \n fix %d", self.outPut, ENTER_PIN_ATTEMPTS_MAX);
}

- (NSString*) getMacAddress
{
    NSUUID *uuid = [[UIDevice currentDevice]identifierForVendor];
    NSString *deviceId = [uuid UUIDString];
    return deviceId;
}


- (NSString *) generateSerialNumber
{
	DeviceManager *deviceManager = sharedDeviceManager();
	char *serialNumberCString;
    if (SHOULD_USE_16_DIGITS_SN) {
        serialNumberCString = deviceManager->generateSerialNo16(ITERATIONFACTOR, [TOKEN_PLATFORM_CODE_12 UTF8String], [TOKEN_VERSION_CODE_12 UTF8String]);
    } else {
        serialNumberCString = deviceManager->generateSerialNo12(22);
    }
	self.deviceSN = [NSString stringWithCString:serialNumberCString encoding:NSUTF8StringEncoding];
    NSLog(@"SD -> generateSN -> PROFILENAME: %@",self.profileName);
     //NSLog(@"########################SD SAVE  %@ ", self.profileName);
	[self newsaveAppStatus];
    
	return deviceSN;
}

- (BOOL) createDevice:(NSString *)acString seed:(NSString *)seedString
{
	DeviceManager *deviceManager = sharedDeviceManager();
    [self generateSerialNumber];
    //NSLog(@"sn: %@  ac: %@",deviceSN, acString);
    int retVal = deviceManager->createDevice([acString UTF8String], [seedString UTF8String], [deviceSN UTF8String]);
    
	if (retVal == 0)
    {

        if(!HID_RCDISPLAY){
            if (deviceManager->m_registrationcode != NULL) {
                self.deviceRC = [NSString stringWithCString:deviceManager->m_registrationcode encoding:NSUTF8StringEncoding];
            } else {
                self.deviceRC = nil;
            }
        }
//        else {
//            self.deviceRC = [NSString stringWithCString:deviceManager->m_registrationcode encoding:NSUTF8StringEncoding];
//        }
		self.isOutOn = deviceManager->m_pinsupport;
		self.isRCDisplay = deviceManager->m_rcdisplay;
		self.cashMode = deviceManager->m_cashMode;
		self.pbkdMode = deviceManager->m_pbkdmode;
		self.cashType = deviceManager->m_cashType;
        NSLog(@"SD -> CREATEDEVICE -> PROFILENAME: %@",self.profileName);
        //NSLog(@"########################SD SAVE  %d", self.isAutomationEnable);
        self.isAutomationEnable = NO;
        [self newsaveAppStatus];
		return YES;
	}
	return NO;
}

- (void) createDirectory:(NSString*)uidgname
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:uidgname];
    //NSLog(@" text: %@ \n ----- FlePath %@", challengeParam,dataPath);
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
        //NSLog(@"createDirectory ----- Yes File Does Not Exist %@", dataPath);
        
        NSError* error;
        if([[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]);
        
        else
        {
            NSLog(@"[%@] ERROR: attempting to write create MyFolder directory", [self class]);
            NSAssert( FALSE, @"Failed to create directory maybe out of disk space?");
        }
        [self setProfileName:uidgname];
        NSString *pathName = [dataPath stringByAppendingPathComponent:APP_STATUS_FILE_NAME];
        NSString *deviceSecretFilePath = [dataPath stringByAppendingPathComponent:DEVICE_SECRET_FILE_NAME];
        [self setDeviceSecretFilePath:deviceSecretFilePath];
        
        [self getSerialNumber];
    }
}


- (void) setDeviceSecretFilePath:(NSString *)filePath {
    //NSLog(@"setDeviceSecretFilePath %@",filePath);
	DeviceManager *deviceManager = sharedDeviceManager();
	deviceManager->setSecretFilePath([filePath UTF8String]);
}

- (void) removeAppStatus
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:self.profileName];
    NSLog(@"removeAppStatus Directory: %@", docsDirectory);
	if ([[NSFileManager defaultManager] fileExistsAtPath:docsDirectory])
    {
		NSError *error;
		[[NSFileManager defaultManager] removeItemAtPath:docsDirectory error:&error];
	}
}


-(void) checkList
{
    NSMutableArray *profileLs = [[NSMutableArray alloc]init];
    NSFileManager *filemgr;
    NSArray *filelist = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [filelist objectAtIndex:0];
    int count;
    int i;
    NSMutableArray *directoryList = [[NSMutableArray alloc] init];
    
    filemgr =[NSFileManager defaultManager];
    filelist = [filemgr contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    count = [filelist count];
    [self newloadAppStatus];
    //check if token n n secret path exsit
    for (i = 0; i < count; i++)
    {
        [directoryList addObject:filelist[i]];
    }
    
    for(NSString *file in directoryList) {
        NSString *path = [documentsDirectory stringByAppendingPathComponent:file];
        BOOL isDir = NO;
        [filemgr fileExistsAtPath:path isDirectory:(&isDir)];
        if(isDir) {
            [profileLs addObject:file];
        }
    }
    
    if (profileLs.count == 1) {
        [self setProfileName:[profileLs objectAtIndex:0]];
    }
    //NSLog(@"count %lu",(unsigned long)profileLs.count);
}

-(void)saveCLLocation :(CLLocationManager*)location gnm:(NSString*)groudId
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    //file to write to
    NSError *error = nil;
    NSMutableArray *arr;
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"location.txt"];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];

    //some lat and long values
    CGFloat latitudes = location.location.coordinate.latitude;
    CGFloat longitudes = location.location.coordinate.longitude;
    
    NSDictionary *locationDict = @{ @"latitude" : [NSNumber numberWithFloat:latitudes], @"longitude" : [NSNumber numberWithFloat:longitudes] };
    
    
    if (fileExists) {
        //NSLog(@"SHAREDDATA ~~~ save YES");
        arr = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
        [arr addObject:groudId];
        [arr addObject:locationDict];
    }
    else{
        //NSLog(@"SHAREDDATA ~~~ save NO");
        arr = [[NSMutableArray alloc]init];
        [arr addObject:groudId];
        [arr addObject:locationDict];
    }
    NSLog(@"SHAREDDATA ~~~ save location %@",arr);
    [arr writeToFile: filePath atomically:YES];
}

-(NSMutableArray*)loadLocation:(NSString*)groupId
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    //file to write to
    NSError *error = nil;
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"location.txt"];
    
    NSMutableArray *arr = [[NSMutableArray alloc]initWithContentsOfFile:filePath];
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    if (arr.count >0) {
       NSLog(@"SHAREDDATA ~~~ load file of array %@",arr);
        if (arr.count >0) {
            
            for(int i=0;i<[arr count];i+=2)
            {
                //NSArray* ar = [arr objectAtIndex:i];
                NSString* gId = [arr objectAtIndex:i];
                if(![gId isEqualToString:groupId]) continue;
                NSDictionary* locDict = [arr objectAtIndex:i+1];
                NSNumber* lat = [locDict valueForKey:@"latitude"];
                NSNumber* lon = [locDict valueForKey:@"longitude"];
                NSLog(@"SHAREDDATA locDict %@",locDict);
                [array addObject:locDict];
            }
        }
    }
    return array;
}

//this to save the location to the existing file
- (void)saveGeoLocation:(double)lat long:(double)longit
{
    //get the directory of the account
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [docsDirectory stringByAppendingPathComponent:self.profileName];
    self.appStatusFilePath = [dataPath stringByAppendingPathComponent:APP_STATUS_FILE_NAME];
    
    NSMutableArray *locationArray =[[NSMutableArray alloc] init];
    self.arrayOfLocations = [[NSMutableArray alloc] init];
    
    //some lat and long values
    CGFloat latitudes = lat;
    CGFloat longitudes = longit;
    
    NSDictionary *locationDict = @{ @"latitude" : [NSNumber numberWithFloat:latitudes], @"longitude" : [NSNumber numberWithFloat:longitudes] };
    [locationArray addObject:locationDict];
    for(int i=0; i<= [self.arrayOfLocations count];i++){
        
    }
    
    if (self.arrayOfLocations != 0) {
        [self.arrayOfLocations addObject:locationArray];
    }
    else {
        [self.arrayOfLocations addObject:locationArray];
    }
    
    //[locationArray writeToFile: self.appStatusFilePath atomically:YES];
    //NSLog(@"########################SD SAVE  locationArray%@ ", locationArray);

    //CLLocation *towerLocation = [[CLLocation alloc] initWithLatitude:lat longitude:longit];
    //[self.arrayOfLocations addObject:[NSData dataWithBytes:&new_coordinate length:sizeof(new_coordinate)] ];

    
    
    NSLog(@"location saved: %@",self.arrayOfLocations);
     [self.arrayOfLocations writeToFile:self.appStatusFilePath atomically:YES];
    //to load
    //CLLocationCoordinate2D coord = [[currentDisplayedTowers lastObject] coordinate];
    //[self newsaveAppStatus];
}


//this is to add gnmuid of all the scaned profile
-(void)save :(NSArray*)data
{
    //NSLog(@"data: %@",data);
    NSString *joinString = [NSString stringWithFormat:@"%@%@",[data valueForKey:@"gnm"],[[data valueForKey:@"uid"] lowercaseString]];
    //NSLog(@"*********SHAREDDATA ~~~ save %@",joinString);
    [self createDirectory:joinString];
    //applications Documents dirctory path
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    //file to write to
    NSError *error = nil;
    NSMutableArray *arr;
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"data.txt"];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists) {
        //NSLog(@"SHAREDDATA ~~~ save YES");
         arr = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
        [arr addObject:data];
    }
    else{
        //NSLog(@"SHAREDDATA ~~~ save NO");
        arr = [[NSMutableArray alloc]init];
        [arr addObject:data];
    }
    [arr writeToFile: filePath atomically:YES];
    
    [self load];
}

//this is to load all the gnmuid save in data file
-(NSMutableArray*)load
{
    //NSLog(@"SHAREDDATA ~~~ load");
    //application Documents dirctory path
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *jsonFilePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"data.txt"];
    
    NSMutableArray *arr = [[NSMutableArray alloc]initWithContentsOfFile:jsonFilePath];
    
    //NSLog(@"SHAREDDATA ~~~ load file of array %@",arr);
    NSMutableArray *array = [[NSMutableArray alloc]init];
    if (arr.count >0) {
        
        for(int i=0;i<[arr count];i++)
        {
            NSArray* ar = [arr objectAtIndex:i];
            NSString* gnm = [ar valueForKey:@"gnm"];
            NSString* uid = [[ar valueForKey:@"uid"] lowercaseString];
            NSString* tac = [ar valueForKey:@"tac"];
            //NSLog(@"%d gnm %@, uid %@, tac %@",i,gnm,uid,tac);
            [array addObject:ar];
        }
    }
    

    return array;
}
-(NSString*)getgnm:(NSString*)gid
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *jsonFilePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"data.txt"];
    
    NSMutableArray *arr = [[NSMutableArray alloc]initWithContentsOfFile:jsonFilePath];
    NSString *gnm = nil;
    if (arr.count >0) {
        
        for(int i=0;i<[arr count];i++)
        {
            NSArray* ar = [arr objectAtIndex:i];
            if ([[ar valueForKey:@"gid"] isEqualToString:gid]) {
                gnm = [ar valueForKey:@"gnm"];
                NSLog(@"***gnm: %@", gnm);
            }         
        }
    }
    return gnm;
}
-(NSMutableArray*)removeNotSaved
{
    //if only one value remove it
    NSLog(@"**************SHARED********REMOVE NOTSAVED**********");
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *jsonFilePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"data.txt"];
    
    NSMutableArray *arr = [[NSMutableArray alloc]initWithContentsOfFile:jsonFilePath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:jsonFilePath])
    {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:jsonFilePath error:&error];
    }
    
}

//this to check gnmuid already exist in data file.
-(NSMutableArray*)checkGnmUid
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *jsonFilePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"data.txt"];
    
    NSMutableArray *arr = [[NSMutableArray alloc]initWithContentsOfFile:jsonFilePath];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    if (arr.count >0) {
        
        for(int i=0;i<[arr count];i++)
        {
            NSArray* ar = [arr objectAtIndex:i];
            NSString* gnmId =[NSString stringWithFormat:@"%@%@",[ar valueForKey:@"gnm"],[[ar valueForKey:@"uid"] lowercaseString]] ;
            //NSLog(@"%d gnm %@",i,gnmId);
            [array addObject:gnmId];
        }
    }
    return array;
}

- (void)saveList :(NSString *)gid gnms:(NSString*)gnm uIDs:(NSString*)uid
{
    //applications Documents dirctory path
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    //file to write to
    NSError *error = nil;
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"list.txt"];
    
    
    //this save all the gnm and uid
    NSMutableDictionary *listDict = [[NSMutableDictionary alloc]init];
    [listDict setObject:gid forKey:@"gid"];
    [listDict setObject:gnm forKey:@"gnm"];
    [listDict setObject:uid forKey:@"uid"];
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    [arr addObject:listDict];
    [arr writeToFile: filePath atomically:YES];
    [self loadList];
}

-(void)removeFromList//:(NSString *)gid gnms:(NSString*)gnm uIDs:(NSString*)uid
{
    //applications Documents dirctory path
    NSLog(@"**************SHARED********REMOVE FROM LIST********** %@",self.profileName);
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    //file to write to
//    NSError *error = nil;
//    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"list.txt"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:self.profileName];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:docsDirectory])
    {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:docsDirectory error:&error];
    }
    //need to remove from data.txt
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"data.txt"];
    NSMutableArray *arr = [[NSMutableArray alloc]initWithContentsOfFile:filePath];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists) {
        
        
        NSMutableArray *array = [[NSMutableArray alloc]init];
        if (arr.count >0) {
            
            for(int i=0;i<[arr count];i++)
            {
                NSArray* ar = [arr objectAtIndex:i];
                NSString* gnmId =[NSString stringWithFormat:@"%@%@",[ar valueForKey:@"gnm"],[[ar valueForKey:@"uid"]lowercaseString]] ;
                NSLog(@"%d gnm %@",i,gnmId);
                if ([gnmId isEqualToString:self.profileName]) {
                    //remove from list
                    NSLog(@"removed %@", gnmId);
                    [arr removeObjectAtIndex:i];
                }
            }
        }
        NSLog(@"file details gnm %@",arr);
    }
    else
    {
        [arr removeObjectAtIndex:0];
    }
    [arr writeToFile: filePath atomically:YES];
    
    [self checkList];
}

-(void)loadList
{
    //load the save list.txt
    NSString *path;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"list.txt"];
    
    NSMutableData *app = [NSMutableData dataWithContentsOfFile:path];
    NSMutableData *unscramble = [app UnScramble:[self getMacAddress] andForData:app];
    
    NSString *ingredients = [[NSString alloc]initWithData:unscramble encoding:NSUTF8StringEncoding];
    NSString *obj = nil;
    NSString *value = nil;
    NSMutableDictionary *listDict = [[NSMutableDictionary alloc]init];
    NSArray *components=[ingredients componentsSeparatedByString:@";"];
    if ([components count]>=1) {
        for (int i=0 ; i < [components count]; i++) {
            ingredients = [NSString stringWithFormat:@"%@",[components objectAtIndex:i]];
            NSArray *components2=[ingredients componentsSeparatedByString:@"="];
            if ([components2 count]>=1) {
                for (int j=0 ; j < [components2 count]; j++) {
                    ingredients = [NSString stringWithFormat:@"%@",[components2 objectAtIndex:j]];
                    if (j == 0) {
                        NSString *s = [ingredients stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        NSString *q = [s stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                        obj = q;
                    }
                }
                
                NSArray *components3=[ingredients componentsSeparatedByString:@"\'"];
                if ([components3 count]>=1) {
                    for (int k=0 ; k < [components3 count]; k++) {
                        ingredients = [NSString stringWithFormat:@"%@",[components3 objectAtIndex:k]];
                        ingredients = [ingredients stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                        ingredients = [ingredients stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                        ingredients = [ingredients stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        value = ingredients;
                        if (obj != nil && value != nil) {
                            [listDict setObject:value forKey:obj];
                        }
                    }
                }
            }
            obj = nil;
            value = nil;
        }
    }
    
    if (listDict != nil)
    {
        //NSLog(@"listDict %@",listDict);
        if ([listDict objectForKey:@"gid"] != nil) {
            self.groupID = [listDict objectForKey:@"gid"];
        }
        if ([listDict objectForKey:@"gnm"] != nil) {
            self.groupName = [listDict objectForKey:@"gnm"];
        }
        if ([listDict objectForKey:@"uid"] != nil) {
            self.userID = [[listDict objectForKey:@"uid"] lowercaseString];
        }
    }
    ingredients = nil;

}

- (void) newsaveAppStatus
{
    //get tokenkey
    //check if there is file exist
    //decrypt the file
    //check if the value exist, yes replace the value else add the value.
    //encrpt the file
    NSString *token = [self getMacAddress];
    NSString *path;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path = [[paths objectAtIndex:0] stringByAppendingPathComponent:self.profileName];
    NSString *pathName = [path stringByAppendingPathComponent:APP_STATUS_FILE_NAME];
    //NSLog(@"SAVE --> Profile Name to Load %@", self.profileName);
    NSMutableDictionary *appDict = [[NSMutableDictionary alloc]init];
    NSString *str = nil;
    
    
    if (self.profileName.length != 0) {
        NSLog(@"SAVE --> Profile Name to save %@", self.profileName);
        appDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                   [NSNumber numberWithBool:self.isActivated], APP_STATUS_IS_ACTIVATED,
                   [NSNumber numberWithInt:self.isDeviceLocked], APP_STATUS_IS_LOCKED,
                   [NSNumber numberWithBool:self.isTiltEnable],APP_STATUS_IS_TILT_ENABLED,
                   [NSNumber numberWithBool:self.isLocationSet],APP_STATUS_IS_LOCATION_ENABLED,
                   [NSNumber numberWithInt:self.isDeviceLocked], APP_STATUS_IS_LOCKED,
                   [NSNumber numberWithDouble:self.latitude], APP_STATUS_LATITUDE_VALUE,
                   [NSNumber numberWithDouble:self.longitude],APP_STATUS_LONGITUDE_VALUE,
                   //[NSNumber numberWithBool:self.isAutomationEnable], APP_STATUS_IS_AUTOMATION_ENABLE,
                   [NSNumber numberWithBool:self.isSecuritySet],APP_STATUS_IS_SECURITY_SET,
                   [NSNumber numberWithBool:self.isTouchIDEnable],APP_STATUS_IS_TOUCH_ID_SET,
                   [NSNumber numberWithBool:[self isTouchIdDevice1]],APP_STATUS_IS_TOUCH_ID_DEVICE,
                   [NSNumber numberWithInt:self.outPut], APP_STATUS_MAX_TILT_ATTEMPTS,
                    [NSNumber numberWithInt:self.isAppOpenedFirstTime], IS_APP_OPENED_FIRST_TIME,
                   // This will for secured alignment
                    [NSNumber numberWithInt:self.isSecureAligned], IS_SECURE_ALIGNED,
                   [NSNumber numberWithInt:self.isFaceRecognised], IS_FACE_RECGNSED,
                //   [NSNumber nu]
                   nil];
        //added 13-02-2014 to have a version
        [appDict setObject:[NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]] forKey:APP_STATUS_VERSION];
//        
//        if ([appDict objectForKey:APP_STATUS_MAX_TILT_ATTEMPTS] != nil) {
//            self.outPut = [[appDict objectForKey:APP_STATUS_MAX_TILT_ATTEMPTS] intValue];
//        }
        
        if ([NSNumber numberWithBool:isAutomationEnable] != nil){
            [appDict setObject:[NSNumber numberWithBool:isAutomationEnable] forKey:APP_STATUS_IS_AUTOMATION_ENABLE];
        }
        
        if (deviceSN != nil) [appDict setObject:deviceSN forKey:APP_STATUS_SERIAL_NUMBER_KEY];
        else [appDict removeObjectForKey:APP_STATUS_SERIAL_NUMBER_KEY];
        
        if (self.gatewayURL != nil) [appDict setObject:self.gatewayURL forKey:APP_STATUS_GATEWAY_URL];
        
        if (self.devicePID != nil) [appDict setObject:self.devicePID forKey:APP_STATUS_PID_KEY];
        if (self.dTiltValue != nil) [appDict setObject:self.dTiltValue forKey:APP_STATUS_SECURE_VALUE];
        if(self.arrayOfLocations.count > 0)[appDict setObject:self.arrayOfLocations forKey:@"LOCATION"];
        str = [NSString stringWithFormat:@"%@", appDict];
    
        //NSLog(@"**NEWSAVE***TokenStatus File DEtails %@", appDict);
    }
    else
    {
        
        NSLog(@"SAVE --> Profile Name to save is NULL");
        appDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                   [NSNumber numberWithBool:self.isActivated], APP_STATUS_IS_ACTIVATED,
                   [NSNumber numberWithInt:self.isDeviceLocked], APP_STATUS_IS_LOCKED,
                    [NSNumber numberWithBool:self.isTiltEnable],APP_STATUS_IS_TILT_ENABLED,
                   [NSNumber numberWithBool:self.isSecuritySet],APP_STATUS_IS_SECURITY_SET,
                   [NSNumber numberWithBool:self.isTouchIDEnable],APP_STATUS_IS_TOUCH_ID_SET,
                   [NSNumber numberWithBool:[self isTouchIdDevice1]],APP_STATUS_IS_TOUCH_ID_DEVICE,
                   [NSNumber numberWithInt:self.outPut], APP_STATUS_MAX_TILT_ATTEMPTS,
                   [NSNumber numberWithInt:self.isAppOpenedFirstTime], IS_APP_OPENED_FIRST_TIME,
                    [NSNumber numberWithInt:self.isSecureAligned], IS_SECURE_ALIGNED,
                   [NSNumber numberWithInt:self.isFaceRecognised], IS_FACE_RECGNSED,
                   nil];
        
        [appDict setObject:APP_STATUS_VERSION_NO forKey:APP_STATUS_VERSION];
            if (self.devicePID != nil) [appDict setObject:self.devicePID forKey:APP_STATUS_PID_KEY];
        if (self.dTiltValue != nil) [appDict setObject:self.dTiltValue forKey:APP_STATUS_SECURE_VALUE];
        if (self.gatewayURL != nil) [appDict setObject:self.gatewayURL forKey:APP_STATUS_GATEWAY_URL];

        str = [NSString stringWithFormat:@"%@", appDict];
        NSLog(@"**NEWSAVE***TokenStatus File DEtails %@", appDict);

    }
    NSLog(@"**NEWSAVE***TokenStatus File Path %@", pathName);
    NSString *newString = [[str componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    NSString *r = [newString stringByReplacingOccurrencesOfString:@"{" withString:@""];
    NSString *s = [r stringByReplacingOccurrencesOfString:@"}" withString:@""];
    NSMutableData *InputData = (NSMutableData *)[s dataUsingEncoding:NSUTF8StringEncoding];
    NSData *scrambleData = [InputData Scramble:token];
    [scrambleData writeToFile:pathName atomically:YES];
    [self newloadAppStatus];
}



- (void) newloadAppStatus
{
    //NSLog(@"Profile Name to Load %@", self.profileName);
    NSString *token = [self getMacAddress];

    NSString *path;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	path = [[paths objectAtIndex:0] stringByAppendingPathComponent:self.profileName];
    NSString *pathName = [path stringByAppendingPathComponent:APP_STATUS_FILE_NAME];
    
    NSLog(@"self.profileName.length =%lu",(unsigned long)self.profileName.length);
    if (self.profileName.length != 0) {
        NSString *deviceSecretFilePath = [path stringByAppendingPathComponent:DEVICE_SECRET_FILE_NAME];
        //NSLog(@"Secret File Path %@", deviceSecretFilePath);
        NSLog(@"****NEWLOAD***TokenStatus File Path %@", pathName);
        [self setDeviceSecretFilePath:deviceSecretFilePath];
    }
    NSMutableData *app = [NSMutableData dataWithContentsOfFile:pathName];
    NSMutableData *unscramble = [app UnScramble:token andForData:app];
    NSString *ingredients = [[NSString alloc]initWithData:unscramble encoding:NSUTF8StringEncoding];
    NSString *obj = nil;
    NSString *value = nil;
    NSMutableDictionary *appDict = [[NSMutableDictionary alloc]init];
    NSArray *components=[ingredients componentsSeparatedByString:@";"];
    if ([components count]>=1) {
        for (int i=0 ; i < [components count]; i++) {
            ingredients = [NSString stringWithFormat:@"%@",[components objectAtIndex:i]];
            NSArray *components2=[ingredients componentsSeparatedByString:@"="];
            
            if ([components2 count]>=1) {
                for (int j=0 ; j < [components2 count]; j++) {
                    ingredients = [NSString stringWithFormat:@"%@",[components2 objectAtIndex:j]];
                    if (j == 0) {
                        NSString *s = [ingredients stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        NSString *q = [s stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                        obj = q;
                    }
                }
                
                NSArray *components3=[ingredients componentsSeparatedByString:@"\'"];
                
                if ([components3 count]>=1) {
                    for (int k=0 ; k < [components3 count]; k++) {
                        ingredients = [NSString stringWithFormat:@"%@",[components3 objectAtIndex:k]];
                        //ADDED 13-02-2014 to remove the spaces and brackets created in the token_status file.
                        ingredients = [ingredients stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                        ingredients = [ingredients stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                        ingredients = [ingredients stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        value = ingredients;
                        if (obj != nil && value != nil) {
                            [appDict setObject:value forKey:obj];
                        }
                        
                    }
                }
            }
            obj = nil;
            value = nil;
        }
    }
    
    if (self.profileName.length != 0) {
        
        if (appDict != nil)
        {
            if ([appDict objectForKey:APP_STATUS_IS_ACTIVATED] != nil) {
                self.isActivated = [[appDict objectForKey:APP_STATUS_IS_ACTIVATED] boolValue];
            }
            
            if ([appDict objectForKey:APP_STATUS_IS_LOCKED] != nil) {
                self.isDeviceLocked = [[appDict objectForKey:APP_STATUS_IS_LOCKED] intValue];
            }
            
            if ([appDict objectForKey:APP_STATUS_MAX_TILT_ATTEMPTS] != nil) {
                self.outPut = [[appDict objectForKey:APP_STATUS_MAX_TILT_ATTEMPTS] intValue];
            }
            
            if ([appDict objectForKey:APP_STATUS_SERIAL_NUMBER_KEY] != nil) {
                self.deviceSN = [appDict objectForKey:APP_STATUS_SERIAL_NUMBER_KEY];
                self.deviceSN = [self.deviceSN stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            }
            if ([appDict objectForKey:APP_STATUS_PID_KEY] != nil) {
                self.devicePID = [appDict objectForKey:APP_STATUS_PID_KEY];
            }
           
            if ([appDict objectForKey:APP_STATUS_IS_TILT_ENABLED] != nil) {
                self.isTiltEnable = [[appDict objectForKey:APP_STATUS_IS_TILT_ENABLED] boolValue];
            }
            if ([appDict objectForKey:APP_STATUS_IS_LOCATION_ENABLED] != nil) {
                self.isLocationSet = [[appDict objectForKey:APP_STATUS_IS_LOCATION_ENABLED] boolValue];
            }
            if ([appDict objectForKey:APP_STATUS_SECURE_VALUE] != nil) {
                self.dTiltValue = [appDict objectForKey:APP_STATUS_SECURE_VALUE];
            }else{
                NSLog(@"asdfasdfsd");
            }
            
            if ([appDict objectForKey:APP_STATUS_LATITUDE_VALUE] != nil) {
                self.latitude = [[appDict objectForKey:APP_STATUS_LATITUDE_VALUE] doubleValue];
            }
            if ([appDict objectForKey:APP_STATUS_LONGITUDE_VALUE] != nil) {
                self.longitude = [[appDict objectForKey:APP_STATUS_LONGITUDE_VALUE] doubleValue];
            }
            if ([appDict objectForKey:APP_STATUS_IS_AUTOMATION_ENABLE] != nil) {
                self.isAutomationEnable = [[appDict objectForKey:APP_STATUS_IS_AUTOMATION_ENABLE] boolValue];
            }
            if ([appDict objectForKey:APP_STATUS_IS_SECURITY_SET] != nil) {
                self.isSecuritySet = [[appDict objectForKey:APP_STATUS_IS_SECURITY_SET] boolValue];
            }
            
            if ([appDict objectForKey:APP_STATUS_IS_TOUCH_ID_SET] != nil) {
                self.isTouchIDEnable = [[appDict objectForKey:APP_STATUS_IS_TOUCH_ID_SET] boolValue];
            }
            if ([appDict objectForKey:APP_STATUS_IS_TOUCH_ID_DEVICE] != nil) {
                self.isTouchIDDevice = [[appDict objectForKey:APP_STATUS_IS_TOUCH_ID_DEVICE] boolValue];
            }
            if ([appDict objectForKey:APP_STATUS_GATEWAY_URL] != nil) {
                self.gatewayURL = [appDict objectForKey:APP_STATUS_GATEWAY_URL];
            }
            
            if ([appDict objectForKey:IS_APP_OPENED_FIRST_TIME] != nil) {
                self.isAppOpenedFirstTime = [[appDict objectForKey:IS_APP_OPENED_FIRST_TIME] boolValue];
            }
            if ([appDict objectForKey:IS_APP_OPENED_FIRST_TIME] != nil) {
                self.isSecureAligned = [[appDict objectForKey:IS_SECURE_ALIGNED] boolValue];
            }
            if ([appDict objectForKey:IS_FACE_RECGNSED] != nil) {
                self.isFaceRecognised = [[appDict objectForKey:IS_FACE_RECGNSED] boolValue];
            }
            
            
//            if ([appDict objectForKey:@"LOCATION"] != nil) {
//                self.arrayOfLocations = [appDict objectForKey:@"LOCATION"];
//            }
        }
    }
    else
    {
        if ([appDict objectForKey:APP_STATUS_IS_ACTIVATED] != nil) {
            self.isActivated = [[appDict objectForKey:APP_STATUS_IS_ACTIVATED] boolValue];
        }
        
        if ([appDict objectForKey:APP_STATUS_IS_LOCKED] != nil) {
            self.isDeviceLocked = [[appDict objectForKey:APP_STATUS_IS_LOCKED] intValue];
        }
        
        if ([appDict objectForKey:APP_STATUS_MAX_TILT_ATTEMPTS] != nil) {
            self.outPut = [[appDict objectForKey:APP_STATUS_MAX_TILT_ATTEMPTS] intValue];
        }
        if ([appDict objectForKey:APP_STATUS_PID_KEY] != nil) {
            self.devicePID = [appDict objectForKey:APP_STATUS_PID_KEY];
        }
    if ([appDict objectForKey:APP_STATUS_IS_TILT_ENABLED] != nil) {
            self.isTiltEnable = [[appDict objectForKey:APP_STATUS_IS_TILT_ENABLED] boolValue];
        }
        if ([appDict objectForKey:APP_STATUS_IS_SECURITY_SET] != nil) {
            self.isSecuritySet = [[appDict objectForKey:APP_STATUS_IS_SECURITY_SET] boolValue];
        }
        if ([appDict objectForKey:APP_STATUS_IS_TOUCH_ID_SET] != nil) {
            self.isTouchIDEnable = [[appDict objectForKey:APP_STATUS_IS_TOUCH_ID_SET] boolValue];
        }
        if ([appDict objectForKey:APP_STATUS_IS_TOUCH_ID_DEVICE] != nil) {
            self.isTouchIDDevice = [[appDict objectForKey:APP_STATUS_IS_TOUCH_ID_DEVICE] boolValue];
        }
        if ([appDict objectForKey:APP_STATUS_SECURE_VALUE] != nil) {
            self.dTiltValue = [appDict objectForKey:APP_STATUS_SECURE_VALUE];
        }else{
            NSLog(@"NIL SECURE VALUE");
        }
        if ([appDict objectForKey:APP_STATUS_GATEWAY_URL] != nil) {
            self.gatewayURL = [appDict objectForKey:APP_STATUS_GATEWAY_URL];
        }
        
        if ([appDict objectForKey:IS_APP_OPENED_FIRST_TIME] != nil) {
            self.isAppOpenedFirstTime = [[appDict objectForKey:IS_APP_OPENED_FIRST_TIME] boolValue];
        }
        if ([appDict objectForKey:IS_APP_OPENED_FIRST_TIME] != nil) {
            self.isSecureAligned = [[appDict objectForKey:IS_SECURE_ALIGNED] boolValue];
        }
        if ([appDict objectForKey:IS_FACE_RECGNSED] != nil) {
            self.isFaceRecognised = [[appDict objectForKey:IS_FACE_RECGNSED] boolValue];
        }
           }
    NSLog(@"**NEWLOAD***TokenStatus File DEtails %@", appDict);

    //NSLog(@"#########SharedData load pin set %@",  appDict);
    ingredients = nil;
}

- (BOOL) loadDeviceWithoutPin
{
	DeviceManager *deviceManager = sharedDeviceManager();
    if (deviceManager->loadDevice())
    {
        //NSLog(@"SD loadDeviceWithoutPin");
        //NSLog(@"SN: %@", self.deviceSN);
         //NSLog(@"isOutOn: %hhd", self.isOutOn);
		self.isDeviceExist = YES;
        if(!HID_RCDISPLAY){
            if (deviceManager->m_registrationcode != NULL) {
                self.deviceRC = [NSString stringWithCString:deviceManager->m_registrationcode encoding:NSUTF8StringEncoding];
            } else {
                self.deviceRC = nil;
            }
        }
		self.deviceSN = [NSString stringWithCString:deviceManager->m_serialnumber encoding:NSUTF8StringEncoding];
		self.isOutOn = deviceManager->m_pinsupport;
		self.isRCDisplay = deviceManager->m_rcdisplay;
		self.cashMode = deviceManager->m_cashMode;
		self.pbkdMode = deviceManager->m_pbkdmode;
		self.cashType = deviceManager->m_cashType;
		return YES;
	}
	
	return NO;
}

- (BOOL) loadDeviceWithPin:(NSString *)pin
{
	DeviceManager *deviceManager = sharedDeviceManager();
    BOOL isUpgrade = NO;
    
    //[self getSecretFileVersion];

    if (deviceManager->loadDevice([pin UTF8String]))
    {
        if (isUpgrade) {
            const char * uFullStr = (const char *) [pin UTF8String];
            //unsigned char* hash = [self getHash:uFullStr stringLen: [pin length]];
            //NSString *fullStrHash = [NSString stringWithFormat:@"%s",hash];
            NSString *fullStrHash = [NSString stringWithFormat:@"%s",uFullStr];

            self.dOut = fullStrHash;
            fullStrHash = nil;
        }
        else{
            self.dOut = pin;
        }
		
		self.isDeviceExist = YES;
		
		self.deviceSN = [NSString stringWithCString:deviceManager->m_serialnumber encoding:NSUTF8StringEncoding];
		self.isOutOn = deviceManager->m_pinsupport;
		self.isRCDisplay = deviceManager->m_rcdisplay;
		self.cashMode = deviceManager->m_cashMode;
		self.pbkdMode = deviceManager->m_pbkdmode;
		self.cashType = deviceManager->m_cashType;
        
        if (!HID_RCDISPLAY) {
            if (deviceManager->m_registrationcode != NULL) {
                self.deviceRC = [NSString stringWithCString:deviceManager->m_registrationcode encoding:NSUTF8StringEncoding];
            } else {
                self.deviceRC = nil;
            }
            
        }
		return YES;
	}
	return NO;
}

- (BOOL) saveDeviceWithoutPin
{
	DeviceManager *deviceManager = sharedDeviceManager();
    if (deviceManager->saveDeviceWithoutPin())
    {
		self.isDeviceExist = YES;
        self.isActivated = YES;
        NSLog(@"SD -> SAVEDEVICEWITHOUTPIN -> PROFILENAME: %@",self.profileName);
        //NSLog(@"########################SD SAVE  %@ ", self.profileName);

	//	[self newsaveAppStatus];
		return YES;
	}
	return NO;
}

- (BOOL) saveDeviceWithPin:(NSString *)pin
{
	DeviceManager *deviceManager = sharedDeviceManager();
    if (deviceManager->saveDeviceWithPin([pin UTF8String]))
    {
        self.dOut = pin;
		self.isDeviceExist = YES;
        
        self.cashMode = deviceManager->m_cashMode;
		self.pbkdMode = deviceManager->m_pbkdmode;
		self.cashType = deviceManager->m_cashType;
        //NSLog(@"########################SD SAVE  savedevicewithpin%@ ", self.profileName);

		[self newsaveAppStatus];
        //setpin pin to something else
        //dOut = @"abcd";
		return YES;
	}
	return NO;
}

- (void) resetoutPut
{
	self.outPut = ENTER_PIN_ATTEMPTS_MAX;
    //NSLog(@"########################SD SAVE resetoutput  %@ ", self.profileName);

    [self newsaveAppStatus];
}

- (void) decreaseoutPut
{
    NSLog(@"SharedData number of attempt %d",self.outPut);
	if (outPut > 0)
    {
		self.outPut -= 1;
             [self newsaveAppStatus];
	}
    else if(outPut <= 0)
    {
    
        int time = 10.0f;
        self.outPut -= 1;
        self.isDeviceLocked = YES;
        [self newsaveAppStatus];

        
        NSLog(@"######################## <0 SD SAVE LOCK");
        
//        time = time*self.outPut;
//        [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(lockApp) userInfo:nil repeats:NO];
//        [self lockApp];
        
    }
}

-(void)lockApp
{
    NSLog(@"%%%%%%%%%%%%%%%%%%%%%%LOCKAPP");
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"App Locked" message:@"Wait For Xsec for app to be unlocked." delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
    [alertView show];
}
- (BOOL) changedOut:(NSString *)pin
{
    //NSLog(@"changedOut");
	if (isOutOn)
    {
		DeviceManager *deviceManager = sharedDeviceManager();
        if (deviceManager->changedOut([dOut UTF8String], [pin UTF8String]))
        {
            //NSLog(@"changedOut YES %@",self.dOut);
			self.dOut = pin;
			return YES;
		}
	}
	return NO;
}

- (void) removeDevice
{
    NSLog(@"**************SHARED********REMOVE DEVICE**********");
	DeviceManager *deviceManager = sharedDeviceManager();
	deviceManager->removeDevice();
}

- (NSString *) getSerialNumber
{
    if (!isDeviceExist)
    {
        if ((SHOULD_USE_16_DIGITS_SN == YES && deviceSN.length < 16) ||
            (SHOULD_USE_16_DIGITS_SN == NO && deviceSN.length > 12))
        {
            deviceSN = nil;
        }
    }
    
    if (deviceSN == nil || deviceSN.length == 0) {
        [self generateSerialNumber];
    }
    return deviceSN;
}

- (NSString *) getHyphenSerialNumber
{
    [self getSerialNumber];
    
    if (deviceSN.length > 12) {
        return [self getHyphenSerialNumber16];
    } else {
        return [self getHyphenSerialNumber12];
    }
}

- (NSString *) getHyphenSerialNumber12
{
	char *serialNumberCString = (char *)[deviceSN UTF8String];
	char inderial[20];
	memset(inderial, 0, 20);
	strncpy(inderial, serialNumberCString, 2);
	strcat(inderial, "-");
	strncpy((inderial + 3), (serialNumberCString + 2), 4);
	strcat((inderial + 3), "-");
	strncpy((inderial + 8), (serialNumberCString + 6), 5);
	strcat((inderial + 8), "-");
	strncpy((inderial + 14), (serialNumberCString + 11), 1);
	
	return [NSString stringWithUTF8String:inderial];
}

- (NSString *) getHyphenSerialNumber16
{
	char *serialNumberCString = (char *)[deviceSN UTF8String];
    char inderial[20];
    memset(inderial, 0, 20);
    strncpy(inderial, serialNumberCString, 4);
    strcat(inderial, "-");
    strncpy((inderial + 5), (serialNumberCString + 4), 4);
    strcat((inderial + 5), "-");
    strncpy((inderial + 10), (serialNumberCString + 8), 4);
    strcat((inderial + 10), "-");
    strncpy((inderial + 15), (serialNumberCString + 12), 4);
    
    return [NSString stringWithUTF8String:inderial];
}

- (long) getClock{
    return sharedDeviceManager()->getClock();
}

// Generate OTP
- (NSString *) generateOTP
{
    startOTPCountTimeMillis = getCurrentTimeInMilliseconds();
	DeviceManager *deviceManager = sharedDeviceManager();
	char *otpCString = NULL;
//	if (isOutOn) {
        //NSLog(@"generateOTP -> dOut: %s", [dOut UTF8String]);
//        otpCString = deviceManager->generateOTP([dOut UTF8String]);
        
//        if (cashType == TOKEN_OTP_TYPE_TIME) {
//            [self getClock];
//        } else {
//            [self getEventCounter];
//        }
//	} else {
		otpCString = deviceManager->generateOTP();
//        if (cashType == TOKEN_OTP_TYPE_TIME) {
//            [self getClock];
//        } else {
//            [self getEventCounter];
//        }
//	}
    NSLog(@"########################SD SAVE generateOTP %@ ", self.profileName);

    [self newsaveAppStatus];
	if (otpCString != NULL)
    {
		self.dUse = [NSString stringWithCString:otpCString encoding:NSUTF8StringEncoding];
		printf("OTP : [%s]\n", otpCString);
        NSString *otp =[NSString stringWithCString:otpCString encoding:NSUTF8StringEncoding];
        //otpCString = NULL;
		//return self.dUse;
        return otp;
	}
    else
    {
		return nil;
	}
}

// Generate Signature
- (NSString *) generateSIGOTP
{
    //NSLog(@"generateSIGOTP");
    //[self newloadAppStatus];
    startOTPCountTimeMillis = getCurrentTimeInMilliseconds();
    DeviceManager *deviceManager = sharedDeviceManager();
    char *otpCString = NULL;
//    if (isOutOn) {
//        otpCString = deviceManager->generateSIGOTP([dOut UTF8String], [signatureParamString UTF8String]);
//        if (cashType == TOKEN_OTP_TYPE_TIME) {
//            [self getClock];
//        } else {
//            [self getEventCounter];
//        }
//    } else {
    
    //[self setDeviceSecretFilePath:self.profileName];
        otpCString = deviceManager->generateSIGOTP([signatureParamString UTF8String]);
//        if (cashType == TOKEN_OTP_TYPE_TIME) {
//            [self getClock];
//        } else {
//            [self getEventCounter];
//        }
//    }
    NSLog(@"#################SD -> generateSIGOTP -> signature: %@   --- length[%lu]",signatureParamString, (unsigned long)[signatureParamString length]);
    NSLog(@"#################SD -> generateSIGOTP -> PROFILENAME: %@",self.profileName);
    //NSLog(@"########################SD SAVE  %@ ", self.profileName);

    [self newsaveAppStatus];
    if (otpCString != NULL)
    {
        //NSLog(@"generateSIGOTP1");
        self.dUse = [NSString stringWithCString:otpCString encoding:NSUTF8StringEncoding];
        NSLog(@"############Sig OTP : [%s]\n", otpCString);
        return self.dUse;
    }
    else
    {
        //NSLog(@"generateSIGOTP2");
        return nil;
    }
}

#pragma Online Mode

- (void) setInternetConnectionStatus:(int)netStatus
{
    //NSLog(@"setInternetConnectionStatus ===> %d    \n%d", netStatus, self.internetConnectionStatus);
    
    switch (netStatus)
    {
        case NETWORK_REACHABILITY_STATUS_NOT_REACHABLE:
        {
            self.internetConnectionStatus = INTERNET_STATUS_NOT_REACHABLE;
            break;
        }
            
        case NETWORK_REACHABILITY_STATUS_REACHABLE_VIA_WWAN:
        {
            self.internetConnectionStatus = INTERNET_STATUS_REACHABLE_VIA_WWAN;
            break;
        }
            
        case NETWORK_REACHABILITY_STATUS_REACHABLE_VIA_WIFI:
        {
            self.internetConnectionStatus = INTERNET_STATUS_REACHABLE_VIA_WIFI;
            break;
        }
    }
}

- (int) getInternetConnectionStatus
{
    return self.internetConnectionStatus;
}



-(NSString *) onlineLogin:(NSString *)uid otp:(NSString *)otp ref:(NSString *)txtRef{
    NSError *error;
    NSDictionary* jsonDict = [[NSMutableDictionary alloc] init];
    [jsonDict setValue:uid forKey:@"actionby"];
    [jsonDict setValue:@"" forKey:@"alsid"];
    [jsonDict setValue:otp forKey:@"otp"];
    [jsonDict setValue:txtRef forKey:@"txref"];
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict
                            options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSASCIIStringEncoding];
    
    NSString *trimmedTweet = [str stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    NSString *nospaceStr = [trimmedTweet stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.backendConnectionError = NO;
    
    Downloader *downloader = [[DownloaderFactory sharedFactory] getDownloader];
    downloader.smallDownloadDelegate = self;
    downloader.loadingFlag = PROCESS_TARGET_ONLINE_LOGIN;
    NSString *actualURL = [NSString stringWithFormat:@"%@/ezwsrest/rest/engine/auth/tx/secpulldetail", [SharedData getGatewayURL]];
    bool strOk = false;
    NSString *encrypted =@"";
    while(!strOk){
        encrypted = [self testSecKey:nospaceStr];
        
        if([encrypted length] > 0) {
            strOk = true;
            encrypted = [NSString stringWithFormat:@"enc{%@}",encrypted];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/ezwsrest/rest/engine/auth/tx/secpulldetail", [SharedData getGatewayURL]]]
                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                            timeoutInterval:HTTP_REQUEST_TIME_OUT];
            [request setHTTPMethod:@"POST"];
            
            NSString *encodedXML = [encrypted urlEncodeUsingEncoding:NSUTF8StringEncoding];
            NSString *params = [NSString stringWithFormat:@"%@=%@", REQUEST_PARAMETER_NAME, encodedXML];
            [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
            NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
            [conn start];
        }
    }
    return @"";
}


-(void)onlineActivationToken:(NSString *)activationCode userid:(NSString *)uid groupid:(NSString *)gid
{
    //NSLog(@"****** %@,%@,%@ *****", uid,gid,activationCode);
    DeviceManager *deviceManager = sharedDeviceManager();
    NSLog(@"****** RC: %@ *****", self.deviceRC);
    NSString *did = [self getMacAddress];
    /*{
        'did':'f018281da231193f488',
        'uid':'chris01',
        'gid':0,
        'sno':'',
        'rc':'',
        'pid':''
    }*/
    
    //build an info object and convert to json
    NSError *error;
    // 1) Get the latest loan
    NSDictionary* jsonDict = [[NSMutableDictionary alloc] init];
    [jsonDict setValue:did forKey:@"did"];
    [jsonDict setValue:gid forKey:@"gid"];
    [jsonDict setValue:self.deviceSN forKey:@"sno"];
    [jsonDict setValue:self.deviceRC forKey:@"rc"];
    [jsonDict setValue:self.devicePID forKey:@"pid"];
    [jsonDict setValue:uid forKey:@"uid"];
     //push id need to know how to fetch from parse.
    
    //NSLog(@"json string %@",[jsonDict allKeys]);
    //convert object to data
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSASCIIStringEncoding];

    
    
    NSString *trimmedTweet = [str stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    
NSString *nospaceStr = [trimmedTweet stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"****************** json ******************** %@",nospaceStr);
    //3.encrypt XML
	//NSString *sendEncrypt = [self testSecKey:bodyXML];
    
	self.backendConnectionError = NO;
	
	Downloader *downloader = [[DownloaderFactory sharedFactory] getDownloader];
	downloader.smallDownloadDelegate = self;
	downloader.loadingFlag = PROCESS_TARGET_ACTIVATE_TOKEN;
    NSString *actualURL = nil;
	if ([self.gatewayURL length] == 0) {
       actualURL = [NSString stringWithFormat:@"%@%@",MAIN_GATEWAY_URL_DEFAULT, GATEWAY_URL_DEFAULT];
        //self.gatewayURL = actualURL;
    }
    else
    {
        actualURL = [NSString stringWithFormat:@"%@%@",self.gatewayURL, GATEWAY_URL_DEFAULT];
    }
    //NSString *url = self.gatewayURL;
    NSLog(@"**URL FOR ACTIVATION: %@ **", actualURL);

    bool strOk = false;
    
    while(!strOk){
    NSString *encrypted = [self testSecKey:nospaceStr];
    
        if([encrypted length] > 0) {
            strOk = true;
            //enc{encrypted}
            encrypted = [NSString stringWithFormat:@"enc{%@}",encrypted];
            NSLog(@"##############ACTIVATION encrypted %@",encrypted);
            
            NSLog(@"URL used %@",actualURL);
            [downloader downloadXMLContentsFromURL:actualURL withXML:encrypted];
        }
    }
}

+ (NSString *)getGatewayURL{
    if([[[SharedData sharedData] gatewayURL] length] == 0){
        return MAIN_GATEWAY_URL_DEFAULT;
    }else{
        return [[SharedData sharedData] gatewayURL];
    }
}

-(void) transactionApproval:(NSDictionary *)jsonDict{
    NSError *error;
    //convert object to data
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSASCIIStringEncoding];
    //NSLog(@"json %@",str);
    
    
    Downloader *downloader = [[DownloaderFactory sharedFactory] getDownloader];
    downloader.smallDownloadDelegate = self;
    downloader.loadingFlag = PROCESS_TARGET_TRANSACTION_SEND;
    self.processTarget = PROCESS_TARGET_TRANSACTION_SEND;
    //    NSString *actualURL = [NSString stringWithFormat:@"%@%@",MAIN_GATEWAY_URL_DEFAULT, GATEWAY_URL_TRANS];
    NSString *actualURL = nil;
    if ([self.gatewayURL length] == 0) {
        actualURL = [NSString stringWithFormat:@"%@%@",MAIN_GATEWAY_URL_DEFAULT, GATEWAY_URL_TRANS];
        //self.gatewayURL = actualURL;
    }
    else
    {
        actualURL = [NSString stringWithFormat:@"%@%@",self.gatewayURL, GATEWAY_URL_TRANS];
    }
    
    // Override here ...
    actualURL = [NSString stringWithFormat:@"%@//ezwsrest/rest/engine/auth/tx/secapproval", [SharedData getGatewayURL]];// @"https://<ip>:<port>/ezwsrest/rest/engine/auth/tx/secapproval";
    
    NSLog(@"**URL FOR TRANSACTION: %@ **", actualURL);
    
    //NSString *url = GATEWAY_URL_TRANS;
    // NSString *encrypted = [self testSecKey:str];
    //enc{encrypted}
    bool strOk = false;
    
    while(!strOk){
        NSString *encrypted = [self testSecKey:str];
        
        if([encrypted length] > 0) {
            strOk = true;
            //enc{encrypted}
            encrypted = [NSString stringWithFormat:@"enc{%@}",encrypted];
            NSLog(@"###########TRANSACTION encrypted [%@]",encrypted);
            NSLog(@"URL Transaction %@",actualURL);
            [downloader downloadXMLContentsFromURL:actualURL withXML:encrypted];
        }
    }
}

//send transaction
-(void)transactionApproval:(NSString *)sigOtp txref:(NSString *)reference txstat:(NSString *)txstatus
{
    /*{
     'did':'xxxxx',
     'sno':'xxxxx',
     'otp':'xxxxx',
     'txref':'xxxxx',
     'txstat':x
     }
*/
    NSString *did = [self getMacAddress];
//NSLog(@"****** did: %@ sno: %@",[array valueForKey:@"did"],[array valueForKey:@"sno"] );
     //NSLog(@"****** %@%@%@%@%@ *****",did, self.deviceSN,sigOtp,reference,txstatus);
    NSError *error;
    // 1) Get the latest loan
    NSDictionary* jsonDict = [[NSMutableDictionary alloc] init];
    [jsonDict setValue:did forKey:@"did"];
    [jsonDict setValue:self.deviceSN forKey:@"sno"];
    [jsonDict setValue:sigOtp forKey:@"otp"];
    [jsonDict setValue:reference forKey:@"txref"];
    [jsonDict setValue:txstatus forKey:@"txstat"];
    
    //NSLog(@"jsonDict : %@",jsonDict);
    //push id need to know how to fetch from parse.
    
    //convert object to data
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSASCIIStringEncoding];
    //NSLog(@"json %@",str);

    
    Downloader *downloader = [[DownloaderFactory sharedFactory] getDownloader];
    downloader.smallDownloadDelegate = self;
    downloader.loadingFlag = PROCESS_TARGET_TRANSACTION_SEND;
    self.processTarget = PROCESS_TARGET_TRANSACTION_SEND;
//    NSString *actualURL = [NSString stringWithFormat:@"%@%@",MAIN_GATEWAY_URL_DEFAULT, GATEWAY_URL_TRANS];
    NSString *actualURL = nil;
    if ([self.gatewayURL length] == 0) {
        actualURL = [NSString stringWithFormat:@"%@%@",MAIN_GATEWAY_URL_DEFAULT, GATEWAY_URL_TRANS];
        //self.gatewayURL = actualURL;
    }
    else
    {
        actualURL = [NSString stringWithFormat:@"%@%@",self.gatewayURL, GATEWAY_URL_TRANS];
    }
    NSLog(@"**URL FOR TRANSACTION: %@ **", actualURL);

    //NSString *url = GATEWAY_URL_TRANS;
   // NSString *encrypted = [self testSecKey:str];
    //enc{encrypted}
    bool strOk = false;
    
    while(!strOk){
        NSString *encrypted = [self testSecKey:str];
        
        if([encrypted length] > 0) {
            strOk = true;
            //enc{encrypted}
            encrypted = [NSString stringWithFormat:@"enc{%@}",encrypted];
            NSLog(@"###########TRANSACTION encrypted [%@]",encrypted);
            NSLog(@"URL Transaction %@",actualURL);
            [downloader downloadXMLContentsFromURL:actualURL withXML:encrypted];
        }
    }
}

- (void) didFinishLoadData:(NSData *)data loadingFlag:(NSInteger)flag statusParam:(NSInteger)param
{
    self.invalidResponseError = NO;
    self.returnCode = RESPONSE_RETURN_CODE_NONE;
    
    printf("~~~ Did [ FINISH ] Load Data\n");
    
    if (data != nil)
    {
//        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        printf("\n# Response = \n");
//        printf("responseString %s\n", [responseString UTF8String]);
//        [responseString release];
        
//        ResponseXMLParser *responseXMLParser = [ResponseXMLParser sharedXMLParser];
//        responseXMLParser.responseXMLType = (int)flag;
//        responseXMLParser.delegate = self;
//        [responseXMLParser parseXML:data];
        
        /*
         {
         "returnCode":x,
         "returnMessage":"xxxxx",
         "authRef":"xxxxx",
         "authStatus":x
         }
*/
//            NSDictionary * jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//            NSString *keyAs = jsonObject[@"someKey"];
//            NSLog(@"keyAs %@", keyAs);
        
        // convert to JSON
        NSError *myError = nil;
        NSDictionary *res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&myError];
        
        NSError *e = nil;
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
        //NSLog(@"Details: %@",jsonArray);
        if (!jsonArray) {
            NSLog(@"Error parsing JSON: %@", e);
            return;
        }
        else{
            self.jsonDictionary = res;
            //NSLog(@"ALL GOOD");
        }
        
        //NSLog(@"res: %@", res);
        // show all values
        /*for(id key in res) {
            
            id value = [res objectForKey:key];
            
            NSString *keyAsString = (NSString *)key;
            NSString *valueAsString = (NSString *)value;
            
            NSLog(@"key: %@", keyAsString);
            NSLog(@"value: %@", valueAsString);
        }
        */
        // extract specific value...
        NSString *result = [res valueForKey:@"returnCode"];
        
        NSString *strPin = [NSString stringWithFormat:@"~~~ returnMessage: [%@] returcode: [%@] from Server ~~~",[res valueForKey:@"returnMessage"],result];
        [SharedData writeToLogFile:strPin];
        //NSArray *results = [res objectForKey:@"returnCode"];
         NSLog(@"returnMessage: %@", [res valueForKey:@"returnMessage"]);

        int val = [result intValue];
        //NSLog(@"int: %d", val);
        
        self.returnCode = val;
        
        //NSLog(@"**********SD: didFinishLoadData returnCode %d", self.returnCode);
        if (!self.invalidResponseError && self.returnCode == RESPONSE_RETURN_CODE_SUCCESS)
        {
            //NSLog(@"********SD:  didFinishLoadData successLoadingProcess");
            [self successLoadingProcess:(int)flag];
            return;
        }
    }
    else
        self.invalidResponseError = YES;
        //NSLog(@"*****failedLoadingProcess 1");
        [self failedLoadingProcess:(int)flag];
}

- (void) successLoadingProcess:(int)loadingFlag
{
     //NSLog(@"SHAREDAPPDATA ****successLoadingProcess");
    if (loadingFlag == PROCESS_TARGET_ACTIVATE_TOKEN)
    {
            self.processResult = PROCESS_RESULT_ACTIVATE_TOKEN_SUCCESS;
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_ACTIVATE_TOKEN_DONE object:nil];
    }
    
    if (loadingFlag == PROCESS_TARGET_TRANSACTION_SEND)
    {
        self.processResult = PROCESS_RESULT_TRANSACTION_SEND_SUCCESS;
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_TRANSACTION_SEND_DONE object:nil];
        //remove the observer
        //[[NSNotificationCenter defaultCenter]removeObserver:self name:NOTIFICATION_NAME_TRANSACTION_SEND_DONE object:nil];
    }
    
    if(loadingFlag == PROCESS_TARGET_ONLINE_LOGIN){
        self.processResult = PROCESS_RESULT_ONLINE_LOGIN_SUCCESS;
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_ONLINE_LOGIN_DONE object:nil];
    }
    
}

- (void) didFailLoadData:(NSError *)error loadingFlag:(NSInteger)flag statusParam:(NSInteger)param
{
    self.backendConnectionError = YES;
    self.returnCode = (int)error.code;//RESPONSE_RETURN_CODE_NONE;
    
    //printf("~~~ Did [ FAIL ] Load Data\n");
    
    //    NSLog(@"~~~ Did [ FAIL ] Load Data\n   error -> %@", error);
    //     NSLog(@"~~~ Did [ FAIL ] Load Data\n   flag -> %d", flag);
    //     NSLog(@"~~~ Did [ FAIL ] Load Data\n   param -> %d", param);
    //NSLog(@"*******failedLoadingProcess 2");
    
    [self failedLoadingProcess:(int)flag];
}


- (void) failedLoadingProcess:(int)loadingFlag
{
    //NSLog(@"SHAREDSATA failedLoadingProcess3");
    if (loadingFlag == PROCESS_TARGET_ACTIVATE_TOKEN)
    {
        self.processResult = PROCESS_RESULT_ACTIVATE_TOKEN_FAILED;
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_ACTIVATE_TOKEN_DONE object:nil];
        
    }
    if (loadingFlag == PROCESS_TARGET_TRANSACTION_SEND)
    {
        self.processResult = PROCESS_RESULT_TRANSACTION_SEND_FAILED;
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_TRANSACTION_SEND_DONE object:nil];
    }
    if(loadingFlag == PROCESS_TARGET_ONLINE_LOGIN){
        self.processResult = PROCESS_RESULT_ONLINE_LOGIN_FAILED;
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_ONLINE_LOGIN_DONE object:nil];
    }
}

-(NSString*) testSecKey:(NSString *)xmlString
{
    // THIS IS A STRING REPRESENTING THE GLOBAL IDENTIFIER FOR MY PUBLIC KEY CERTIFICATE
    // ON THE IOS "KEYCHAIN".
    const UInt8 keychainIdStr[] = "com.ezmcom.eztoken" ; // YOU MUST USE A CHAR ARRAY[], YOU
    // MAY NOT USE char* OR UInt8* FOR THE POINTER TYPE. Encryption will fail if you do.
    
    // CREATE MY KEYCHAIN IDENTIFIER.  It has to be a CFDataRef.
    CFDataRef CFKEYCHAINID = CFDataCreate( 0, keychainIdStr, sizeof(keychainIdStr) ) ;
    
    // If you want, we can DELETE the item corresponding to the CFKEYCHAINID
    // that we created on the last run.
    //////
    //SecCertificateDeleteFromKeyChain( CFKEYCHAINID ) ; // DELETE OLD KEY
    
    SecKeyRef PUBLICKEY = SecKeyFromKeyChain( CFKEYCHAINID ) ;
    if( PUBLICKEY )  NSLog(@ "<< KEY RETRIEVAL FROM KEYCHAIN OK!! >>" ) ;
    else
    {
        NSLog( @"FAILED TO LOAD SECKEY FROM KEYCHAIN!!!!!" ) ;
        NSLog( @"Loading from certificate.cer.." ) ;
        
        // LOAD THE PUBLIC KEY FROM certificate.cer.
        NSString* certPath = [[NSBundle mainBundle] pathForResource:@"eztoken" ofType:@"cer"];
        NSLog(@"certPath: %@", certPath);
        PUBLICKEY = SecKeyFromPathAndSaveInKeyChain( certPath, CFKEYCHAINID ) ;
        if( !PUBLICKEY )
        {
            NSLog(@ "DOUBLE FAIL!!!!!  MAKE SURE YOU HAVE LOADED certificate.cer INTO THE XCODE PROJECT "
                 "AND THAT IT IS SET UNDER 'COPY BUNDLE RESOURCES'!!!" ) ;
            //return ;
        }
        else
        {
            NSLog(@"PUBLICKEY LOADED OK");
        }
    }
    const char *charString = [xmlString UTF8String];

    int strLen = (int)([xmlString length]+1);
    uint8_t *binaryData = (uint8_t *)malloc( strLen ) ;
    for( int i = 0 ; i < strLen-1; i++ )
        binaryData[i] = charString[i];// loop the alphabet
    binaryData[ strLen-1 ] = 0 ; // NULL TERMINATED ;)
    
    NSLog( @"\nORIGINAL DATA:\n%s\n", (char*)binaryData ) ;
    
    int blockSize = (int)(SecKeyGetBlockSize( PUBLICKEY )-11 + strLen);
    NSLog( @"\nblockSize %d\n", blockSize ) ;
    //NSLog( @"\ strLen %d\n", strLen ) ;
    uint8_t *encrypted = (uint8_t *)malloc(blockSize) ;
    size_t encryptedLen = blockSize;
    NSLog( @"\nencryptedLen before %zu\n", encryptedLen ) ;

    SecCheck( SecKeyEncrypt( PUBLICKEY, kSecPaddingPKCS1, binaryData, strLen-1, encrypted, &encryptedLen ),"SecKeyEncrypt" ) ;
    
    NSLog( @"\nencryptedLen before %zu\n", encryptedLen ) ;

    free( binaryData ) ;
    
    int base64DataLen;
    char* base64Data = base64( encrypted, (int)encryptedLen, &base64DataLen ) ;
    NSString *str = [NSString stringWithUTF8String:base64Data];
    //NSLog(@"################ before return string %@", str);
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"################ after return string %@", str);
    NSLog(@"returned length of b64 op [%d], length of returned string [%lu]",base64DataLen,(unsigned long)[str length]);
    
    return str;
}

////////3 December 2014 //Harsha// for logging purpose////////////
+(void) writeToLogFile:(NSString*)content{
    
        NSDate *currDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEE dd/MMM/yyyy-HH:mm:ss"];
        NSString *newDate = [dateFormatter stringFromDate:currDate];
        
        content = [NSString stringWithFormat:@"%@: %@\n",newDate,content];
        
        [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
        NSString *fileDate = [dateFormatter stringFromDate:currDate];
        
        //get the documents directory:
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *fileName = [NSString stringWithFormat:@"%@/EzTokenLog_%@.txt", documentsDirectory,fileDate];
        
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:fileName];
        if (fileHandle){
            [fileHandle seekToEndOfFile];
            [fileHandle writeData:[content dataUsingEncoding:NSUTF8StringEncoding]];
            [fileHandle closeFile];
        }
        else{
            [content writeToFile:fileName
                      atomically:NO
                        encoding:NSStringEncodingConversionAllowLossy
                           error:nil];
        }
    
    
}


#define ALLOW_CONNECTION_DESPITE_BAD_CERTIFICATE	YES

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        if (ALLOW_CONNECTION_DESPITE_BAD_CERTIFICATE)
        {
            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]
                 forAuthenticationChallenge:challenge];
        }
    }
    
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}


- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)space {
    if([[space authenticationMethod] isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        //if(shouldAllowSelfSignedCert) {
        return YES; // Self-signed cert will be accepted
        // } else {
        //     return NO;  // Self-signed cert will be rejected
        // }
        // Note: it doesn't seem to matter what you return for a proper SSL cert
        //       only self-signed certs
    }
    // If no other authentication is required, return NO for everything else
    // Otherwise maybe YES for NSURLAuthenticationMethodDefault and etc.
    return NO;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    /////////////////////////////////// BAD HACK ///////////////////////////////////
    
    NSString *j = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *first = @"\"msg\":{";
    NSString *second = @"}";
    NSRange one = [j rangeOfString:first];
    
    if(one.location == NSNotFound){
        UIAlertView *v = [[UIAlertView alloc] initWithTitle:@"Error processing transaction" message:@"There was an error while trying to process the transaction. Please contact your service provider for assistance." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [v show];
        RegisterViewController *reg = [[RegisterViewController alloc] init];
        [(UINavigationController*)((AppDelegate *)[[UIApplication sharedApplication] delegate]).window.rootViewController pushViewController:reg animated:NO];
        return;
    }
    
    NSRange two = [[j substringFromIndex:one.location + one.length] rangeOfString:second];
    NSRange final = NSMakeRange(one.location + first.length, two.location);
    NSString *j1 = [j substringWithRange:final];
    NSLog(@"MSG --- %@", j1);
    
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appdelegate.dictSequence = [[NSMutableDictionary alloc] init];
    
    NSMutableString *strStore = [[NSMutableString alloc] init];
    
    NSString *tg = @"\":\"";
    NSString *tg2 = @"\"";
    NSInteger iCount = 0;
    while(1){
        NSRange o = [j1 rangeOfString:tg];
        if(o.location == NSNotFound){
            break;
        }
        NSRange o1 = [[j1 substringFromIndex:o.location+o.length] rangeOfString:tg2];
        NSRange f = NSMakeRange(o.location + tg.length, o1.location);
        NSString *s = [j1 substringWithRange:f];
        [strStore appendFormat:@"%@",s];
        [appdelegate.dictSequence setObject:s forKey:[NSString stringWithFormat:@"%ld", (long)iCount]];
        ++iCount;
        NSRange o2 = [j1 rangeOfString:s];
        NSString *s2 = [j1 substringFromIndex:o2.location+o2.length];
        j1 = s2;
    }
    
//    if(strStore.length > 0){
//        [strStore deleteCharactersInRange:NSMakeRange([strStore length]-1, 1)];
//    }
    
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [userdefaults setObject:strStore forKey:@"SIGOTPSTRING"];
    [userdefaults synchronize];
    
    /////////////////////////////////// BAD HACK ///////////////////////////////////
    
    NSLog(@"Recieved Data %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    NSError *e = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
    
    NSLog(@"jsonArray valueForkey=f txtBio =%@",[[[jsonArray valueForKey:@"f"] valueForKey:@"txUBio"] class]);
    
    // This is the bioMetric authentication value sent at the time of transaciton
    
    if([[[[jsonArray valueForKey:@"f"] valueForKey:@"txUBio"] stringValue] isEqualToString:@"0"]){
        [userdefaults setBool:NO forKey:KEY_REQUIRE_BOTH];
        [userdefaults synchronize];
    }
    else if ([[[[jsonArray valueForKey:@"f"] valueForKey:@"txUBio"] stringValue] isEqualToString:@"1"]){
        [userdefaults setBool:NO forKey:KEY_REQUIRE_BOTH];
        [userdefaults synchronize];
    }
    else if([[[[jsonArray valueForKey:@"f"] valueForKey:@"txUBio"] stringValue] isEqualToString:@"2"]){
        [userdefaults setBool:YES forKey:KEY_REQUIRE_BOTH];
        [userdefaults synchronize];
    }else{
        [userdefaults setBool:NO forKey:KEY_REQUIRE_BOTH];
        [userdefaults synchronize];
    }
    
    if (!jsonArray) {
        NSLog(@"Error parsing JSON: %@", e);
        return;
    }
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSMutableString *combinedString = [[NSMutableString alloc]init];
    
    if ([jsonArray valueForKey:@"msg"] != nil) {
        //NSLog(@"Value: %@", [jsonArray valueForKey:@"msg"]);
        NSArray *array = [jsonArray valueForKey:@"msg"];
        //self.arrMsg = jsonArray;
        
        //NSLog(@"array!!: %@",self.arrMsg);
        [dict setValue:[array valueForKey:@"Date/Time"] forKey:@"Time"];
        [dict setValue:[array valueForKey:@"Location"] forKey:@"Location"];
        [dict setValue:[array valueForKey:@"Host/IP"] forKey:@"Client IP"];
        
        NSMutableDictionary *msgDict = [[NSMutableDictionary alloc] init];
        for (id key in array){
            [msgDict setValue:[array valueForKey:key] forKey:key];
        }
        
        NSArray *msgkeys = [msgDict allKeys];
        NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
        NSArray *sorters = [[NSArray alloc] initWithObjects:sorter, nil];
        NSArray *sortedArray = [msgkeys sortedArrayUsingDescriptors:sorters];
        NSMutableArray *sortArray = [[NSMutableArray alloc] init];
        [sortArray addObjectsFromArray:sortedArray];
        NSString *str = nil;
        
        for(int i=0; i<[sortArray count]; i++)
        {
            NSString *test = [msgDict valueForKey:[sortArray objectAtIndex:i]];
            str = [NSString stringWithFormat:@"%@",test];
            [combinedString appendFormat:@"%@", str];
        }
    }
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AuthorizeViewController *authorizeVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"authorizeView"];
    authorizeVC.dict = dict;
    authorizeVC.combStr = combinedString;
    authorizeVC.arrmsg = jsonArray;
    authorizeVC.txref = [jsonArray valueForKey:@"ref"];
    
    [(UINavigationController*)[[UIApplication sharedApplication] delegate].window.rootViewController pushViewController:authorizeVC animated:NO];
}


-(BOOL)isTouchIdDevice1{
    
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error])
        return YES;
    else
        return NO;

}

-(BOOL)isSecuredAlignedORNot{

}
@end
